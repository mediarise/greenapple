<?php
/**
 * @var $this \yii\web\View
 * @var $searchModel \common\models\GradeSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use frontend\widgets\IBox;
use frontend\widgets\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use common\models\Grade;

$this->title = 'Grade list';
$this->params['breadcrumbs'][] = ['label' => 'Grade list'];
?>

<div class="grade-list-page">


    <?php IBox::begin([
        'title' => 'List of grades',
    ]); ?>

    <p class="mb-2">
        <?= Html::a('<i class="fa fa-plus"></i> Add new', ['/schooladmin/grade/create'], ['class' => 'btn btn-success btn-xl']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'enablePjax' => true,
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'filterRowOptions' => [
            'class' => 'filters row xm'
        ],
        'columns' => [
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'year',
                'content' => function ($model) {
                    return $model->getFormattedYear();
                }
            ],
            [
                'attribute' => 'schoolName',
                'label' => 'Schools',
                'content' => function ($model) {
                    return $model->school->name;
                },
            ],
            [
                'attribute' => 'boardName',
                'label' => 'Board',
                'content' => function ($model) {
                    return $model->school->board->name;
                },
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {delete}',
                'header' => 'Actions',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<i class="fa fa-pencil"></i>',
                            $url,
                            [
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'Edit grade',
                            ]
                        );
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<i class="fa  fa-trash"></i>',
                            $url,
                            [
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'Delete grade',
                                'data-confirm' => 'Are you sure you want to delete this item?',
                            ]
                        );
                    },
                ],
            ]
        ],
        'filters' => [
            [
                'type' => GridView::FILTER_TYPE_TEXT_INPUT,
                'name' => 'name',
                'template' => '<div class="col-lg-6 mb-15">{input}</div>',
                'options' => [
                    'id' => 'search-filter',
                    'placeholder' => 'Search'
                ],
            ],
            [
                'type' => GridView::FILTER_TYPE_SELECT_2,
                'template' => '<div class="col-sm-6">{widget}</div>',
                'widgetConfig' => [
                    'name' => 'year',
                    'data' => Grade::getAllYears(true),
                    'value' => Grade::getCurrentAcademicYear(),
                    'options' => [
                        'placeholder' => 'Select year ...',
                    ],
                ]
            ],
        ],
    ]) ?>

    <?php IBox::end(); ?>
</div>