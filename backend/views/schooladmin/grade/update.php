<?php
/**
 * @var $this \yii\web\View
 * @var $model
 * @var $schoolId
 */

use frontend\widgets\IBox;

$this->title = 'Update grade';
$this->params['breadcrumbs'][] = ['label' => 'Grade list', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Update grade'];

?>

<div class="create-grade-page">
    <?php IBox::begin([
        'title' => 'Update grade form'
    ]); ?>
    <?= $this->render('_form', [
        'schoolId' => $schoolId,
        'model' => $model,
    ]) ?>

    <?php IBox::end(); ?>
</div>

