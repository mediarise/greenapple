<?php
/**
 * @var $this \yii\web\View
 * @var $model
 * @var $schoolId
 */

use frontend\widgets\IBox;

$this->title = 'Create grade';
$this->params['breadcrumbs'][] = ['label' => 'Grade list', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Create grade'];

?>

<div class="create-grade-page">
    <?php IBox::begin([
        'title' => 'Create grade form'
    ]); ?>
    <?= $this->render('_form', [
        'schoolId' => $schoolId,
        'model' => $model,
    ]) ?>

    <?php IBox::end(); ?>
</div>

