<?php
/**
 * @var $this \yii\web\View
 * @var $model
 * @var $schoolId
 */

use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>
<div class="col-xs-12">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
        ]
    ]) ?>

    <div class="hidden">
        <?= $form->field($model, 'school_id')->hiddenInput(['value' => $schoolId])->label(false) ?>
    </div>

    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Name']) ?>

    <?= $form->field($model, 'year')->widget(MaskedInput::class, [
        'mask' => '2099'
    ]) ?>

    <div class="form-group">

        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-xl mr-125']); ?>

        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary btn-xl']) ?>

    </div>


    <?php ActiveForm::end(); ?>

</div>