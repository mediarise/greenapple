<?php
/**
 * @var $this \yii\web\View
 * @var array $studentList
 * @var array $gradeList
 * @var array $teacherList
 */

use frontend\widgets\IBox;
use frontend\widgets\DualListBox;
use yii\helpers\Html;

$this->title = 'Visibility group';
$this->params['breadcrumbs'][] = ['label' => 'Visibility group'];
?>

<div class="teacher-visibility-group-page">

    <?php IBox::begin([
        'title' => 'Visibility group'
    ]) ?>
    <?= Html::beginForm('', 'post', [
        'class' => 'form-horizontal'
    ]); ?>

    <div class="form-group">
        <label for="" class="col-sm-2 control-label">Group name *</label>
        <div class="col-sm-10">
            <?= Html::textInput('name', null, ['class' => 'form-control']) ?>
        </div>
    </div>

    <div class="hr-line-dashed"></div>

    <div class="row">
        <div class="col-lg-2">
            <label for="" class="control-label">Teachers</label>
        </div>
        <div class="form-group col-lg-10">
            <?= DualListBox::widget([
                'name' => 'teachers',
                'items' => $teacherList,
                'options' => [
                    'multiple' => true,
                    'size' => 10,
                ],
                'clientOptions' => [
                    'selectedListLabel' => 'Group members',
                    'nonSelectedListLabel' => 'Available',
                ],
            ]) ?>
        </div>
    </div>

    <div class="hr-line-dashed"></div>

    <div class="row">
        <div class="col-lg-2">
            <label for="" class=control-label">Grades</label>
        </div>
        <div class="form-group col-lg-10">
            <?= DualListbox::widget([
                'name' => 'grades',
                'items' => $gradeList,
                'options' => [
                    'multiple' => true,
                    'size' => 10,
                ],
                'clientOptions' => [
                    'selectedListLabel' => 'Group members',
                    'nonSelectedListLabel' => 'Available',
                ],
            ]) ?>
        </div>
    </div>

    <div class="hr-line-dashed"></div>

    <div class="row">
        <div class="col-lg-2">
            <label for="" class="control-label">Students</label>
        </div>
        <div class="form-group col-lg-10">
            <?= DualListbox::widget([
                'name' => 'students',
                'items' => $studentList,
                'options' => [
                    'multiple' => true,
                    'size' => 10,
                ],
                'clientOptions' => [
                    'selectedListLabel' => 'Group members',
                    'nonSelectedListLabel' => 'Available',
                ],
            ]) ?>
        </div>
    </div>

    <div class="hr-line-dashed"></div>

    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-10">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']); ?>

            <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?= Html::endForm(); ?>

    <?php IBox::end() ?>

</div>

