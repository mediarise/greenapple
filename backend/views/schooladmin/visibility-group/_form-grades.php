<?php
/**
 * @var $this \yii\web\View
 * @var $model
 * @var $form
 * @var $gradeList
 * @var $memberedGradeList
 * @var $currentYearGrades
 * @var $memberedCurrentYearGradeList
 */

use frontend\widgets\DualListBox;
use yii\helpers\Json;
use yii\web\View;
?>


<?php
$this->registerJs(/* @lang JavaScript */
    "
    var currentYearMemberedGrades = " . Json::encode($memberedCurrentYearGradeList) . ";
    var allMemberedGrades = " . Json::encode($memberedGradeList) . ";
    
    refreshGradeList(allMemberedGrades);
    
    $('body').on('change', '#only-current-year-grades', function() {
        if ($(this).prop('checked')) {
            refreshGradeList(currentYearMemberedGrades);
        } else {
            refreshGradeList(allMemberedGrades);
        }
    });
    
    function refreshGradeList(gradeList) {
        $('#grade-duallistbox').find('option').remove();
        var options = document.createDocumentFragment();
        
        $.each(gradeList, function(key, grade) {
            var isMember = grade.is_member;
            options.appendChild(new Option(grade.name, grade.id, isMember, isMember));
        });
        $('#grade-duallistbox').append(options).bootstrapDualListbox('refresh', true);
    }
", View::POS_READY);
?>
<div class="panel-body">
    <div class="list-box-area">

        <?= $form->field($model, 'grades', [
            'enableClientValidation' => false,
            'validateOnChange' => false
        ])->widget(DualListBox::class, [
            'items' => $gradeList,
            'options' => [
                'id' => 'grade-duallistbox',
                'multiple' => true,
                'size' => 10,
            ],
            'clientOptions' => [
                'selectedListLabel' => 'Group members',
                'nonSelectedListLabel' => 'Grades',
            ],
        ])->label(false) ?>


    </div>
</div>
