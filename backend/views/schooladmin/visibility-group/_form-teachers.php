<?php
/**
 * @var $this \yii\web\View
 * @var $memberedTeacherList
 * @var $form
 * @var $model
 */

use common\widgets\Select2;
use frontend\widgets\DualListBox;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

?>

<?php
$this->registerJs(/* @lang JavaScript */
    "
    var memberedTeachers = " . Json::encode($memberedTeacherList) . ";
    
    refreshTeacherList(memberedTeachers);
    
    function refreshTeacherList(teacherList) {
        $('#teacher-duallistbox').find('option').remove();
        var options = document.createDocumentFragment();
        
        $.each(teacherList, function(key, grade) {
            var isMember = grade.is_member;
            options.appendChild(new Option(grade.name, grade.id, isMember, isMember));
        });
        $('#teacher-duallistbox').append(options).bootstrapDualListbox('refresh', true);
    }
", View::POS_READY);
?>
<div class="panel-body">
    <div class="list-box-area">

        <?= $form->field($model, 'teachers', [
            'enableClientValidation' => false,
            'validateOnChange' => false
        ])->widget(DualListBox::class, [
            'items' => [],
            'options' => [
                'id' => 'teacher-duallistbox',
                'multiple' => true,
                'size' => 10,
            ],
            'clientOptions' => [
                'selectedListLabel' => 'Group members',
                'nonSelectedListLabel' => 'Grades',
            ],
        ])->label(false) ?>


    </div>
</div>

