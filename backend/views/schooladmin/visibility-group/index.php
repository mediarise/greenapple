<?php

use yii\helpers\Html;
use yii\grid\ActionColumn;
use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VisibilityGroup */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $hasGrades boolean */
/* @var $hasStudents boolean */

$this->title = 'Visibility Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visibility-group-index">

    <?php IBox::begin(['title' => 'Visibility group']) ?>

    <?= Html::button('<i class="fa fa-eye"></i> Create Visibility Group', [
        'id' => 'create-btn',
        'class' => 'btn btn-xl btn-success ladda-button mb-2',
        'data-style' => 'zoom-out'
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'board_id',
                'label' => 'Board',
                'value' => function($visibilityGroup) {
                    return $visibilityGroup->board->name;
                },
            ],
            [
                'attribute' => 'school_id',
                'label' => 'School',
                'value' => function($visibilityGroup) {
                    return $visibilityGroup->school->name;
                },
            ],
            [
                'attribute' => 'grade_id',
                'label' => 'Grade',
                'value' => function($visibilityGroup) {
                    if ($visibilityGroup->grade) {
                        return $visibilityGroup->grade->name;
                    }
                    return '-';
                },
            ],
            [
                'attribute' => 'members',
                'content' => function ($visibilityGroup) {
                    return $visibilityGroup->memberCount;
                }
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update}{delete}',
                'header' => 'Actions',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm mr-05',
                            'title' => 'Edit Visibility Group',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Delete Visibility Group',
                            'data-confirm' => 'Are you sure you want to delete this visibility group?',
                        ]);
                    },
                ],
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center', 'style' => 'min-width: 120px'],
            ],
        ],
    ]); ?>

    <?php IBox::end() ?>

</div>

<?php
$createUrl = Url::toRoute(['schooladmin/visibility-group/check-before-create']);
$this->registerJs("
        var createBtn = $('#create-btn').ladda();
        createBtn.on('click', function() {
            createBtn.ladda('start').attr('disabled', true);
            greenApple.ajax.perform('$createUrl', {}, {
                complete: function(data) {
                    createBtn.ladda('stop');
                }
            });
        });
    ", yii\web\View::POS_READY);
?>
