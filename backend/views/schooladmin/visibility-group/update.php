<?php

use yii\helpers\Html;
use frontend\widgets\IBox;

/* @var $this yii\web\View */
/* @var $model common\models\teacher\VisibilityGroupForm */
/* @var array $gradeList */
/* @var array $studentList */
/* @var array $haveStudentMember */
/* @var array $currentYearGrades */
/* @var array $memberedGradeList */
/* @var array $haveGradeMember */
/* @var array $memberedCurrentYearGradeList */
/* @var array $memberedTeacherList */
/* @var array $haveTeacherMember */

$this->title = 'Update Visibility Group';
$this->params['breadcrumbs'][] = ['label' => 'Visibility Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="visibility-group-update col-md-12 mb-4">

    <?php IBox::begin(['title' => 'Visibility group']) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'gradeList' => $gradeList,
        'studentList' => $studentList,
        'haveStudentMember' => $haveStudentMember,
        'currentYearGrades' => $currentYearGrades,
        'memberedGradeList' => $memberedGradeList,
        'haveGradeMember' => $haveGradeMember,
        'memberedCurrentYearGradeList' => $memberedCurrentYearGradeList,
        'memberedTeacherList' => $memberedTeacherList,
        'haveTeacherMember' => $haveTeacherMember,
    ]) ?>

    <?php IBox::end() ?>

</div>
