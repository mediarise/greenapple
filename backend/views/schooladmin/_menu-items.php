<?php
return [
    [
        'label' => 'Home',
        'iconClass' => 'fa fa-home',
        'url' => ['/schooladmin/index']
    ],
    [
        'label' => 'Grades',
        'iconClass' => 'fa fa-graduation-cap',
        'url' => ['/schooladmin/grade/index']
    ],
    [
        'label' => 'User menu',
        'iconClass' => 'fa fa-users',
        'items' => [
            'signup-request' => [
                'label' => 'Signup Request',
                'url' => ['/schooladmin/signup-request'],
            ],
            [
                'label' => 'Guardians',
                'url' => ['/schooladmin/guardian/index']
            ],
            [
                'label' => 'Students',
                'url' => ['/schooladmin/student/index']
            ],
        ],
    ],
    [
        'label' => 'Events',
        'iconClass' => 'fa fa-calendar',
        'url' => ['/schooladmin/event/index']
    ],
    [
        'label' => 'Products',
        'iconClass' => 'fa fa-shopping-cart',
        'url' => ['/schooladmin/product/index']
    ],
    'forms' => [
        'label' => 'Forms',
        'iconClass' => 'fa fa-file-text-o',
        'url' => ['/schooladmin/form/index'],
    ],
    [
        'label' => 'Visibility groups',
        'iconClass' => 'fa fa-eye',
        'url' => ['/schooladmin/visibility-group/index']
    ],
    'reports' => [
        'label' => 'Reports',
        'iconClass' => 'fa fa-line-chart',
        'items' => [
            [
                'label' => 'Sales report',
                'url' => ['/schooladmin/report/sales'],
            ],
            [
                'label' => 'Daily sales report',
                'url' => ['/schooladmin/report/daily-sales'],
            ]
        ]
    ],
    [
        'label' => 'Holidays',
        'iconClass' => 'fa fa-calendar',
        'url' => ['/schooladmin/holiday/index']
    ],
    [
        'label' => 'Suppliers',
        'iconClass' => 'fa fa-user',
        'url' => ['/schooladmin/supplier/index']
    ],
];