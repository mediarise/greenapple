<?php

/**
 * @var $this yii\web\View
 * @var $upcomingDataProvider \yii\data\ArrayDataProvider
 */

use frontend\widgets\IBox;
use yii\helpers\Url;

if (empty($productModel)) {
    $this->title = 'Events';
} else {
    $this->title = 'Events with product - ' . \yii\helpers\Html::encode($productModel->title);
}

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
?>
<div class="site-index">

    <?php IBox::begin(['title' => $this->title]) ?>
    <?= $this->render('_upcoming-grid', [
        'upcomingDataProvider' => $upcomingDataProvider,
    ]) ?>


    <?php IBox::end() ?>

</div>
