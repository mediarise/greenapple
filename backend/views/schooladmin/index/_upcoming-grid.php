<?php

use common\components\Formatter;
use yii\grid\GridView;
use common\models\schooladmin\DocEvent;
use yii\widgets\Pjax;
use common\widgets\grid\LinkColumn;

/* @var $this \yii\web\View */
/* @var $upcomingDataProvider \yii\data\ArrayDataProvider */


?>

<?php Pjax::begin([
    'id' => 'upcoming-grid',
    'enablePushState' => false,
]) ?>

<?= /** @noinspection PhpUnhandledExceptionInspection */
GridView::widget([
    'dataProvider' => $upcomingDataProvider,
    'summary' => false,
    'tableOptions' => [
        'class' => 'table table-striped'
    ],
    'columns' => [
        [
            'attribute' => 'start_date',
            'label' => 'Date',
            'value' => function (DocEvent $event) {
                return Formatter::period($event->start_date, $event->end_date);
            }
        ],
        [
            'class' => LinkColumn::class,
            'attribute' => 'title',
            'url' => [
                'schooladmin/event-statistic/view',
                'id' => '$id',
            ],
            'urlOptions' => [
                'data-pjax' => 0,
            ]
        ],
        [
            'attribute' => 'ordersRatio',
            'label' => 'Orders',
            'value' => function (DocEvent $event) {
                if ($event->type == DocEvent::EVENT_TYPE_NUTRITION) {
                    return '-';
                }
                return Formatter::ratio($event->getProductPurchased(), $event->getTotalOrders());
            }
        ],
        [
            'attribute' => 'formsRatio',
            'label' => 'Forms',
            'value' => function (DocEvent $event) {
                return Formatter::ratio($event->forms_signed, $event->getTotalForms());
            }
        ],
        [
            'label' => 'Teacher',
            'value' => function (DocEvent $event) {
                $name = "{$event->first_name} {$event->last_name}";
                if ($name) {
                    return $name;
                }
                return '-';
            }
        ]
    ],
]) ?>

<?php Pjax::end() ?>
