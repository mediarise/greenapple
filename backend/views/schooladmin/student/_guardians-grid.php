<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

?>

<?php Pjax::begin([
    'id' => 'linked-grid',
    'timeout' => 10000,
    'enablePushState' => false,
]); ?>

<?= GridView::widget([
    'dataProvider' => $guardiansDataProvider,
    'summary' => false,
    'tableOptions' => [
        'class' => 'table'
    ],
    'columns' => [
        [
            'attribute' => 'first_name',
            'label' => 'Full name',
            'format' => 'raw',
            'value' => function ($guardian) {
                return '<b>' . Html::a('<i class="fa fa-user mr"></i>' . $guardian->getOfficialName(), [
                        '/schooladmin/guardian/update',
                        'id' => $guardian->id,
                    ], ['data-pjax' => 0]) . '</b>';
            }
        ],
        [
            'label' => 'Actions',
            'format' => 'raw',
            'value' => function ($guardian) use ($student) {
                return Html::a('Remove relationship', ['schooladmin/guardian/remove-relationship', 'guardianId' => $guardian->id, 'studentId' => $student->id], [
                    'class' => 'remove-relationship',
                    'data-pjax' => 0,
                ]);
            }
        ],

    ],
]) ?>

<?php Pjax::end() ?>
