<?php

use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\grid\DataColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Student */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index">

    <?php IBox::begin(['title' => $this->title]); ?>

    <p class="mb-2">
        <?= Html::a('<i class="fa fa-plus"></i> Create Student', ['create'], ['class' => 'btn btn-success btn-xl']) ?>
    </p>

    <div class="row">
        <div class="col-md-12">

            <?= GridView::widget([
                'enablePjax' => true,
                'summary' => false,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-hover'
                ],
                'filterRowOptions' => [
                    'class' => 'filters mb-15'
                ],
                'columns' => [
                    'oen',
                    [
                        'class' => DataColumn::class,
                        'attribute' => 'first_name',
                        'format' => 'raw',
                        'label' => 'Name',
                        'value' => function ($model) {
                            return Html::a(Html::encode($model->getFullName()),
                                ['update', 'id' => $model->id]);
                        }
                    ],
                    [
                        'attribute' => 'grade.school.board.name',
                        'label' => 'Board',
                    ],
                    [
                        'attribute' => 'grade.school.name',
                        'label' => 'School',
                    ],
                    [
                        'attribute' => 'grade.name',
                        'label' => 'Grade',
                    ],
                    [
                        'class' => DataColumn::class,
                        'label' => 'Actions',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a('<i class="fa  fa-trash"></i>', ['delete', 'id' => $model->id], [
                                'data-pjax' => 0,
                                'data-confirm' => 'Are you sure you want to delete this item?',
                                'data-method' => 'post',
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'Delete student',
                            ]);
                        }
                    ],
                ],
                'filters' => [
                    [
                        'type' => GridView::FILTER_TYPE_TEXT_INPUT,
                        'name' => 'search',
                        'options' => ['placeholder' => 'Search'],
                        'template' => '<div class="col-lg-6 px-0 mb-15">{input}</div>',
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?php IBox::end(); ?>
</div>
