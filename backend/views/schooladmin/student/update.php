<?php

use common\components\bl\FamilyRelationship;
use common\widgets\Select2;
use frontend\widgets\AutocompletedInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $student common\models\Student
 * @var \common\models\Board[] $boards
 * @var \common\models\SchoolForm $schoolForm
 * @var \common\models\GuardianStudent $guardianStudent
 */

$this->title = 'Update Student: ' . $student->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $student->id, 'url' => ['view', 'id' => $student->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->registerJsFile('js/signup-request.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="ibox-content">

    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-signup-request">
                    <b><?= $this->title ?></b>
                </a>
            </li>
        </ul><!-- nav-tabs -->

        <div class="tab-content">

            <div class="tab-pane active" id="tab-signup-request">
                <div class="panel-body">
                    <div class="row">
                        <?php $form = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => [
                                'id' => 'update-student-form'
                            ],
                        ]) ?>

                        <?= $this->render('_form', [
                            'boards' => $boards,
                            'schoolForm' => $schoolForm,
                            'form' => $form,
                            'model' => $student,
                        ]) ?>

                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-xl btn-success">Save</button>
                        </div>

                        <?php ActiveForm::end() ?>

                        <div class="col-xs-12">
                            <hr>
                        </div>
                        <div class="clearfix"></div>

                        <?php $form = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'action' => Url::to(['schooladmin/guardian/assign-student']),
                            'errorCssClass' => 'has-warning',
                            'options' => [
                                'id' => 'guardian-student-link-form'
                            ],
                        ]) ?>
                        <?= Html::activeHiddenInput($guardianStudent, 'guardian_id', ['id' => 'linked-id', 'class' => 'form-control']) ?>
                        <?= Html::activeHiddenInput($guardianStudent, 'student_id') ?>

                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Guardian</label>
                                <?= /** @noinspection PhpUnhandledExceptionInspection */
                                AutocompletedInput::widget([
                                    'id' => 'guardianstudent-guardian_id',
                                    'name' => 'twitter_oss',
                                    'dataUrl' => Url::toRoute(['get-guardians'], true),
                                    'options' => [
                                        'placeholder' => 'Search guardians ...'
                                    ],
                                    'itemTemplate' => "<div><i class=\"fa fa-user\"></i> <b>{{value}}</b></div>",
                                    'pluginEvents' => [
                                        'typeahead:select' => 'function(typeahead,item){console.log(item);$("#linked-id").val(item.id);}',
                                    ]
                                ]); ?>
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <?= $form->field($guardianStudent, 'relationship')->widget(Select2::class, [
                                'data' => FamilyRelationship::getNames(),
                                'options' => [
                                    'placeholder' => 'Select a relationship ...',
                                ],
                            ]) ?>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-xl btn-success mr-125">
                                    <i class="fa fa-tag"></i> Assign
                                </button>
                                <a class="btn btn-xl btn-primary"
                                   href="<?= Url::to(['schooladmin/guardian/create']) ?>">
                                    <i class="fa fa-plus"></i> Add new guardian
                                </a>
                            </div>
                        </div>

                        <?php ActiveForm::end() ?>

                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <?= $this->render('_guardians-grid', compact('guardiansDataProvider', 'student')) ?>
                            </div>
                        </div>
                    </div>
                </div><!-- panel-body -->
            </div><!-- tab-pane -->

        </div><!-- tab-content -->
    </div><!-- tabs-container -->

</div>
