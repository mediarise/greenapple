<?php

use yii\widgets\ActiveForm;


/**
 * @var $this yii\web\View
 * @var common\models\Student $student
 * @var \common\models\Board[] $boards
 * @var  \common\models\SchoolForm $schoolForm
 */

$this->title = 'Create Student';
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox-content">

    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-signup-request">
                    <b>Signup request</b>
                </a>
            </li>
        </ul><!-- nav-tabs -->

        <div class="tab-content">

            <div class="tab-pane active" id="tab-signup-request">
                <div class="panel-body">
                    <div class="row">
                        <?php $form = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => [
                                'id' => 'create-student-form'
                            ],
                        ]) ?>
                        <?= $this->render('_form', [
                            'model' => $student,
                            'boards' => $boards,
                            'schoolForm' => $schoolForm,
                            'form' => $form,
                        ]) ?>

                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-xl btn-success">Save</button>
                        </div>

                        <?php ActiveForm::end() ?>

                    </div>
                </div><!-- panel-body -->
            </div><!-- tab-pane -->

        </div><!-- tab-content -->
    </div><!-- tabs-container -->

</div>
