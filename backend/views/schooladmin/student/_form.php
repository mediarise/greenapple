<?php

/**
 * @var $this \yii\web\View
 * @var $model common\models\GuardianAddStudentTemplateForm
 * @var $boards array
 * @var $selectFieldOptions array
 * @var $changeCallback string
 * @var ActiveForm $form
 */
/* @var $guardiansDataProvider */

/* @var $schoolForm \common\models\SchoolForm */

/* @var $signupRequestStudent \common\models\SignupRequestStudent */

use common\components\bl\SchoolHierarchy;
use yii\bootstrap\ActiveForm;
use dosamigos\datepicker\DatePicker;
use common\widgets\Select2;

$selectBoardId = 'student-' . $model->id . '-board-id';
$selectSchoolId = 'student-' . $model->id . '-school-id';
$selectGradeId = 'student-' . $model->id . '-grade-id';

$selectFieldOptions = [
    'validateOnChange' => false,
    'validateOnBlur' => false,
];

$frontendUrlManager = Yii::$app->urlManagerFrontend;

$changeCallback = 'function(e) { document.SignupForm.loadOptions("%s", e.target.value, "%s") }';

$this->registerJsFile('js/add-child.js', ['depends' => 'yii\web\JqueryAsset']);

?>

<div class="col-sm-8 col-md-6 col-lg-4">
    <?= $form->field($model, 'first_name')->textInput(['autocomplete' => 'given-name']); ?>
</div>
<div class="col-sm-8 col-md-6 col-lg-4">
    <?= $form->field($model, 'last_name')->textInput(['autocomplete' => 'family-name']); ?>
</div>
<div class="col-sm-8 col-md-6 col-lg-4">
    <?php
    $defaultViewDate = ['year' => 1985];
    ?>
    <?= $form->field($model, 'birthday')->widget(DatePicker::class, [
        'template' => '
                                        {input}
                                        <span class="input-group-addon" id="basic-addon1">
                                        <span class="fa fa-calendar"></span>
                                        </span>
                                    ',
        'clientOptions' => [
            'autoclose' => true,
            'maxViewMode' => 'years',
            'startView' => 2,
            'defaultViewDate' => $defaultViewDate,
            'format' => 'yyyy-mm-dd'
        ],
        'options' => [
            'placeholder' => 'Select birth date'
        ]
    ]); ?>
</div><!-- col-sm-4 -->

<div class="col-sm-8 col-md-6 col-lg-4">
    <?= $form
        ->field($schoolForm, 'boardId', $selectFieldOptions)
        ->widget(Select2::class, [
            'data' => $boards,
            'options' => [
                'id' => $selectBoardId,
                'placeholder' => 'Select a board ...',
            ],
            'pluginEvents' => [
                'change' => sprintf(
                    $changeCallback,
                    $selectSchoolId,
                    $frontendUrlManager->createUrl(['signup/get-schools', 'boardId' => ''])
                ),
            ],
        ])
        ->label('Board')
    ?>
</div>
<div class="col-sm-8 col-md-6 col-lg-4">
    <?= $form
        ->field($schoolForm, 'schoolId', $selectFieldOptions)
        ->widget(Select2::class, [
            'data' => SchoolHierarchy::getSchools($schoolForm->boardId),
            'options' => [
                'id' => $selectSchoolId,
                'placeholder' => 'Select a school ...',
                'class' => ['dependent-select']
            ],
            'pluginEvents' => [
                'change' => sprintf(
                    $changeCallback,
                    $selectGradeId,
                    $frontendUrlManager->createUrl(['signup/get-grades', 'schoolId' => ''])
                ),
            ],
        ])
        ->label('School')
    ?>
</div>
<div class="col-sm-8 col-md-6 col-lg-4">
    <?= $form
        ->field($model, 'grade_id', $selectFieldOptions)
        ->widget(Select2::class, [
            'data' => SchoolHierarchy::getGrades($schoolForm->schoolId),
            'options' => [
                'id' => $selectGradeId,
                'placeholder' => 'Select a class ...',
                'class' => ['dependent-select']
            ],])
        ->label('Grade')
    ?>
</div>

<div class="col-sm-8 col-md-6 col-lg-4">
    <?= $form->field($model, 'oen')->textInput(); ?>
</div>


