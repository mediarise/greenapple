<?php

use common\components\bl\Greeting;
use common\widgets\Select2;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $guardian \common\models\Guardian */

$emailFieldOptions = $user->isNewRecord ? [] : ['readonly' => ''];

?>

<div class="guardian-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'id' => 'guardian-form'
        ],
    ]) ?>
    <div class="row mt-2">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($guardian, 'first_name')->textInput(); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($guardian, 'last_name')->textInput(); ?>
                </div>
<!--                <div class="clearfix"></div>-->
                <div class="col-md-6">
                    <?= $form->field($guardian, 'greeting')->widget(Select2::class, [
                        'data' => Greeting::getNames(),
                        'options' => [
                            'placeholder' => 'Select a gender ...',
                        ]
                    ]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($user, 'email')->textInput($emailFieldOptions); ?>
                </div>
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-xl btn-success">Save</button>
                </div>
            </div> <!-- row: first form row -->
        </div><!-- form inputs column -->
    </div><!-- row -->

    <?php ActiveForm::end() ?>

</div>
