<?php

use common\components\bl\FamilyRelationship;
use common\models\User;
use common\widgets\Select2;
use frontend\widgets\AutocompletedInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var common\models\Guardian $guardian
 * @var \common\models\GuardianStudent $guardianStudent
 * @var User $user
 */


$this->title = 'Update Guardian: ' . $guardian->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Guardians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $guardian->id;

$this->registerJsFile('js/signup-request.js', ['depends' => 'yii\web\JqueryAsset']);
?>

<div class="ibox-content">
    <div>

        <div class="tabs-container">

            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#tab-signup-request">
                        <b><?= $this->title ?></b>
                    </a>
                </li>
            </ul><!-- nav-tabs -->

            <div class="tab-content">

                <div class="tab-pane active" id="tab-signup-request">
                    <div class="panel-body">
                        <?= $this->render('_form', [
                            'guardian' => $guardian,
                            'user' => $user,
                        ]); ?>

                        <hr>

                        <div class="row">

                            <?php $form = ActiveForm::begin([
                                'enableClientValidation' => false,
                                'action' => Url::to(['schooladmin/guardian/assign-student']),
                                'errorCssClass' => 'has-warning',
                                'options' => [
                                    'id' => 'guardian-student-link-form'
                                ],
                            ]) ?>
                            <?= Html::activeHiddenInput($guardianStudent, 'guardian_id') ?>
                            <?= Html::activeHiddenInput($guardianStudent, 'student_id', ['id' => 'linked-id', 'class' => 'form-control']) ?>

                            <div class="col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Student</label>
                                    <?= /** @noinspection PhpUnhandledExceptionInspection */
                                    AutocompletedInput::widget([
                                        'id' => 'guardianstudent-student_id',
                                        'name' => 'twitter_oss',
                                        'dataUrl' => Url::toRoute(['get-students'], true),
                                        'options' => [
                                            'placeholder' => 'Search students ...'
                                        ],
                                        'itemTemplate' => "<div><b>{{value}}</b><br><i class=\"fa fa-user\"></i>{{oen}}</div>",
                                        'pluginEvents' => [
                                            'typeahead:select' => 'function(typeahead,item){$("#linked-id").val(item.id);}',
                                        ]
                                    ]); ?>
                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?= $form->field($guardianStudent, 'relationship')->widget(Select2::class, [
                                    'data' => FamilyRelationship::getNames(),
                                    'options' => [
                                        'placeholder' => 'Select a relationship ...',
                                    ],
                                ]) ?>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-xl btn-success mr-125">
                                        <i class="fa fa-tag"></i> Assign
                                    </button>
                                    <a class="btn btn-xl btn-primary"
                                       href="<?= Url::to(['schooladmin/student/create']) ?>">
                                        <i class="fa fa-plus"></i> Add new student
                                    </a>
                                </div>
                            </div>

                            <?php ActiveForm::end() ?>

                            <?php Pjax::begin([
                                'id' => 'linked-grid',
                                'timeout' => 10000,
                                'enablePushState' => false,
                            ]); ?>

                            <div id="disabled-without-guardian">
                                <div class="col-md-12 table-responsive">
                                    <?= $this->render('_students-grid', compact('studentsDataProvider', 'guardian')) ?>
                                </div>
                            </div>

                            <div class="col-sm-12">

                                <div class="form-group">
                                    <div class="inline mr-125">
                                        <?php $inviteCaption = empty($guardian->invite_token) ? 'Invite' : 'Reinvite'; ?>
                                        <a class="btn btn-xl btn-primary" data-pjax="0"
                                           href="<?= Url::to(['schooladmin/guardian/invite', 'id' => $guardian->id]) ?>"
                                           id="invite-guardian">
                                            <i class="fa fa-check"></i> <span><?= $inviteCaption ?> guardian</span>
                                        </a>
                                    </div>
                                    <?php if ($user->status === User::STATUS_ACTIVE) : ?>
                                        <div class="inline">
                                            <a class="btn btn-xl btn-primary" id="switch-user-status" data-pjax="0"
                                               href="<?= Url::to(['schooladmin/guardian/switch-status', 'userId' => $user->id]) ?>">
                                                <i class="fa fa-lock"></i> <span>Block user</span>
                                            </a>
                                        </div>
                                    <?php elseif ($user->status === User::STATUS_BANNED) : ?>
                                        <div class="inline">
                                            <a class="btn btn-sm btn-default" id="switch-user-status" data-pjax="0"
                                               href="<?= Url::to(['schooladmin/guardian/switch-status', 'userId' => $user->id]) ?>">
                                                <i class="fa fa-lock"></i> <span>Activate user</span>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <?php Pjax::end(); ?>

                        </div>


                    </div><!-- panel-body -->
                </div><!-- tab-pane -->

            </div><!-- tab-content -->
        </div><!-- tabs-container -->

    </div>
</div>
