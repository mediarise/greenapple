<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $guardian common\models\Guardian */

$this->title = 'Create Guardian';
$this->params['breadcrumbs'][] = ['label' => 'Guardians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-12 col-md-12 col-lg-10">

    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-signup-request">
                    <b><?= $this->title ?></b>
                </a>
            </li>
        </ul><!-- nav-tabs -->

        <div class="tab-content">

            <div class="tab-pane active" id="tab-signup-request">
                <div class="panel-body">
                    <ul class="sortable-list connectList agile-list ui-sortable" id="todo">
                        <li class="warning-element" id="task1">
                            <?= $this->render('_form', [
                                'guardian' => $guardian,
                                'user' => $user,
                            ]) ?>

                        </li>
                    </ul>
                </div><!-- panel-body -->
            </div><!-- tab-pane -->

        </div><!-- tab-content -->
    </div><!-- tabs-container -->

</div>
