<?php

/* @var $this \yii\web\View */
/* @var $studentsDataProvider \yii\data\ActiveDataProvider */

/* @var $message string */

use yii\bootstrap\Html;
use yii\grid\GridView;
?>

<?= GridView::widget([
    'dataProvider' => $studentsDataProvider,
    'summary' => false,
    'tableOptions' => [
        'class' => 'table'
    ],
    'columns' => [
        [
            'attribute' => 'first_name',
            'label' => 'Full name',
            'format' => 'raw',
            'value' => function ($data) {
                return '<b>' . Html::a('<i class="fa fa-user mr"></i>' . $data->getFullName(), [
                        '/schooladmin/student/update',
                        'id' => $data->id,
                    ], ['data-pjax' => 0]) . '</b>';
            }
        ],
        [
            'attribute' => 'grade.school.name',
            'label' => 'School',
            'format' => 'html'
        ],
        [
            'attribute' => 'grade.name',
            'label' => 'Grade',
            'format' => 'html'
        ],
        [
            'label' => 'Actions',
            'format' => 'raw',
            'value' => function ($student) use ($guardian) {
                return Html::a('Remove relationship', ['remove-relationship', 'guardianId' => $guardian->id, 'studentId' => $student->id], [
                    'class' => 'remove-relationship',
                    'data-pjax' => 0
                ]);
            }
        ],
    ],
]) ?>
