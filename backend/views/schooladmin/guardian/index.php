<?php

use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\bootstrap\Html;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Guardian */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guardians';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guardian-index">
    <?php IBox::begin(['title' => $this->title]); ?>

    <p class="mb-2">
        <?= Html::a('<i class="fa fa-plus"></i> Create Guardian', ['create'], ['class' => 'btn btn-success btn-xl']) ?>
    </p>


    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'enablePjax' => true,
                'summary' => false,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-hover'
                ],
                'filterRowOptions' => [
                    'class' => 'filters mb-15'
                ],
                'columns' => [
                    /*['class' => 'yii\grid\SerialColumn'],*/
                    [
                        'attribute' => 'first_name',
                        'format' => 'raw',
                        'label' => 'Name',
                        'value' => function ($model) {
                            return Html::a(Html::encode($model->getOfficialName()),
                                ['update', 'id' => $model->id]);
                        }
                    ],
                    [
                        'attribute' => 'user.email',
                        'format' => 'raw',
                        'label' => 'Email',
                        'value' => function ($model) {
                            return Html::a(Html::encode($model->user->email),
                                ['update', 'id' => $model->id]);
                        }
                    ],
                    [
                        'label' => 'Actions',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a('<i class="fa  fa-trash"></i>', ['delete', 'id' => $model->id], [
                                'data-pjax' => 0,
                                'data-confirm' => 'Are you sure you want to delete this item?',
                                'data-method' => 'post',
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'Delete guardian',
                            ]);
                        }
                    ],
                ],
                'filters' => [
                    [
                        'type' => GridView::FILTER_TYPE_TEXT_INPUT,
                        'name' => 'search',
                        'options' => [
                            'placeholder' => 'Search'
                        ],
                        'template' => '<div class="col-lg-6 px-0 mb-15">{input}</div>',
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?php IBox::end(); ?>
</div>
