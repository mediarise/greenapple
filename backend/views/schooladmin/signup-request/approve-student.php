<?php

/**
 * @var $this \yii\web\View
 * @var \common\models\Student $student
 * @var \common\models\SignupRequestStudent $signupRequestStudent
 * @var \common\models\Board[] $boards
 * @var \common\models\SchoolForm $schoolForm
 */

$this->title = 'Signup request';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => ['/schooladmin/signup-request']
];
?>

<?php $this->registerJsFile('js/add-child.js', ['depends' => 'yii\web\JqueryAsset']); ?>


<div class="ibox-content">

    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-signup-request">
                    <b>Signup request</b>
                </a>
            </li>
        </ul><!-- nav-tabs -->

        <div class="tab-content">

            <div class="tab-pane active" id="tab-signup-request">
                <div class="panel-body">
                    <div class="row mt-2">
                        <?= $this->render('_student-form', [
                            'model' => $student,
                            'signupRequestStudent' => $signupRequestStudent,
                            'boards' => $boards,
                            'schoolForm' => $schoolForm,
                        ]) ?>
                    </div>

                </div><!-- panel-body -->
            </div><!-- tab-pane -->

        </div><!-- tab-content -->
    </div><!-- tabs-container -->

</div>
