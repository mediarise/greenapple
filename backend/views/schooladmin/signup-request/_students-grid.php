<?php

/* @var $this \yii\web\View */
/* @var $studentsDataProvider \yii\data\ActiveDataProvider */
/* @var $showActionColumn bool */

/* @var $message string */

use common\models\SignupRequestStudent;
use frontend\widgets\GridView;
use yii\bootstrap\Html;

$gridColumns = [
    [
        'attribute' => 'first_name',
        'label' => 'Full name',
        'format' => 'raw',
        'value' => function (SignupRequestStudent $student) {
            if ($student->status !== SignupRequestStudent::STATUS_APPROVED) {
                return Html::encode($student->getFullName());
            }
            return '<b>' . Html::a('<i class="fa fa-user mr"></i>' .
                    Html::encode($student->getFullName()), ['/schooladmin/student/update', 'id' => $student->student_id]) . '</b>';
        }
    ],
    [
        'attribute' => 'grade.school.name',
        'label' => 'School',
        'format' => 'text'
    ],
    [
        'attribute' => 'grade.name',
        'label' => 'Grade',
        'format' => 'text'
    ],
    [
        'attribute' => 'status',
        'format' => 'text',
        'value' => function (SignupRequestStudent $student) {
            return $student->getStatusName();
        }
    ]
];

if ($showActionColumn) {
    $gridColumns[] = [
        'label' => 'Actions',
        'format' => 'raw',
        'value' => function (SignupRequestStudent $student) {
            switch ($student->status) {
                case SignupRequestStudent::STATUS_APPROVED:
                    return Html::a('View', [
                        '/schooladmin/student/update',
                        'id' => $student->student_id
                    ], ['data-pjax' => 0]);
                case SignupRequestStudent::STATUS_REJECTED:
                    return Html::a('Create & Approve', [
                        'approve-student',
                        'requestId' => $student->signup_request_id,
                        'id' => $student->id
                    ], ['data-pjax' => 0]);
                default:
                    return Html::a('Create & Approve', [
                            'approve-student',
                            'requestId' => $student->signup_request_id,
                            'id' => $student->id
                        ], ['data-pjax' => 0]) .
                        ' | ' . Html::a('Reject', [
                            'schooladmin/signup-request/reject-student',
                            'requestId' => $student->signup_request_id,
                            'id' => $student->id,
                        ], ['class' => 'reject']);
            }
        }
    ];
}

?>


<?= /** @noinspection PhpUnhandledExceptionInspection */
GridView::widget([
    'enablePjax' => false,
    'dataProvider' => $studentsDataProvider,
    'summary' => false,
    'tableOptions' => [
        'class' => 'table'
    ],
    'columns' => $gridColumns,
]) ?>
