<?php

/**
 * @var $this \yii\web\View
 * @var $guardian common\models\Guardian
 * @var $boards array
 * @var $selectFieldOptions array
 * @var $changeCallback string
 * @var $studentsDataProvider \yii\data\ArrayDataProvider
 * @var $canInvite bool
 * @var $rejectionForm \common\models\SignupRequestRejectionForm
 * @var $signupRequest SignupRequestGuardian
 */

use common\components\BitHelper;
use common\models\SignupRequestGuardian;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$status = $signupRequest->getAdminStatus();

?>
<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'options' => [
        'id' => 'guardian-form'
    ],
    'action' => Url::to(['schooladmin/signup-request/create-guardian', 'id' => $signupRequest->id]),
]) ?>
<?php if ($guardian->isNewRecord): ?>
    <?= Html::activeHiddenInput($guardian, 'greeting') ?>
<?php endif; ?>


    <div class="col-sm-10 col-md-8 col-lg-6">
        <?= $form->field($guardian, 'first_name')->textInput(['readonly' => '']); ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-10 col-md-8 col-lg-6">
        <?= $form->field($guardian, 'last_name')->textInput(['readonly' => '']); ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-10 col-md-8 col-lg-6">
        <?= $form->field($signupRequest, 'email')->textInput(['readonly' => '']); ?>
    </div>

    <div class="clearfix"></div>

    <hr>

<?php ActiveForm::end() ?>

<?php Pjax::begin([
    'id' => 'linked-grid',
    'timeout' => 10000,
    'enablePushState' => false,
]); ?>

    <div id="disabled-without-guardian">

        <div class="col-sm-12">
            <div class="table-responsive">
                <?= $this->render('_students-grid', [
                    'studentsDataProvider' => $studentsDataProvider,
                    'showActionColumn' => $status != SignupRequestGuardian::STATUS_REJECTED && $status != SignupRequestGuardian::STATUS_COMPLETED,
                ]) ?>
            </div>
        </div>


        <div class="col-sm-12">
            <div class="form-group">
                <?php if ($canInvite): ?>
                    <div class="inline">
                        <?php $inviteCaption = $guardian->invite_token === null ? 'Invite' : 'Reinvite'; ?>
                        <a class="btn btn-xl btn-success mr-125"
                           href="<?= Url::to(['schooladmin/signup-request/invite', 'id' => $signupRequest->id]) ?>"
                           id="invite-guardian">
                            <i class="fa fa-check"></i><span><?= $inviteCaption ?> guardian</span>
                        </a>
                    </div>
                <?php endif; ?>

                <?php if (BitHelper::testBits($status, SignupRequestGuardian::STATUS_NEW |
                    SignupRequestGuardian::STATUS_INCOMPLETE |
                    SignupRequestGuardian::STATUS_NOT_INVITED)): ?>

                    <div class="inline">
                        <?php Modal::begin([
                            'header' => '<h2>Reject request</h2>',
                            'toggleButton' => [
                                'label' => '<i class="fa fa-times"></i> Reject request',
                                'class' => 'btn btn-xl btn-primary'],
                        ]); ?>

                        <?= $this->render('_rejection-form', [
                            'rejectionForm' => $rejectionForm,
                            'signupRequest' => $signupRequest,
                        ]) ?>

                        <?php Modal::end() ?>
                    </div>

                <?php endif; ?>
            </div>
        </div>
    </div>

<?php Pjax::end() ?>