<?php

/* @var \common\models\SignupRequestGuardian $signupRequest */
/* @var $studentsDataProvider \yii\data\ActiveDataProvider */
?>
<?php

/**
 * @var $this \yii\web\View
 * @var $model common\models\GuardianAddStudentTemplateForm
 * @var $students common\models\SignupRequestStudent []
 * @var $guardian common\models\SignupRequestGuardian
 * @var $rejectionForm common\models\SignupRequestRejectionForm
 * @var $boards array
 * @var $canInvite bool
 */

$this->title = 'Signup request';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => ['schooladmin/signup-request']
];

$this->registerJsFile('js/signup-request.js', ['depends' => 'yii\web\JqueryAsset']);

$status = $signupRequest->getAdminStatus();
?>


<div class="ibox-content">
    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-signup-request">
                    <b class="mr-125">Signup request</b> <?= /** @noinspection PhpUnhandledExceptionInspection */
                    \common\widgets\signup_request\StatusWidget::widget(['status' => $status]) ?>
                </a>
            </li>
        </ul><!-- nav-tabs -->

        <div class="tab-content">
            <div class="tab-pane active" id="tab-signup-request">
                <div class="panel-body">
                    <div class="row mt-2">

                        <?= $this->render('_guardian-form', [
                            'guardian' => $guardian,
                            'signupRequest' => $signupRequest,
                            'studentsDataProvider' => $studentsDataProvider,
                            'rejectionForm' => $rejectionForm,
                            'canInvite' => $canInvite,
                        ]) ?>
                    </div>

                </div><!-- panel-body -->
            </div><!-- tab-pane -->
        </div><!-- tab-content -->
    </div><!-- tabs-container -->
</div>