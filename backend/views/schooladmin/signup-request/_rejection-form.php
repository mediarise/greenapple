<?php

/* @var $this \yii\web\View */
/* @var $rejectionForm \common\models\SignupRequestRejectionForm */
/* @var $signupRequest \common\models\SignupRequestGuardian */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>


<?php $form = ActiveForm::begin([
    //'enableClientValidation' => false,
    'action' => Url::to(['schooladmin/signup-request/reject', 'id' => $signupRequest->id]),
    'errorCssClass' => 'has-warning',
    'options' => [
        'id' => 'reject-form'
    ],
]) ?>

<div class="row mt-3">

    <div class="col-md-12 mb-3" id="rejection-templates">
        <p><?= Html::a('<i class="fa fa-minus-circle mr"></i> <span>Reject because of some problems with a guardian identification'); ?></span></p>
        <p><?= Html::a('<i class="fa fa-minus-circle mr"></i> <span>Reject because of unauthorized access'); ?></span></p>
        <p><?= Html::a('<i class="fa fa-minus-circle mr"></i> <span>Reject because of something else...'); ?></span></p>
    </div>

    <div class="col-md-12">
        <?= $form->field($rejectionForm, 'rejectReason')->textarea()->label('Why are you want to reject this request?'); ?>
    </div>

    <div class="col-md-12">
        <div class="pt-2">
            <button type="submit" class="btn btn-xl btn-primary">
                <i class="fa fa-times"></i> Reject signup request
            </button>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>
