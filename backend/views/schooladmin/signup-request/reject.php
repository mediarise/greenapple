<?php

/* @var $this \yii\web\View */

/* @var $model \common\models\SignupRequestRejectionForm */

use frontend\widgets\IBox;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>
<div>
        <?php IBox::begin(['title' => 'Enter reason of reject']); ?>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'rejectReason')->textarea(['rows' => 15])->label('') ?>

        <p class="pull-right">
            <?= Html::a('Cancel', ['signup-request/index'], ['class' => 'btn btn-warning']) ?>
            <?= Html::submitButton('Reject', ['class' => 'btn btn-danger']) ?>
        </p>

        <div class="clearfix"></div>

        <?php ActiveForm::end(); ?>

        <?php IBox::end(); ?>
</div>
