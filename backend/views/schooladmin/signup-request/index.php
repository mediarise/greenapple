<?php

/* @var $this \yii\web\View */

/* @var SignupRequestGuardian $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var $filterModel \common\models\search\SignupRequestGuardian
 */

use common\models\SignupRequestGuardian;
use common\widgets\signup_request\StatusWidget;
use frontend\widgets\IBox;
use yii\grid\DataColumn;
use frontend\widgets\GridView;
use yii\helpers\Html;


$this->title = 'Signup request';
$this->params['breadcrumbs'][] = 'Signup request';

?>


<?php IBox::begin([
    'title' => 'Signup Requests',
]); ?>
<div class="row">
    <div class="col-md-12">
        <?=
        /** @noinspection PhpUnhandledExceptionInspection */
        GridView::widget([
            'dataProvider' => $dataProvider,
            'enablePjax' => true,
            'filterModel' => $filterModel,
            'summary' => false,
            'tableOptions' => [
                'class' => 'table table-hover'
            ],
            'filterRowOptions' => [
                'class' => 'filters row xm mb-15'
            ],
            'columns' => [

                [
                    'attribute' => 'first_name',
                    'label' => 'Name',
                    'format' => 'raw',
                    'value' => function (SignupRequestGuardian $model) {
                        return Html::a(Html::encode($model->getFullName()), ['schooladmin/signup-request/view', 'id' => $model->id], ['data-pjax' => 0]);
                    },
                ],
                [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'format' => 'raw',
                    'value' => function (SignupRequestGuardian $model) {
                        return Html::a(Html::encode($model->email), ['schooladmin/signup-request/view', 'id' => $model->id], ['data-pjax' => 0]);
                    },
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function (SignupRequestGuardian $model) {
                        return StatusWidget::widget([
                            'status' => $model->getAdminStatus(),
                            'options' => [
                                'class' => 'label-sm inline'
                            ],
                        ]);
                    },
                ],
                [
                    'label' => 'Actions',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a(
                            '<i class="fa fa-eye"></i>',
                            ['schooladmin/signup-request/view', 'id' => $model->id],
                            [
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'View signup request',
                                'data-pjax' => 0,
                            ]);
                    }
                ]
            ],
            'filters' => [
                [
                    'type' => GridView::FILTER_TYPE_TEXT_INPUT,
                    'name' => 'search',
                    'options' => [
                        'placeholder' => 'Search'
                    ],
                    'template' => '<div class="col-sm-6">{input}</div>',
                ],
                [
                    'type' => GridView::FILTER_TYPE_SELECT_2,
                    'template' => '<div class="col-sm-6">{widget}</div>',
                    'widgetConfig' => [
                        'name' => 'status',
                        'data' => SignupRequestGuardian::getGuardianStatusNames(),
                        'options' => [
                            'placeholder' => 'Select status ...',
                        ],
                    ]
                ]
            ],
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<?php IBox::end() ?>

