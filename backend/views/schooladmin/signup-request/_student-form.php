<?php

/**
 * @var $this \yii\web\View
 * @var $model common\models\GuardianAddStudentTemplateForm
 * @var $boards array
 * @var $selectFieldOptions array
 * @var $changeCallback string
 */
/* @var $guardiansDataProvider */

/* @var $schoolForm \common\models\SchoolForm */

/* @var $signupRequestStudent \common\models\SignupRequestStudent */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$selectBoardId = 'student-' . $model->id . '-board-id';
$selectSchoolId = 'student-' . $model->id . '-school-id';
$selectGradeId = 'student-' . $model->id . '-grade-id';

$frontendUrlManager = Yii::$app->urlManagerFrontend;

?>
    <div>
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => [
                'id' => 'approve-student-form'
            ],
        ]) ?>
        <?= $this->render('/schooladmin/student/_form', [
            'model' => $model,
            'boards' => $boards,
            'schoolForm' => $schoolForm,
            'form' => $form,
        ]) ?>
        <?php ActiveForm::end() ?>
    </div>


    <div class="col-xs-12 mt-2">
        <?php ActiveForm::begin(['action' => Url::to([
            'schooladmin/signup-request/reject-student',
            'requestId' => $signupRequestStudent->signup_request_id,
            'id' => $signupRequestStudent->id
        ])]); ?>

        <div class="form-group">
            <p>
                <a type="button" class="btn btn-xl btn-success mr-125" id="approve-student-btn">
                    <i class="fa fa-check"></i> Approve student
                </a>
                <button type="submit" class="btn btn-xl btn-primary" name="action" value="reject"><i
                            class="fa fa-times"></i>
                    Reject student
                </button>
            </p>
        </div>
        <?php ActiveForm::end() ?>

    </div>

<?php $this->registerJs('jQuery("#approve-student-btn").click(function(){jQuery("#approve-student-form").submit();return false;});'); ?>