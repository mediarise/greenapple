<?php

/**
 * @var $this \yii\web\View
 * @var $model common\models\GuardianAddStudentTemplateForm
 * @var $students common\models\SignupRequestStudent []
 * @var $guardian common\models\SignupRequestGuardian
 * @var $student common\models\SignupRequestStudent
 * @var $rejectionForm common\models\SignupRequestRejectionForm
 * @var $boards array
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\IBox;

$this->title = 'Signup request';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => Html::a('Signup requests', Url::to(['/']))
];

//$studentsDataProvider = $guardiansDataProvider = new \yii\data\ArrayDataProvider([
//    'models' => [
//        [
//            'student' => 'Bart Simpson',
//            'school' => 'Springfield Elementary',
//            'grade' => '4-1',
//            'status' => 'New',
//            'actions' => Html::a('Create & Approve', '#') .
//                ' | ' . Html::a('Edit', '#') .
//                ' | ' . Html::a('Reject', '#'),
//        ],
//        [
//            'student' => 'Maggie Simpson',
//            'school' => 'Springfield Elementary',
//            'grade' => '1-2',
//            'status' => 'Match found',
//            'actions' => Html::a('Approve & Match', '#') .
//                ' | ' . Html::a('Reject', '#'),
//        ],
//        [
//            'student' => 'Lisa Simpson',
//            'school' => 'Springfield Elementary',
//            'grade' => '2-3',
//            'status' => 'Approved',
//            'actions' => Html::a('Edit', '#') .
//                ' | ' . Html::a('Remove relationship', '#'),
//        ]
//    ]
//]);

$selectFieldOptions = [
    'validateOnChange' => false,
    'validateOnBlur' => false,
];

$changeCallback = 'function(e) { document.SignupForm.loadOptions("%s", e.target.value, "%s") }';

?>

<?php $this->registerJsFile('js/add-child.js', ['depends' => 'yii\web\JqueryAsset']); ?>


<div class="col-sm-12 col-md-12 col-lg-12">

    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-signup-request">
                    <b>Signup request</b>
                </a>
            </li>
        </ul><!-- nav-tabs -->

        <div class="tab-content">

            <div class="tab-pane active" id="tab-signup-request">
                <div class="panel-body">

                    <?= $this->render('_student-form', [
                        'model' => $student,
                        'guardiansDataProvider' => $guardiansDataProvider,
                        'changeCallback' => $changeCallback,
                        'selectFieldOptions' => $selectFieldOptions,
                        'boards' => $boards
                    ]) ?>

                </div><!-- panel-body -->
            </div><!-- tab-pane -->

        </div><!-- tab-content -->
    </div><!-- tabs-container -->

</div>