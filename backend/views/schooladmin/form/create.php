<?php
/**
 * @var $this View
 * @var $model Form
 */

use common\models\Form;
use frontend\widgets\IBox;
use yii\web\View;

$this->title = 'Create form';
$this->params['breadcrumbs'][] = ['label' => 'Form list', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Create form'];
?>

<div class="form-create-page">
    <?php IBox::begin([
        'title' => 'Create form'
    ]) ?>
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
    <?php IBox::end(); ?>
</div>