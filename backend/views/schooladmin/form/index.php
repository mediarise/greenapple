<?php
/**
 * @var $this View
 * @var $filterModel FormSearch
 * @var $dataProvider ActiveDataProvider
 */

use common\models\FormSearch;
use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;
use common\models\Form;

$this->title = 'Form list';
$this->params['breadcrumbs'][] = ['label' => 'Form list'];
?>

<div class="form-list-page">

    <?php IBox::begin([
        'title' => 'Form list table'
    ]); ?>

    <p class="mb-2">
        <?= Html::a('<i class="fa fa-plus"></i> Add new', ['create'], ['class' => 'btn btn-success btn-xl']); ?>
    </p>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $filterModel,
        'summary' => false,
        'enablePjax' => true,
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'filterRowOptions' => [
            'class' => 'filters row xm mb-15'
        ],
        'columns' => [
            [
                'attribute' => 'title',
                'content' => function ($model) {
                    return Html::a(Html::encode($model->title), ['update', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'description',
                'content' => function ($model) {
                    if ($model->description != '') {
                        return StringHelper::truncateWords($model->description, 10);
                    }
                    return '-';
                }
            ],
            [
                'attribute' => 'control',
                'content' => function ($model) {
                    return $model->getControlField();
                }
            ],
            [
                'class' => \yii\grid\ActionColumn::class,
                'header' => 'Actions',
                'buttons' => [],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<i class="fa fa-pencil"></i>',
                            $url,
                            [
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'Edit form',
                            ]
                        );
                    },
                    'delete' => function ($url, Form $model) {
                        if (count($model->lnkEventForms)) {
                            return Html::a('<i class="fa fa-trash"></i>', $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'Delete form',
                                'data-pjax' => false,
                                'onclick' => new \yii\web\JsExpression("
                                    event.preventDefault();
                                    toastr.warning('This form is related to existing event. Please delete event firstly.');
                                ")
                            ]);
                        }
                        return Html::a(
                            '<i class="fa  fa-trash"></i>',
                            $url,
                            [
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'Delete',
                                'data-confirm' => 'Are you sure you want to delete this item?',
                            ]
                        );
                    },
                ],
            ],
        ],
        'filters' => [
            [
                'type' => GridView::FILTER_TYPE_TEXT_INPUT,
                'name' => 'search',
                'template' => '<div class="col-lg-6">{input}</div>',
                'options' => [
                    'id' => 'search-filter',
                    'placeholder' => 'Search'
                ],
            ],
        ],
    ]); ?>
    <?php IBox::end(); ?>
</div>
