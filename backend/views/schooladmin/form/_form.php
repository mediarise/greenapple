<?php
/**
 * @var $this View
 * @var $model Form
 */

use common\models\Form;
use common\widgets\Select2;
use frontend\widgets\HtmlEditor;
use yii\helpers\Html;
use yii\web\View;
use yii\bootstrap\ActiveForm;
?>


<div class="col-xs-12">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
        ],
    ]) ?>
    <?= $form->field($model, 'title')->textInput(); ?>

    <?= $form->field($model, 'description')->textarea(); ?>

    <?= $form->field($model, 'html')->widget(HtmlEditor::class, []); ?>

    <?= $form->field($model, 'control')->widget(Select2::class, [
        'data' => Form::getControlFields()
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-xl btn-success mr-125']) ?>

        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-xl btn-primary']) ?>
    </div>

    <?php $form->end(); ?>

</div>
