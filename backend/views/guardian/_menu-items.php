<?php

use common\helpers\FrontendUrl;

return [
    [
        'label' => 'Home',
        'iconClass' => 'fa fa-home',
        'url' => ['/guardian/index'],
    ],
    [
        'label' => 'Messages',
        'url' => '#',
        'iconClass' => 'fa fa-envelope-open-o',
    ],
    [
        'label' => 'Account Settings',
        'url' => '#',
        'iconClass' => 'fa fa-wrench',
    ],
];