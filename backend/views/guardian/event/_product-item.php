<?php
/**
 * @var $this \yii\web\View
 * @var $product \common\models\guardian\AttendProduct
 * @var $form \yii\widgets\ActiveForm
 */

use kartik\touchspin\TouchSpin;
use yii\helpers\Html;
use common\widgets\Select2;
use yii\helpers\ArrayHelper;

$inCart = !empty($product->quantity);
$formName = sprintf('products[%s]', $product->id);
$quantityInputName = $formName . '[quantity]';
?>


<div class="ibox-content">

    <div class="col-lg-2 col-sm-3 col-xs-12 mb-2 px-0">
        <?= Html::img($product->url, ['width' => 80, 'height' => 80]) ?>
    </div>


    <div class="col-lg-6 col-sm-9 col-xs-8 mb-2 px-0 pr-4">
        <h3>
            <a href="#" class="text-navy"><?= $product->title ?></a>
        </h3>
        <p class="small"><?= $product->description ?></p>
    </div>


    <div class="col-lg-4 col-xs-12 px-0">

        <div class="col-xs-12 px-0 mb-2">
            <?php
            if ($inCart): ?>
                <i class="fa fa-check"></i>
                already in cart.
            <?php
            else:
                $min_quantity = $product->min_quantity;
                if ($min_quantity > 0 && $min_quantity === $product->max_quantity): ?>
                    <div class="untouchable">
                        <?= Html::hiddenInput($quantityInputName, $min_quantity) ?>
                        <?= Html::textInput($quantityInputName, $min_quantity, [
                            'class' => 'form-control text-center product-quantity-input',
                            'disabled' => 'disabled',
                        ]) ?>
                    </div>
                <?php else:
                    $pluginOptions = [
                        'min' => $min_quantity,
                        'buttonup_class' => 'input-group-addon on-right',
                        'buttondown_class' => 'input-group-addon on-left',
                        'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                        'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                    ];
                    if ($product->max_quantity != 0) {
                        $pluginOptions['max'] = $product->max_quantity;
                    }
                    ?>
                    <div style="max-width: 150px;">
                        <?= /** @noinspection PhpUnhandledExceptionInspection */

                        TouchSpin::widget([
                            'name' => $quantityInputName,
                            'id' => "product-quantity-{$product->id}",
                            'value' => $product->min_quantity,
                            'options' => [
                                'class' => 'text-center product-quantity-input',
                            ],
                            'pluginOptions' => $pluginOptions,
                        ]); ?>
                    </div>


                <?php
                endif;
            endif;
            ?>
        </div>
        <?php if (!empty($product->variations)) : ?>
            <div class="col-xs-12 px-0 mb-2">
                <div class="pb-05">Product variation:</div>
                <div style="max-width: 280px;">
                    <?php
                    $varName = 'prices' . $product->id;
                    $this->registerJsVar($varName, ArrayHelper::map($product->variations, 'id', 'price'));
                    $first = $product->variations[0];
                    $product->price = $first->price;
                    ?>
                    <?= /** @noinspection PhpUnhandledExceptionInspection */
                    Select2::widget([
                        'name' => $formName . '[variation]',
                        'value' => $first->id,
                        'data' => ArrayHelper::map($product->variations, 'id', 'name'),
                        'disabled' => $inCart,
                        'options' => [
                            'class' => 'variation-selector',
                            'data-var-name' => $varName,
                        ],
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'allowClear' => false,
                        ]
                    ]) ?>
                </div>
            </div>
        <?php endif; ?>

    </div>

</div>
