<?php

/**
 * @var $this \yii\web\View
 * @var $formList
 * @var $currentStepNumber
 * @var $involvedForms
 * @var $lastStepNumber
 * @var $hasProducts
 */

use yii\helpers\Url;

$tabs = [];
$currentFormNumber = $currentStepNumber - 1;
foreach ($formList as $formNumber => $formModel) {
    $stepNumber = $formNumber + 1;
    $tabStatusClass = 'disabled';
    $url = '';
    if (isset($involvedForms[$formModel->id])) {
        $tabStatusClass = 'done';
        $url = Url::toRoute([
            'show-form',
            'eventId' => Yii::$app->request->get('eventId'),
            'studentId' => Yii::$app->request->get('studentId'),
            'step' => $stepNumber,
        ]);
    }
    if ($formNumber == $currentFormNumber) {
        $tabStatusClass = 'current';
    }

    $tabs[] = $this->render('_step-tab', [
        'title' => $formModel->title,
        'isFirst' => ($stepNumber == 1),
        'tabStatusClass' => $tabStatusClass,
        'stepNumber' => $stepNumber,
        'url' => $url,
    ]);
}
$tabStatusClass = 'disabled';
$url = '';
if (count($involvedForms) === count($formList)) {
    $tabStatusClass = 'done';
    $url = Url::toRoute([
        'show-product',
        'eventId' => Yii::$app->request->get('eventId'),
        'studentId' => Yii::$app->request->get('studentId')
    ]);
}
if ($currentStepNumber == $lastStepNumber) {
    $tabStatusClass = 'current';
}
if ($hasProducts) {
    $tabs[] = $this->render('_step-tab', [
        'title' => 'Product',
        'isLast' => true,
        'tabStatusClass' => $tabStatusClass,
        'stepNumber' => $lastStepNumber,
        'url' => $url,
    ]);
}
echo implode('', $tabs);
?>



