<?php
/**
 * @var $this \yii\web\View
 * @var $currentStepNumber
 * @var $lastStepNumber
 * @var $isMandatory
 */

use yii\helpers\Url;

$url = Url::toRoute([
    'show-form',
    'eventId' => Yii::$app->request->get('eventId'),
    'studentId' => Yii::$app->request->get('studentId'),
    'step' => $currentStepNumber + 1,
]);

if ($currentStepNumber == $lastStepNumber) {
    $tabStatusClass = 'done';
    $url = Url::toRoute([
        '/guardian/index',
    ]);
}

if ($currentStepNumber == $lastStepNumber - 1) {
    $tabStatusClass = 'done';
    $url = Url::toRoute([
        'show-product',
        'eventId' => Yii::$app->request->get('eventId'),
        'studentId' => Yii::$app->request->get('studentId')
    ]);
}

?>

<?php if ($isReadonly): ?>
    <li class="" aria-disabled="true">
        <?= \yii\helpers\Html::a('Back', ['guardian/index'], ['class' => 'btn btn-lg btn-success']) ?>
    </li>
<?php else: ?>

    <li class="" aria-disabled="true">
        <a href="#" class="decline-event btn btn-lg btn-primary" data-toggle="modal" data-target="#decline-modal">Decline</a>
    </li>

    <?php if ($currentStepNumber != $lastStepNumber) : ?>
        <li aria-hidden="false" aria-disabled="false">
            <a href="<?= $url ?>" role="menuitem" id="<?= $isMandatory ? 'mandatory-next-step-btn' : 'next-step-btn' ?>"
               class="btn btn-lg btn-success">Next</a>
        </li>
    <?php else: ?>
        <?php if ($hasProducts) : ?>
            <li><a href="#" class="btn btn-lg btn-success" id="add-to-cart-btn">Add to cart</a></li>
        <?php else: ?>
            <li aria-hidden="false" aria-disabled="false">
                <a href="<?= $url ?>" role="menuitem"
                   id="<?= $isMandatory ? 'mandatory-next-step-btn' : 'next-step-btn' ?>"
                   class="btn btn-lg btn-success">Finish</a>
            </li>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>

<?php

$script = <<<JS
function setAddToCartBtnState() {
    var quantityInputs = $('.product-quantity-input');
    var sum = 0;
    
    $.each(quantityInputs, function(index, value) {
        sum = sum + parseInt($(value).val());
    });
    
    if (sum === 0) {
        $('#add-to-cart-btn').addClass('disabled');
    } else {
        $('#add-to-cart-btn').removeClass('disabled');
    }
}
$(document).ready(function() {
  setAddToCartBtnState();
})
$('body').on('change', '.product-quantity-input', setAddToCartBtnState)

JS;
$this->registerJs($script);

