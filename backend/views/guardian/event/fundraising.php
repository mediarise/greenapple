<?php

/**
 * @var $this \yii\web\View
 * @var \common\models\DocEvent $eventModel
 * @var \common\models\Student $studentModel
 * @var \common\models\Fundraising $fundraising
 * @var \common\models\guardian\DeclineEventForm $declineEventForm
 * @var \common\models\Guardian $guardian
 * @var bool $isReadonly
 */

use kartik\touchspin\TouchSpin;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Attend event: ' . Html::encode($eventModel->title);

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$imgUrl = $eventModel->mainPhoto->url ?? Yii::$app->params['eventPhotoPlaceholder'];

?>

<div class="ibox">
    <div class="ibox-content">

        <div class="table-responsive">
            <table class="table shoping-cart-table">
                <tbody>
                <tr>
                    <td width="90">
                        <?= Html::img($imgUrl, ['width' => '130px']) ?>
                    </td>
                    <td class="desc">
                        <h3>
                            <p class="text-navy">
                                <?= $eventModel->title ?>
                            </p>
                        </h3>

                        <p class="small">
                            <?= $eventModel->start_date ?>
                        </p>

                        <p class="small">
                            <?php if ($eventModel->is_repeatable) : ?>
                                Repeats Every Weekday
                            <?php else: ?>
                                One time event
                            <?php endif; ?>
                        </p>

                        <div class="m-t-sm">
                            <?= $studentModel->first_name ?> <?= $studentModel->last_name ?>
                        </div>
                    </td>

                    <td>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="ibox-content">
    <?php if (!empty($fundraising->note)): ?>
        <h2>Note:</h2>
        <div class="col-lg-6 px-0 mb-3">
            <p><?= Html::encode($fundraising->note) ?></p>
        </div>
    <?php endif; ?>
    <?php $form = ActiveForm::begin() ?>

    <div class="clearfix"></div>

    <div class="col-xs-6 col-sm-5 col-md-4 col-lg-3 px-0">
        <?php if ($fundraising->step == 0) : ?>
            <?= $form
                ->field($fundraising, 'value')
                ->widget(\kartik\number\NumberControl::class, [
                    'maskedInputOptions' => [
                        'prefix' => '$ ',
                        'allowMinus' => false,
                        'alias' => 'decimal',
                        'digits' => 2,
                        'groupSeparator' => ',',
                        'autoGroup' => true,
                        'autoUnmask' => true
                    ],
                    'displayOptions' => [
                        'class' => 'text-left form-control'
                    ]
                ]); ?>
        <?php else: ?>
            <?= $form
                ->field($fundraising, 'value')
                ->widget(TouchSpin::class, [
                    'options' => [
                        'class' => 'text-center product-quantity-input'
                    ],
                    'pluginOptions' => [
                        'readonly' => $isReadonly,
                        'min' => $fundraising->min,
                        'max' => $fundraising->max,
                        'step' => $fundraising->step,
                        'buttonup_class' => 'input-group-addon on-right',
                        'buttondown_class' => 'input-group-addon on-left',
                        'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                        'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                    ],
                ])
                ->label('Amount')
            ?>
        <?php endif; ?>
    </div>

    <div class="clearfix"></div>

    <?php if ($isReadonly): ?>
        <?= Html::a('Back', ['guardian/index'], ['class' => 'btn btn-xl btn-primary']) ?>
    <?php else: ?>
        <a href="#" class="decline-event btn btn-xl btn-primary" data-toggle="modal"
           data-target="#decline-modal">Decline</a>
        <?= Html::submitButton('Add to cart', ['class' => 'btn btn-xl btn-success']) ?>
    <?php endif; ?>

    <?php ActiveForm::end() ?>
    <?= $this->render('_decline-form', [
        'declineEventForm' => $declineEventForm,
        'guardianId' => $guardian->id,
        'studentId' => $studentModel->id,
        'eventId' => $eventModel->id,
    ]) ?>
</div>
