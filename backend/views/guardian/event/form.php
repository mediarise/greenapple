<?php
/**
 * @var $this \yii\web\View
 * @var $eventModel
 * @var $studentModel
 * @var $formModel Form
 * @var $formHtml
 * @var $acceptForm
 * @var $currentStepNumber
 * @var $involvedForms
 * @var $formList
 * @var $lastStepNumber
 * @var $declineEventForm
 * @var $guardian
 * @var $lnkEventForm \common\models\LnkEventForm
 * @var $lastFormActionDate
 * @var $hasProducts
 */

use frontend\widgets\CheckBox;
use yii\widgets\ActiveForm;
use common\models\Form;

?>

<?= $this->render('_event-info', [
    'eventModel' => $eventModel,
    'studentModel' => $studentModel,
    'eventPhoto' => $eventPhoto,
]) ?>


    <div class="wizard-big wizard clearfix">
        <div class="steps clearfix">
            <ul role="tablist">
                <?= $this->render('_tabs', [
                    'formList' => $formList,
                    'currentStepNumber' => $currentStepNumber,
                    'involvedForms' => $involvedForms,
                    'lastStepNumber' => $lastStepNumber,
                    'hasProducts' => $hasProducts,
                ]) ?>
            </ul>
        </div>

        <div class="content clearfix scrolled">
            <div class="body">
                <h2><?= $formModel->title ?></h2>

                <?= Yii::$app->formatter->asHtml($formHtml) ?>

                <div class="ml-15 mt-2">
                    <?php

                    if ($lastFormActionDate) {
                        $datatime = new DateTime($lastFormActionDate);

                        echo 'You have already accepted this form at ' . $datatime->format('m/d/Y');
                    } else {
                        $form = ActiveForm::begin([
                            'id' => 'form-accept',
                            'options' => [
                                'class' => 'form-horizontal',
                            ],
                        ]);

                        switch ($formModel->control) {
                            case Form::CONTROL_CHECKBOX_AGREE :
                                echo $form->field($acceptForm, 'isAccept')->widget(CheckBox::class, [
                                    'label' => 'I agree',
                                    'options' => [
                                        'id' => 'form-control',
                                    ]
                                ])->label(false);
                                break;
                            case Form::CONTROL_YES_NO :
                                echo $form->field($acceptForm, 'isAccept')
                                    ->radioList([1 => 'Yes', 0 => 'No'], [
                                        'id' => 'radio-form-control',
                                    ])
                                    ->label('I agree');
                                break;
                            case Form::CONTROL_AGREE_DECLINE :
                                echo $form->field($acceptForm, 'isAccept')->widget(CheckBox::class, [
                                    'label' => 'Agree/decline',
                                    'options' => [
                                        'id' => 'form-control',
                                    ]
                                ])->label(false);
                                break;
                        }

                        ActiveForm::end();
                    }
                    ?>
                </div>

            </div>
        </div>
        <div class="actions clearfix">
            <ul role="menu" aria-label="Pagination">
                <?= $this->render('_buttons', [
                    'isMandatory' => $lnkEventForm->is_mandatory,
                    'currentStepNumber' => $currentStepNumber,
                    'lastStepNumber' => $lastStepNumber,
                    'hasProducts' => $hasProducts,
                    'isReadonly' => $isReadonly,
                ]) ?>
            </ul>
        </div>
    </div>

<?= $this->render('_decline-form', [
    'declineEventForm' => $declineEventForm,
    'guardianId' => $guardian->id,
    'studentId' => $studentModel->id,
    'eventId' => $eventModel->id,
]) ?>


<?php
$script = <<<JS
$(document).ready(function() {
        var body = $('body');
        var form = $('#form-accept');
        
        if (form.length === 0) {
            return;
        }

        body.on('click', '#mandatory-next-step-btn', function() {
            if ($('#form-control').is(":checked") || $('#radio-form-control input:checked').val() == 1) {
                 form.submit();
                 return false;
            }
            toastr.warning('Please, accept this form before you continue', 'Accept this form');
            return false;
        });
        body.on('click', '#next-step-btn', function(e) {
            e.preventDefault();
            form.submit();
            return false;
        });
    })
JS;
$this->registerJs($script);
?>