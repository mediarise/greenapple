<?php
/**
 * @var $this \yii\web\View
 * @var $eventModel \common\models\DocEvent
 * @var $studentModel \common\models\Student
 * @var $eventPhoto \common\models\EventPhoto
 */

use yii\helpers\Html;
use yii\helpers\Url;

$imgUrl = empty($eventPhoto) ? Yii::$app->params['eventPhotoPlaceholder'] : $eventPhoto->url;

$this->title = $eventModel->title;

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => 'Attend event: ' . $this->title];
?>

<div class="ibox">
    <div class="ibox-content">

        <div class="table-responsive">
            <table class="table shoping-cart-table">
                <tbody>
                <tr>
                    <td width="90">
                        <?= Html::img($imgUrl, ['width' => '130px']) ?>
                    </td>
                    <td class="desc">
                        <h3>
                            <a href="#" class="text-navy">
                                <?= $eventModel->title ?>
                            </a>
                        </h3>

                        <p class="small">
                            <?= $eventModel->start_date ?>
                        </p>

                        <p class="small">
                            <?php if ($eventModel->is_repeatable) : ?>
                                Repeats Every Weekday
                            <?php else: ?>
                                One time event
                            <?php endif; ?>
                        </p>

                        <div class="m-t-sm">
                            <?= $studentModel->first_name ?> <?= $studentModel->last_name ?>
                        </div>
                    </td>

                    <td>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

