<?php

/**
 * @var $this \yii\web\View
 * @var $declineEventForm \common\models\guardian\DeclineEventForm
 * @var $guardianId
 * @var $studentId
 * @var $eventId
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="modal inmodal" id="decline-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <?php $form = ActiveForm::begin([
        'action' => Url::toRoute(['decline-event'])
    ]) ?>
    <?= $form->field($declineEventForm, 'guardianId')->hiddenInput(['value' => $guardianId])->label(false)?>
    <?= $form->field($declineEventForm, 'eventId')->hiddenInput(['value' => $eventId])->label(false)?>
    <?= $form->field($declineEventForm, 'studentId')->hiddenInput(['value' => $studentId])->label(false)?>
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">Close</span></button>
                <i class="fa fa-laptop modal-icon"></i>
                <h4 class="modal-title">Decline event</h4>
                <small class="font-bold">Please, enter the decline reason</small>
            </div>
            <div class="modal-body">
                <?= $form->field($declineEventForm, 'reason')->textarea() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-xl" data-dismiss="modal">Close</button>
                <?= Html::submitButton('Confirm', ['class' => 'btn btn-success btn-xl'])?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

