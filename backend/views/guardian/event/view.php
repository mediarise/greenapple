<?php
/**
 * @var $this View
 * @var $eventModel \common\models\DocEvent
 * @var $studentModel \common\models\Student
 * @var $eventPhoto
 * @var array $forms
 * @var array $acceptedForms
 * @var $guardian \common\models\Guardian
 * @var $isReadonly
 */

use backend\assets\VueFormWizardAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use common\models\Form;
use yii\widgets\ActiveForm;

$this->title = 'Event accepting';
$this->params['breadcrums'][] = 'Event view';

VueFormWizardAsset::register($this);

$this->registerJsVar('acceptFormUrl', Url::toRoute(['/guardian/event/accept-form']));
$this->registerJsVar('eventId', $eventModel->id);
$this->registerJsVar('studentId', $studentModel->id);
$this->registerJsVar('dashboardUrl', Url::toRoute(['/guardian']));

$acceptedFormCount = count($acceptedForms);

$formJson = Json::encode($forms);

function getJsValidator($formName)
{
    return /* @lang JavaScript */
        "validate_{$formName}() {
            var vm = this;
            var accept = vm.forms.{$formName}.accept;
            var isAccepted = vm.forms.{$formName}.isAccepted;
            if (isAccepted) {
                return true;
            }
            if (accept == true || accept == '1') {
                vm.acceptForm('{$formName}');
                return true;
            } else {
                toastr.warning('Please, accept this form before you continue', 'Accept this form');
                return false;
            }
            return false;
         }";
}

$formValidators = [];
foreach ($forms as $formName => $formData) {
    $formValidators[] = getJsValidator($formName);
}

$sctipt = /* @lang JavaScript */
    'Vue.use(VueFormWizard, {});
var wizard = new Vue({
    el: "#wizard",
    data: {
        maxStep: ' . $acceptedFormCount . ',
        forms: ' . $formJson . ',
        nextBtnText: "Next",
        haveProducts: ' . intval(!empty($products)) . '
    },
    methods: {
        onComplete: function () {
            if (this.haveProducts == "1") {
                $("#products-form").submit();return false; 
            } else {
                this.goToDashboard();
            }
        },
        goToDashboard: function () {
              location.href = dashboardUrl;
        },
        onTabChange: function(prevIndex, nextIndex) {
            this.setNextBtnText(prevIndex, nextIndex);
        },
        setNextBtnText: function(prevIndex, nextIndex) {
            var vm = this;
            var tabCount = this.$refs.wizard.tabs.length;
            var activeTabIndex = this.$refs.wizard.activeTabIndex;
            var nexTabIndex = activeTabIndex + 1;
            var lastStep = tabCount - 1;
            
            var haveProducts = vm.haveProducts;
            var oneTab = tabCount == 1;
            
            if (haveProducts && (oneTab || lastStep == nextIndex)) {
                vm.nextBtnText = "Add to cart";
                return;
            }
            if (!haveProducts && (oneTab || lastStep == nextIndex)) {
                vm.nextBtnText = "Finish";
                return;
            }
            vm.nextBtnText = "Next";
            
        },
        acceptForm: function (formName) {
            var vm = this;
            var formData = vm.forms[formName];
            var nextBtn = $("#next-step-btn").ladda();
            
            nextBtn.ladda(\'start\').attr(\'disabled\', true);
            
            greenApple.ajax.perform(acceptFormUrl, {
                formId: formData.id,
                    eventId: eventId,
                    studentId: studentId
                }, {
                    resultNotification: function(data) {
                        nextBtn.ladda("stop");
                    },
                    resultError: function(data) {
                        nextBtn.ladda("stop");
                    },
                    resultSuccess: function(data) {
                        nextBtn.ladda("stop");
                        formData.lastActionDate = data.date;
                        formData.isAccepted = true;
                    }
                });
        },
        ' . implode(',', $formValidators) . '
    },
    mounted() {
        this.setNextBtnText();
        var tabs = this.$refs.wizard.tabs;
        tabs.forEach((tab) => tab.checked = true);
        this.$refs.wizard.maxStep = this.maxStep;
    }
})

$("body").on("click", "#next-step-btn", function() {
    wizard.$refs.wizard.nextTab();
})
';
$this->registerJs($sctipt, View::POS_END);
?>

<?= $this->render('_event-info', [
    'eventModel' => $eventModel,
    'studentModel' => $studentModel,
    'eventPhoto' => $eventPhoto,
]) ?>
    <style>
        .wizard-nav.wizard-nav-pills {

        <?php
        $tabCount = count($forms);
        if (!empty($products)) {
            $tabCount++;
        }
        if ($tabCount > 1  && $tabCount < 4) {
           echo 'max-width: 500px';
        } elseif ($tabCount == 1) {
            echo 'max-width: 150px';
        } ?>

        }

        .wizard-progress-with-circle {
            display: none;
        }

        .wizard-card-footer {
            display: none !important;
        }
    </style>

    <div id="wizard">
        <form-wizard @on-complete="onComplete"
                     @on-change="onTabChange"
                     ref="wizard"
                     shape="tab"
                     color="#5e5fc6">

            <?php foreach ($forms as $formName => $form) : ?>
                <tab-content title="<?= $form['title'] ?>"
                             icon="fa fa-check-square-o"
                             :before-change="validate_<?= $formName ?>"
                             ref="<?= $formName ?>">

                    <div class="row">
                        <div class="col-lg-8">
                            <?= $form['html'] ?>

                            <div class=" py-15">
                                <div v-if="forms.<?= $formName ?>.isAccepted">
                                    You have already accepted this form at {{ forms.<?= $formName ?>.lastActionDate }}
                                </div>
                                <div v-else>
                                    <?php
                                    switch ($form['controlType']) {
                                        case Form::CONTROL_CHECKBOX_AGREE :
                                            echo Html::checkbox('control', false, [
                                                'label' => 'I agree',
                                                'id' => 'form-control',
                                                'v-model' => "forms.{$formName}.accept"
                                            ]);
                                            break;
                                        case Form::CONTROL_YES_NO :
                                            echo Html::radioList('control', null, [1 => 'Yes', 0 => 'No'], [
                                                'id' => 'radio-form-control',
                                                'label' => 'I agree',
                                                'itemOptions' => [
                                                    'class' => ['some-class'],
                                                    'v-model' => "forms.{$formName}.accept",
                                                ],
                                            ]);
                                            break;
                                        case Form::CONTROL_AGREE_DECLINE :
                                            echo Html::checkbox('control', false, [
                                                'label' => 'Agree/decline',
                                                'id' => 'form-control',
                                                'v-model' => "forms.{$formName}.accept"
                                            ]);
                                            break;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>


                </tab-content>
            <?php endforeach; ?>

            <?php if (!empty($products)) : ?>
                <tab-content title="Products"
                             icon="fa fa-shopping-cart">
                    <?php ActiveForm::begin([
                        'id' => 'products-form',
                        'action' => Url::to([
                            'add-to-cart',
                            'eventId' => $eventModel->id,
                            'studentId' => $studentModel->id,
                        ])
                    ]) ?>
                    <div class="product-list">
                        <?php foreach ($products as $product) {
                            $product->url = $product->url ?? Yii::$app->params['eventPhotoPlaceholder'];
                            echo $this->render('_product-item', [
                                'product' => $product,
                            ]);
                        } ?>
                    </div>
                    <?php ActiveForm::end() ?>
                </tab-content>
            <?php endif; ?>
        </form-wizard>

        <?php if (!$isReadonly): ?>
            <div class="row">
                <div class="mb-4 col-lg-12">
                    <a href="#" class="decline-event btn btn-xl btn-primary" data-toggle="modal"
                       data-target="#decline-modal">Decline</a>
                    <button id="next-step-btn" class="btn btn-success btn-xl ladda-button ml-125" data-style="zoom-out">
                        {{ nextBtnText }}
                    </button>
                </div>
            </div>
        <?php endif; ?>


    </div>

<?= $this->render('_decline-form', [
    'declineEventForm' => $declineEventForm,
    'guardianId' => $guardian->id,
    'studentId' => $studentModel->id,
    'eventId' => $eventModel->id,
]) ?>