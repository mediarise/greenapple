<?php
/**
 * @var $this \yii\web\View
 * @var $stepNumber
 * @var $title
 * @var $tabStatusClass
 * @var $isFirst
 * @var $isLast
 * @var $url
 */


$isFirst = isset($isFirst) ? $isFirst : false;
$isLast = isset($isLast) ? $isLast : false;
?>

<li role="tab" class="<?= $isFirst ? 'first' : '' ?> <?= $isLast ? 'last' : '' ?> <?= $tabStatusClass ?>">
    <a id="form-t-<?= $stepNumber ?>" href="<?= $url ?>" aria-controls="tab-p-<?= $stepNumber ?>">
        <span class="current-info audible">current step: </span><span class="number"><?= $stepNumber ?>.</span>
        <?= $title ?>
    </a>
</li>
