<?php
/**
 * @var $this \yii\web\View
 * @var $eventModel \common\models\DocEvent
 * @var $studentModel \common\models\Student
 * @var $currentStepNumber
 * @var $involvedForms
 * @var $formList
 * @var $lastStepNumber
 * @var $products \common\models\EventProduct[]
 * @var $declineEventForm
 * @var $guardian \common\models\Guardian
 * @var $eventPhoto
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?= $this->render('_event-info', [
    'eventModel' => $eventModel,
    'studentModel' => $studentModel,
    'eventPhoto' => $eventPhoto,
]) ?>

<div class="wizard-big wizard clearfix">
    <div class="steps clearfix">
        <ul role="tablist">
            <?= $this->render('_tabs', [
                'formList' => $formList,
                'currentStepNumber' => $currentStepNumber,
                'involvedForms' => $involvedForms,
                'lastStepNumber' => $lastStepNumber,
                'hasProducts' => true,
            ]) ?>
        </ul>
    </div>

    <div class="content clearfix">
        <?php ActiveForm::begin([
            'id' => 'products-form',
            'action' => Url::to([
                'add-to-cart',
                'eventId' => $eventModel->id,
                'studentId' => $studentModel->id,
            ])
        ]) ?>
        <div class="product-list">
            <?php foreach ($products as $product) {
                $product->url = $product->url ?? Yii::$app->params['eventPhotoPlaceholder'];
                echo $this->render('_product-item', [
                    'product' => $product,
                ]);
            } ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>

    <div class="actions clearfix">
        <ul role="menu" aria-label="Pagination">
            <?= $this->render('_buttons', [
                'currentStepNumber' => $currentStepNumber,
                'lastStepNumber' => $lastStepNumber,
                'isMandatory' => false,
                'hasProducts' => true,
                'isReadonly' => $isReadonly,
            ]) ?>
        </ul>
    </div>
</div>

<?= $this->render('_decline-form', [
    'declineEventForm' => $declineEventForm,
    'guardianId' => $guardian->id,
    'studentId' => $studentModel->id,
    'eventId' => $eventModel->id,
]) ?>

<?php
$this->registerJs(<<<JS
    $("#add-to-cart-btn").click(function(){ $("#products-form").submit();return false; });
    $(".variation-selector").on("change", function() {
        var jThis = $(this);
        var varPrice = jThis.attr("data-var-name");
        var price = window[varPrice][jThis.val()];
        jThis.closest("tr").find(".price-holder").html(price);
    });
JS
);
?>
