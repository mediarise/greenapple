<?php

use frontend\widgets\IBox;
use yii\widgets\ActiveForm;

/* @var \yii\web\View $this */

$this->title = 'Add card';
?>

<script>
    "use strict";

    function doMonerisSubmit() {
        var monFrameRef = document.getElementById('monerisFrame').contentWindow;
        monFrameRef.postMessage('', 'https://esqa.moneris.com/HPPtoken/index.php');
    }

    var respMsg = function (e) {
        var respData = eval("(" + e.data + ")");
        if (typeof respData.errorMessage === "undefined") {
            $("#data-key").val(respData.dataKey);
            $("#token-form").submit();
        } else {
            document.getElementById("monerisResponse").innerHTML = e.origin + " SENT " + " - " +
                respData.responseCode + "-" + respData.dataKey + "-" + respData.errorMessage;
        }
    };

    window.onload = function () {
        if (window.addEventListener) {
            window.addEventListener("message", respMsg, false);
        }
        else {
            if (window.attachEvent) {
                window.attachEvent("onmessage", respMsg);
            }
        }
    }
</script>

<?php IBox::begin(['title' => $this->title]); ?>
<div class="row">
    <div class="add-card-page">
        <div class="moneris-frame" style="width: 300px; margin: 0 auto">
            <?= \yii\helpers\Html::tag('iframe', '', [
                'id' => 'monerisFrame',
                'src' => 'https://esqa.moneris.com/HPPtoken/index.php?' . http_build_query([
                        'id' => 'ht22KBX3CRSL6QN',
                        'css_body' => 'padding:25px;',
                        'css_textbox' => 'width:100%;border:1px solid #e5e6e7;height:25px;padding:3px 8px;',
                        'css_input_label' => 'font-size:13px;color:#676a6c;',
                        'display_labels' => 1,
                        'css_textbox_exp' => 'width:60px;',
                        'enable_exp' => 1,
                        'css_textbox_cvd' => 'width:60px;',
                        'enable_cvd' => 1,
                    ]),
                'style' => [
                    'width' => '100%',
                    'height' => '175px',
                ],
                'frameborder' => 0,
            ]) ?>
            <?php ActiveForm::begin(['id' => 'token-form', 'method' => 'post']) ?>
            <?= \yii\helpers\Html::hiddenInput('data_key', '', ['id' => 'data-key']) ?>
            <?php ActiveForm::end() ?>
            <button class="btn btn-primary col-xs-6 col-xs-offset-3" onClick="doMonerisSubmit()">Add card</button>
            <div id="monerisResponse"></div>
        </div>
    </div>
</div>
<?php IBox::end() ?>
