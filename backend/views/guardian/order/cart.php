<?php

use yii\widgets\ActiveForm;
use kartik\spinner\Spinner;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\moneris\HostedTokenizationForm;

/* @var \yii\web\View $this */
/* @var \yii\data\ArrayDataProvider $cartDataProvider */
/* @var float $amount */
/* @var int $itemsCount */
/* @var int $totalCount */
/* @var \common\models\guardian\SelectCardForm $selectCardForm */
/* @var \common\models\GuardianCard[] $cards */

$this->title = 'Cart';

$this->params['breadcrumbs'][] = ['label' => 'Cart'];

$updateQuantityUrl = Url::to(['update-quantity']);
$updateVariationUrl = Url::to(['update-variation']);

$this->registerJs(/** @lang JavaScript */
    <<<"JS"
$(document)
.on("pjax:send", function() {
  $("#cart-spinner").removeClass("invisible");
})
.on("pjax:complete", function() {
  $("#cart-spinner").addClass("invisible");
})
.on("change", ".cart-quantity", function() {
    var jThis = $(this);
    var url = "{$updateQuantityUrl}?" + $.param({
        cartId: jThis.attr("data-cart-id"),
        quantity: jThis.val()
    });
    $.pjax({url: url, container: "#cart-pjax", history: false, timeout: 5000, replace: true, scrollTo: false});
})
.on("change", ".variation-selector", function() {
    var jThis = $(this);
    var url = "{$updateVariationUrl}?" + $.param({
        cartId: jThis.attr("data-cart-id"),
        variation: jThis.val()
    });
    $.pjax({url: url, container: "#cart-pjax", history: false, timeout: 5000, replace: true, scrollTo: false});
});
JS

);

?>
<div class="pos-r">
    <div class="row">
        <div class="col-md-9">
            <?= $this->render('_product-list', [
                'cartDataProvider' => $cartDataProvider,
                'itemsCount' => $itemsCount,
                'totalCount' => $totalCount,
                'amount' => $amount,
            ]) ?>

        </div>

        <div class="col-md-3">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Cart Summary</h5>
                </div>
                <div class="ibox-content">
                    <span>Total</span>
                    <h2 id="cart-amount" class="font-bold">
                        $<?= $amount ?>
                    </h2>

                    <hr>
                    <span class="text-muted small">*For United States, France and Germany applicable sales tax will be applied</span>

                    <div id="cart-controls" style="display: none;">
                        <div id="cart-accept-scenario" style="display: none;">
                            <div class="m-t-sm">
                                <button id="cart-accept" class="btn btn-success btn-xl">
                                    <i class="fa fa-shopping-cart"></i> Complete
                                </button>
                            </div>
                        </div>

                        <div id="cart-checkout-scenario" style="display: none;">

                            <?= $this->render('select-card', [
                                'cards' => $cards,
                                'selectCardForm' => $selectCardForm
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Support</h5>
                </div>
                <div class="ibox-content text-center">
                    <h3><i class="fa fa-phone"></i> +43 100 783 001</h3>
                    <span class="small">Please contact with us if you have any questions. We are avalible 24h.</span>
                </div>
            </div>

        </div>
    </div>

    <div id="cart-spinner" class="spinner-overlap invisible">
        <div class="spinner-wrapper-fixed">
            <?= /** @noinspection PhpUnhandledExceptionInspection */
            Spinner::widget([
                'preset' => 'large',
                'align' => 'center',
                'color' => '#5E5FC6',
            ]); ?>
        </div>
    </div>
</div>


<?php
$acceptUrl = Url::to(['accept-cart']);
$this->registerJs(<<<JS
var acceptCartLadda = Ladda.create(document.getElementById("cart-accept"));
$("#cart-accept").click(function () {
    acceptCartLadda.start();
    // noinspection JSUnusedGlobalSymbols
greenApple.ajax.perform("$acceptUrl", {}, {
        resultError: function (data) {
            this.base("resultError", data);
            acceptCartLadda.stop();
        }
    });
});
JS
) ?>
