<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\widgets\Select2;
use yii\helpers\ArrayHelper;

/* @var \common\models\Cart[] $model */
/* @var string $key */

?>
<div class="ibox mb-6">
    <div class="ibox-title mb mt">
        <?= $key ?>
    </div>

    <?php foreach ($model as $item) :
        /* @var $item \common\models\Cart */
        $product = $item->product;
        $variations = $product->variations;
        if (empty($eventProduct)) {
            $eventProduct = new \common\models\EventProduct();
            $eventProduct->min_quantity = 1;
            $eventProduct->max_quantity = 1;
        }
        $student = $item->student;

        $imgUrl = $item->getPhoto() ?? Yii::$app->params['eventPhotoPlaceholder'];
        ?>

        <div class="ibox-content">


            <div class="col-lg-2 col-sm-4 col-xs-12 mb-2 px-0">
                <?= Html::img($imgUrl, ['width' => 80, 'height' => 80]) ?>
            </div>


            <div class="col-lg-6 col-sm-8 col-xs-8 mb-2 px-0 pr-2">
                <h3>
                    <a href="#" class="text-navy"><?= $product->title ?></a>
                </h3>
                <p class="small"><?= $product->description ?></p>
                <div class="m-t-sm">
                    <a href="<?= Url::to(['remove-cart-item', 'id' => $item->id]) ?>" class="text-muted">
                        <i class="fa fa-trash"></i> Remove item
                    </a>
                    <span class="ml-25"><?= Html::encode($student->getFullName()) ?> </span>
                </div>
            </div>


            <div class="col-lg-4 col-xs-12 px-0">

                <div class="col-xs-12 px-0 mb-2">
                    <?php $minQuantity = max($eventProduct->min_quantity, 1); ?>

                    <span>Total: <label>$<?= $item->total ?></label></span>

                    <?php if ($minQuantity !== $eventProduct->max_quantity) : ?>

                        <div class="pb-05 mt-2">Select quantity:</div>
                        <div class="input-group bootstrap-touchspin" style="max-width: 200px;">
                            <?php
                            $leftBtnStyle = 'btn input-group-addon on-left bootstrap-touchspin-down';
                            $rightBtnStyle = 'btn input-group-addon on-right bootstrap-touchspin-up';
                            if ($item->quantity <= $minQuantity) {
                                $leftBtnStyle .= ' disabled';
                            }
                            if ($eventProduct->max_quantity != 0 && $item->quantity >= $eventProduct->max_quantity) {
                                $rightBtnStyle .= ' disabled';
                            }
                            ?>
                            <span class="input-group-btn">
                                <?= Html::a('<i class="glyphicon glyphicon-minus"></i>', [
                                    'update-quantity',
                                    'cartId' => $item->id,
                                    'quantity' => $item->quantity - 1
                                ], ['class' => $leftBtnStyle]) ?>
                            </span>

                            <?= Html::textInput('quantity', $item->quantity, [
                                'data-cart-id' => $item->id,
                                'title' => 'quantity',
                                'class' => 'form-control input-sm cart-quantity text-center',
                                'disabled' => $minQuantity == $eventProduct->max_quantity,
                            ]) ?>

                            <span class="input-group-btn">
                                <?= Html::a('<i class="glyphicon glyphicon-plus"></i>', [
                                    'update-quantity',
                                    'cartId' => $item->id,
                                    'quantity' => $item->quantity + 1
                                ], ['class' => $rightBtnStyle]) ?>
                            </span>

                        </div>

                    <?php endif; ?>
                </div>

                <?php if (!empty($variations)) : ?>
                    <div class="col-xs-12 px-0 mb-2">
                        <div class="pb-05">Product variation:</div>
                        <div style="max-width: 280px;">
                            <?= /** @noinspection PhpUnhandledExceptionInspection */
                            Select2::widget([
                                'name' => 'test-' . $item->id,
                                'value' => $item->product_variation_id,
                                'data' => ArrayHelper::map($product->variations, 'id', 'name'),
                                'options' => [
                                    'class' => 'variation-selector',
                                    'data-cart-id' => $item->id,
                                ],
                                'hideSearch' => true,
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ]
                            ]) ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>

        </div>

    <?php endforeach; ?>

    <div class="clearfix"></div>
</div>