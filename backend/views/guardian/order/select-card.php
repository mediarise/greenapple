<?php

use yii\widgets\ActiveForm;
use frontend\widgets\moneris\HostedTokenizationForm;
use yii\helpers\Html;

/**
 * @var $selectCardForm
 * @var $cards
 */

?>

    <div class="mt-125">
        <span>Select payment method:</span>
        <?php $form = ActiveForm::begin(['id' => 'select-card-form', 'action' => 'checkout']); ?>
        <?php $cardsCount = count($cards); ?>

        <?= Html::activeRadioList($selectCardForm, 'tokenId', $cards, [
            'item' => function (/** @noinspection PhpUnusedParameterInspection */
                $index, $label, $name, $checked, $value) {
                return Html::tag('h3', Html::radio($name, $checked, [
                    'label' => "$label <i class=\"fa fa-credit-card\"></i>",
                    'value' => $value,
                ]));
            }
        ]) ?>
        <h3>
            <label>
                <input type="radio" name="SelectCardForm[tokenId]" value="new-card"
                       id="new-card-radio" <?= $cardsCount == 0 ? 'checked="checked"' : '' ?> >
                Add new card
            </label>
        </h3>
        <h3>
            <label>
                <input type="radio" name="SelectCardForm[tokenId]" value="1" disabled="disabled">
                INTERAC
            </label>
        </h3>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="m-t-sm">
        <button id="cart-checkout" class="btn btn-success btn-xl" data-style="zoom-out">
            <i class="fa fa-shopping-cart"></i> Checkout
        </button>
        <?= /** @noinspection PhpUnhandledExceptionInspection */
        HostedTokenizationForm::widget([
            'buttonOptions' => [
                'id' => 'cart-checkout',
                'label' => '<i class="fa fa-shopping-cart"></i> Checkout',
                'class' => 'btn btn-success btn-xl',
                'data-style' => 'zoom-out'
            ]
        ]) ?>
    </div>

<?php $this->registerJs(<<<JS
var checkoutLadda = Ladda.create(document.getElementById("cart-checkout"));
$("#cart-checkout").click(function () {
    if ($("#new-card-radio").is(":checked")) {
        $("#add-card-modal").modal("show");
        return;
    }
    checkoutLadda.start();
    // noinspection JSUnusedGlobalSymbols
greenApple.ajax.submitForm($("#select-card-form"), {
        complete: function () {
            this.base("complete");
            checkoutLadda.stop();
        }
    });
});
JS
) ?>