<?php

use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \yii\data\ArrayDataProvider $cartDataProvider */
/* @var int $itemsCount */
/* @var int $totalCount */
/* @var float $amount */

?>

<?php Pjax::begin(['id' => 'cart-pjax', 'enablePushState' => false, 'timeout' => 5000]); ?>
    <div class="ibox">
        <div class="ibox-title">
            <span class="pull-right">(<strong><?= $itemsCount ?></strong>) items</span>
            <h5>Items in your cart</h5>
        </div>
        <?= /** @noinspection PhpUnhandledExceptionInspection */
        ListView::widget([
            'dataProvider' => $cartDataProvider,
            'itemView' => '_cart-item',
            'summary' => '',
            'emptyText' => 'Cart is empty.',
        ]); ?>


        <div class="ibox-content">
            <?= Html::a('<i class="fa fa-arrow-left"></i> Back to dashboard', ['guardian/index'], ['class' => 'btn btn-primary btn-xl', 'data-pjax' => 0]) ?>
        </div>

    </div>
<?php
$this->registerJs(sprintf('$("#cart-icon .label").html("%s");', $totalCount));
$this->registerJs(sprintf('$("#cart-amount").html("$%s");', $amount));

if ($itemsCount === 0) {
    $this->registerJs('$("#cart-controls").hide()');
} else {
    $this->registerJs('$("#cart-controls").show()');
}

if ($amount == 0) {
    $this->registerJs('$("#cart-accept-scenario").show()');
    $this->registerJs('$("#cart-checkout-scenario").hide()');
} else {
    $this->registerJs('$("#cart-accept-scenario").hide()');
    $this->registerJs('$("#cart-checkout-scenario").show()');
}

//$this->registerJs('$("#product-list-checkout").click(function(){$("#").click("cart-checkout");});')
Pjax::end();
