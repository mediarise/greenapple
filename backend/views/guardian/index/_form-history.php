<?php

use common\components\bl\ledger\GuardianEventLedger;
use common\components\Formatter;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var \yii\data\ArrayDataProvider $dataProvider */

?>

<?php Pjax::begin(['id' => 'pjax-form-history', 'enablePushState' => false]) ?>

<?= /** @noinspection PhpUnhandledExceptionInspection */
GridView::widget([
    'dataProvider' => $dataProvider,
    'summary' => '',
    'tableOptions' => [
        'class' => 'table table-striped'
    ],
    'columns' => [
        [
            'attribute' => 'date',
            'value' => function($model){
                return Formatter::shortDate($model['date']);
            }
        ],
        [
            'attribute' => 'form',
        ],
        [
            'attribute' => 'event',
        ],
        [
            'attribute' => 'action',
            'value' => function($model){
                return GuardianEventLedger::getFormActionLabel($model['action']);
            }
        ],
    ],
]) ?>

<?php Pjax::end() ?>
