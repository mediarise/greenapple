<?php

use common\components\Formatter;
use common\widgets\Select2;
use frontend\widgets\EventCard;
use frontend\widgets\IBox;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\guardian\UpcomingEvent;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\bootstrap\Tabs;

$this->title = 'Dashboard';

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];

/** @var yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $upcomingDataProvider */
/** @var \yii\data\ArrayDataProvider $pendingDataProvider */
/** @var string $events */
/** @var \yii\data\ActiveDataProvider $productHistoryDataProvider */
/** @var \yii\data\ActiveDataProvider $formHistoryDataProvider */
/** @var array $students */
/** @var \common\models\guardian\SelectStudentForm $selectStudentForm */

?>

<?php $this->beginBlock('student-selector') ?>
<div class="navbar-student-list">
    <div style="margin: 10px;">
        <?php $form = ActiveForm::begin([
            'id' => 'select-student-form',
            'action' => Url::to(['/guardian/index/select-student']),
            'validateOnBlur' => false,
            'validateOnChange' => false]); ?>
        <?= Html::activeHiddenInput($selectStudentForm, 'returnUrl') ?>
        <?= $form->field($selectStudentForm, 'studentId')->label('')->widget(Select2::class, [
            'data' => $students,
            'hideSearch' => true,
            'theme' => Select2::THEME_KRAJEE,
            'pluginOptions' => [
                'allowClear' => false,
            ],
            'pluginEvents' => [
                'select2:select' => 'function() { $("#select-student-form").submit(); }',
            ]
        ]) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php $this->endBlock() ?>

<?php $this->beginBlock('header-student-selector') ?>

<div class="pl-15 pr-15">
    <?php $form = ActiveForm::begin([
        'id' => 'header-select-student-form',
        'action' => Url::to(['/guardian/index/select-student']),
        'validateOnBlur' => false,
        'validateOnChange' => false]); ?>
    <?= Html::activeHiddenInput($selectStudentForm, 'returnUrl') ?>
    <?= $form->field($selectStudentForm, 'studentId')->label('')->widget(Select2::class, [
        'data' => $students,
        'hideSearch' => true,
        'options' => [
            'id' => 'header-selectstudentform-studentid'
        ],
        'pluginOptions' => [
            'allowClear' => false,
        ],
        'pluginEvents' => [
            'select2:select' => 'function() { $("#header-select-student-form").submit(); }',
        ]
    ]) ?>
    <?php ActiveForm::end(); ?>
</div>

<?php $this->endBlock() ?>

<div class="dashboard-sample-page">


    <?php IBox::begin(['title' => 'Pending']) ?>

    <div class="row px-0">


        <?= /** @noinspection PhpUnhandledExceptionInspection */
        ListView::widget([
            'dataProvider' => $pendingDataProvider,
            'summary' => false,
            'emptyText' => '<div class="col-lg-12">Pending section is empty </div>',
            'itemView' => function (UpcomingEvent $model) {
                $acceptLink = Url::to($model->getRoute());
                ob_start();
                echo '<div class="col-lg-4 col-xs-12">';
                echo EventCard::widget([
                    'title' => $model->title,
                    'url' => Url::toRoute(['/guardian/index/event-view']),
                    'obligation' => $model->required ? EventCard::OBLIGATION_REQUIRED : EventCard::OBLIGATION_OPTIONAL,
                    'previewImageSrc' => $model->img_url,
                    'period' => Formatter::period($model->start_date, $model->end_date),
                    'acceptLink' => $acceptLink,
                    'studentFirstName' => $model->first_name,
                    'studentLastName' => $model->last_name,
                    'formsToSign' => $model->forms_to_sign,
                    'productsToPurchase' => $model->products_to_purchase,
                    'required' => $model->required,
                    'dueDate' => $model->due_date,
                    'date' => Formatter::period($model->start_date, $model->end_date),
                    'eventType' => $model->type,
                ]);
                echo '</div>';
                return ob_get_clean();
            }
        ]) ?>


    </div>

    <?php IBox::end(); ?>


    <?php IBox::begin(['title' => 'Upcoming']) ?>

    <div class="row px-0">
        <div class="mb-5 col-lg-12">

            <div class="tabs-container">

                <?= /** @noinspection PhpUnhandledExceptionInspection */
                Tabs::widget([
                    'items' => [
                        [
                            'label' => '<i class="fa fa-table"></i>Table',
                            'encode' => false,
                            'active' => true,
                            'content' => $this->render('_upcoming-grid', [
                                'dataProvider' => $upcomingDataProvider,
                            ]),
                            'headerOptions' => [
                                'id' => 'grid-tab'
                            ]
                        ],
                        [
                            'label' => '<i class="fa fa-calendar"></i>Calendar',
                            'encode' => false,
                            'content' => $this->render('_upcoming-calendar', [
                                'events' => $events
                            ]),
                            'headerOptions' => [
                                'id' => 'calendar-tab'
                            ]
                        ],
                    ]
                ]) ?>

            </div>

        </div>
    </div>

    <?php IBox::end(); ?>


    <?php IBox::begin(['title' => 'History',]) ?>

    <div class="row px-0">
        <div class="col-lg-12">

            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-credit-card"></i>Product</a>
                    </li>
                    <li class=""><a data-toggle="tab" href="#tab-2"><i class="fa fa-file-text-o"></i>Forms</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <?= $this->render('_product-history', [
                                'dataProvider' => $productHistoryDataProvider,
                            ]) ?>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            <?=
                            $this->render('_form-history', [
                                'dataProvider' => $formHistoryDataProvider,
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php IBox::end(); ?>

</div>