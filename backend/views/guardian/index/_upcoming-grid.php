<?php

use common\components\Formatter;
use common\helpers\ArrayHelper;
use common\models\DocEvent;
use common\models\guardian\UpcomingEvent;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\widgets\grid\LinkColumn;

/** @var \yii\data\ArrayDataProvider $dataProvider */

?>

<div class="panel-body event-upcoming-section">

    <?php Pjax::begin(['id' => 'pjax-upcoming-grid', 'enablePushState' => false]) ?>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'rowOptions' => function ($model) {
            if ($model->required) {
                $statusIsSuitable = ArrayHelper::isIn($model->status, [
                    UpcomingEvent::STATUS_PENDING_REQUIRED,
                    UpcomingEvent::STATUS_INCOMPLETE
                ]);
                if ($statusIsSuitable) {
                    return ['class' => 'required'];
                }
            }
            return [];
        },
        'columns' => [
            [
                'attribute' => 'start_date',
                'label' => 'Date',
                'value' => function (UpcomingEvent $model) {
                    return Formatter::period($model->start_date, $model->end_date);
                }
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function (UpcomingEvent $model) {
                    $text = Html::encode($model->title);
                    return Html::a($text, $model->getRoute(), ['data-pjax' => 0]);
                },
            ],
            [
                'attribute' => 'first_name',
                'label' => 'Student',
                'value' => function (UpcomingEvent $model) {
                    return "$model->first_name $model->last_name";
                }
            ],
            [
                'attribute' => 'forms_signed',
                'label' => 'Forms',
                'value' => function (UpcomingEvent $model) {
                    return Formatter::ratio($model->forms_signed, $model->forms_to_sign);
                }
            ],
            [
                'attribute' => 'products_purchased',
                'label' => 'Products',
                'value' => function (UpcomingEvent $model) {
                    return Formatter::ratio($model->products_purchased, $model->products_to_purchase);
                }
            ],
            [
                'attribute' => 'status',
                'value' => function (UpcomingEvent $model) {
                    return $model->getStatusLabel();
                }
            ],
        ],
    ]) ?>

    <?php Pjax::end() ?>

</div>
