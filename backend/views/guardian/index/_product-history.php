<?php

use common\components\bl\ledger\GuardianEventLedger;
use common\components\Formatter;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var \yii\data\ArrayDataProvider $dataProvider */

?>

<?php Pjax::begin(['id' => 'pjax-product-history', 'enablePushState' => false]) ?>

<?= /** @noinspection PhpUnhandledExceptionInspection */
GridView::widget([
    'dataProvider' => $dataProvider,
    'summary' => '',
    'options' => [
        'class' => 'table-responsive'
    ],
    'tableOptions' => [
        'class' => 'table table-striped'
    ],
    'columns' => [
        [
            'attribute' => 'date',
            'value' => function ($model) {
                return Formatter::shortDate($model['date']);
            }
        ],
        [
            'attribute' => 'product',
        ],
        [
            'attribute' => 'event',
        ],
        [
            'attribute' => 'quantity',
        ],
        [
            'attribute' => 'action',
            'value' => function ($model) {
                return GuardianEventLedger::getPurchaseActionLabel($model['action']);
            }
        ]
    ],
]) ?>

<?php Pjax::end() ?>
