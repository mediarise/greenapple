<?php

/**
 * @var $this \yii\web\View
 * @var string $events
 */

use backend\assets\CalendarAsset;

CalendarAsset::register($this);

?>

<div class="panel-body">
    <div id="upcoming-calendar-view">
        <div id="calendar"></div>
    </div>
</div>

<?php
$script = <<<JS
    
    var calendar = $('#calendar');
    calendar.fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        droppable: false,
        events: $events
    });
    
    $('body').on('click', '#calendar-tab a', function() {
        setTimeout(function() { 
            calendar.fullCalendar('render');
        }, 100);
    })
   
JS;
$this->registerJs($script, \yii\web\View::POS_END);
?>
