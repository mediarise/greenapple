<?php
/**
 * @var $this \yii\web\View
 * @var array $products
 * @var $studentModel \common\models\Student
 * @var $guardianModel \common\models\Guardian
 */

use common\models\EventProduct;
use common\models\guardian\StockProduct;
use frontend\widgets\GridView;
use kartik\touchspin\TouchSpin;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

/** @noinspection PhpUnhandledExceptionInspection */
echo GridView::widget([
    'options' => [
        'class' => 'table-responsive',
    ],
    'dataProvider' => (new ArrayDataProvider(['allModels' => $products])),
    'summary' => false,
    'columns' => [
        [
            'label' => 'Product',
            'content' => function (StockProduct $data) {
                return $data->title;
            }
        ],
        [
            'label' => 'Needed',
            'content' => function (StockProduct $data) {
                return $data->max_quantity;
            }
        ],
        [
            'label' => 'Contributed',
            'content' => function (StockProduct $data) {
                return $data->contributed;
            }
        ],
        [
            'label' => 'Will Donate',
            'headerOptions' => [
                'style' => 'min-width: 150px;',
                'width' => '200'
            ],
            'content' => function (StockProduct $data, $index) use ($guardianModel, $studentModel) {
                $name = sprintf('DonateOrderItem[%s]', $index);
                return
                    Html::hiddenInput($name . '[eventId]', $data->doc_event_id) .
                    Html::hiddenInput($name . '[guardianId]', $guardianModel->id) .
                    Html::hiddenInput($name . '[studentId]', $studentModel->id) .
                    Html::hiddenInput($name . '[productId]', $data->product_id) .
                    TouchSpin::widget([
                        'name' => $name . '[quantity]',
                        'value' => 0,
                        'options' => [
                            'class' => 'text-center'
                        ],
                        'pluginOptions' => [
                            'buttonup_class' => 'input-group-addon on-right',
                            'buttondown_class' => 'input-group-addon on-left',
                            'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                            'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                            'min' => 0,
                            'max' => $data->max_quantity - $data->contributed,
                        ],
                    ]);
            }
        ],

    ],
]);