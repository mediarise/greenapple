<?php
/**
 * @var $this View
 * @var $eventModel \common\models\guardian\DocEvent
 * @var $studentModel \common\models\Student
 * @var $guardianModel \common\models\Guardian
 * @var array $products
 * @var boolean $eventIsClosed
 */


use frontend\widgets\IBox;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Html::encode($eventModel->title);

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => 'Stock The Classroom Event'];

$imgUrl = empty($eventModel->mainPhoto) ? Yii::$app->params['eventPhotoPlaceholder'] : $eventModel->mainPhoto->url;

?>


    <div class="ibox">
        <div class="ibox-content">

            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <tbody>
                    <tr>
                        <td width="90">
                            <?= Html::img($imgUrl, ['width' => '130px']) ?>
                        </td>
                        <td class="desc">
                            <h3>
                                <p class="text-navy">
                                    <?= $eventModel->title ?>
                                </p>
                            </h3>

                            <p class="small">
                                <?= $eventModel->start_date ?>
                            </p>

                            <p class="small">
                                <?php if ($eventModel->is_repeatable) : ?>
                                    Repeats Every Weekday
                                <?php else: ?>
                                    One time event
                                <?php endif; ?>
                            </p>

                            <div class="m-t-sm">
                                <?= $studentModel->first_name ?> <?= $studentModel->last_name ?>
                            </div>
                        </td>

                        <td>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

<?php IBox::begin([
    'title' => 'Products'
]) ?>

<?php ActiveForm::begin(['action' => ['guardian/stock-event/donate', 'eventId' => $eventModel->id, 'studentId' => $studentModel->id]]) ?>

<?= $this->render('_product-grid', [
    'products' => $products,
    'guardianModel' => $guardianModel,
    'studentModel' => $studentModel,
]) ?>

    <p>NOTE: Please purchase and send donated items with your child to school.</p>

    <div class="pb-125">
        <?php if ($eventIsClosed): ?>
            <?= Html::a('Back', ['guardian/index'], [
                'class' => [
                    'btn',
                    'btn-xl',
                    'btn-success'
                ]
            ]); ?>
        <?php else: ?>
            <?= Html::submitButton('Donate', [
                'class' => [
                    'btn',
                    'btn-xl',
                    'btn-success'
                ]
            ]); ?>
        <?php endif; ?>
    </div>

<?php ActiveForm::end() ?>

<?php IBox::end(); ?>

<?php
$this->registerJs('$("body").on("click", ".disabled", function(){ return false});', View::POS_END)
?>