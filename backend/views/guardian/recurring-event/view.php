<?php

use backend\assets\VuetifulAsset;
use common\models\DocEvent;
use frontend\widgets\IBox;
use kartik\select2\Select2Asset;
use kartik\select2\ThemeDefaultAsset;
use kartik\touchspin\TouchSpinAsset;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;

/**
 * @var View $this
 * @var \common\models\Product $product
 * @var DocEvent $eventModel
 * @var \common\models\Student $studentModel
 * @var array $eventProducts
 * @var array $variations
 *
 */

$this->title = Html::encode($eventModel->title);

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => 'Recurring Event'];

Select2Asset::register($this);
ThemeDefaultAsset::register($this);
TouchSpinAsset::register($this);

VuetifulAsset::register($this);
$this->registerJsFile('js/vue/jquery-wrappers.js', ['depends' => [VuetifulAsset::class]]);

$buyUrl = \yii\helpers\Url::to(['add-to-cart', 'studentId' => $studentModel->id]);
$declineUrl = \yii\helpers\Url::to(['decline', 'eventId' => $eventModel->id, 'studentId' => $studentModel->id]);

$this->registerJs(<<<JS
    var laddas = {};
    new Vue({

    el: "#app",

    data: function() {
        return {
            products: products,
            selected: [],
            variations: variations,
            taxRatio: taxRatio
        };
    },
    methods: {
        updateTotal(model){
            model.total = model.price * model.quantity * this.taxRatio;
        },
        updatePrice(model, price){
            model.price = price;
            this.updateTotal(model);
        },
        formatCurrency(value){
            return "$" + value.toFixed(2)
        },
        formatVariation(value){
            return this.variations.find(function(variation) {
                return variation.id == value;
            }).text;
        },
        startLadda(btn){
            var ladda = null;
            if (btn.id in laddas) {
                ladda = laddas[btn.id];
            } else {
                ladda = Ladda.create(btn);
                laddas[btn.id] = ladda;
            }
            ladda.start();
            return ladda;
        },
        buy(event){
            var l = this.startLadda(event.target);
            greenApple.ajax.perform("$buyUrl", { products: this.selected }, {
                complete: function() {
                    l.stop();
                }
            });
        },
        decline(event){
            var l = this.startLadda(event.target);
            var vm = this;
            greenApple.ajax.perform("$declineUrl", { products: this.selected }, {
                complete: function() {
                    l.stop();
                },
                resultSuccess: function(data) {
                    this.base('resultSuccess', data);
                    vm.products = data.items;
                    vm.selected = [];
                }
            });
        },
        isUntouchable(model){
            return model.isBought || model.minQuantity == model.maxQuantity; 
        }
    },
    computed: {
        selectAll: {
            get: function () {
                return this.selected.length == this.notBought.length;
            },
            set: function (value) {
                this.selected = value ? this.notBought : [];
            }
        },
        notBought: function() {
            return this.products.filter(function(model) {
                return !model.isBought;
            });
        }
    }
});
JS
    , View::POS_END);

$this->registerJsVar('products', $eventProducts);
$this->registerJsVar('taxRatio', $product->getTaxRatio());
$this->registerJsVar('variations', $variations);
?>

<?= $this->render('_header', [
    'eventModel' => $eventModel,
    'studentModel' => $studentModel,
]) ?>

<?php IBox::begin([
    'title' => 'Products'
]) ?>

<div id="app">
    <div class="pb-125">
        <?= Html::button('Buy selected', [
            'class' => 'ladda-button btn btn-xl btn-success mt-025 mb-025 mr',
            'data-style' => 'zoom-out',
            '@click' => 'buy',
        ]); ?>
        <?= Html::button('Decline selected', [
            'class' => 'ladda-button btn btn-xl btn-primary mt-025 mb-025',
            'data-style' => 'zoom-out',
            '@click' => 'decline',
        ]); ?>
    </div>

    <div class="vue-data-grid">
        <datatable :source="products" :editable="false" :filterable="false" class="table-responsive">
            <datatable-column id="sel" width="50px" :sortable="false">
                <checkbox id="select-all-checkbox" v-model="selectAll"></checkbox>
            </datatable-column>
            <datatable-column id="date" width="150px"></datatable-column>
            <datatable-column id="product" width="150px"></datatable-column>
            <?php if (!empty($variations)) : ?>
                <datatable-column id="variation" label="options" width="200px"
                                  :formatter="formatVariation"></datatable-column>
            <?php endif; ?>
            <datatable-column id="quantity" width="164px"></datatable-column>
            <datatable-column id="price" width="80px" :formatter="formatCurrency"></datatable-column>
            <datatable-column id="total" width="80px" :formatter="formatCurrency"></datatable-column>
            <datatable-column id="status" width="90px"></datatable-column>

            <template slot="sel" slot-scope="cell">
                <input v-if="cell.row.isBought" type="checkbox" checked="checked" disabled="disabled">
                <checkbox
                        v-else
                        :id="cell.row.name"
                        :value="cell.row"
                        v-model="selected">
                </checkbox>
            </template>
            <template slot="variation" slot-scope="cell">
                <div>
                    <span v-if="cell.row.isBought">{{ formatVariation(cell.row.variation) }}</span>
                    <select2
                            v-else
                            v-model="cell.row.variation"
                            :options="variations"
                            @select2-selected="updatePrice(cell.row, $event.price)">
                    </select2>
                </div>
            </template>
            <template slot="quantity" slot-scope="cell">
                <div>
                    <div class="untouchable" v-if="isUntouchable(cell.row)">
                        <input type="text" class="form-control text-center" v-model="cell.row.quantity"
                               disabled="disabled">
                    </div>
                    <touch-spin
                            v-else
                            v-model="cell.row.quantity"
                            :min="cell.row.minQuantity"
                            :step="cell.row.minQuantity"
                            :max="cell.row.maxQuantity"
                            @input="updateTotal(cell.row)">
                    </touch-spin>
                </div>
            </template>
        </datatable>
    </div>
</div>

<?php IBox::end(); ?>
