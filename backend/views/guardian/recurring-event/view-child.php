<?php

use common\models\DocEvent;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var DocEvent $eventModel
 * @var \common\models\Student $studentModel
 * @var array $product
 */


$this->title = Html::encode($eventModel->title);

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => 'Nutrition Event'];

?>

<?= $this->render('_header', [
    'eventModel' => $eventModel,
    'studentModel' => $studentModel,
]) ?>

<div class="ibox-content">
    <div class="table-responsive">
        <table class="table">
            <tbody>
            <tr>
                <td width="90">
                    <?= Html::img($product['url'] ?? Yii::$app->params['eventPhotoPlaceholder'], ['width' => 80, 'height' => 80]) ?>
                </td>
                <td class="desc">
                    <h3>
                        <a href="#" class="text-navy">
                            <?= $product['title'] ?>
                        </a>
                    </h3>
                    <p class="small">
                        <?= $product['description'] ?>
                    </p>

                </td>
                <td style="min-width:200px;">
                    <?php if ($product['variation_name'] !== null) : ?>
                        <?= $product['variation_name'] ?>
                    <?php endif; ?>
                </td>
                <td>
                    $<span class="price-holder"><?= Html::encode($product['price']); ?></span>
                </td>

                <td width="160" style="min-width: 160px;">
                    <?= $product['quantity'] ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="pb-125">
        <?= Html::a('Back', ['guardian/index'], [
            'class' => [
                'btn',
                'btn-xl',
                'btn-success'
            ]
        ]); ?>
    </div>

</div>
