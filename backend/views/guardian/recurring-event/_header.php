<?php

use common\components\Formatter;
use common\models\DocEvent;
use common\models\Student;
use yii\helpers\Html;
use yii\web\View;
/**
 * @var $this View
 * @var $eventModel DocEvent
 * @var $studentModel Student
 */

$imgUrl = empty($eventModel->mainPhoto) ? Yii::$app->params['eventPhotoPlaceholder'] : $eventModel->mainPhoto->url;

?>

<div class="ibox">
    <div class="ibox-content">

        <div class="table-responsive">
            <table class="table shoping-cart-table">
                <tbody>
                <tr>
                    <td width="130">
                        <div class="m-t-sm">
                            <?= Html::img($imgUrl, ['width' => '130px']) ?>
                        </div>
                    </td>
                    <td class="desc">
                        <div class="ml-125">
                            <h3>
                                <p class="text-navy">
                                    <?= $eventModel->description ?>
                                </p>
                            </h3>

                            <p class="small">
                                <?= Formatter::period($eventModel->start_date, $eventModel->end_date) ?>
                            </p>

                            <p class="small">
                                <?php switch ($eventModel->frequency) {
                                    case DocEvent::FREQUENCY_DAILY:
                                        echo 'Repeats Every Day';
                                        break;
                                    case DocEvent::FREQUENCY_WEEKLY:
                                        echo 'Repeats Every Week';
                                        break;
                                    case DocEvent::FREQUENCY_MONTHLY:
                                        echo 'Repeats Every Month';
                                        break;
                                } ?>
                            </p>

                            <div class="m-t-sm">
                                <?= $studentModel->first_name ?> <?= $studentModel->last_name ?>
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
