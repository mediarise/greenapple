<?php

use yii\widgets\ActiveForm;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $variation \common\models\ProductVariation */
/* @var $form ActiveForm */

$maskedInputOptions = [
    'prefix' => '$ ',
    'allowMinus' => false,
    'alias' => 'decimal',
    'digits' => 2,
    'groupSeparator' => ',',
    'autoGroup' => true,
    'autoUnmask' => true
];
?>


<tr>
    <td>
        <?= $form->field($variation, 'name', $fieldConfig)->textInput(['placeholder' => 'Enter name'])->label(false) ?>
    </td>
    <td class="not-free-product-details">
        <?= $form->field($variation, 'price', $fieldConfig)->widget(NumberControl::class, [
            'maskedInputOptions' => $maskedInputOptions,
            'displayOptions' => [
                'placeholder' => 'enter price',
                'class' => 'text-left form-control'
            ]
        ])->label(false); ?>
    </td>
    <td class="not-free-product-details">
        <?= $form->field($variation, 'cost', $fieldConfig)->widget(NumberControl::class, [
            'maskedInputOptions' => $maskedInputOptions,
            'displayOptions' => [
                'placeholder' => 'enter cost',
                'class' => 'text-left form-control'
            ]
        ])->label(false); ?>
    </td>
    <td>
        <button type="button" class="btn btn-xl btn-primary btn-remove"><i class="fa fa-trash"></i></button>
    </td>
</tr>
