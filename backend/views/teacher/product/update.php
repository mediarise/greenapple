<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $product common\models\Product
 * @var $returnUrl
 * @var $suppliers
 * @var $variations
 * @var $variationIndex
 * @var $uploadPhotoUrl
 */

$this->title = 'Update Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="product-update col-md-12 mb-4 px-0">

    <?= $this->render('_product-tabs', [
        'product' => $product,
        'returnUrl' => $returnUrl,
        'suppliers' => $suppliers,
        'title' => Html::encode($product->title),
        'variations' => $variations,
        'variationIndex' => $variationIndex,
        'uploadPhotoUrl' => $uploadPhotoUrl
    ]) ?>

</div>
