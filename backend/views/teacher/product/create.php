<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $product common\models\Product */
/* @var $returnUrl */
/* @var $suppliers */
/* @var $uploadPhotoUrl */
/* @var $variations */
/* @var $variationIndex */

$this->title = 'Create Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-create col-md-12 mb-4 px-0">

    <?= $this->render('_product-tabs', [
        'product' => $product,
        'returnUrl' => $returnUrl,
        'suppliers' => $suppliers,
        'variationIndex' => $variationIndex,
        'variations' => $variations,
        'title' => Html::encode($this->title),
        'uploadPhotoUrl' => $uploadPhotoUrl
    ]) ?>

</div>
