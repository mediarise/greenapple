<?php

/* @var $this yii\web\View */

use frontend\widgets\IBox;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/**
 * @var $product common\models\Product
 * @var $returnUrl
 * @var $suppliers
 * @var $title
 * @var $variations
 * @var $variationIndex
 * @var $uploadPhotoUrl
 */

$formOptions = [];

if ($product->is_free) {
    Html::addCssClass($formOptions, 'product-is-free');
}

?>

<?php IBox::begin(['title' => $title]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'product-form',
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'options' => $formOptions,
    'fieldConfig' => [
        'template' => "
          <div class=\"form-group\">
          {label}\n<div class=\"col-lg-9\">{input}\n{hint}\n{error}</div>
          </div>
        ",
        'labelOptions' => [
            'class' => 'col-lg-offset-1 col-lg-2 control-label'
        ],
    ],
]); ?>

<?php
$tabItems = [
    [
        'label' => 'Product info',
        'content' => $this->render('_form', [
            'product' => $product,
            'returnUrl' => $returnUrl,
            'suppliers' => $suppliers,
            'form' => $form,
            'uploadPhotoUrl' => $uploadPhotoUrl
        ])
    ],
    [
        'label' => 'Variations',
        'content' => $this->render('_variations', [
            'variations' => $variations,
            'returnUrl' => $returnUrl,
            'form' => $form,
            'variationIndex' => $variationIndex,
            'isProductFree' => (boolean)$product->is_free
        ])
    ]
];
?>

<div class="tabs-container">
    <?= /** @noinspection PhpUnhandledExceptionInspection */
    Tabs::widget([
        'id' => 'form-tabs',
        'items' => $tabItems
    ]) ?>
</div>

<?php ActiveForm::end(); ?>

<?php IBox::end(); ?>

