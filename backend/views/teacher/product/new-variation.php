<?php

use yii\widgets\ActiveForm;

?>

<table id="table" class="table table-responsive">
    <?php $form = ActiveForm::begin(); ?>

    <?= $this->render('_variation-form', [
        'form' => $form,
        'fieldConfig' => [],
        'variation' => $newVariation,
        'isProductFree' => $isProductFree
    ]) ?>

    <?php ActiveForm::end(); ?>
</table>