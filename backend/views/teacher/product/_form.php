<?php

use common\components\bl\enum\ProductTax;
use common\models\Product;
use frontend\widgets\Cropper;
use frontend\widgets\SwitchInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\assets\ProductAsset;
use common\widgets\Select2;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $product common\models\Product */
/* @var $form ActiveForm */
/* @var $returnUrl */
/* @var $suppliers */
/* @var $uploadPhotoUrl */

ProductAsset::register($this);
$userRole = Yii::$app->user->getHighestRole();

$isUpdateScenario = $product->getScenario() === Product::SCENARIO_UPDATE;
?>

<div class="panel-body product-form pt-3 form-horizontal">

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    Cropper::widget([
        'placeholderSrc' => Yii::$app->params['eventPhotoPlaceholder'],
        'imageSrc' => $product->mainPhotoUrl,
        'uploadUrl' => $uploadPhotoUrl,
        'modalTitle' => 'Crop product photo',
    ]) ?>

    <div class="col-md-12 col-lg-6 col-xl-8">

        <?php
        $moneyInputOptions = [
            'maskedInputOptions' => [
                'prefix' => '$ ',
                'allowMinus' => false,
                'alias' => 'decimal',
                'digits' => 2,
                'groupSeparator' => ',',
                'autoGroup' => true,
                'autoUnmask' => true
            ],
            'displayOptions' => [
                'class' => 'text-left form-control'
            ]
        ];
        ?>

        <?= $form->field($product, 'sku')->textInput() ?>
        <?= $form->field($product, 'title')->textInput() ?>
        <?= $form->field($product, 'description')->textarea() ?>
        <?= $form->field($product, 'supplier_id')->widget(Select2::class, [
            'data' => $suppliers,
            'options' => [
                'id' => 'supplier-select',
                'placeholder' => 'Select supplier'
            ],
        ]); ?>

        <?= $form->field($product, 'is_free')->widget(SwitchInput::class, [
            'id' => 'product-is_free',
            'class' => 'mt',
            'disabled' => $isUpdateScenario,
            'options' => [
                'label' => false,
            ],
            'wrapperOptions' => [
                'tag' => 'span',
                'data-toggle' => 'popover',
                'data-trigger' => 'hover',
                'data-content' => 'Please note: you cannot change this choice after saving',
                'data-placement' => 'right',
            ]

        ], false)->label('Free'); ?>

        <div class="not-free-product-details">
            <?= $form->field($product, 'price')->widget(NumberControl::class, $moneyInputOptions); ?>
            <?= $form->field($product, 'cost')->widget(NumberControl::class, $moneyInputOptions); ?>
            <?= $form->field($product, 'tax')->widget(Select2::class, [
                'data' => ProductTax::titles(),
                'options' => [
                    'id' => 'tax-variant-select',
                    'placeholder' => 'Select tax'
                ],
            ]); ?>
        </div>

        <?= Html::hiddenInput('Product[mainPhotoUrl]', $product->mainPhotoUrl, ['id' => 'main-photo-url-input']) ?>

    </div>

    <?php
    $taxVariant = $product->tax ?: ProductTax::HST_13;
    $this->registerJs("$('#tax-variant-select').val('$taxVariant').trigger('change');", yii\web\View::POS_END);
    ?>

    <?= Html::hiddenInput('returnUrl', $returnUrl) ?>


    <div class="col-md-12">
        <div class="form-group pull-right">
            <?= Html::submitButton('Save', [
                'class' => 'btn btn-xl btn-success mr-075',
                'id' => 'save-product-btn',
                'data-style' => "zoom-out"
            ]) ?>
            <?= Html::a('Cancel', $returnUrl, ['class' => 'btn btn-xl btn-primary']) ?>
        </div>
    </div>

</div><!-- _product-form -->


<?php

$formId = $form->id;
$this->registerJsVar('isNewModel', $product->isNewRecord);

$script = <<<JS

    $('[data-toggle="popover"]').popover();
    
    $('body').on('change', '#product-is_free', function() {
        $('#product-form').toggleClass('product-is-free');
    });
    
    if (isNewModel) {
        var saveBtn = $('#save-product-btn').ladda();
        
        $('#{$formId}')
            .on("beforeValidate", function() {
                saveBtn.ladda('start').attr('disabled', true);
            })
            .on("afterValidate", function(event, message, errorAttributes) {
                if (errorAttributes.length > 0 ) {
                    saveBtn.ladda('stop');
                }
            });
    }

JS;
$this->registerJs($script);
?>
