<?php

use common\helpers\BackendUrl;
use yii\helpers\Html;

/**
 * @var $variations \common\models\ProductVariation[]
 * @var $newVariation \common\models\ProductVariation
 * @var $this yii\web\View
 * @var $variationIndex
 * @var $isProductFree
 *
 */

$maskedInputOptions = [
    'prefix' => '$ ',
    'allowMinus' => false,
    'alias' => 'decimal',
    'digits' => 2,
    'groupSeparator' => ',',
    'autoGroup' => true,
    'autoUnmask' => true
];

Yii::$container->setDefinitions([
    \kartik\number\NumberControl::class => [
        'maskedInputOptions' => $maskedInputOptions,
        'displayOptions' => [
            'class' => 'text-left form-control'
        ]
    ]
]);

$fieldConfig = [
    'template' => '{label}{input}{hint}{error}',
    'labelOptions' => [
        'class' => 'control-label'
    ],
];

?>

<div class="panel-body product-form pt-3">

    <div class="row">
        <div class="col-md-12 mb-15 ml">
            <button type="button" class="btn btn-xl ladda-button btn-primary mb-2" data-style="zoom-out" id="btn-add-variation">
                <i class="fa fa-plus"></i> Add new
            </button>
        </div>
    </div>

    <?= Html::hiddenInput('variationIndex', $variationIndex, ['id' => 'variation-index']) ?>

    <?php $this->registerJsVar('addVariationUrl', BackendUrl::to(['add-variation'])); ?>
    <?php $this->registerJsVar('numericOptions', $maskedInputOptions) ?>
    <?php $this->registerJsVar('isProductFree', $isProductFree) ?>
    <?php $this->registerJsVar('variationsCount', count($variations)) ?>

    <?php $this->registerJs(/** @lang JavaScript */
<<<JS

    toggleTabularFormHeader();
    
    var addNewBtn = $('#btn-add-variation').ladda();

    $("#btn-add-variation").click(function() {
        var jInput = $("#variation-index");
        var index = jInput.val();
        jInput.val(index - 1);
        
        addNewBtn.ladda('start');
    
        $.ajax({
            url: addVariationUrl,
            method: "POST",
            data: {
                index: index,
                cost: $("#product-cost").val(),
                price: $("#product-price").val(),
                isProductFree: isProductFree
            },
            success: function(data) {
                variationsCount++;
                toggleTabularFormHeader(); 
                $(data).find("tbody").children().appendTo("#table-variations tbody");
                
                $("#productvariation-" + index + "-price").numberControl({
                    "displayId":"productvariation-" + index + "-price-disp",
                    "maskedInputOptions":numericOptions
                });
                $("#productvariation-" + index + "-cost").numberControl({
                    "displayId":"productvariation-" + index + "-cost-disp",
                    "maskedInputOptions":numericOptions
                });
            },
            complete: function() {
                addNewBtn.ladda('stop');
            }
        });
    });
    
    moveToError();
    
    $("#product-form").on("afterValidate", function() {
        moveToError();
    });
    
    $("#table-variations").on("click", ".btn-remove", function() {
        variationsCount--;
        toggleTabularFormHeader();
        $(this).parents("tr").remove();
    });
    
    function moveToError() {
        var tab = $(".has-error").first().closest(".tab-pane").attr("id");
        $("#form-tabs [href=\"#"+ tab +"\"]").tab("show");
    }
    
    function toggleTabularFormHeader() {
        if (variationsCount == 0) {
            $('.no-product-variations').css({'display': 'table-row'});
            $('.tabular-form-heading').hide();
        } else {
            $('.no-product-variations').hide();
            $('.tabular-form-heading').css({'display': 'table-row'});
        }
    }

JS
    ); ?>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-responsive" id="table-variations">
                <tbody class="pt-15">
                <tr class="tabular-form-heading">
                    <th>Name</th>
                    <th class="not-free-product-details">Price</th>
                    <th class="not-free-product-details">Cost</th>
                    <th></th>
                </tr>
                <tr class="no-product-variations">
                    <th>No product variations</th>
                </tr>
                <?php foreach ($variations as $variation): ?>
                    <?= $this->render('_variation-form', [
                        'form' => $form,
                        'variation' => $variation,
                        'fieldConfig' => $fieldConfig,
                        'isProductFree' => $isProductFree
                    ]) ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group ml">
                <?= Html::submitButton('Save', ['class' => 'btn btn-xl btn-success mr-075']) ?>
                <?= Html::a('Cancel', $returnUrl, ['class' => 'btn btn-xl btn-primary']) ?>
            </div>
        </div>
    </div>

</div>
