<?php

use common\models\Product;
use yii\helpers\Html;
use frontend\widgets\GridView;
use yii\grid\ActionColumn;
use frontend\widgets\IBox;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SearchProduct */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-index col-md-12 px-0">

    <?php IBox::begin(['title' => 'Products']); ?>

    <p class="mb-2"><?= Html::a('<i class="fa fa-shopping-cart"></i> Create Product', ['create'], ['class' => 'btn btn-success btn-xl']) ?></p>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'enablePjax' => true,
        'options' => [
            'class' => 'data-grid table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            [
                'attribute' => 'sku',
                'value' => function($product) {
                    return $product->sku ?: '-';
                },
                'contentOptions' => ['style' => 'max-width: 110px'],
            ],
            [
                'attribute' => 'title',
                'label' => 'Title',
                'format' => 'raw',
                'value' => function($product) {
                    $userRole = $this->params['userRole'];
                    $statisticUrl = ["{$userRole}/index/index", 'productId' => $product->id];

                    return Html::a(Html::encode($product->title), $statisticUrl, ['data-pjax' => '0']);
                },
                'contentOptions' => ['style' => 'min-width: 210px; max-width: 210px;'],
            ],
            [
                'attribute' => 'description',
                'value' => function($product) {
                    return $product->description ?: '-';
                },
                'contentOptions' => ['style' => 'min-width: 190px; max-width: 190px'],
            ],
            [
                'attribute' => 'price',
                'label' => 'Price',
                'value' => function($product) {
                    if ($product->is_free) {
                        return 'Free';
                    }
                    return $product->price ? '$ ' . $product->price : '-';
                },
                'contentOptions' => ['style' => 'min-width: 100px'],
            ],
            [
                'attribute' => 'cost',
                'label' => 'Cost',
                'value' => function($product) {
                    if ($product->is_free) {
                        return 'Free';
                    }
                    return $product->cost ? '$ ' . $product->cost : '-';
                },
                'contentOptions' => ['style' => 'min-width: 100px'],
            ],
            [
                'attribute' => 'tax',
                'label' => 'Tax',
                'format' => 'html',
                'value' => function($product) {
                    $taxVariants = Product::getTaxVariants();
                    return $taxVariants[$product->tax] ?? '-';
                },
                'contentOptions' => ['style' => 'min-width: 120px'],
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update}{delete}',
                'header' => 'Actions',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm mr-05',
                            'title' => 'Edit Product',
                            'data-pjax' => 0
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        if (count($model->lnkEventProducts)) {
                            return Html::a('<i class="fa fa-trash"></i>', $url, [
                                'class' => 'btn btn-primary btn-sm',
                                'title' => 'Delete Product',
                                'data-pjax' => false,
                                'onclick' => new \yii\web\JsExpression("
                                    event.preventDefault();
                                    toastr.warning('This product is related to existing event. Please delete event firstly.');
                                ")
                            ]);
                        }
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Delete Product',
                            'data-confirm' => 'Are you sure you want to delete this product?',
                        ]);
                    },
                ],
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center', 'style' => 'min-width: 120px'],
            ],
        ],
        'filters' => [
            [
                'type' => GridView::FILTER_TYPE_TEXT_INPUT,
                'name' => 'search',
                'label' => 'Search',
                'template' => '<div class="row"> <div class="col-lg-6 mb-15">{input}</div></div>',
                'options' => [
                    'id' => 'search-filter',
//                    'class' => 'mb',
                    'placeholder' => 'Search'
                ],
            ],
        ],
    ]) ?>

    <?php IBox::end(); ?>
</div>
