<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\DocEvent
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $filterModel \common\models\FormSearch
 * @var $visibilitySection \common\models\EventVisibilitySection
 * @var $studentsFilterModel \common\models\teacher\event\StudentSearch
 * @var $studentsDataProvider \yii\data\ActiveDataProvider
 *
 * @var $gradesFilterModel \common\models\teacher\event\GradeSearch
 * @var $gradesDataProvider \yii\data\ActiveDataProvider
 *
 * @var $visibilityGroupsFilterModel \common\models\teacher\event\VisibilityGroupSearch
 * @var $visibilityGroupsDataProvider \yii\data\ActiveDataProvider
 */

use common\helpers\BackendUrl;
use common\models\DocEvent;
use yii\helpers\Html;
use common\widgets\Select2;
use common\models\EventVisibilitySection;
use yii\helpers\Json;
use yii\helpers\Url;

?>

<div id="visibility-section" class="panel-body">

    <h3>Choose participants</h3>

    <div class="col-lg-12 px-0 mb-15">
        <?= Html::a(
            '<i class="fa fa-plus"></i> Create new visibility group',
            BackendUrl::toRoute([$this->params['userRole'] . '/visibility-group/create']), [
                'id' => 'visibility-group-create-btn',
                'class' => 'btn btn-success btn-xl ladda-button',
                'data' => [
                    'style' => 'zoom-out',
                    'pjax' => 0,
                    'params' => [
                        'returnUrl' => Url::current(),
                    ],
                ]
            ]) ?>
    </div>

    <?= Html::beginForm('', 'post', ['id' => 'visibility-section-form']) ?>

    <div class="mb-3">
        <?= Html::label('Participant type') ?>
        <?= /** @noinspection PhpUnhandledExceptionInspection */
        Select2::widget(['model' => $visibilitySection,
            'attribute' => 'filter_by',
            'hideSearch' => true,
            'data' => $visibilitySection::getFilterVariants(),
            'options' => ['id' => 'visibility-sections-filter',
                'placeholder' => 'Filter By'],
            'pluginOptions' => ['allowClear' => false,]]); ?>
    </div>

    <?= Html::endForm(); ?>

    <?= $studentsTab = $this->render('visibilitySection/_students', ['visibilitySection' => $visibilitySection,
        'filterModel' => $studentsFilterModel,
        'dataProvider' => $studentsDataProvider]); ?>

    <?= $this->render('visibilitySection/_grades', ['visibilitySection' => $visibilitySection,
        'filterModel' => $gradesFilterModel,
        'dataProvider' => $gradesDataProvider]); ?>

    <?= $this->render('visibilitySection/_visibilityGroups', ['visibilitySection' => $visibilitySection,
        'visibilityGroupsFilterModel' => $visibilityGroupsFilterModel,
        'visibilityGroupsDataProvider' => $visibilityGroupsDataProvider]); ?>


    <?php if ($model->status !== DocEvent::STATUS_PUBLISHED) : ?>
        <?= Html::button('Save', ['class' => 'event-save-btn ladda-button btn btn-xl btn-success mr-125',
            'data-style' => 'zoom-out']) ?>
    <?php endif; ?>

    <?php if ($model->status === DocEvent::STATUS_SAVED) : ?>
        <?= Html::a('Publish', Url::toRoute(['publish', 'id' => $model->id]), ['class' => 'event-publish-btn ladda-button btn btn-xl btn-success mr-125',
            'data-style' => 'zoom-out']); ?>
    <?php endif; ?>

    <a href="<?= Url::toRoute(['index']) ?>" class="btn btn-xl btn-primary">Cancel</a>

</div>


<?php
$filterByGrades = EventVisibilitySection::FILTER_BY_GRADES;
$filterByGroups = EventVisibilitySection::FILTER_BY_VISIBILITY_GROUPS;

$js = <<< JS
    
        $(body).on('change', '#visibility-sections-filter', function() {
            $('#visibility-section .event-form-tab').hide();
            var activeTab = $(getActiveTab());
            saveFormState(activeTab.find('.data-grid'));
            activeTab.show();
            window.activeTab = "#" + activeTab.attr("id");
        });
        
        function getActiveTab() {
            var filterBy = $('#visibility-sections-filter').val();
            switch (filterBy) {
                case '$filterByGrades': return $('#grades-tab');
                case '$filterByGroups': return $('#groups-tab');
                default: return $('#students-tab');
            }
        }
        
JS;

$this->registerJs($js, \yii\web\View::POS_READY);
?>

<?php
$createUrl = BackendUrl::toRoute([$this->params['userRole'] . '/visibility-group/check-before-create', 'redirectOnSuccess' => false]);
$postData = Json::encode(['returnUrl' => Url::current()]);

$js = <<< JS
        var vgCreateBtn = $('#visibility-group-create-btn').ladda();
        vgCreateBtn.on('click', function(e) {
            e.preventDefault();
            params = getGoOutParams(e);
            vgCreateBtn.ladda('start').attr('disabled', true);
            greenApple.ajax.perform('$createUrl', $postData, {
                complete: function() {
                    vgCreateBtn.ladda('stop').attr('disabled', false);
                },
                resultSuccess: function() {
                    runGoOutEventFormAction(params);
                }
            });
        });
JS;

$this->registerJs($js, yii\web\View::POS_READY);
?>
