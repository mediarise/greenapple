<?php
/**
 * @var $this \yii\web\View
 * @var $model DocEventForm
 * @var $visibilitySection \common\models\EventVisibilitySection
 */

use common\components\BitHelper;
use common\helpers\BackendUrl;
use common\models\DocEvent;
use common\models\DocEventForm;
use common\models\EventVisibilitySection;
use common\widgets\Select2;
use dosamigos\datepicker\DatePicker;
use frontend\widgets\ClockPicker;
use frontend\widgets\Cropper;
use frontend\widgets\SwitchInput;
use kartik\touchspin\TouchSpin;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$classRecurringEvent = 'recurring-event';
$classFrequencyMonthly = 'frequency-monthly';

$formOptions = [
    'class' => 'form-horizontal',
];

if ($model->isRecurring()) {
    Html::addCssClass($formOptions, $classRecurringEvent);
}

if ($model->frequency == DocEvent::FREQUENCY_MONTHLY) {
    Html::addCssClass($formOptions, $classFrequencyMonthly);
}

?>

    <div class="panel-body">

        <?= Cropper::widget([
            'placeholderSrc' => Yii::$app->params['eventPhotoPlaceholder'],
            'imageSrc' => $model->mainPhotoUrl,
            'uploadUrl' => Url::to([$this->params['userRole'] . '/event/upload-photo']),
            'modalTitle' => 'Crop event image',
        ]) ?>

        <div class="col-lg-8">

            <?php $form = ActiveForm::begin([
                'options' => [
                    'id' => 'event-form',
                    'class' => $formOptions['class'],
                ],
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]); ?>

            <?= Html::activeHiddenInput($model, 'creator_id') ?>

            <?= $form->field($model, 'title')->textInput(['placeholder' => 'Title'])->label(false) ?>

            <?= $form->field($model, 'description')->textarea(['placeholder' => 'Description'])->label(false) ?>

            <?= $form->field($model, 'type')->widget(Select2::class, [
                'disabled' => !$model->isNewRecord,
                'data' => DocEvent::getEventTypes(),
                'hideSearch' => true,
                'options' => [
                    'placeholder' => 'Select event type'
                ],
            ])->label(false) ?>

            <div class="row px-0">
                <div class="col-lg-6">
                    <?= $form->field($model, 'umbrella_account')->widget(Select2::class, [
                        'data' => DocEvent::getUmbrellaAccounts(),
                        'hideSearch' => true,
                        'options' => [
                            'placeholder' => 'Select Umbrella account'
                        ],
                    ])->label(false) ?>
                </div>

                <div class="col-lg-offset-1 col-lg-5">
                    <?= $form->field($model, 'ledger_account_id')->widget(\kartik\depdrop\DepDrop::class, [
                        'data' => DocEvent::getMappedLedgerAccounts($model->umbrella_account),
                        //'hideSearch' => true,
                        'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
                        'select2Options' => [
                            'theme' => 'default',
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ],
                        'options' => [
                            'placeholder' => 'Select ledger account'
                        ],
                        'pluginOptions' => [
                            'url' => Url::to([$this->params['userRole'] . '/event/get-accounts']),
                            'depends' => ['doceventform-umbrella_account']
                        ]
                    ])->label(false) ?>
                </div>
            </div>

            <?= Html::label('Requires parental action', 'is_parent_action_required', ['class' => 'phl']); ?>

            <?= $form->field($model, 'is_parent_action_required')->widget(SwitchInput::class, [
                'options' => [
                    'label' => false,
                ],
            ])->label(false) ?>


            <div class="<?= !$model->isRecurring() ? 'hidden' : '' ?> event-frequency row px-0">
                <div class="col-lg-4">
                    <?= $form->field($model, 'frequency')->widget(Select2::class, [
                        'data' => DocEvent::getFrequencies(),
                        'hideSearch' => true,
                        'options' => [
                            'placeholder' => 'Select frequency ...',
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-8 pt-4 pl-25 repeat-on-days">
                    <?= $form->field($model, 'repeatOnDays')->checkboxList(DocEvent::getDaysOfWeek(), [
                        'item' => function ($index, $label, $name, $checked, $value) {
                            $checkbox =
                                Html::label($label, null, ['class' => 'pr-05']) .
                                Html::checkbox($name, $checked, [
                                    'value' => $value
                                ]);

                            return Html::tag('span', $checkbox, ['class' => 'mr']);
                        },
                        'separator' => false
                    ])->label(false) ?>
                </div>
                <div class="col-xs-12" id="repeat-on-week">
                    <?= $form->field($model, 'repeat_on_week')->widget(Select2::class, [
                        'data' => DocEvent::getWeeks(),
                        'hideSearch' => true,
                        'options' => [
                            'placeholder' => 'select week ...',
                        ]
                    ])->label('Week') ?>
                </div>
            </div>


            <div class="row px-0">
                <div class="col-lg-6">
                    <?= Html::activeLabel($model, 'startDate', ['class' => 'phl']) ?>
                    <?php
                    $defaultViewDate = new \stdClass();
                    $defaultViewDate->year = date('Y', time());
                    $defaultViewDate->month = date('m', time());

                    echo $form->field($model, 'startDate')->widget(DatePicker::class, [
                        'template' => '
                        {input}
                        <span class="input-group-addon" id="basic-addon1">
                        <span class="fa fa-calendar"></span>
                        </span>
                        ',
                        'clientOptions' => [
                            'autoclose' => true,
                            'maxViewMode' => 'years',
                            'startView' => 2,
                            'defaultViewDate' => $defaultViewDate,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => [
                            'placeholder' => 'Select start date'
                        ],
                    ])->label(false);
                    ?>
                </div>
                <div class="col-lg-offset-1 col-lg-5 start-time-input-container">
                    <?= Html::activeLabel($model, 'startTime', ['class' => 'phl']) ?>
                    <?= $form->field($model, 'startTime')->widget(ClockPicker::class, [
                        'options' => [
                            'placeholder' => 'Select start time'
                        ]
                    ])->label(false) ?>
                </div>
            </div>


            <div class="row px-0 <?= $model->isRecurring() ? 'hidden' : '' ?>" id="due-date-input-block">
                <div class="col-lg-6">
                    <?= Html::activeLabel($model, 'dueDate', ['class' => 'phl']) ?>
                    <?php
                    $defaultViewDate = new \stdClass();
                    $tomorrow = new DateTime();
                    $defaultViewDate->year = $tomorrow->format('Y');
                    $defaultViewDate->month = $tomorrow->format('m');
                    echo $form->field($model, 'dueDate')->widget(DatePicker::class, [
                        'template' => '
                            {input}
                            <span class="input-group-addon" id="basic-addon1">
                            <span class="fa fa-calendar"></span>
                            </span>
                            ',
                        'clientOptions' => [
                            'autoclose' => true,
                            'maxViewMode' => 'years',
                            'startView' => 2,
                            'defaultViewDate' => $defaultViewDate,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => [
                            'placeholder' => 'Select due date'
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>


            <div class="row px-0 <?= !$model->isRecurring() ? 'hidden' : '' ?>" id="due-period-and-unit-input-block">
                <div class="col-lg-3">
                    <?= $form->field($model, 'due_lead_period')->widget(TouchSpin::class, [
                        'options' => [
                            'class' => 'text-center',
                        ],
                        'pluginOptions' => [
                            'buttonup_class' => 'input-group-addon on-right',
                            'buttondown_class' => 'input-group-addon on-left',
                            'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                            'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                            'min' => 1,
                        ],
                    ]) ?>
                </div>
                <div class="col-lg-offset-1 col-lg-5">
                    <?= $form->field($model, 'due_unit')->widget(Select2::class, [
                        'data' => DocEvent::getDueUnits(),
                        'hideSearch' => true,
                        'options' => [
                            'placeholder' => 'Select due unit',
                        ],
                    ]) ?>
                </div>
            </div>


            <div class="end-date-settings-block">
                <div class="row px-0 pb-2 end-date-toggle-block">
                    <div class="col-lg-12 px-0">
                        <a href="#" id="duration-btn" class="btn btn-primary btn-xl mr-125">
                            <span class="glyphicon glyphicon-th-list"></span> Duration
                        </a>
                        <a href="#" id="end-date-btn" class="btn btn-primary btn-xl">
                            <span class="glyphicon glyphicon-th"></span> End date
                        </a>
                    </div>
                </div>


                <div class="row px-0 <?= $model->useDuration ? '' : 'hidden' ?>" id="duration-input-block">
                    <div class="col-lg-6">
                        <?= Html::activeLabel($model, 'duration', ['class' => 'phl']) ?>
                        <?= $form->field($model, 'duration')->widget(TouchSpin::class, [
                            'pluginOptions' => [
                                'buttonup_class' => 'input-group-addon on-right',
                                'buttondown_class' => 'input-group-addon on-left',
                                'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                                'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                                'min' => 1,
                            ]
                        ])->label(false) ?>
                    </div>
                    <div class="col-lg-offset-1 col-lg-5">
                        <?= Html::activeLabel($model, 'duration_unit', ['class' => 'phl']) ?>
                        <?= $form->field($model, 'duration_unit')->widget(Select2::class, [
                            'data' => DocEvent::getDurationUnits(),
                            'hideSearch' => true,
                            'options' => [
                                'placeholder' => 'Select duration unit',
                            ],
                        ])->label(false) ?>
                    </div>
                </div>

                <div class="row px-0 <?= $model->useDuration ? 'hidden' : '' ?>" id="end-date-input-block">
                    <div class="col-lg-6">
                        <?= Html::activeLabel($model, 'endDate', ['class' => 'phl']) ?>

                        <?php
                        $defaultViewDate = new \stdClass();
                        $defaultViewDate->year = date('Y', time());
                        $defaultViewDate->month = date('m', time());

                        echo $form->field($model, 'endDate')->widget(DatePicker::class, [
                            'template' => '
                            {input}
                            <span class="input-group-addon" id="basic-addon1">
                            <span class="fa fa-calendar"></span>
                            </span>
                            ',
                            'name' => 'Date',
                            'clientOptions' => [
                                'autoclose' => true,
                                'maxViewMode' => 'years',
                                'startView' => 2,
                                'defaultViewDate' => $defaultViewDate,
                                'format' => 'yyyy-mm-dd'
                            ],
                            'options' => [
                                'placeholder' => 'Select end date'
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-lg-offset-1 col-lg-5 hidden">
                        <?= $form->field($model, 'endTime')->widget(ClockPicker::class, [
                            'options' => [
                                'placeholder' => 'Select start time'
                            ]
                        ])->label(false) ?>
                    </div>
                </div>

                <?= $form->field($model, 'mainPhotoUrl', ['inputOptions' => [
                    'id' => 'main-photo-url-input'
                ]])->hiddenInput()->label(false) ?>


            </div>

            <div class="row">
                <div class="col-lg-12 px-0">

                    <?php if ($model->status !== DocEvent::STATUS_PUBLISHED) : ?>
                        <?= Html::button('Save', [
                            'class' => 'event-save-btn ladda-button btn btn-xl btn-success mr-125',
                            'data-style' => 'zoom-out'
                        ]) ?>
                    <?php endif; ?>

                    <?php if ($model->status === DocEvent::STATUS_SAVED) : ?>
                        <?= Html::a('Publish', '#', [
                            'class' => 'event-publish-btn ladda-button btn btn-xl btn-success mr-125',
                            'data-style' => 'zoom-out'
                        ]); ?>
                    <?php endif; ?>

                    <a href="<?= Url::toRoute(['index']) ?>" class="btn btn-xl btn-primary">Cancel</a>

                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

<?php

$saveEventUrl = $model->isNewRecord
    ? BackendUrl::toRoute([$this->params['userRole'] . '/event/create'])
    : BackendUrl::toRoute([$this->params['userRole'] . '/event/update', 'id' => $model->id]);

$publishUrl = BackendUrl::toRoute([$this->params['userRole'] . '/event/publish', 'id' => $model->id]);

$this->registerJsVar('isCreateScenario', $model->isNewRecord);
$this->registerJsVar('saveEventUrl', $saveEventUrl);
$this->registerJsVar('publishUrl', $publishUrl);
$this->registerJsVar('filterByVisibilityGroups', EventVisibilitySection::FILTER_BY_VISIBILITY_GROUPS);

$this->registerJsVar('groupsTab', '#groups-tab');
$this->registerJsVar('studentsTab', '#students-tab');
$this->registerJsVar('gradesTab', '#grades-tab');
$this->registerJsVar('formsTab', '#forms-tab');
$this->registerJsVar('productsTab', '#products-tab');
$this->registerJsVar('eventTypeNutrition', DocEvent::EVENT_TYPE_NUTRITION);
$this->registerJsVar('eventTypeFieldTrip', DocEvent::EVENT_TYPE_FIELD_TRIP);
$this->registerJsVar('durationUnitDay', DocEvent::DURATION_UNIT_DAY);
$this->registerJsVar('createVisibilityGroupUrl', BackendUrl::toRoute([$this->params['userRole'] . '/visibility-group/check-before-create']));
$this->registerJsVar('postData', Json::encode(['returnUrl' => Url::current()]));
$this->registerJsVar('recurringEventTypes', DocEvent::getRecurringEventTypes());
$this->registerJsVar('oneFullDayEventTypes', DocEvent::getOneFullDayEventTypes());
$this->registerJsVar('isSingleProductEvent', BitHelper::testBits($model->type,
    DocEvent::EVENT_TYPE_NUTRITION_ONE_TIME | DocEvent::EVENT_TYPE_NUTRITION));

$this->registerJsVar('classFrequencyMonthly', $classFrequencyMonthly);
$this->registerJsVar('classRecurringEvent', $classRecurringEvent);

$script = <<<JS
var body = $('body');

var gridInputs = [
    '#products-grid tbody input', '#forms-grid tbody input',
    '#students-grid tbody input', '#grades-grid tbody input', '#groups-grid tbody input'
];
var filters = [
    '#products-filter .filter-input', '#forms-filter .filter-input',
    '#students-filter .filter-input', '#grades-filter .filter-input', '#groups-filter .filter-input'
];
var forms = [
    '#event-form', '#products-form', '#forms-form',
    '#students-form', '#grades-form', '#groups-form',
    '#visibility-section-form', '#reminder-form', '#fundraising-form'
];
var paginationLinks = [
    '#products-grid .pagination ul li a', '#forms-grid .pagination ul li a',
    '#students-grid .pagination ul li a', '#grades-grid .pagination ul li a', '#groups-grid .pagination ul li a'
];
var allForms = $(forms.join(','));

var oneFullDayEventDisabledFields = [
    '.end-date-settings-block', '.start-time-input-container'
];

localStorage.clear();

var hasUnsavedChanges = false;

// set the settings for the one full day event
setOneFullDayEventSettings();

// event saving
body.on('click', '.event-save-btn', function (e) {
    e.preventDefault();

    // event creation
    if (isCreateScenario) {
        var saveBtn = $('.event-save-btn').ladda();
        saveBtn.ladda('start').attr('disabled', true);
        greenApple.ajax.submitForm($('#event-form'), {
            resultValidation: function (data) {
                this.base('resultValidation', data);
                saveBtn.ladda('stop');
            },
            resultError: function () {
                saveBtn.ladda('stop');
            }
        });
        return;
    }

    saveEvent(true);
});

// event publishing
body.on('click', 'a.event-publish-btn', function (e) {
    e.preventDefault();
    var publishBtn = $(this).ladda();
    publishBtn.ladda('start').attr('disabled', true);


    //saving forms
    greenApple.ajax.perform(saveEventUrl, allForms.serialize(), {
        resultValidation: function (data) {
            greenApple.form.setTabularFormErrors(allForms, data.validation);
            publishBtn.ladda('stop');
        },
        resultError: function () {
            publishBtn.ladda('stop');
        },
        resultSuccess: function (data) {

            // publish event
            greenApple.ajax.perform(publishUrl, {}, {
                resultNotification: function (data) {
                    publishBtn.ladda("stop");
                    this.base("resultNotification", data);
                    if ("switchToTab" in data) {
                        var anchor = $("#" + data.switchToTab + "-tab-header a").tab("show");
                        window.activeTab = anchor.attr("href");
                    }
                },
                resultError: function (data) {
                    publishBtn.ladda("stop");
                }
            });
        }
    });

});

// filtration
body.on('keyup', filters.join(','), function () {
    runFiltration(this);
}).on('change', filters.join(','), function () {
    runFiltration(this);
});

// pagination
body.on('click', paginationLinks.join(','), function (e) {
    e.preventDefault();
    saveFormState($(this).closest('data-grid'));
    $.pjax({
        url: e.target.attr('href'),
        container: '#products-pjax',
        push: false,
        timeout: 10000
    });
});

// tabs switching
body.on('click', '.tabs-container ul li a', function () {
    var targetTab = $(this).closest('li').data('target-tab');
    if (targetTab) {
        window.activeTab = targetTab;
    }
    window.changeSelectAllControlStateIfNeeded();
});

// changing form values
body.on('change', gridInputs.join(','), function () {
    hasUnsavedChanges = true;
    saveFormState($(this).closest('.data-grid'));
    window.changeSelectAllControlStateIfNeeded();
});

// checking all rows
body.on('change', '.check-all', function () {
    $(this).closest('.data-grid')
        .find('.checkbox-control.is-checked')
        .prop('checked', $(this).is(':checked'))
        .trigger('change');
});

function runFiltration(inputElement) {
    saveFormState($(inputElement).data('grid-selector'));
    $.pjax({
        url: saveEventUrl + '&' + $(inputElement).closest('form').serialize(),
        container: $(inputElement).data('pjax-container-selector'),
        push: false,
        timeout: 10000
    });
}

function saveFormState(grid) {
    var gridForm = $(grid).closest('form');
    $.each(gridForm.find('tbody input:checkbox'), function (i, checkbox) {
        var ckeckboxFallback = $(checkbox).siblings('.checkbox-fallback');
        $(checkbox).is(':checked')
            ? ckeckboxFallback.val('checked')
            : ckeckboxFallback.val('not-checked');
    });
    var currentFormState = gridForm.serializeArray();
    var previousFormState = JSON.parse(localStorage.getItem('eventFormState')) || currentFormState;
    var formState = _.unionWith(currentFormState, previousFormState, 'name');
    formState = _.uniqBy(formState, 'name');
    localStorage.setItem('eventFormState', JSON.stringify(formState));
}

body.on('beforeFilter', '.no-js-grid', function (event) {
    return false; // prevents applyFilter
});

window.changeSelectAllControlStateIfNeeded = function () {
    var isAllCheked = true;
    var activeTab = $(window.activeTab);
    var checkboxes = activeTab.find('.checkbox-control.is-checked');
    checkboxes.each(function (i, checkbox) {
        isAllCheked = isAllCheked && $(checkbox).is(':checked');
    });
    activeTab.find('.check-all').prop('checked', isAllCheked);
};

$(document).on('pjax:success', function () {

    var previousFormState = JSON.parse(localStorage.getItem('eventFormState')) || [];
    $.each(previousFormState, function (i, data) {
        var input = $('input[name="' + data.name + '"]');
        if (input.length > 1) {
            return;
        }

        // checkboxes (fallback hidden inputs)
        if (input.hasClass('checkbox-fallback')) {
            input.siblings('.checkbox-control').prop('checked', data.value == 'checked');
        }

        // number control plugin fields
        var numberControlInput = new RegExp(/^w(\d+)(-disp)?$/);
        if (numberControlInput.test(data.name)) {
            return;
        }
        var moneyInput = new RegExp(/^(.*)\[price\]$/);
        if (moneyInput.test(data.name)) {
            input.closest('td').find('input').val(data.value);
            return;
        }

        // other inputs
        input.prop('value', data.value);
    });

    window.changeSelectAllControlStateIfNeeded();
});

$("#duration-btn").click(function (e) {
    e.preventDefault();
    showDuration();
});

$("#end-date-btn").click(function (e) {
    e.preventDefault();
    showEndDate();
});

body.on('change', '#doceventform-type', function () {
    if (isRecurringEvent()) {
        showDuePeriodAndUnit();
        showEndDate();
        hideEndDateDurationToggle();
    } else {
        showDueDate();
        showDuration();
        showEndDateDurationToggle();
    }
    setOneFullDayEventSettings();
});

body.on('change', '#doceventform-frequency', function() {
    var frequency = $('#doceventform-frequency').val();
    $('#event-form').removeClass(classFrequencyMonthly);
    if (frequency == 4){
        $('#event-form').addClass(classFrequencyMonthly);
    }
});

if (isSingleProductEvent) {
    body.on("change", "#products-tab .is-checked", function () {
        var isChecked = $(this).prop('checked');
        $('#products-tab .is-checked').prop('checked', false);
        $(this).prop('checked', isChecked);
    })
}

function isRecurringEvent() {
    var type = $('#doceventform-type').val();
    type = parseInt(type);
    return recurringEventTypes.indexOf(type) !== -1;
}

function isOneTimeEvent() {
    !isRecurringEvent();
}

function showDuePeriodAndUnit() {
    $('.event-frequency').removeClass('hidden');
    $('#due-date-input-block').addClass('hidden');
    $('#due-period-and-unit-input-block').removeClass('hidden');
}

function showDueDate() {
    $('.event-frequency').addClass('hidden');
    $('#due-date-input-block').removeClass('hidden');
    $('#due-period-and-unit-input-block').addClass('hidden');
}

function showEndDate() {
    $("#end-date-input-block").removeClass('hidden');
    $("#duration-input-block").addClass('hidden');
}

function showDuration() {
    $("#duration-input-block").removeClass('hidden');
    $("#end-date-input-block").addClass('hidden');
}

function hideEndDateDurationToggle() {
    $('.end-date-toggle-block').addClass('hidden');
}

function showEndDateDurationToggle() {
    $('.end-date-toggle-block').removeClass('hidden');
}

function saveEvent(redirectAfterSaving) {
    var saveBtn = $('.event-save-btn').ladda();
    saveBtn.ladda('start').attr('disabled', true);

    greenApple.ajax.perform(saveEventUrl, allForms.serialize(), {
        async: true,
        resultValidation: function (data) {
            greenApple.form.setErrors(allForms, data.validation);
            greenApple.form.setTabularFormErrors(allForms, data.validation);
            var tab = $(".has-error, .has-warning").first().closest(".tab-pane").attr("id");
            $("#event-form-tab [href=\"#" + tab + "\"]").tab("show");
            window.activeTab = tab;
            saveBtn.ladda('stop');
        },
        resultError: function () {
            saveBtn.ladda('stop');
        },
        resultSuccess: function () {
            if (typeof redirectAfterSaving !== 'undefined') {
                document.location.href = saveEventUrl;
            }
        }
    });
}

function setOneFullDayEventSettings() {
    if (isOneFullDayEvent()) {
        setAsOneFullDayEvent();
    } else {
        setAsNotOneFullDayEvent();
    }
}

function isOneFullDayEvent() {
    var type = $('#doceventform-type').val();
    type = parseInt(type);
    return oneFullDayEventTypes.indexOf(type) !== -1;
}

function setAsOneFullDayEvent() {
    $('#doceventform-duration').val('1');
    $('#doceventform-duration_unit').val(durationUnitDay).trigger('change');
    $('#doceventform-starttime').val('00:00');
    $(oneFullDayEventDisabledFields.join(', ')).addClass('hidden');
}

function setAsNotOneFullDayEvent() {
    $(oneFullDayEventDisabledFields.join(', ')).removeClass('hidden');
}

function newSwal() {
    return swal({
        title: "Are you sure?",
        text: "you will lose all unsaved changes!",
        icon: "warning",
        buttons: {
            confirm: {
                text: 'Save & Continue!',
                value: true,
                visible: true,
                className: "btn btn-xl btn-success",
                closeModal: true
            },
            // ignore: {
            //     text: "Reject & Continue",
            //     value: false,
            //     visible: true,
            //     className: "btn btn-xl btn-primary",
            //     closeModal: true
            // },
            cancel: {
                text: "Cancel",
                value: null,
                visible: true,
                className: "btn btn-xl btn-primary",
                closeModal: true
            }
        }
    });
}

function runGoOutEventFormAction(params) {
    if (!hasUnsavedChanges) {
        redirectPost(params);
        return;
    }
    newSwal()
        .then(function (needToSaveChanges) {
            if (needToSaveChanges === true){
                $.when(saveEvent()).done(function () {
                    redirectPost(params);
                });
            } else if (needToSaveChanges === false) {
                redirectPost(params);
            }
        });
}

window.redirectPost = function(params) {
    $.redirectPost(params.url, params.data);    
};

window.getGoOutParams = function(e) {
    var target = $(e.target).closest("a");
    return {
        url: target.attr("href"),
        data: target.data("params")
    }
};

body.on('click', '.go-out-event-form', function(e) {
    e.preventDefault();
    var params = getGoOutParams(e);
    
    runGoOutEventFormAction(params);
});

JS;
$this->registerJs($script);

?>