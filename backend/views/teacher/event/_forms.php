<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\DocEvent
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $filterModel \common\models\teacher\event\FormSearch
 */

use common\helpers\BackendUrl;
use common\models\DocEvent;
use frontend\widgets\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div id="forms-tab" class="panel-body event-form-tab">

    <h3>Choose available forms from the list</h3>

    <div class="col-lg-12 px-0 mb-15">
        <?= Html::a('<i class="fa fa-plus"></i> Create new', [$this->params['userRole'] . '/form/create'], [
            'id' => 'form-create-btn',
            'class' => 'btn btn-success btn-xl go-out-event-form',
            'data' => [
                'style' => 'zoom-out',
                'pjax' => 0,
                'params' => [
                    'returnUrl' => Url::current(),
                ],
            ],
        ]) ?>
    </div>


    <?= Html::beginForm('#', 'get', ['id' => 'forms-filter']) ?>

    <div class="row">
        <div class="col-md-6 mb-15">
            <?= Html::textInput('FormSearch[search]', null, [
                'placeholder' => 'Search',
                'data-grid-selector' => '#forms-grid',
                'data-pjax-container-selector' => '#forms-pjax',
                'class' => 'filter-input form-control'
            ]); ?>
        </div>
    </div>

    <?= Html::endForm(); ?>


    <?php \yii\widgets\Pjax::begin([
        'id' => 'forms-pjax',
        'timeout' => 10000,
        'enablePushState' => false
    ]); ?>

    <?= Html::beginForm('', 'post', ['id' => 'forms-form']) ?>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'id' => 'forms-grid',
        'dataProvider' => $dataProvider,
        'summary' => false,
        'filterModel' => false,
        'filterRowOptions' => [
            'class' => 'hidden'
        ],
        'layout' => '{items}{pager}',
        'options' => [
            'class' => 'no-js-grid data-grid'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            [
                'header' => Html::checkbox('', false, ['class' => 'check-all']),
                'format' => 'html',
                'content' => function ($form) {
                    return
                        Html::checkbox('Form[checked][]', $form['checked'], [
                            'class' => 'checkbox-control is-checked form-checkbox',
                            'value' => $form['id']
                        ]) .
                        Html::hiddenInput("Form[{$form['id']}][is_checked]",
                            $form['checked'] ? 'checked' : 'not-checked',
                            ['class' => 'checkbox-fallback is-checked']
                        ) .
                        Html::hiddenInput("Form[{$form['id']}][form_id]", $form['id']);
                },
                'contentOptions' => ['style' => 'width: 70px']
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'content' => function ($form) {
                    return Html::a(
                            $form['title'],
                            BackendUrl::toRoute([$this->params['userRole'] . '/form/update', 'id' => $form['id']]), [
                                'class' => 'go-out-event-form',
                                'data' => [
                                    'pjax' => 0,
                                    'params' => [
                                        'returnUrl' => Url::current(),
                                    ],
                                ],
                            ]
                        ) .
                        Html::hiddenInput("Form[{$form['id']}][form_id]", $form['id']);
                },
                'contentOptions' => ['style' => 'width: 250px']
            ],
            [
                'label' => 'Mandatory',
                'format' => 'raw',
                'content' => function ($form) {
                    $isMandatory = $form['is_mandatory'] && $form['checked'];
                    return
                        Html::checkbox("Form[is_mandatory][]", $isMandatory, [
                            'class' => 'checkbox-control is-mandatory',
                            'value' => $form['id'],
                        ]) .
                        Html::hiddenInput("Form[{$form['id']}][is_mandatory]",
                            $form['is_mandatory'] ? 'checked' : 'not-checked',
                            ['class' => 'checkbox-fallback']
                        );
                },
            ],
        ]
    ]) ?>

    <?= Html::endForm(); ?>

    <?php \yii\widgets\Pjax::end(); ?>


    <?php if ($model->status !== DocEvent::STATUS_PUBLISHED) : ?>
        <?= Html::button('Save', [
            'class' => 'event-save-btn ladda-button btn btn-xl btn-success mr-125',
            'data-style' => 'zoom-out'
        ]) ?>
    <?php endif; ?>

    <?php if ($model->status === DocEvent::STATUS_SAVED) : ?>
        <?= Html::a('Publish', Url::toRoute(['publish', 'id' => $model->id]), [
            'class' => 'event-publish-btn ladda-button btn btn-xl btn-success mr-125',
            'data-style' => 'zoom-out'
        ]); ?>
    <?php endif; ?>

    <a href="<?= Url::toRoute(['index']) ?>" class="btn btn-xl btn-primary">Cancel</a>

</div>

<?php

$script = <<<JS
    
       body.on('change', '.checkbox-control.is-mandatory', function() {
            if ($(this).is(':checked')) {
                $(this)
                    .closest('tr')
                    .find('.is-checked')
                    .prop('checked', true)
                    .trigger('change');
            }
       });
       
       body.on('change', '.checkbox-control.is-checked.form-checkbox', function() {
            $(this)
                .closest('tr')
                .find('.checkbox-control.is-mandatory')
                .prop('checked', $(this).is(':checked'));
       });

JS;
$this->registerJs($script);

?>
