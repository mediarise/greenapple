<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\DocEvent
 * @var $filterModel \common\models\SearchProduct
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use common\helpers\ArrayHelper;
use common\helpers\BackendUrl;
use common\models\DocEvent;
use kartik\number\NumberControl;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$moneyInputOptions = [
    'maskedInputOptions' => [
        'prefix' => '$ ',
        'allowMinus' => false,
        'alias' => 'numeric',
        'digits' => 2,
        'groupSeparator' => ',',
        'autoGroup' => true,
        'autoUnmask' => true,
        'unmaskAsNumber' => false
    ],
    'displayOptions' => [
        'class' => 'text-left form-control'
    ]
];
?>

<div id="products-tab" class="panel-body event-form-tab">

    <h3>Choose available product from the list</h3>

    <div class="col-lg-12 px-0 mb-15">
        <?= Html::a('<i class="fa fa-plus"></i> Create new', [$this->params['userRole'] . '/product/create'], [
            'id' => 'product-create-btn',
            'class' => 'btn btn-success btn-xl go-out-event-form',
            'data' => [
                'style' => 'zoom-out',
                'pjax' => 0,
                'params' => [
                    'returnUrl' => Url::current(),
                ],
            ],
        ]) ?>
    </div>


    <?= Html::beginForm('#', 'get', ['id' => 'products-filter']) ?>

    <div class="row">
        <div class="col-md-6 mb-15">
            <?= Html::textInput('ProductSearch[search]', null, [
                'placeholder' => 'Search',
                'data-grid-selector' => '#products-grid',
                'data-pjax-container-selector' => '#products-pjax',
                'class' => 'filter-input form-control'
            ]); ?>
        </div>
    </div>

    <?= Html::endForm(); ?>


    <?php \yii\widgets\Pjax::begin([
        'id' => 'products-pjax',
        'timeout' => 10000,
        'enablePushState' => false
    ]); ?>

    <?= Html::beginForm('', 'post', ['id' => 'products-form']) ?>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'id' => 'products-grid',
        'dataProvider' => $dataProvider,
        'summary' => false,
        'filterModel' => false,
        'filterRowOptions' => [
            'class' => 'hidden'
        ],
        'layout' => '{items}{pager}',
        'options' => [
            'class' => 'no-js-grid data-grid'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            [
                'header' => $model->type !== DocEvent::EVENT_TYPE_NUTRITION
                    ? Html::checkbox('', false, ['class' => 'check-all'])
                    : '',
                'content' => function ($product) {
                    return
                        Html::checkbox('Product[checked][]', $product['checked'], [
                            'class' => 'checkbox-control is-checked',
                            'value' => $product['id']
                        ]) .
                        Html::hiddenInput("Product[{$product['id']}][is_checked]",
                            $product['checked'] ? 'checked' : 'not-checked',
                            ['class' => 'checkbox-fallback is-checked']
                        ) .
                        Html::hiddenInput("Product[{$product['id']}][product_id]", $product['id']);
                },
                'format' => 'html',
                'contentOptions' => [
                    'class' => 'cell-center'
                ]
            ],
            [
                'attribute' => 'sku',
                'content' => function ($product) {
                    if ($product['sku'] != '') {
                        return $product['sku'];
                    }
                    return '-';
                }
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'content' => function ($product) {
                    return Html::a(
                        $product['title'],
                        BackendUrl::toRoute([$this->params['userRole'] . '/product/update', 'id' => $product['id']]), [
                            'class' => 'go-out-event-form',
                            'data' => [
                                'pjax' => 0,
                                'params' => [
                                    'returnUrl' => Url::current(),
                                ],
                            ],
                        ]
                    );
                },
                'contentOptions' => [
                    'class' => 'cell-center',
                    'style' => 'width: 250px'
                ]
            ],
            [
                'attribute' => 'price',
                'content' => function ($product) use ($moneyInputOptions) {
                    if ($product['isFree']) {
                        return 'free';
                    }
                    return NumberControl::widget(ArrayHelper::merge([
                        'name' => "Product[{$product['id']}][price]",
                        'value' => $product['price'],
                    ], $moneyInputOptions));
                },
                'contentOptions' => function ($product) {
                    if ($product['isFree']) {
                        return ['class' => 'cell-center'];
                    }
                    return [];
                }
            ],
            [
                'attribute' => 'minQuantity',
                'content' => function ($product) {
                    $validationClass = "product-{$product['id']}-min_quantity";
                    return Html::textInput("Product[{$product['id']}][min_quantity]", $product['min_quantity'], [
                        'class' => "form-control product-min-quantity $validationClass"
                    ]);
                }
            ],
            [
                'attribute' => 'maxQuantity',
                'content' => function ($product) {
                    $validationClass = "product-{$product['id']}-max_quantity";
                    return Html::textInput("Product[{$product['id']}][max_quantity]", $product['max_quantity'], [
                        'class' => "form-control product-max-quantity $validationClass"
                    ]);
                }
            ],
        ]
    ]) ?>

    <?= Html::endForm(); ?>

    <?php \yii\widgets\Pjax::end(); ?>


    <?php if ($model->status !== DocEvent::STATUS_PUBLISHED) : ?>
        <?= Html::button('Save', [
            'class' => 'event-save-btn ladda-button btn btn-xl btn-success mr-125',
            'data-style' => 'zoom-out'
        ]) ?>
    <?php endif; ?>

    <?php if ($model->status === DocEvent::STATUS_SAVED) : ?>
        <?= Html::a('Publish', Url::toRoute(['publish', 'id' => $model->id]), [
            'class' => 'event-publish-btn ladda-button btn btn-xl btn-success mr-125',
            'data-style' => 'zoom-out'
        ]); ?>
    <?php endif; ?>

    <a href="<?= Url::toRoute(['index']) ?>" class="btn btn-xl btn-primary">Cancel</a>

</div>