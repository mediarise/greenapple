<?php
/**
 * @var $this \yii\web\View
 *
 * @var $visibilitySection \common\models\EventVisibilitySection
 * @var $filterModel \common\models\teacher\event\StudentSearch
 * @var $dataProvider \yii\data\ArrayDataProvider
 */

use common\models\EventVisibilitySection;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div id="grades-tab" class="event-form-tab visibility-section-tab mb-4"
     style="display:<?= $visibilitySection->filter_by === EventVisibilitySection::FILTER_BY_GRADES ? 'block' : 'none' ?>">


<?= Html::beginForm('#', 'get', ['id' => 'grades-filter']) ?>

<div class="row">
    <div class="col-md-6 mb-15 form-group">
        <div class="checkbox">
            <label for="grades-search">
                <?= Html::checkbox('GradeSearch[onlyThisYear]', true, [
                    'id' => 'grades-search',
                    'placeholder' => 'Search',
                    'data-grid-selector' => '#grades-grid',
                    'data-pjax-container-selector' => '#grades-pjax',
                    'class' => 'filter-input'
                ]); ?>
                Only this year
            </label>
        </div>
    </div>
</div>

<?= Html::endForm(); ?>


<?php \yii\widgets\Pjax::begin([
    'id' => 'grades-pjax',
    'timeout' => 10000,
    'enablePushState' => false
]); ?>

<?= Html::beginForm('', 'post', ['id' => 'grades-form']) ?>

<?= GridView::widget([
    'id' => 'grades-grid',
    'dataProvider' => $dataProvider,
    'summary' => false,
    'filterModel' => false,
    'filterRowOptions' => [
        'class' => 'hidden'
    ],
    'layout' => '{items}{pager}',
    'options' => [
        'class' => 'no-js-grid data-grid'
    ],
    'tableOptions' => [
        'class' => 'table table-striped'
    ],
    'columns' => [
        [
            'header' => Html::checkbox('', false, ['class' => 'check-all']),
            'format' => 'html',
            'content' => function ($grade) {
                return
                    Html::checkbox('Grade[checked][]', $grade['checked'], [
                        'class' => 'checkbox-control is-checked',
                        'value' => $grade['id'],
                        'data-year' => $grade['year']
                    ]) .
                    Html::hiddenInput("Grade[{$grade['id']}][is_checked]",
                        $grade['checked'] ? 'checked' : 'not-checked',
                        ['class' => 'checkbox-fallback']
                    );
            },
            'contentOptions' => ['style' => 'width: 70px']
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 250px']
        ],
        'year'
    ]
]) ?>

<?= Html::endForm(); ?>

<?php \yii\widgets\Pjax::end(); ?>

</div>

<?php
$this->registerJs(<<<JS
$('body').on('changer', '#grades-search', function(event) {
    $(this).val($(this).isChecked());
})
JS
)
?>