<?php
/**
 * @var $this \yii\web\View
 *
 * @var $visibilitySection \common\models\EventVisibilitySection
 * @var $filterModel \common\models\teacher\event\StudentSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use common\models\EventVisibilitySection;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div id="students-tab" class="event-form-tab visibility-section-tab mb-4"
     style="display:<?= $visibilitySection->filter_by === EventVisibilitySection::FILTER_BY_STUDENTS ? 'block' : 'none' ?>">


<?= Html::beginForm('#', 'get', ['id' => 'students-filter']) ?>

<div class="row">
    <div class="col-md-6 mb-15">
        <?= Html::textInput('StudentSearch[search]', null, [
            'placeholder' => 'Search',
            'data-grid-selector' => '#students-grid',
            'data-pjax-container-selector' => '#students-pjax',
            'class' => 'filter-input form-control'
        ]); ?>
    </div>
</div>

<?= Html::endForm(); ?>


<?php \yii\widgets\Pjax::begin([
    'id' => 'students-pjax',
    'timeout' => 10000,
    'enablePushState' => false
]); ?>

<?= Html::beginForm('', 'post', ['id' => 'students-form']) ?>

<?= GridView::widget([
    'id' => 'students-grid',
    'dataProvider' => $dataProvider,
    'summary' => false,
    'filterModel' => false,
    'filterRowOptions' => [
        'class' => 'hidden'
    ],
    'layout' => '{items}{pager}',
    'options' => [
        'class' => 'no-js-grid data-grid'
    ],
    'tableOptions' => [
        'class' => 'table table-striped'
    ],
    'columns' => [
        [
            'header' => Html::checkbox('', false, ['class' => 'check-all']),
            'format' => 'html',
            'content' => function ($student) {
                return
                    Html::checkbox('Student[checked][]', $student['checked'], [
                        'class' => 'checkbox-control is-checked',
                        'value' => $student['id']
                    ]) .
                    Html::hiddenInput("Student[{$student['id']}][is_checked]",
                        $student['checked'] ? 'checked' : 'not-checked',
                        ['class' => 'checkbox-fallback']
                    );
            },
            'contentOptions' => ['style' => 'width: 70px']
        ],
        [
            'attribute' => 'first_name',
            'format' => 'raw',
            'value' => function ($student) {
                return $student['first_name'];
            },
            'contentOptions' => ['style' => 'width: 250px']
        ],
        'oen'
    ],
]) ?>

<?= Html::endForm(); ?>

<?php \yii\widgets\Pjax::end(); ?>

</div>
