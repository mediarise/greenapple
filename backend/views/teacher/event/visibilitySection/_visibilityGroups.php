<?php
/**
 * @var $this \yii\web\View
 *
 * @var $visibilitySection \common\models\EventVisibilitySection
 * @var $visibilityGroupsFilterModel \common\models\teacher\event\StudentSearch
 * @var $visibilityGroupsDataProvider \yii\data\ActiveDataProvider
 */

use common\models\EventVisibilitySection;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div id="groups-tab" class="event-form-tab visibility-section-tab mb-4"
     style="display:<?= $visibilitySection->filter_by === EventVisibilitySection::FILTER_BY_VISIBILITY_GROUPS ? 'block' : 'none' ?>">


<?= Html::beginForm('#', 'get', ['id' => 'groups-filter']) ?>

<div class="row">
    <div class="col-md-6 mb-15">
        <?= Html::textInput('VisibilityGroupSearch[search]', null, [
            'placeholder' => 'Search',
            'data-grid-selector' => '#groups-grid',
            'data-pjax-container-selector' => '#groups-pjax',
            'class' => 'filter-input form-control'
        ]); ?>
    </div>
</div>

<?= Html::endForm(); ?>


<?php \yii\widgets\Pjax::begin([
    'id' => 'groups-pjax',
    'timeout' => 10000,
    'enablePushState' => false
]); ?>

<?= Html::beginForm('', 'post', ['id' => 'groups-form']) ?>

<?= /** @noinspection PhpUnhandledExceptionInspection */
GridView::widget([
    'id' => 'groups-grid',
    'dataProvider' => $visibilityGroupsDataProvider,
    'summary' => false,
    'filterModel' => false,
    'filterRowOptions' => [
        'class' => 'hidden'
    ],
    'layout' => '{items}{pager}',
    'options' => [
        'class' => 'no-js-grid data-grid'
    ],
    'tableOptions' => [
        'class' => 'table table-striped'
    ],
    'columns' => [
        [
            'header' => Html::checkbox('', false, ['class' => 'check-all']),
            'format' => 'html',
            'content' => function ($visibilityGroup) {
                return
                    Html::checkbox('VisibilityGroup[checked][]', $visibilityGroup['checked'], [
                        'class' => 'checkbox-control is-checked',
                        'value' => $visibilityGroup['id']
                    ]) .
                    Html::hiddenInput("VisibilityGroup[{$visibilityGroup['id']}][is_checked]",
                        $visibilityGroup['checked'] ? 'checked' : 'not-checked',
                        ['class' => 'checkbox-fallback']
                    );
            },
            'contentOptions' => ['style' => 'width: 70px']
        ],
        'name',
    ],
]) ?>

<?= Html::endForm(); ?>

<?php \yii\widgets\Pjax::end(); ?>

</div>
