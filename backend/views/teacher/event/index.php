<?php
/**
 * @var $this \yii\web\View
 * @var $filterModel \common\models\DocEventSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use common\models\DocEvent;

$this->title = 'Event list';

$this->params['breadcrumbs'][] = ['label' => 'Event list'];

?>

<div class="event-list-page">

    <?php IBox::begin([
        'title' => 'Event list'
    ]) ?>

    <?= Html::a('<i class="fa fa-calendar"></i> Create Event', ['create'], ['class' => 'btn btn-xl btn-success mb-2']); ?>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'id' => 'events-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $filterModel,
        'summary' => false,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'rowOptions' => function (DocEvent $model) {
            return ['class' => $model->isPublished() ? 'event-published' : ''];
        },
        'columns' => [
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function (DocEvent $model) {
                    return Html::a(Html::encode($model->title), [$this->params['userRole'] . '/event-statistic/view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'description',
                'content' => function (DocEvent $model) {
                    if ($model->description != '') {
                        return StringHelper::truncateWords($model->description, 10);
                    }
                    return '-';
                }
            ],
            'start_date',
            [
                'class' => \yii\grid\ActionColumn::class,
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        /* @var $model DocEvent */
                        $icon = $model->isPublished() ? 'eye' : 'pencil';
                        return Html::a('<i class="fa fa-' . $icon . '"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm btn-view',
                            'title' => $model->isPublished() ? 'View Event' : 'Edit Event'
                        ]);
                    },
                    'delete' => function ($url, DocEvent $model) {
                        if ($model->status == DocEvent::STATUS_PUBLISHED) {
                            return '';
                        }
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'event-delete-btn btn btn-primary btn-sm',
                            'title' => 'Delete Event',
                            'data-confirm' => 'Are you sure you want to delete this event?'
                        ]);
                    },
                ],
            ]
        ]
    ]) ?>

    <?php IBox::end() ?>
</div>

<?php

$script = <<<JS
    
    // prevents published event from deleting
    $('body').on('click', 'tr.event-published .event-delete-btn', function(e) {
        toastr.warning('Published event cannot be deleted.');
        return false;
    });

JS;
$this->registerJs($script);

?>
