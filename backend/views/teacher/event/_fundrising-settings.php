<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\DocEvent
 */

use common\models\DocEvent;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\number\NumberControl;

$numericInputOptions = [
    'maskedInputOptions' => [
        //'prefix' => '$ ',
        'allowMinus' => false,
        'alias' => 'numeric',
        'digits' => 2,
        'groupSeparator' => ',',
        'autoGroup' => true,
        'autoUnmask' => true
    ],
    'displayOptions' => [
        'class' => 'text-left form-control'
    ]
];

?>

<div class="panel-body">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'id' => 'fundraising-form',
        'fieldConfig' => [
            'template' => '<div class="col-sm-6 pt-15">{label}</div><div class="col-sm-6">{input}{error}</div>'
        ]
    ]); ?>

    <div class="col-sm-12 col-md-10 col-lg-8">
        <?= $form
            ->field($model, 'fundraising_message', [
                'template' => '{input}'
            ])
            ->textarea(['placeholder' => 'Optional message', 'rows' => 7])
            ->label('') ?>

    </div>

    <div class="clearfix"></div>

    <div class="col-sm-6 col-lg-4">
        <?= $form
            ->field($model, 'fundraising_min')
            ->widget(NumberControl::class, $numericInputOptions)
            ->label('Minimum, $') ?>
    </div>

    <div class="col-sm-6 pt-15 col-lg-8">
        use 0 for unlimited
    </div>

    <div class="clearfix"></div>

    <div class="col-sm-6 col-lg-4">
        <?= $form
            ->field($model, 'fundraising_max')
            ->widget(NumberControl::class, $numericInputOptions)
            ->label('Maximum, $') ?>
    </div>

    <div class="col-sm-6  pt-15 col-lg-8">
        use 0 for unlimited
    </div>

    <div class="clearfix"></div>

    <div class="col-sm-6  col-lg-4">

        <?= $form
            ->field($model, 'fundraising_step')
            ->widget(NumberControl::class, $numericInputOptions)
            ->label('step, $') ?>
    </div>

    <div class="col-sm-6 pt-15 col-lg-8">
        use 0 to allow user enter any amount between min and max
    </div>

    <div class="col-xs-12 mt-2">

        <?php if ($model->status !== DocEvent::STATUS_PUBLISHED) : ?>
            <?= Html::button('Save', [
                'class' => 'event-save-btn ladda-button btn btn-xl btn-success mr-125',
                'data-style' => 'zoom-out'
            ]) ?>
        <?php endif; ?>

        <?php if ($model->status === DocEvent::STATUS_SAVED) : ?>
            <?= Html::a('Publish', '#', [
                'class' => 'event-publish-btn ladda-button btn btn-xl btn-success mr-125',
                'data-style' => 'zoom-out'
            ]); ?>
        <?php endif; ?>

        <a href="<?= Url::toRoute(['index']) ?>" class="btn btn-xl btn-primary">Cancel</a>

    </div>


    <?php ActiveForm::end(); ?>

</div>
