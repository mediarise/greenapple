<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\DocEvent
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $filterModel \yii\base\Model
 * @var array $checkedFormIds
 * @var array $mandatoryFormIds
 * @var array $linkedEventProduct
 * @var $visibilitySection \common\models\EventVisibilitySection
 *
 * @var $studentsFilterModel \common\models\teacher\event\StudentSearch
 * @var $studentsDataProvider \yii\data\ActiveDataProvider
 *
 * @var $gradesFilterModel \common\models\teacher\event\GradeSearch
 * @var $gradesDataProvider \yii\data\ActiveDataProvider
 *
 * @var $visibilityGroupsFilterModel \common\models\teacher\event\VisibilityGroupSearch
 * @var $visibilityGroupsDataProvider \yii\data\ActiveDataProvider
 *
 * @var $formsFilterModel \common\models\teacher\event\ProductSearch
 * @var $formsDataProvider \yii\data\ActiveDataProvider
 *
 * @var $productsFilterModel \common\models\teacher\event\FormSearch
 * @var $productsDataProvider \yii\data\ActiveDataProvider
 *
 * @var $previousUrl string
 *
 * @var $newlyCreatedForm int|null
 * @var $newlyCreatedProduct int|null
 *
 * @var $remainderForm \common\models\teacher\event\ReminderForm
 */

use common\components\BitHelper;
use common\helpers\ArrayHelper;
use common\helpers\BackendUrl;
use common\models\DocEventForm;
use common\models\EventVisibilitySection;
use frontend\widgets\IBox;
use yii\bootstrap\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->isNewRecord ? 'Create event' : 'Update event';
$this->params['breadcrumbs'][] = ['label' => 'Event list', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

    <div class="event-form-page">
        <div class="tabs-container">

            <?php IBox::begin(['title' => 'Event list']); ?>

            <?php
            $items = [
                [
                    'label' => '<i class="fa fa-info-circle"></i>Event info',
                    'encode' => false,
                    'active' => true,
                    'headerOptions' => [
                        'id' => 'main-tab-header',
                        'class' => 'event-form-tab-header'
                    ],
                    'content' => $this->render('_info', [
                        'model' => $model,
                        'visibilitySection' => $visibilitySection,
                    ])
                ]
            ];

            if (!$model->isNewRecord) {
                $items = array_merge($items, [
                    [
                        'label' => '<i class="fa fa-eye"></i>Participants',
                        'encode' => false,
                        'headerOptions' => [
                            'id' => 'visibility-section-tab-header',
                            'data-target-tab' => '#students-tab',
                            'class' => 'event-form-tab-header'
                        ],
                        'content' => $this->render('_visibility-sections', [
                            'model' => $model,
                            'visibilitySection' => $visibilitySection,
                            'studentsFilterModel' => $studentsFilterModel,
                            'studentsDataProvider' => $studentsDataProvider,
                            'gradesFilterModel' => $gradesFilterModel,
                            'gradesDataProvider' => $gradesDataProvider,
                            'visibilityGroupsFilterModel' => $visibilityGroupsFilterModel,
                            'visibilityGroupsDataProvider' => $visibilityGroupsDataProvider,
                        ])
                    ],
                ]);

                //      Products Tab
                if (!BitHelper::testBits($model->type,
                    DocEventForm::EVENT_TYPE_FILL_OUT_FORM |
                    DocEventForm::EVENT_TYPE_FUNDRAISING)) {
                    $items[] = [
                        'label' => '<i class="fa fa-credit-card"></i>Products',
                        'encode' => false,
                        'active' => false,
                        'headerOptions' => [
                            'id' => 'products-tab-header',
                            'data-target-tab' => '#products-tab',
                            'class' => 'event-form-tab-header'
                        ],
                        'content' => $this->render('_product', [
                            'model' => $model,
                            'dataProvider' => $productsDataProvider,
                            'filterModel' => $productsFilterModel
                        ])
                    ];
                }

                //      Forms Tab
                if (!BitHelper::testBits($model->type,
                    DocEventForm::EVENT_TYPE_NUTRITION |
                    DocEventForm::EVENT_TYPE_NUTRITION_ONE_TIME |
                    DocEventForm::EVENT_TYPE_SELL_PRODUCT |
                    DocEventForm::EVENT_TYPE_STOCK_THE_CLASSROOM |
                    DocEventForm::EVENT_TYPE_FUNDRAISING |
                    DocEventForm::EVENT_TYPE_FUNDRAISING_SELL)) {

                    $items[] = [
                        'label' => '<i class="fa fa-file-text-o"></i>Forms',
                        'encode' => false,
                        'active' => false,
                        'headerOptions' => [
                            'id' => 'forms-tab-header',
                            'data-target-tab' => '#forms-tab',
                            'class' => 'event-form-tab-header'
                        ],
                        'content' => $this->render('_forms', [
                            'model' => $model,
                            'dataProvider' => $formsDataProvider,
                            'filterModel' => $formsFilterModel
                        ])
                    ];
                }

                if (BitHelper::testBits($model->type, DocEventForm::EVENT_TYPE_FUNDRAISING)) {
                    $items[] = [
                        'label' => '<i class="fa fa-file-text-o"></i>Fundraising settings',
                        'encode' => false,
                        'active' => false,
                        'headerOptions' => [
                            'id' => 'fundraising-tab-header',
                            'data-target-tab' => '#fundraising-tab',
                            'class' => 'fundraising-tab-header'
                        ],
                        'content' => $this->render('_fundrising-settings', [
                            'model' => $model,
                        ])
                    ];
                }

                $items[] = [
                    'label' => '<i class="fa fa-bell-o"></i>Reminders',
                    'encode' => false,
                    'active' => false,
                    'headerOptions' => [
                        'id' => 'remainders-tab-header',
                        'data-target-tab' => '#remainders-tab',
                        'class' => 'event-remainder-tab-header'
                    ],
                    'content' => $this->render('_reminders', [
                        'model' => $remainderForm,
                        'eventModel' => $model,
                    ])
                ];
            }
            ?>

            <?= Tabs::widget(['id' => 'event-form-tab', 'items' => $items]) ?>

            <?php IBox::end() ?>

        </div>
    </div>

<?php

// switching to previous form tab

switch (true) {
    case ((bool)strpos($previousUrl, 'form/create')):
        $previousTab = '#forms-tab-header';
        $this->registerJsVar('newlyCreatedElementId', $newlyCreatedForm ?? null);
        break;
    case ((bool)strpos($previousUrl, 'product/create')):
        $previousTab = '#products-tab-header';
        $this->registerJsVar('newlyCreatedElementId', $newlyCreatedProduct ?? null);
        break;
    case ((bool)$visibilitySection->newlyCreatedVisibilityGroup):
        $previousTab = '#visibility-section-tab-header';
        $this->registerJsVar('newlyCreatedElementId', $visibilitySection->newlyCreatedVisibilityGroup);
        break;
}

$this->registerJsVar('isVisibilityGroupTab', $visibilitySection->newlyCreatedVisibilityGroup);
$this->registerJsVar('previousTab', $previousTab ?? null);

$filterBy = EventVisibilitySection::FILTER_BY_VISIBILITY_GROUPS;

$script = <<<JS

    // switching to previous form tab
    if (previousTab) {
        $(previousTab + ' a').tab('show');
        if (isVisibilityGroupTab) {
            $('#visibility-sections-filter').val('$filterBy').trigger('change');
            var activeTab = $('#groups-tab');
        } else {
            window.activeTab = previousTab;
        }
        checkNewlyCreatedFormElement(newlyCreatedElementId);
    }
    
    function checkNewlyCreatedFormElement(id) {
        activeTabPane = (window.activeTab).replace('-header', '');
        console.log(activeTabPane);
        console.log($(activeTabPane));
        $(activeTabPane)
            .find('input.checkbox-control.is-checked[value="' + id + '"]')
            .prop('checked', true)
            .trigger('change');
    }
    
JS;
$this->registerJs($script);

?>