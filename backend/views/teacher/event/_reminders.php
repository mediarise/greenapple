<?php
/**
 * @var $this \yii\web\View
 * @var $model ReminderForm
 * @var $eventModel \common\models\DocEvent
 */

use common\models\DocEvent;
use common\models\teacher\event\ReminderForm;
use common\widgets\Select2;
use frontend\widgets\HtmlEditor;
use kartik\touchspin\TouchSpin;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


?>

    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'reminder-form'
        ]) ?>

        <?= $form->field($model, 'eventId')->hiddenInput(['value' => $eventModel->id])->label(false) ?>

        <div class="row">
            <div class="col-lg-5">
                <?= $form->field($model, 'forPendingEvents')->checkbox() ?>

                <div class="row">
                    <div class="col-sm-5">
                        <?= $form->field($model, 'pendingInterval')
                            ->widget(TouchSpin::class, [
                                'options' => [
                                    'class' => 'text-center',
                                ],
                                'pluginOptions' => [
                                    'buttonup_class' => 'input-group-addon on-right',
                                    'buttondown_class' => 'input-group-addon on-left',
                                    'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                                    'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                                ],
                            ])
                            ->label(false) ?>

                    </div>
                    <div class="col-sm-7">
                        <?= $form->field($model, 'pendingIntervalUnit')
                            ->widget(Select2::class, [
                                'data' => ReminderForm::getDurationUnitLabels(),
                                'hideSearch' => true,
                                'options' => [
                                    'placeholder' => 'Select interval'
                                ],
                            ])
                            ->label(false) ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <?= $form->field($model, 'textForPending')
                    ->widget(HtmlEditor::class, [
                        'enableDefaultSettings' => false,
                        'previewEventTitle' => $eventModel->title,
                    ])
                    ->label(false) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-5">
                <?= $form->field($model, 'forUpcomingEvents')->checkbox() ?>


                <div class="row">
                    <div class="col-sm-5">
                        <?= $form->field($model, 'upcomingInterval')
                            ->widget(TouchSpin::class, [
                                'options' => [
                                    'class' => 'text-center',
                                ],
                                'pluginOptions' => [
                                    'buttonup_class' => 'input-group-addon on-right',
                                    'buttondown_class' => 'input-group-addon on-left',
                                    'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                                    'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                                ],
                            ])
                            ->label(false) ?>

                    </div>
                    <div class="col-sm-7">
                        <?= $form->field($model, 'upcomingIntervalUnit')
                            ->widget(Select2::class, [
                                'data' => ReminderForm::getDurationUnitLabels(),
                                'hideSearch' => true,
                                'options' => [
                                    'placeholder' => 'Select interval'
                                ],
                            ])
                            ->label(false) ?>
                    </div>
                </div>
            </div>

            <div class="col-lg-7">
                <?= $form->field($model, 'textForUpcoming')
                    ->widget(HtmlEditor::class, [
                        'enableDefaultSettings' => false,
                        'previewEventTitle' => $eventModel->title,
                    ])
                    ->label(false) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12 mt-2">

                <?php if ($eventModel->status !== DocEvent::STATUS_PUBLISHED) : ?>
                    <?= Html::button('Save', [
                        'class' => 'event-save-btn ladda-button btn btn-xl btn-success mr-125',
                        'data-style' => 'zoom-out'
                    ]) ?>
                <?php endif; ?>

                <?php if ($eventModel->status === DocEvent::STATUS_SAVED) : ?>
                    <?= Html::a('Publish', '#', [
                        'class' => 'event-publish-btn ladda-button btn btn-xl btn-success mr-125',
                        'data-style' => 'zoom-out'
                    ]); ?>
                <?php endif; ?>

                <a href="<?= Url::toRoute(['index']) ?>" class="btn btn-xl btn-primary">Cancel</a>

            </div>
        </div>

        <?php ActiveForm::end() ?>
    </div>


<?php
$this->registerJsVar('forPendingId', Html::getInputId($model, 'forPendingEvents'));
$this->registerJsVar('pendingIntervalId', Html::getInputId($model, 'pendingInterval'));
$this->registerJsVar('pendingIntervalUnitId', Html::getInputId($model, 'pendingIntervalUnit'));
$this->registerJsVar('forUpcomingId', Html::getInputId($model, 'forUpcomingEvents'));
$this->registerJsVar('upcomingIntervalId', Html::getInputId($model, 'upcomingInterval'));
$this->registerJsVar('upcomingIntervalUnitId', Html::getInputId($model, 'upcomingIntervalUnit'));

$script = <<<JS
    $('body')
    .on('change', '#' + pendingIntervalId, function() {
        chooseForPending();
    })
    .on('change', '#' + pendingIntervalUnitId, function() {
        chooseForPending();
    })
    .on('change', '#' + upcomingIntervalId, function() {
        chooseForUpcoming();
    })
    .on('change', '#' + upcomingIntervalUnitId, function() {
        chooseForUpcoming();
    });

    function chooseForPending() {
        $('#' + forPendingId).prop('checked', true);
    }
    
    function chooseForUpcoming() {
        $('#' + forUpcomingId).prop('checked', true);
    }
JS;

$this->registerJs($script);
?>