<?php
return [
    [
        'label' => 'Home',
        'iconClass' => 'fa fa-home',
        'url' => ['/teacher/index']
    ],
    [
        'label' => 'Events',
        'iconClass' => 'fa fa-calendar',
        'url' => ['/teacher/event/index']
    ],
    [
        'label' => 'Products',
        'iconClass' => 'fa fa-shopping-cart',
        'url' => ['/teacher/product/index']
    ],
    'forms' => [
        'label' => 'Forms',
        'iconClass' => 'fa fa-file-text-o',
        'url' => ['/teacher/form/index'],
    ],
    [
        'label' => 'Visibility groups',
        'iconClass' => 'fa fa-eye',
        'url' => ['/teacher/visibility-group/index']
    ],
    'reports' => [
        'label' => 'Reports',
        'iconClass' => 'fa fa-line-chart',
        'items' => [
            [
                'label' => 'Sales report',
                'url' => ['/teacher/report/sales'],
            ],
            [
                'label' => 'Daily sales report',
                'url' => ['/teacher/report/daily-sales'],
            ]
        ]
    ],
];