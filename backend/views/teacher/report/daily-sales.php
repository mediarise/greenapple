<?php

/** @var View $this */
/** @var array|null $sales */
/** @var array $predefinedPeriods */
/** @var string $startDate */
/** @var array $umbrellaAccounts */

/** @var string $endDate */

use common\helpers\BackendUrl;
use common\models\DocEvent;
use common\widgets\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use backend\assets\VuetifulAsset;
use frontend\widgets\IBox;
use kartik\select2\Select2Asset;
use kartik\select2\ThemeDefaultAsset;
use kartik\touchspin\TouchSpinAsset;
use dosamigos\datepicker\DatePickerAsset;

Select2Asset::register($this);
ThemeDefaultAsset::register($this);
TouchSpinAsset::register($this);
DatePickerAsset::register($this);

VuetifulAsset::register($this);
$this->registerJsFile('js/vue/jquery-wrappers.js', ['depends' => [VuetifulAsset::class]]);

$this->title = 'Daily Sales Report';
$this->params['breadcrumbs'][] = ['label' => 'Reports'];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

$filterByDateUrl = BackendUrl::toRoute(['/teacher/report/daily-sales']);

$this->registerJsVar('predefinedPeriod', $predefinedPeriod, View::POS_END);
$this->registerJsVar('startDate', $startDate, View::POS_END);
$this->registerJsVar('endDate', $endDate, View::POS_END);
$this->registerJsVar('predefinedPeriods', $predefinedPeriods, View::POS_END);
$this->registerJsVar('filterByDateUrl', $filterByDateUrl, View::POS_END);
$this->registerJsVar('sales', $sales, View::POS_END);
$this->registerJsVar('umbrellaAccounts', $umbrellaAccounts, View::POS_END);
$this->registerJsVar('ledgerAccountsUrl', Url::toRoute(['teacher/report/get-ledger-accounts']), View::POS_END);

$this->registerJs(<<<JS

    var withRange = dateFns.isWithinRange;
    var parse = dateFns.parse;
    var format = dateFns.format;
    var isBefore = dateFns.isBefore
    
    var getDay = dateFns.getDay;
    var getWeek = dateFns.getWeek;
    var getMonth = dateFns.getMonth;
    var getYear = dateFns.getYear;
    var addDays = dateFns.addDays;
    var addWeeks = dateFns.addWeeks;
    var addMonths = dateFns.addMonths;
    var addYears = dateFns.addYears;
    var startOfWeek = dateFns.startOfWeek;
    var endOfWeek = dateFns.endOfWeek;
    var startOfMonth = dateFns.startOfMonth
    var endOfMonth = dateFns.endOfMonth
    var startOfYear = dateFns.startOfYear
    var endOfYear = dateFns.endOfYear
    
    var outputDateFormat = 'MM/DD/YYYY';
    
    new Vue({
        el: "#sale-report",
        components: {
            scrollbar: Vue2Scrollbar
        },
        data: function() {
            return {
                styling: {
                  scrollbar: {
                    minWidth: '650px',
                    maxHeight: '650px'
                  },
                },
                sales: sales,
                searchString: '',
                umbrellaAccount: '',
                umbrellaAccounts: umbrellaAccounts,
                ledgerAccount: '',
                ledgerAccounts: [],
                predefinedPeriods: predefinedPeriods,
                predefinedPeriod: predefinedPeriod,
                startDate: format(parse(startDate), outputDateFormat),
                endDate: format(parse(endDate), outputDateFormat)
            };
        },
        methods: {
            formatCurrency(value){
                return "$" + parseFloat(value).toFixed(2) 
            },
            formatDate(value){
                return format(value, outputDateFormat);
            },
            formatProductVariation(value){
                return value || '-';
            },
            isRelevant(sale){
                var searchString = this.searchString.toLowerCase();
                return _.some(_.filter(_.values(sale), function(value) {
                    return _.toString(value).toLowerCase().indexOf(searchString) > -1;
                }));
            },
            filterByPredefinedPeriod(period) {
                if (!period) {
                    return;
                }
                weekStartsOn = {weekStartsOn: 1};
                
                var today = new Date();
                var yesterday = addDays(today, -1);
                var weekAgo = addWeeks(today, -1);
                var monthAgo = addMonths(today, -1);
                var yearAgo = addYears(today, -1);
                
                switch(period.toLowerCase()) {
                    case 'yesterday': 
                        period = [yesterday, yesterday];
                        break;
                    case 'this week': 
                        period = [startOfWeek(today, weekStartsOn), endOfWeek(today, weekStartsOn)];
                        break;
                    case 'last week': 
                        period = [startOfWeek(weekAgo, weekStartsOn), endOfWeek(weekAgo, weekStartsOn)];
                        break;
                    case 'this month': 
                        period = [startOfMonth(today), endOfMonth(today)];
                        break;
                    case 'last month': 
                        period = [startOfMonth(monthAgo), endOfMonth(monthAgo)];
                        break;
                    case 'this year': 
                        period = [startOfYear(today), endOfYear(today)];
                        break;
                    case 'last year':
                        period = [startOfYear(yearAgo), endOfYear(yearAgo)];
                        break;
                    default:
                        period = [today, today];
                }
                this.startDate = format(period[0], outputDateFormat);
                this.endDate = format(period[1], outputDateFormat);
            },
            datePicked() {
                if (!this.isDateRangeValid()) {
                    return;
                }
                this.predefinedPeriod = null;
            },
            filterByDate() {
                if (!this.isDateRangeValid()) {
                    return;
                }
                    
                var vm = this;
                var data = {
                    startDate: vm.startDate,
                    endDate: vm.endDate
                };
                
                var filterByDateLadda = Ladda.create(document.getElementById('filter-by-date'));
                filterByDateLadda.start();
                
                //noinspection JSUnresolvedVariable
                greenApple.ajax.perform(filterByDateUrl , data, {
                    resultSuccess: function(data) {
                        vm.sales = data.sales;
                        filterByDateLadda.stop();
                    },
                    complete: function() {
                        filterByDateLadda.stop();
                    }
                });
            },
            filterByUmbrellaAccount(umbrellaAccount) {
                var vm = this;
                var data = {
                    umbrellaAccount: umbrellaAccount
                };
                
                //noinspection JSUnresolvedVariable
                greenApple.ajax.perform(filterByDateUrl , data, {
                    resultSuccess: function(data) {
                        vm.sales = data.sales;
                    }
                });
            },
            filterByLedgerAccount(ledgerAccount) {
                var vm = this;
                var data = {
                    ledgerAccount: ledgerAccount
                };
                
                //noinspection JSUnresolvedVariable
                greenApple.ajax.perform(filterByDateUrl , data, {
                    resultSuccess: function(data) {
                        vm.sales = data.sales;
                    }
                });
            },
            isDateRangeValid() {
                var isDatesSelected = _.isString(this.startDate) && _.isString(this.endDate);
                if (isDatesSelected && isBefore(this.endDate, this.startDate)) {
                    greenApple.alert.warning('Start date must be early than end date.');
                    return false;
                }
                return true;
            },
            loadLedgerAccounts() {
                var vm = this;
                var data = {
                    umbrellaAccount: vm.umbrellaAccount
                };

                $.post(
                    ledgerAccountsUrl, 
                    data, 
                    function(data) {
                        vm.ledgerAccounts = data;
                        if (data.length > 0) {
                            vm.ledgerAccount = data[0]['id'];
                        }
                    },
                    'json'
                );
            }
        },
        computed: {
            total: function() {
                return _.sumBy(this.filtered, function(o) { return parseFloat(o.total) || 0; }).toFixed(2);
            },
            filtered: function() {
                var self = this;
                return this.sales.filter(function(sale) {
                    return self.searchString.length ? self.isRelevant(sale) : true;
                });
            },
            itemsCounter: function() {
                return this.filtered.length + ' of ' + this.sales.length;
            }
        }
    });

JS
    , View::POS_READY);

?>

<?php IBox::begin(['title' => 'Sales report']); ?>

<div id="sale-report">

    <div class="row mb mt">
        <div class="col-md-3">
            <label>Period</label>
            <select2
                    v-model="predefinedPeriod"
                    :options="predefinedPeriods"
                    placeholder="Select Period"
                    @select2-selected="filterByPredefinedPeriod(predefinedPeriod)">
            </select2>
        </div>
        <div class="col-md-3 mb">
            <label>Date from</label>
            <datepicker
                    v-model="startDate"
                    placeholder="Date from"
                    css-class="form-control"
                    @date-picked="datePicked()">
            </datepicker>
        </div>
        <div class="col-md-3 mb">
            <label>Date to</label>
            <datepicker
                    v-model="endDate"
                    placeholder="Date to"
                    css-class="form-control"
                    @date-picked="datePicked()">
            </datepicker>
        </div>
        <div class="col-md-2 mb">
            <?= Html::button('Filter by Date', [
                'id' => 'filter-by-date',
                'class' => 'btn ladda-button btn-xl btn-primary mt-275',
                'data-style' => 'zoom-out',
                '@click' => 'filterByDate',
            ]); ?>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-6">
            <label>Search</label>
            <input v-model="searchString" placeholder="Search" class="form-control">
        </div>

        <div class="col-md-3 mb">
            <label>Umbrella account</label>
            <select2
                    v-model="umbrellaAccount"
                    :options="umbrellaAccounts"
                    placeholder="Select umbrella account"
                    @select2-selected="filterByUmbrellaAccount(umbrellaAccount); loadLedgerAccounts()"
                    :allow-clear="true">
            </select2>
        </div>

        <div class="col-md-3 mb form-group">
            <label>Account</label>
            <select2
                    v-model="ledgerAccount"
                    :options="ledgerAccounts"
                    placeholder="Select account"
                    @select2-selected="filterByLedgerAccount(ledgerAccount)"
                    :allow-clear="true">
            </select2>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="vue-data-grid">

                <div class="row report-fixed-header mx-0">
                    <div class="report-total col-md-6"><b>Total:</b> ${{total}}</div>
                    <div class="report-summary col-md-6">{{itemsCounter}} items</div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php \frontend\widgets\Simplebar::begin() ?>

                        <datatable :source="filtered" :editable="false" :filterable="false" style="min-width: 620px">
                            <datatable-column id="transaction_date" :formatter="formatDate" style="max-width: 110px">
                                Date
                            </datatable-column>
                            <datatable-column id="umbrella_account">Umbrella account</datatable-column>
                            <datatable-column id="ledger_account">Account</datatable-column>
                            <datatable-column id="total" :formatter="formatCurrency">Total</datatable-column>
                        </datatable>

                        <?php \frontend\widgets\Simplebar::end() ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<?php IBox::end() ?>


