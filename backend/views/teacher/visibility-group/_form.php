<?php

use common\models\VisibilityGroup;
use frontend\widgets\CheckBox;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Json;
use common\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Teacher;
use common\widgets\Select2;
use frontend\widgets\DualListBox;
use function Stringy\create as s;

/**
 * @var $this yii\web\View
 * @var $model common\models\teacher\VisibilityGroupForm
 * @var $form yii\widgets\ActiveForm
 * @var array $gradeList
 * @var array $studentList
 * @var array $currentYearGrades
 * @var $memberedGradeList
 * @var $haveGradeMember
 * @var $memberedCurrentYearGradeList
 */

$dualListBoxId = 'dual-list-box';
$dualListBox = (string)s($dualListBoxId)->camelize();

if (!$model->grade_id) {
    $model->grade_id = ArrayHelper::getFirstKey($gradeList);
}

$studentListByGrade = $studentList[$model->grade_id];
$plainStudentList = ArrayHelper::map(array_merge(...$studentList), 'id', 'full_name');
?>

    <div class="visibility-group-form">


        <?php $form = ActiveForm::begin([
            'validateOnChange' => false,
            'validateOnBlur' => false
        ]); ?>

        <div class="row pt-2">
            <div class="col-xl-8 col-lg-9 tabs-container">
                <?= $form->field($model, 'name')->textInput()->label('Group name *') ?>

                <div class="hidden">
                    <?php $memberType = $haveGradeMember ? VisibilityGroup::MEMBER_TYPE_GRADE : VisibilityGroup::MEMBER_TYPE_STUDENT ?>
                    <?= $form->field($model, 'member_type')->hiddenInput(['value' => $memberType])->label(false) ?>
                </div>

                <?= Html::checkbox('only-current-year', false, [
                    'id' => 'only-current-year-grades',
                    'label' => 'Only current year',
                ]) ?>

                <?= Tabs::widget(['id' => 'vb-form-tab', 'items' => [
                    [
                        'label' => 'Students',
                        'encode' => false,
                        'active' => !$haveGradeMember,
                        'headerOptions' => [
                            'id' => 'students-tab-header',
                            'data-member-type' => VisibilityGroup::MEMBER_TYPE_STUDENT,
                        ],
                        'content' => $this->render('_form-students', [
                            'gradeList' => $gradeList,
                            'currentYearGrades' => $currentYearGrades,
                            'studentList' => $studentList,
                            'studentListByGrade' => $studentListByGrade,
                            'form' => $form,
                            'model' => $model,
                            'dualListBox' => $dualListBox,
                            'plainStudentList' => $plainStudentList,
                            'dualListBoxId' => $dualListBoxId,

                        ]),
                    ], [
                        'label' => 'Grades',
                        'encode' => false,
                        'active' => $haveGradeMember,
                        'headerOptions' => [
                            'id' => 'grades-tab-header',
                            'data-member-type' => VisibilityGroup::MEMBER_TYPE_GRADE,
                        ],
                        'content' => $this->render('_form-grades', [
                            'gradeList' => $gradeList,
                            'form' => $form,
                            'model' => $model,
                            'memberedGradeList' => $memberedGradeList,
                            'currentYearGrades' => $currentYearGrades,
                            'memberedCurrentYearGradeList' => $memberedCurrentYearGradeList,
                        ]),
                    ]
                ]]) ?>
            </div>
        </div>


        <div class="hr-line-dashed"></div>

        <div class="row">
            <div class="col-xl-8 col-lg-9">
                <?= Html::submitButton('Save', [
                    'class' => 'btn btn-xl btn-success mr-125',
                    'id' => 'save-group-btn',
                    'data-style' => "zoom-out"
                ]); ?>

                <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-xl btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<?php
$formId = $form->id;
$script = <<<JS
var saveBtn = $('#save-group-btn').ladda();

$('#{$formId}')
    .on("beforeValidate", function() {
        saveBtn.ladda('start').attr('disabled', true);
    })
    .on("afterValidate", function(event, message, errorAttributes) {
        if (errorAttributes.length > 0 ) {
            saveBtn.ladda('stop');
        }
    })
    .on('click', '#vb-form-tab > li', function() {
        $('#visibilitygroupform-member_type').val($(this).attr('data-member-type'));
    });
JS;
$this->registerJs($script);
?>