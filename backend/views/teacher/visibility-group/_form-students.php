<?php
/**
 * @var $this \yii\web\View
 * @var $gradeList
 * @var $currentYearGrades
 * @var $studentList
 * @var $studentListByGrade
 */

use common\widgets\Select2;
use frontend\widgets\DualListBox;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

?>

<div class="panel-body">
    <div class="">
        <?php if (count($gradeList) > 1) : ?>

            <?php
            $script = "
                var currentYearGrades = " . Json::encode($currentYearGrades) . "
                var allGrades = " . Json::encode($gradeList) . "
                $('body').on('change', '#only-current-year-grades', function() {
                    if ($(this).prop('checked')) {
                        setGradeList(currentYearGrades);
                    } else {
                        setGradeList(allGrades);
                    }
                });
                
                function setGradeList(gradeList) {
                    $('#vg-type-switcher option').remove();
                    var options = document.createDocumentFragment();
                    $.each(gradeList, function(key, grade) {
                        options.appendChild(new Option(grade, key, ));
                    });
                    $('#vg-type-switcher').append(options).change();
                }
                ";
            $this->registerJs($script);
            ?>

            <?= $form->field($model, 'grade_id', [
                'enableClientValidation' => true,
                'validateOnChange' => false
            ])->widget(Select2::class, [
                'name' => 'grade_list',
                'data' => $gradeList,
                'options' => [
                    'id' => 'vg-type-switcher',
                    'placeholder' => 'Select grade',
                ]
            ])->label('Grade') ?>

        <?php else: ?>
            <?= $form->field($model, 'grade_id')->hiddenInput()->label(false) ?>
        <?php endif; ?>

        <?php
        $this->registerJs("
                    var studentListByGrades = " . Json::encode($studentList) . ";   
                    refreshStudentList(" . Json::encode($studentListByGrade) . ");
                    
                    $('#vg-type-switcher').on('change', function() {
                        studentList = studentListByGrades[$(this).val()];
                        refreshStudentList(studentList);
                    });
                    function refreshStudentList(studentList) {
                        $dualListBox.find('option').remove();
                        var options = document.createDocumentFragment();
                        $.each(studentList, function(key, student) {
                            var isMember = student.is_member;
                            options.appendChild(new Option(student.name, student.id, isMember, isMember));
                        });
                        $dualListBox.append(options).bootstrapDualListbox('refresh', true);
                    }
                ", View::POS_READY);
        ?>
    </div>

    <div class="list-box-area mt-2">

        <?= $form->field($model, 'students', [
            'enableClientValidation' => false,
            'validateOnChange' => false
        ])->widget(DualListBox::class, [
            'items' => $plainStudentList,
            'options' => [
                'id' => $dualListBoxId,
                'multiple' => true,
                'size' => 10,
            ],
            'clientOptions' => [
                'selectedListLabel' => 'Group members',
                'nonSelectedListLabel' => 'Students',
            ],
        ])->label(false) ?>


    </div>
</div>
