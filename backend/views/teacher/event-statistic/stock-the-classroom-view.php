<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\DocEvent
 * @var $donateProgress
 * @var $studentStatusDataProvider
 */

use common\models\teacher\statistics\StockTheClassroomEventStatistic;
use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\helpers\Url;

$this->title = $model->title;

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => 'Event Statistics'];
?>

<div class="event-statistic-page">

    <?= $this->render('_header', [
            'eventModel' => $model,
            'photoUrl' => null,
    ])?>

    <?php IBox::begin(['title' => 'Event Statistics']); ?>

    <div class="p-2">
        <div class="pull-left"><b>Progress:</b></div>
        <div class="pull-left pl-2">
            <?php foreach ($donateProgress as $product) : ?>
                <p><?= $product['title'] ?>: <?= (int)$product['quantity'] ?>/<?= (int)$product['max_quantity'] ?></p>
            <?php endforeach; ?>
        </div>
    </div>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $studentStatusDataProvider,
        'summary' => false,
        'columns' => [
            [
                'label' => 'Student',
                'content' => function (StockTheClassroomEventStatistic $data) {
                    return $data->student_name;
                }
            ],
            [
                'label' => 'Product',
                'content' => function (StockTheClassroomEventStatistic $data) {
                    return implode('<br>', $data->products);
                }
            ],
        ]
    ]) ?>
    <?php IBox::end(); ?>
</div>
