<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\DocEvent
 * @var $statisticDataProvider \yii\data\ArrayDataProvider
 */

use common\components\Formatter;
use common\models\teacher\statistics\NutritionChildEventStatistic;
use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\helpers\Url;

$this->title = $model->title;

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => 'Parent event', 'url' => ['view', 'id' => $model->predecessor]];
$this->params['breadcrumbs'][] = ['label' => 'Event statistics'];
?>

<div class="event-statistic-page">
    <?= $this->render('_header', [
        'eventModel' => $model,
        'photoUrl' => $photoUrl,
    ]) ?>

    <?php IBox::begin(['title' => 'Event Statistics']); ?>
    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $statisticDataProvider,
        'summary' => false,
        'columns' => [
            [
                'label' => 'Date',
                'content' => function (NutritionChildEventStatistic $data) {
                    return Formatter::period($data->start_date, $data->end_date);
                }
            ],
            [
                'label' => 'Title',
                'content' => function (NutritionChildEventStatistic $data) {
                    return $data->title;
                }
            ],
            [
                'label' => 'Student',
                'content' => function (NutritionChildEventStatistic $data) {
                    return "{$data->first_name} {$data->last_name}";
                }
            ],
            [
                'label' => 'Product',
                'content' => function (NutritionChildEventStatistic $data) {
                    return Formatter::productName($data->product_name, $data->variation_name);
                }
            ],
            [
                'label' => 'Orders',
                'content' => function (NutritionChildEventStatistic $data) {
                    return Formatter::ratio($data->total, $data->min_quantity);
                }
            ],
            [
                'label' => 'Status',
                'content' => function (NutritionChildEventStatistic $data) {
                    return $data->getStatusLabel();
                }
            ],
        ]
    ]) ?>
    <?php IBox::end(); ?>
</div>

