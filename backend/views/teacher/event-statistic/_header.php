<?php
/**
 * @var $this \yii\web\View
 * @var \common\models\DocEvent $eventModel
 */

use common\components\Formatter;
use yii\helpers\Html;

$imgUrl = $eventModel->mainPhoto->url ?? $photoUrl ?? Yii::$app->params['eventPhotoPlaceholder'];
?>

<div class="ibox">
    <div class="ibox-content">

        <div class="table-responsive">
            <table class="table shoping-cart-table">
                <tbody>
                <tr>
                    <td width="90">
                        <?= Html::img($imgUrl, ['width' => '130px']) ?>
                    </td>
                    <td class="desc">
                        <h3>
                            <p class="text-navy">
                                <?= $eventModel->title ?>
                            </p>
                        </h3>

                        <p class="small">
                            <?= Formatter::shortDate($eventModel->start_date) ?>
                            - <?= Formatter::shortDate($eventModel->end_date) ?>
                        </p>

                        <p class="small">
                            <?php if ($eventModel->is_repeatable) : ?>
                                Repeats Every Weekday
                            <?php else: ?>
                                One time event
                            <?php endif; ?>
                        </p>

                        <p class="small">
                            <?= $eventModel->getEventTypeLabel() ?>
                        </p>
                    </td>

                    <td>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
