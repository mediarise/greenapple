<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\teacher\DocEvent
 * @var $studentStatusDataProvider \yii\data\ArrayDataProvider
 */

use common\models\teacher\statistics\FundraisingEventStatistic;
use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\helpers\Url;

$this->title = $model->title;

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => 'Event Statistics'];
?>

<div class="event-statistic-page">

    <?= $this->render('_header', [
        'eventModel' => $model,
        'photoUrl' => null,
    ])?>

    <?php IBox::begin(['title' => 'Event Statistics']); ?>
    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $studentStatusDataProvider,
        'summary' => false,
        'columns' => [
            [
                'label' => 'Student',
                'content' => function (FundraisingEventStatistic $data) {
                    return "{$data->first_name} {$data->last_name}";
                }
            ],
            [
                'label' => 'Donated',
                'content' => function (FundraisingEventStatistic $data) {
                    return $data->total;
                }
            ],
//            [
//                'label' => 'Status',
//                'content' => function ( FundraisingEventStatistic $data) {
//                    return $data->getStatusLabel();
//                }
//            ]
        ]
    ]) ?>
    <?php IBox::end(); ?>
</div>