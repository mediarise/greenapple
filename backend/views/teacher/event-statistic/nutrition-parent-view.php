<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\DocEvent
 * @var $statisticDataProvider \yii\data\ArrayDataProvider
 */

use common\components\Formatter;
use common\models\teacher\statistics\NutritionParentEventStatistic;
use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;

$this->params['homeLink'] = false;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => Url::home()];
$this->params['breadcrumbs'][] = ['label' => 'Event Statistics'];
?>

<div class="event-statistic-page">
    <?= $this->render('_header', [
        'eventModel' => $model,
        'photoUrl' => null,
    ]) ?>


    <?php IBox::begin(['title' => 'Event Statistics']); ?>
    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $statisticDataProvider,
        'summary' => false,
        'columns' => [
            [
                'label' => 'Date',
                'content' => function (NutritionParentEventStatistic $data) {
                    return Formatter::period($data->start_date, $data->end_date);
                }
            ],
            [
                'label' => 'Title',
                'content' => function (NutritionParentEventStatistic $data) {
                    return Html::a($data->title, ['/teacher/event-statistic/view', 'id' => $data->doc_event_id]);
                }
            ],
            [
                'label' => 'Orders',
                'content' => function (NutritionParentEventStatistic $data) {
                    return Formatter::ratio($data->products_purchased, $data->getTotalOrders());
                }
            ],
        ]
    ]) ?>
    <?php IBox::end(); ?>
</div>

