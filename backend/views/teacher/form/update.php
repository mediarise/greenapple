<?php
/**
 * @var $this View
 * @var $model Form
 * @var $returnUrl
 */

use common\models\Form;
use frontend\widgets\HtmlEditor;
use frontend\widgets\IBox;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = 'Update form';
$this->params['breadcrumbs'][] = ['label' => 'Form list', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Update form'];
?>

<div class="form-create-page">
    <?php IBox::begin([
        'title' => 'Update form'
    ]) ?>
    <?= $this->render('_form', [
        'model' => $model,
        'returnUrl' => $returnUrl,
    ]) ?>
    <?php IBox::end(); ?>
</div>