<?php
/**
 * @var $this View
 * @var $model Form
 * @var $returnUrl
 */

use common\models\Form;
use common\widgets\Select2;
use frontend\widgets\HtmlEditor;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

?>

    <div class="col-xs-12">

        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
            ],
        ]) ?>

        <?= Html::hiddenInput('returnUrl', $returnUrl) ?>

        <?= $form->field($model, 'title')->textInput(); ?>

        <?= $form->field($model, 'description')->textarea(); ?>

        <?= $form->field($model, 'html')->widget(HtmlEditor::class, []); ?>

        <?= $form->field($model, 'control')->widget(Select2::class, [
            'data' => Form::getControlFields()
        ]) ?>

        <?= Html::hiddenInput('returnUrl', $returnUrl) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-xl btn-success mr-125', 'id' => 'save-form-btn', 'data-style' => "zoom-out"]) ?>

            <?= Html::a('Cancel', $returnUrl, ['class' => 'btn btn-xl btn-primary']) ?>
        </div>

        <?php $form::end(); ?>

    </div>

<?php
$formId = $form->id;
$this->registerJsVar('isNewModel', $model->isNewRecord);
//$this->registerJsVar('previewEventTitle', 'AGO visit');

$script = <<<JS
if (isNewModel) {
    var saveBtn = $('#save-form-btn').ladda();
    
    $('#{$formId}')
        .on("beforeValidate", function() {
            saveBtn.ladda('start').attr('disabled', true);
        })
        .on("afterValidate", function(event, message, errorAttributes) {
            if (errorAttributes.length > 0 ) {
                saveBtn.ladda('stop');
            }
        });
}
JS;
$this->registerJs($script);
?>