<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\BackendAsset;
use common\helpers\FrontendUrl;
use frontend\widgets\SideBar;
use frontend\widgets\Toastr;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\widgets\SideBarHeader;

BackendAsset::register($this);

$menuItemsFile = Yii::getAlias("@backend/views/{$this->params['userRole']}/_menu-items.php");

$sidebarItems = file_exists($menuItemsFile) ? require $menuItemsFile : [];

$sidebarItems[] = [
    'label' => 'Log out',
    'url' => FrontendUrl::to('/site/logout'),
    'linkOptions' => [
        'data-method' => 'post',
    ],
    'iconClass' => 'fa fa-sign-out',
];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="boxed-layout">
<?php $this->beginBody() ?>

<div id="wrapper">

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    SideBar::widget(['options' => ['role' => 'navigation'],
        'header' => SideBarHeader::widget(),
        'items' => $sidebarItems]) ?>
    <div id="page-wrapper" class="white-bg">
        <div class="row">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header px-0">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <?php if (isset($this->params['cartQuantity'])): ?>
                        <li>
                            <a class="dropdown-toggle count-info" id="cart-icon"
                               href="<?= \yii\helpers\Url::to(['guardian/order/cart']) ?> ">
                                <i class="fa fa-shopping-cart"></i> <span
                                        class="label label-primary"><?= $this->params['cartQuantity'] ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
        <div class="row header-student-list">
            <?php if (isset($this->blocks['header-student-selector'])): ?>
                <?= $this->blocks['header-student-selector']; ?>
            <?php endif; ?>
        </div>
        <div class="row wrapper white-bg page-heading">
            <div class="col-lg-10 pl">
                <h2><?= Html::encode($this->title) ?></h2>
                <?= /** @noinspection PhpUnhandledExceptionInspection */
                Breadcrumbs::widget([
                    'homeLink' => $this->params['homeLink'] ?? null,
                    'links' => $this->params['breadcrumbs'] ?? [],
                ]) ?>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="row wrapper wrapper-content animated">
            <?= $content ?>
        </div>
        <div class="footer">
            <div>
                <strong>Copyright</strong> Green Apple &copy; 2018
            </div>
        </div>
    </div>
</div>

<?php $toastrOptions = isset($this->params['toasterOption']) ? $this->params['toasterOption'] : [] ?>
<?= /** @noinspection PhpUnhandledExceptionInspection */
Toastr::widget(['options' => $toastrOptions]); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
