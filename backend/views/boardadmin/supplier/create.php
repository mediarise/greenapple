<?php

use frontend\widgets\IBox;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Supplier */

$this->title = 'Create Supplier';
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supplier-create">

    <?php IBox::begin([
        'title' => 'Create supplier form',
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php IBox::end(); ?>

</div>
