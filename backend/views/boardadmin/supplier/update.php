<?php

use frontend\widgets\IBox;

/* @var $this yii\web\View */
/* @var $model common\models\Supplier */

$this->title = 'Update Supplier: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="supplier-update">
    <?php IBox::begin([
        'title' => 'Update supplier form',
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php IBox::end(); ?>
</div>
