<?php

use frontend\widgets\IBox;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use frontend\widgets\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\widgets\grid\LinkColumn;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Suppliers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supplier-index">

    <?php IBox::begin([
        'title' => 'Suppliers',
    ]); ?>

    <p class="mb-2">
        <?= Html::a('<i class="fa fa-plus"></i> Create new', ['create'], ['class' => 'btn btn-xl btn-success']) ?>
    </p>

    <?= Html::beginForm('#', 'get', ['id' => 'holidays-filter']) ?>

    <div class="row">
        <div class="col-md-6 mb-15">
            <?= Html::textInput('SupplierSearch[search]', null, [
                'placeholder' => 'Search',
                'data-grid-selector' => '#suppliers-grid',
                'data-pjax-container-selector' => '#suppliers-pjax',
                'class' => 'filter-input form-control'
            ]); ?>
        </div>
    </div>

    <?= Html::endForm(); ?>


    <?php Pjax::begin([
        'id' => 'suppliers-pjax',
        'timeout' => 10000,
        'enablePushState' => false
    ]); ?>


    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'suppliers-gird',
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'columns' => [
            [
                'attribute' => 'code'
            ],
            [
                'class' => LinkColumn::class,
                'attribute' => 'name',
                'url' => [
                    'update',
                    'id' => '$id',
                ]
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update}{delete}',
                'header' => 'Actions',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm mr-05',
                            'title' => 'Edit supplier',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Delete supplier',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this supplier?',
                                'pjax' => 0,
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center', 'style' => 'min-width: 120px'],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <?php IBox::end(); ?>
</div>

<?php $this->registerJsVar('supplierFilterUrl', Url::to(['/schooladmin/supplier/index'])) ?>

<?php $this->registerJs('
    // filtration
    $("body").on(\'keyup\', ".filter-input", function() {
        $.pjax({
            url: supplierFilterUrl + \'?\' + $(this).closest(\'form\').serialize(),
            container: $(this).data(\'pjax-container-selector\'),
            push: false,
            timeout: 10000
        });
    });
') ?>