<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Supplier */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-sm-10 col-md-8 col-lg-6 px-0">
    <div class="supplier-form">

        <?php $form = ActiveForm::begin([
            'validateOnChange' => false,
            'validateOnBlur' => false,
        ]); ?>

        <?= Html::activeLabel($model, 'name') ?>

        <?= $form->field($model, 'name')
            ->textInput(['maxlength' => true, 'placeholder' => 'Enter name'])
            ->label(false) ?>

        <?= Html::activeLabel($model, 'code') ?>

        <?= $form->field($model, 'code')
            ->textInput(['maxlength' => true, 'placeholder' => 'Enter code'])
            ->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-xl btn-success mr-15']) ?>
            <?= Html::a('cancel', ['index'], ['class' => 'btn btn-primary btn-xl']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
