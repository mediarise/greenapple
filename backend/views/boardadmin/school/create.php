<?php

use frontend\widgets\IBox;


/* @var $this yii\web\View */
/* @var $model common\models\School */

$this->title = 'Create School';
$this->params['breadcrumbs'][] = ['label' => 'School list', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-create">
    <?php IBox::begin([
        'title' => 'Create school form'
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?php IBox::end() ?>

</div>
