<?php

use frontend\widgets\IBox;

/* @var $this yii\web\View */
/* @var $model common\models\School */

$this->title = 'Update School: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'School list', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="school-update">
    <?php IBox::begin([
        'title' => 'Update school form'
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?php IBox::end() ?>

</div>
