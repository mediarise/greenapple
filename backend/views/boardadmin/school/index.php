<?php

use common\widgets\grid\LinkColumn;
use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\School */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School list';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-index">

    <?php IBox::begin(['title' => $this->title]) ?>

    <p>
        <?= Html::a('Create School', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'enablePjax' => true,
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'columns' => [
            [
                'class' => LinkColumn::class,
                'attribute' => 'name',
                'url' => ['update', 'id' => '$id'],
                'urlOptions' => ['data-Pjax' => 0],
            ],
            [
                'label' => 'Actions',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Delete', ['delete', 'id' => $model->id], [
                        'data-pjax' => 0,
                        'data-confirm' => 'Are you sure you want to delete this item?',
                        'data-method' => 'post',
                    ]);
                }
            ],
        ],
        'filters' => [
            [
                'type' => GridView::FILTER_TYPE_TEXT_INPUT,
                'name' => 'name',
                'label' => 'School name',
                'labelOptions' => [
                    'class' => 'col-sm-3 control-label'
                ],
                'template' => '<div class="form-group col-sm-6">{label}<div class="col-sm-8">{input}</div></div>',
            ],
        ],
    ]) ?>

    <?php IBox::end() ?>

</div>
