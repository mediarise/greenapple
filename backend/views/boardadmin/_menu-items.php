<?php
return [
    [
        'label' => 'Home',
        'iconClass' => 'fa fa-home',
        'url' => ['/boardadmin/index']
    ],
    [
        'label' => 'Visibility groups',
        'iconClass' => 'fa fa-eye',
        'url' => ['/boardadmin/visibility-group/index']
    ],
    [
        'label' => 'Schools',
        'iconClass' => 'fa fa-graduation-cap',
        'url' => ['/boardadmin/school/index']
    ],
    [
        'label' => 'Holidays',
        'iconClass' => 'fa fa-calendar',
        'url' => ['/boardadmin/holiday/index']
    ],
    [
        'label' => 'Suppliers',
        'iconClass' => 'fa fa-user',
        'url' => ['/boardadmin/supplier/index']
    ],
];