<?php

use frontend\widgets\IBox;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use frontend\widgets\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\widgets\grid\LinkColumn;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\HolidaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Holidays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="holiday-index">
    <?php IBox::begin(['title' => $this->title]); ?>


    <p class="mb-2">
        <?= Html::a('<i class="fa fa-plus"></i> Create new', ['create'], ['class' => 'btn btn-success btn-xl']) ?>
    </p>

    <?= Html::beginForm('#', 'get', ['id' => 'holidays-filter']) ?>

    <div class="row">
        <div class="col-md-6 mb-15">
            <?= Html::textInput('HolidaySearch[title]', null, [
                'placeholder' => 'Search',
                'data-grid-selector' => '#holidays-grid',
                'data-pjax-container-selector' => '#holidays-pjax',
                'class' => 'filter-input form-control'
            ]); ?>
        </div>
    </div>

    <?= Html::endForm(); ?>

    <?php Pjax::begin([
        'id' => 'holidays-pjax',
        'timeout' => 10000,
        'enablePushState' => false
    ]); ?>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'holidays-grid',
        //'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'columns' => [
            [
                'attribute' => 'start_date',
            ],
            [
                'attribute' => 'end_date',
            ],
            [
                'class' => LinkColumn::class,
                'attribute' => 'title',
                'url' => [
                    'update',
                    'id' => '$id',
                ]
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update}{delete}',
                'header' => 'Actions',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm mr-05',
                            'title' => 'Edit holiday',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Delete holiday',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this holiday?',
                                'pjax' => 0,
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center', 'style' => 'min-width: 120px'],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <?php IBox::end(); ?>
</div>

<?php $this->registerJsVar('holidayFilterUrl', Url::to(['/schooladmin/holiday/index'])) ?>

<?php $this->registerJs('
    // filtration
    $("body").on(\'keyup\', ".filter-input", function() {
        $.pjax({
            url: holidayFilterUrl + \'?\' + $(this).closest(\'form\').serialize(),
            container: $(this).data(\'pjax-container-selector\'),
            push: false,
            timeout: 10000
        });
    });
') ?>