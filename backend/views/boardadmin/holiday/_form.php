<?php

use dosamigos\datepicker\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Holiday */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-sm-10 col-md-8 col-lg-6 px-0">
    <div class="holiday-form">

        <?php $form = ActiveForm::begin([
            'validateOnChange' => false,
            'validateOnBlur' => false,
        ]); ?>

        <?= Html::activeLabel($model, 'title')?>

        <?= $form->field($model, 'title')
            ->textInput(['maxlength' => true, 'placeholder' => 'Enter title'])
            ->label(false) ?>

        <div class="row">
            <div class="col-lg-6">
                <?= Html::activeLabel($model, 'start_date')?>

                <?php
                echo $form->field($model, 'start_date')->widget(DatePicker::class, [
                    'template' => '
                {input}
                <span class="input-group-addon" id="basic-addon1">
                <span class="fa fa-calendar"></span>
                </span>
                ',
                    'clientOptions' => [
                        'autoclose' => true,
                        'maxViewMode' => 'years',
                        'startView' => 1,
                        'format' => 'yyyy-mm-dd',
                        'zIndexOffset' => 1000,
                    ],
                    'options' => [
                        'placeholder' => 'Select date'
                    ],
                ])->label(false);
                ?>
            </div>
            <div class="col-lg-6">
                <?= Html::activeLabel($model, 'end_date')?>

                <?php
                echo $form->field($model, 'end_date')->widget(DatePicker::class, [
                    'template' => '
                {input}
                <span class="input-group-addon" id="basic-addon1">
                <span class="fa fa-calendar"></span>
                </span>
                ',
                    'clientOptions' => [
                        'autoclose' => true,
                        'maxViewMode' => 'years',
                        'startView' => 1,
                        'format' => 'yyyy-mm-dd',
                        'zIndexOffset' => 1000,
                    ],
                    'options' => [
                        'placeholder' => 'Select date'
                    ],
                ])->label(false);
                ?>
            </div>
        </div>



        <div class="form-group mt-2">
            <?= Html::submitButton('Save', ['class' => 'btn btn-xl btn-success mr-15']) ?>
            <?= Html::a('cancel', ['index'], ['class' => 'btn btn-primary btn-xl']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
