<?php

use frontend\widgets\IBox;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Holiday */

$this->title = 'Update Holiday: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Holidays', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="holiday-update">

    <?php IBox::begin([
        'title' => 'Update holiday form',
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php IBox::end(); ?>

</div>
