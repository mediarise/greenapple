<?php

use frontend\widgets\IBox;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Holiday */

$this->title = 'Create Holiday';
$this->params['breadcrumbs'][] = ['label' => 'Holidays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="holiday-create">

    <?php IBox::begin([
        'title' => 'Create holiday form',
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php IBox::end(); ?>
</div>
