<?php
namespace backend\controllers;

use Yii;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $loginUrl = Yii::$app->urlManagerFrontend->createUrl(['site/login']);

        return $this->redirect($loginUrl);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
