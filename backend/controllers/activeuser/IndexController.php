<?php
namespace backend\controllers\activeuser;

use backend\controllers\BaseController;

class IndexController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}