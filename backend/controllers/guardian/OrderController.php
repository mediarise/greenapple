<?php

namespace backend\controllers\guardian;

use backend\controllers\BaseController;
use common\components\AjaxResponse;
use common\components\bl\CartManager;
use common\components\ModelHelper;
use common\models\BankCard;
use common\moneris\Moneris;
use common\moneris\TransactionException;
use yii\helpers\ArrayHelper;
use common\models\Cart;
use common\models\guardian\SelectCardForm;
use common\models\GuardianCard;
use Yii;
use yii\base\Module;
use yii\data\ArrayDataProvider;

class OrderController extends BaseController
{
    /**
     * @var CartManager
     */
    private $cartManager;

    public function __construct(string $id, Module $module, array $config = [], CartManager $cartManager)
    {
        parent::__construct($id, $module, $config);
        $this->cartManager = $cartManager;
    }

    public function behaviors()
    {
        return []; //TODO: add new rule for guardian and remove this overriding
    }

    public function actionCart()
    {
        return $this->renderCart();
    }

    public function actionUpdateQuantity($cartId, $quantity)
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['cart']);
        }
        if (!is_numeric($quantity)) {
            return $this->renderCart();
        }

        $cartManager = $this->cartManager;

        $cartManager->updateQuantity($cartId, $quantity);

        return $this->renderCart();
    }

    public function actionUpdateVariation($cartId, $variation)
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['cart']);
        }
        $cartManager = $this->cartManager;
        $cartManager->updateVariation($cartId, $variation);
        return $this->renderCart();
    }

    public function actionRemoveCartItem($id)
    {
        Cart::deleteAll(['id' => $id]);
        return $this->renderCart();
    }

    public function actionCheckout()
    {
        $selectCardForm = new SelectCardForm();

        $cardId = null;

        if ($selectCardForm->load(Yii::$app->request->post()) && $selectCardForm->validate()) {
            $cardId = $selectCardForm->tokenId;
        } else {
            $cardId = Yii::$app->session->remove('cardId');
        }

        if (empty($cardId)) {
            return $this->asJson(AjaxResponse::error('error'));
        }

        $dataKey = GuardianCard::find()
            ->select('token')
            ->where(['id' => $cardId, 'guardian_id' => $this->actor->id])
            ->scalar();

        $moneris = new Moneris();
        $cartManager = $this->cartManager;
        $purchaseTransaction = null;
        $response = null;

        try {
            $purchaseTransaction = $cartManager->beginTransaction($cardId);
            $response = $moneris->purchase($purchaseTransaction->id, $cartManager->getAmount(), $dataKey);
            $cartManager->endTransaction($purchaseTransaction, $response);
        } catch (TransactionException $exception) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $purchaseTransaction->delete();
            return $this->asJson(AjaxResponse::error($exception->getMessage()));
        } catch (\Throwable $e) {
            return $this->asJson(AjaxResponse::error('Payment error'));
        }
        Yii::$app->session->setFlash('success', 'Payment completed successfully');
        return $this->redirect(['/guardian/index']);
    }

    public function actionMakePayment()
    {
        $card = new BankCard();
        $card->load(Yii::$app->request->post());
        if (!$card->validate()) {
            return $this->asJson(AjaxResponse::validationError(ModelHelper::errorsForForm($card)));
        }

        $moneris = new Moneris();
        $cartManager = $this->cartManager;
        $purchaseTransaction = null;
        $response = null;

        try {
            $purchaseTransaction = $cartManager->beginTransaction(null);
            $response = $moneris->purchaseByCard($purchaseTransaction->id, $card, $cartManager->getAmount());
            $cartManager->endTransaction($purchaseTransaction, $response);
        } catch (TransactionException $exception) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $purchaseTransaction->delete();
            return $this->asJson(AjaxResponse::error($exception->getMessage()));
        } catch (\Throwable $e) {
            return $this->asJson(AjaxResponse::error('Payment error'));
        }
        Yii::$app->session->setFlash('success', 'Payment Completed Successfully');

        try {
            $response = $moneris->tokenizeCard($response);
            $cartManager->registerCard($this->actor->id, $response->getDataKey(),
                $response->getResDataMaskedPan(), $purchaseTransaction->id);
        } catch (TransactionException $exception) {
            Yii::$app->session->setFlash('error', $exception->getMessage());
        } catch (\Throwable $e) {
            Yii::$app->session->setFlash('error', 'Couldn\'t save card info');
        }

        return $this->redirect(['/guardian/index']);
    }

    public function actionAcceptCart()
    {
        $cartManager = $this->cartManager;

        try {
            $cartManager->acceptCart();
        } catch (\Throwable $e) {
            return $this->asJson(AjaxResponse::error('Something went wrong'));
        }

        Yii::$app->session->setFlash('success', 'Items accepted successfully');

        return $this->redirect(['/guardian/index']);
    }

    public function actionCheckoutSuccess()
    {
        return $this->render('checkout-success');
    }

    private function renderCart()
    {
        $cartManager = $this->cartManager;

        $cartItems = $cartManager->getItems();
        $itemsCount = count($cartItems);

        $cartItems = ArrayHelper::map($cartItems, 'id', function (Cart $item) {
            return $item;
        }, function (Cart $item) {
            return $item->docEvent->title;
        });

        $cartDataProvider = new ArrayDataProvider([
            'allModels' => $cartItems,
        ]);

        $params = [
            'cartDataProvider' => $cartDataProvider,
            'itemsCount' => $itemsCount,
            'totalCount' => $cartManager->getTotalCount(),
            'amount' => $cartManager->getAmount(),
        ];

        if (Yii::$app->request->isPjax) {
            return $this->renderPartial('_product-list', $params);
        }

        $selectCardForm = new SelectCardForm();
        /** @var GuardianCard[] $cards */
        $cards = $this->actor->cards;
        if (!empty($cards)) {
            $selectCardForm->tokenId = $cards[0]->id;
        }

        return $this->render('cart', array_merge($params, [
            'selectCardForm' => $selectCardForm,
            'cards' => ArrayHelper::map($cards, 'id', 'mask'),
        ]));
    }
}