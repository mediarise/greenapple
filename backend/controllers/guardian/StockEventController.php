<?php


namespace backend\controllers\guardian;


use backend\controllers\BaseController;
use common\components\bl\ledger\GuardianEventLedger;
use common\components\Model;
use common\models\EventProduct;
use common\models\guardian\DocEvent;
use common\models\guardian\purchase\DonateOrderItem;
use common\models\guardian\purchase\OrderItem;
use common\models\guardian\StockProduct;
use common\models\Product;
use common\models\Student;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

class StockEventController extends BaseController
{
    /**
     * @param $eventId
     * @param $studentId
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($eventId, $studentId)
    {
        $eventModel = $this->findEvent($eventId);
        $studentModel = $this->findStudent($studentId);

        $products = $this->getProducts($eventId, $studentId);
        return $this->render('view', [
            'eventModel' => $eventModel,
            'guardianModel' => $this->actor,
            'studentModel' => $studentModel,
            'products' => $products,
            'eventIsClosed' => $this->eventIsClosed($eventId, $studentId),
        ]);
    }

    public function actionDonate($eventId, $studentId)
    {
        $ledger = new GuardianEventLedger($this->actor->id);
        $products = EventProduct::find()
            ->where(['doc_event_id' => $eventId])
            ->all();
        try {
            $orderItems = Model::loadMultiple(DonateOrderItem::class, \Yii::$app->request->post('DonateOrderItem'));
            $ledger->donateProducts($orderItems);
            return $this->redirect(['/guardian/index']);
        } catch (\Throwable $e) {
            \Yii::$app->session->setFlash('error', 'something went wrong');
            return $this->redirect([
                '/guardian/stock-event/view',
                'eventId' => $eventId,
                'studentId' => $studentId,
            ]);
        }
    }


    protected function getProducts($eventId)
    {
        $contribQuery = (new Query())
            ->select([
                'lp.product_id',
                'sum(lp.quantity) contributed',
            ])
            ->from('ldg_event_activity_purchase lp')
            ->where(['lp.doc_event_id' => $eventId])
            ->groupBy('lp.product_id');

        $model = StockProduct::find()
            ->alias('ep')
            ->select([
                'ep.id',
                'ep.max_quantity',
                'p.title',
                'contrib.contributed',
                'ep.product_id',
                'ep.doc_event_id',
            ])
            ->joinWith('product p', false)
            ->leftJoin(['contrib' => $contribQuery], 'ep.product_id=contrib.product_id')
            ->where(['ep.doc_event_id' => $eventId])
            ->all();
        return $model;
    }

    protected function eventIsClosed($eventId, $studentId)
    {
        $ledger = new GuardianEventLedger($this->actor->id);
        $lastAction = $ledger->getPreviousEventAction($eventId, $studentId);

        return !empty($lastAction);
    }

    /**
     * @param $id
     * @return DocEvent
     * @throws NotFoundHttpException
     */
    protected function findEvent($id)
    {
        $model = DocEvent::find()
            ->with('mainPhoto')
            ->where(['id' => $id, 'type' => DocEvent::EVENT_TYPE_STOCK_THE_CLASSROOM])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        /** @var $model DocEvent */
        return $model;
    }

    /**
     * @param $id
     * @return Student
     * @throws NotFoundHttpException
     */
    protected function findStudent($id)
    {
        $model = Student::find()
            ->where(['id' => $id])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}