<?php


namespace backend\controllers\guardian;


use backend\controllers\BaseController;
use common\components\AjaxResponse;
use common\components\bl\CartManager;
use common\components\bl\ledger\GuardianEventLedger;
use common\components\FormHtmlReplacer;
use common\exception\LogicException;
use common\helpers\ArrayHelper;
use common\models\Cart;
use common\models\DocEvent;
use common\models\EventProduct;
use common\models\Form;
use common\models\Fundraising;
use common\models\guardian\AttendProduct;
use common\models\guardian\DeclineEventForm;
use common\models\guardian\FormAcceptForm;
use common\models\LnkEventForm;
use common\models\Product;
use common\models\Student;
use DateTime;
use ErrorException;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class EventController extends BaseController
{
    /**
     * @param $eventId
     * @param $studentId
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($eventId, $studentId)
    {
        $event = $this->findEvent($eventId);
        $student = $this->findStudent($studentId);
        $guardian = $this->actor;
        Url::remember();
        $forms = $this->getForms($event, $student, $guardian);
        $acceptedForms = $this->getAcceptedForms($event, $student, $guardian);
        $productList = AttendProduct::findByEvent($eventId, $guardian->id, $studentId);
        $declineEventForm = new DeclineEventForm();
        $guardianEventLedger =  new GuardianEventLedger($guardian->id);
        $previousEventAction = $guardianEventLedger->getPreviousEventAction($eventId, $studentId);
        $isReadonly = !empty($previousEventAction);
        return $this->render('view', [
            'eventModel' => $event,
            'studentModel' => $student,
            'eventPhoto' => $event->mainPhoto,
            'forms' => $forms,
            'acceptedForms' => $acceptedForms,
            'products' => $productList,
            'guardian' => $guardian,
            'declineEventForm' => $declineEventForm,
            'isReadonly' => $isReadonly,
        ]);
    }

    /**
     * @throws LogicException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionAcceptForm()
    {
        $formId = Yii::$app->request->post('formId');
        $eventId = Yii::$app->request->post('eventId');
        $studentId = Yii::$app->request->post('studentId');
        $guardian = $this->actor;
        $guardianEventLedger = new GuardianEventLedger($guardian->id);
        Yii::$app->response->format = Response::FORMAT_JSON;

        try {
            $guardianEventLedger->acceptForm($eventId, $studentId, $formId);
            $lastFormActionDate = $guardianEventLedger->getLastFormActionDate($eventId, $studentId, $formId);
            if ($lastFormActionDate) {
                $datatime = new DateTime($lastFormActionDate);
                $lastFormActionDate = $datatime->format('m/d/Y');
            }
            return $this->asJson(AjaxResponse::success([
                'message' => 'Form successfully accepted.',
                'date' => $lastFormActionDate,
            ]));

        } catch (\Exception $e) {
            return $this->asJson(AjaxResponse::error('Unable to accept form. Please try again later.'));
        }


    }

    protected function getForms(DocEvent $event, Student $student, $guardian)
    {
        $formList = $event->getForms()->all();
        $guardianEventLedger = new GuardianEventLedger($guardian->id);
        $acceptedForms = $this->getAcceptedForms($event, $student, $guardian);
        $forms = [];

        foreach ($formList as $form) {
            /* @var $form Form */

            $formHtmlReplacer = new FormHtmlReplacer();
            $formHtmlReplacer->eventModel = $event;
            $formHtmlReplacer->formModel = $form;
            $formHtmlReplacer->studentModel = $student;
            $formHtmlReplacer->guardianModel = $guardian;
            $formHtml = $formHtmlReplacer->getResult();

            $lastFormActionDate = $guardianEventLedger->getLastFormActionDate($event->id, $student->id, $form->id);
            if ($lastFormActionDate) {
                $datatime = new DateTime($lastFormActionDate);
                $lastFormActionDate = $datatime->format('m/d/Y');
            }

            $formName = "form{$form->id}";
            $forms[$formName] = [
                'id' => $form->id,
                'html' => $formHtml,
                'controlType' => $form->control,
                'title' => $form->title,
                'isAccepted' => isset($acceptedForms[$form->id]),
                'lastActionDate' => $lastFormActionDate,
                'accept' => isset($acceptedForms[$form->id]),
            ];
        }
        return $forms;
    }

    protected function getAcceptedForms(DocEvent $event, Student $student, $guardian)
    {
        $guardianEventLedger = new GuardianEventLedger($guardian->id);
        $formActionTypes = $guardianEventLedger->getPreviousFormsActions($event->id, $student->id);
        $acceptedForms = ArrayHelper::map($formActionTypes, 'form_id', 'action_type');
        return $acceptedForms;
    }


    /**
     * @param $eventId
     * @param $studentId
     * @param int $step
     * @return \yii\web\Response|string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \common\exception\LogicException
     * @throws \yii\db\Exception
     */
    public function actionShowForm($eventId, $studentId, $step = 1)
    {
        $count = LnkEventForm::find()
            ->where(['doc_event_id' => $eventId])
            ->count();
        if ($count === 0) {
            return $this->redirect(ArrayHelper::merge(['show-product'], Yii::$app->request->get()));
        }
        Url::remember();
        $event = $this->findEvent($eventId);
        $student = $this->findStudent($studentId);
        $guardian = $this->actor;
        $formModel = $this->getCurrentForm($event);

        $guardianEventLedger = new GuardianEventLedger($guardian->id);
        $previousEventAction = $guardianEventLedger->getPreviousEventAction($eventId, $studentId);
        $isReadonly = !empty($previousEventAction);

        $formList = $event->forms;

        $formActionTypes = $guardianEventLedger->getPreviousFormsActions($eventId, $studentId);
        $lastFormActionDate = $guardianEventLedger->getLastFormActionDate($eventId, $studentId, $formModel->id);
        $involvedForms = ArrayHelper::map($formActionTypes, 'form_id', 'action_type');

        $lnkEventForm = LnkEventForm::find()->where(['doc_event_id' => $eventId, 'form_id' => $formModel->id])->one();
        $lastStepNumber = count($formList) + 1;

        // Form accepting
        $acceptForm = new FormAcceptForm();
//        $acceptForm->involvedForms = $involvedForms;
        $hasProducts = $event->getLnkEventProducts()->exists();
        if ($acceptForm->load(Yii::$app->request->post()) && $acceptForm->validate()) {

            $isInvolved = isset($involvedForms[$formModel->id]);
            if (!$isInvolved) {
                if ($acceptForm->isAccept) {
                    $guardianEventLedger->acceptForm($eventId, $studentId, $formModel->id);
                } else {
                    $guardianEventLedger->skipForm($eventId, $studentId, $formModel->id);
                }
            }
            if ($step == count($formList)) {
                if ($hasProducts) {
                    return $this->redirect(['show-product', 'eventId' => $eventId, 'studentId' => $studentId]);
                } else {
                    return $this->redirect(['/guardian/index']);
                }
            }
            return $this->redirect(Url::current(['step' => ($step + 1)]));
        }

        $formHtmlReplacer = new FormHtmlReplacer();
        $formHtmlReplacer->eventModel = $event;
        $formHtmlReplacer->formModel = $formModel;
        $formHtmlReplacer->studentModel = $student;
        $formHtmlReplacer->guardianModel = $guardian;
        $formHtml = $formHtmlReplacer->getResult();

//        $acceptForm->setAccepting($formModel->id);

        if (!$hasProducts) {
            $lastStepNumber--;
        }

        $declineEventForm = new DeclineEventForm();
        $this->view->registerCssFile('css/plugins/steps/jquery.steps.css', ['depends' => JqueryAsset::class]);
        return $this->renderTabs('form', [
            'eventModel' => $event,
            'studentModel' => $student,
            'formModel' => $formModel,
            'formHtml' => $formHtml,
            'acceptForm' => $acceptForm,
            'currentStepNumber' => $step,
            'involvedForms' => $involvedForms,
            'formList' => $formList,
            'lastStepNumber' => $lastStepNumber,
            'declineEventForm' => $declineEventForm,
            'guardian' => $guardian,
            'lnkEventForm' => $lnkEventForm,
            'lastFormActionDate' => $lastFormActionDate,
            'hasProducts' => $hasProducts,
            'isReadonly' => $isReadonly,
        ]);
    }

    /**
     * @param $eventId
     * @param $studentId
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionShowProduct($eventId, $studentId)
    {
        Url::remember();
        $event = $this->findEvent($eventId);
        $student = $this->findStudent($studentId);
        $guardian = $this->actor;

        //check event status
        $guardianEventLedger = new GuardianEventLedger($guardian->id);
        $previousEventAction = $guardianEventLedger->getPreviousEventAction($eventId, $studentId);
        $isReadonly = !empty($previousEventAction);

        $formActionTypes = $guardianEventLedger->getPreviousFormsActions($eventId, $studentId);
        $involvedForms = ArrayHelper::map($formActionTypes, 'form_id', 'action_type');


        $productList = AttendProduct::findByEvent($eventId, $guardian->id, $studentId);

        $formList = $event->forms;
        $lastStepNumber = count($formList) + 1;

        $declineEventForm = new DeclineEventForm();

        $this->view->registerCssFile('css/plugins/steps/jquery.steps.css', ['depends' => JqueryAsset::class]);
        return $this->renderTabs('product', [
            'eventModel' => $event,
            'studentModel' => $student,
            'currentStepNumber' => $lastStepNumber,
            'involvedForms' => $involvedForms,
            'formList' => $formList,
            'lastStepNumber' => $lastStepNumber,
            'products' => $productList,
            'declineEventForm' => $declineEventForm,
            'guardian' => $guardian,
            'isReadonly' => $isReadonly,
        ]);
    }

    private function renderTabs($view, $params)
    {
        $event = $params['eventModel'];
        return $this->render($view, array_merge($params, [
            'eventPhoto' => $event->mainPhoto
        ]));
    }

    public function actionFundraising($eventId, $studentId)
    {
        $eventModel = $this->findEvent($eventId);
        $fundraising = Fundraising::fromEvent($eventModel);

        if ($fundraising->load(Yii::$app->request->post()) && $fundraising->validate()) {
            $product = Product::getFundraisingProduct();

            $fakeEventProduct = new EventProduct();
            $fakeEventProduct->doc_event_id = $eventId;
            $fakeEventProduct->product_id = $product->id;
            $fakeEventProduct->min_quantity = 1;
            $fakeEventProduct->max_quantity = 1;

            $cartManager = $this->getCartManager();
            try {
                $cartManager->addToCart($studentId, $fakeEventProduct, null, $fundraising->value, 1);
                return $this->redirect(['/guardian/order/cart']);
            } catch (LogicException $e) {
                Yii::$app->session->setFlash('error', Html::encode($e->getMessage()));
            }
        }

        $studentModel = $this->findStudent($studentId);

        $guardian = $this->actor;
        $guardianEventLedger = new GuardianEventLedger($guardian->id);
        $previousEventAction = $guardianEventLedger->getPreviousEventAction($eventId, $studentId);
        $isReadonly = !empty($previousEventAction);

        $declineEventForm = new DeclineEventForm();

        return $this->render('fundraising', [
            'eventModel' => $eventModel,
            'studentModel' => $studentModel,
            'fundraising' => $fundraising,
            'isReadonly' => $isReadonly,
            'declineEventForm' => $declineEventForm,
            'guardian' => $guardian,
        ]);
    }


    /**
     * @param $eventId
     * @param $studentId
     * @return Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionAddToCart($eventId, $studentId)
    {
        $products = Yii::$app->request->post('products');

        $manager = $this->getCartManager();
        try {
            $manager->addToCartMany($eventId, $studentId, $products);
        } catch (LogicException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->goBack();
        } catch (\Throwable $e) {
            Yii::$app->session->setFlash('error', 'Unable to add to cart.');
            return $this->goBack();
        }


        return $this->redirect(['guardian/order/cart']);
    }

    /**
     * @return Response
     * @throws \Throwable
     * @throws \common\exception\LogicException
     * @throws \yii\db\Exception
     */
    public function actionDeclineEvent()
    {
        $declineForm = new DeclineEventForm();
        if ($declineForm->load(Yii::$app->request->post()) && $declineForm->validate()) {

            $transaction = Yii::$app->db->beginTransaction();
            try {

                if (!$declineForm->save()) {
                    throw new ErrorException('Unable to decline event.');
                }
                Yii::$app->db->createCommand()->delete(
                    Cart::tableName(),
                    [
                        'guardian_id' => $declineForm->guardianId,
                        'doc_event_id' => $declineForm->eventId,
                        'student_id' => $declineForm->studentId,
                    ]
                )->execute();

                $transaction->commit();

            } catch (\Exception $e) {
                $transaction->rollBack();

                return $this->goBack();
            }

            return $this->redirect(['/guardian']);
        }
        return $this->goBack();
    }

    /**
     * return current form model
     *
     * @param $eventModel
     * @return null|Form
     */
    protected function getCurrentForm($eventModel)
    {
        $stepNumber = ArrayHelper::getValue(Yii::$app->request->queryParams, 'step', 1);
        $formNumber = $stepNumber - 1;
        $formList = $eventModel->forms;
        if (isset($formList[$formNumber])) {
            return $formList[$formNumber];
        }
        return null;
    }


    /**
     * @param $id
     * @return DocEvent
     * @throws NotFoundHttpException
     */
    protected function findEvent($id)
    {
        $model = DocEvent::find()->where(['id' => $id])->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        /** @var $model DocEvent */
        return $model;
    }

    /**
     * @param $studentId
     * @return Student|null
     * @throws NotFoundHttpException
     */
    protected function findStudent($studentId)
    {
        $model = Student::find()->where(['id' => $studentId])->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        return $model;
    }


    /**
     * @return CartManager
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function getCartManager()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return Yii::$container->get(CartManager::class);
    }
}