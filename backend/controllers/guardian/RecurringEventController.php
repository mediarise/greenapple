<?php


namespace backend\controllers\guardian;

use backend\controllers\BaseController;
use common\components\AjaxResponse;
use common\components\bl\CartManager;
use common\components\bl\ledger\GuardianEventLedger;
use common\exception\LogicException;
use common\models\EventProduct;
use common\models\guardian\DocEvent;
use common\models\guardian\NutritionProduct;
use common\models\guardian\UpcomingEvent;
use common\models\Product;
use common\models\Student;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\JsExpression;
use yii\web\NotFoundHttpException;
use yii\web\UrlNormalizerRedirectException;

class RecurringEventController extends BaseController
{
    /**
     * @param DocEvent $event
     * @param $studentId
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($eventId, $studentId)
    {
        $eventModel = $this->findEvent($eventId, $studentId);

        $studentModel = $this->findStudent($studentId);
        $product = $eventModel->products[0];

        if (!empty($product->variations)) {
            $defaultVariation = $product->variations[0];
        }

        $eventProducts = $this->getEventProductData($eventModel, $studentId, $defaultVariation, $product);

        return $this->render('view', [
            'eventModel' => $eventModel,
            'studentModel' => $studentModel,
            'eventProducts' => $eventProducts,
            'product' => $product,
            'variations' => $this->getVariations($product),
        ]);
    }

    /**
     * @param $eventId
     * @param $studentId
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionViewChild($eventId, $studentId)
    {
        $eventModel = $this->findChildEvent($eventId, $studentId);
        $studentModel = $this->findStudent($studentId);

        $product = $this->getChildEventProductData($eventId, $studentId);

        return $this->render('view-child', [
            'eventModel' => $eventModel,
            'studentModel' => $studentModel,
            'product' => $product,
        ]);
    }

    public function actionAddToCart($studentId)
    {
        $products = Yii::$app->request->post('products', []);
        $cartManager = new CartManager($this->actor->id);
        $eventIds = array_column($products, 'eventId');
        $products = iterator_to_array($this->mapChoices($products));
        try {
            $cartManager->addToCartMany($eventIds, $studentId, $products);
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (LogicException $e) {
            return $this->asJson(AjaxResponse::error($e->getMessage()));
        } catch (\Throwable $e) {
            return $this->asJson(AjaxResponse::error('Server error. Try again later.'));
        }
        return $this->redirect(['guardian/order/cart']);
    }

    public function actionDecline($eventId, $studentId)
    {
        $products = Yii::$app->request->post('products', []);
        $ledger = new GuardianEventLedger($this->actor->id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($products as $product) {
                $ledger->declineEvent($product['eventId'], $studentId);
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return $this->asJson(AjaxResponse::error('Server error. Try again later.'));
        }
        $transaction->commit();

        try {
            $eventModel = $this->findEvent($eventId, $studentId);
        } catch (ForbiddenHttpException $e) {
            return $this->redirect(['guardian/index']);
        }

        $defaultVariation = null;

        $product = $eventModel->products[0];

        if (!empty($product->variations)) {
            $defaultVariation = $product->variations[0];
        }

        $eventProducts = $this->getEventProductData($eventModel, $studentId, $defaultVariation, $product);

        return $this->asJson(AjaxResponse::success([
            'items' => $eventProducts,
            'message' => 'Products declined.'
        ]));
    }

    protected function mapChoices($products)
    {
        foreach ($products as $product) {
            yield $product['id'] => [
                'quantity' => $product['quantity'],
                'variation' => $product['variation'],
            ];
        }
    }

    protected function getVariations($product)
    {
        return array_map(function ($variation) {
            return [
                'id' => $variation->id,
                'text' => $variation->name,
                'price' => (float)$variation->price,
            ];
        }, $product->variations);
    }

    protected function getChildEventProductData($eventId, $studentId)
    {
        $data = EventProduct::find()
            ->alias('ep')
            ->asArray()
            ->select([
                'p.title',
                'p.description',
                'ep.price',
                'leap.quantity',
                'pv.name variation_name',
                'ph.url',
            ])
            ->leftJoin(
                'ldg_event_activity_purchase leap',
                'leap.doc_event_id = ep.doc_event_id AND leap.student_id = :studentId',
                [
                    ':studentId' => $studentId
                ])
            ->leftJoin('product_variation pv', 'pv.id = leap.product_variation_id')
            ->leftJoin('product p', 'p.id = ep.product_id')
            ->leftJoin('product_photo ph', 'ph.product_id = p.id')
            ->where(['ep.doc_event_id' => $eventId])
            ->one();

        return $data;
    }

    protected function getEventProductData(DocEvent $event, $studentId, $defaultVariation, Product $product)
    {
        $childEventsQuery = DocEvent::find()
            ->where(['predecessor' => $event->id])
            ->select('id');

        $products = NutritionProduct::find()
            ->alias('ep')
            ->select([
                'ep.id',
                'ep.doc_event_id',
                'e.start_date',
                'e.end_date',
                'ep.product_id',
                'p.title',
                'ep.min_quantity',
                'ep.max_quantity',
                'ep.price',
                'ap.quantity bought',
                'ap.product_variation_id bought_variation',
                'ap.price bought_price',
                'ap.total bought_total',
            ])
            ->leftJoin('ldg_event_activity_purchase ap',
                "ap.student_id = :studentId AND ep.doc_event_id = ap.doc_event_id AND ep.product_id = ap.product_id", [
                    ':studentId' => (int)$studentId,
                ]
            )
            ->leftJoin('ldg_event_student_status ss',
                "ss.student_id = :studentId AND ep.doc_event_id = ss.doc_event_id", [
                    ':studentId' => (int)$studentId,
                ]
            )
            ->joinWith('docEvent e', false)
            ->joinWith('product p', false)
            ->where([
                'AND',
                ['e.predecessor' => $event->id],
                'coalesce(ss.status, 8) != 2',
                'e.due_date > NOW()'
            ])
            ->orderBy('e.start_date')
            ->all();

        $infinite = (string)(new JsExpression('infinite'));
        $products = array_map(function (NutritionProduct $nutritionProduct) use ($infinite, $defaultVariation, $product, $event) {
            $price = (float)($nutritionProduct->bought_price ?? $defaultVariation->price ?? $nutritionProduct->price);
            $quantity = $nutritionProduct->getQuantity();
            return [
                'id' => $nutritionProduct->id,
                'product_id' => $nutritionProduct->product_id,
                'eventId' => $nutritionProduct->doc_event_id,
                'name' => $nutritionProduct->getName(),
                'date' => $nutritionProduct->getDate($event),
                'product' => $nutritionProduct->title,
                'variation' => $nutritionProduct->bought_variation ?? $defaultVariation->id ?? null,
                'quantity' => $nutritionProduct->getQuantity(),
                'minQuantity' => $nutritionProduct->getMinQuantity(),
                'maxQuantity' => $nutritionProduct->max_quantity > 0 ? $nutritionProduct->max_quantity : $infinite,
                'price' => $price,
                'total' => (float)($nutritionProduct->bought_total ?? ($product->applyTax($price) * $quantity)),
                'isBought' => $nutritionProduct->getIsBought(),
                'status' => $nutritionProduct->getIsBought() ? 'planned' : 'new',
            ];
        }, $products);

        return $products;
    }

    /**
     * @param $id
     * @param $studentId
     * @return DocEvent
     * @throws NotFoundHttpException
     * @throws UrlNormalizerRedirectException
     */
    protected function findEvent($id, $studentId)
    {
        $model = DocEvent::find()
            ->alias('e')
            ->select('e.*, ss.status student_status')
            ->with('mainPhoto', 'products', 'products.variations')
            ->leftJoin('ldg_event_student_status ss',
                "ss.student_id = :studentId AND e.id = ss.doc_event_id", [
                    ':studentId' => (int)$studentId,
                ])
            ->where(['e.id' => $id, 'is_repeatable' => true])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        if ($model->student_status == UpcomingEvent::STATUS_DECLINED) {
            throw new ForbiddenHttpException('Event is rejected');
        }
        /** @var $model DocEvent */
        return $model;
    }

    /**
     * @param $id
     * @param $studentId
     * @return array|DocEvent|null|\yii\db\ActiveRecord
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    protected function findChildEvent($id, $studentId)
    {
        $model = DocEvent::find()
            ->alias('e')
            ->select('e.*, ss.status student_status')
            ->with('mainPhoto', 'products', 'products.variations')
            ->leftJoin('ldg_event_student_status ss',
                "ss.student_id = :studentId AND e.id = ss.doc_event_id", [
                    ':studentId' => (int)$studentId,
                ])
            ->where(['e.id' => $id])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        if ($model->student_status == UpcomingEvent::STATUS_DECLINED) {
            throw new ForbiddenHttpException('Event is rejected');
        }
        /** @var $model DocEvent */
        return $model;
    }

    /**
     * @param $id
     * @return Student
     * @throws NotFoundHttpException
     */
    protected function findStudent($id)
    {
        $model = Student::find()
            ->where(['id' => $id])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}