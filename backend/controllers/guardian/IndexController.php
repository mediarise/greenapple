<?php

namespace backend\controllers\guardian;

use backend\controllers\BaseController;
use common\components\BitHelper;
use common\components\bl\GuardianDashboardManager;
use common\components\DateHelper;
use common\components\Formatter;
use common\helpers\ArrayHelper;
use common\models\DocEvent;
use common\models\guardian\SelectStudentForm;
use common\models\guardian\UpcomingEvent;
use common\models\Holiday;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

class IndexController extends BaseController
{
    private function getDashboardManager()
    {
        static $dashboardManager;
        if (empty($dashboardManager)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $actor = \Yii::$container->get('Actor');
            $selectedId = \Yii::$app->session->get('selectedStudent', 0);
            if ($selectedId == 0) {
                $ids = ArrayHelper::getColumn($actor->students, 'id');
            } else {
                $ids = [$selectedId];
            }
            $dashboardManager = new GuardianDashboardManager($actor->getId(), $ids);
        }
        return $dashboardManager;
    }

    public function init()
    {
        parent::init();
        //\Yii::$container->set(SideBarHeader::class, \frontend\widgets\guardian\SideBarHeader::class);
    }

    public function actionIndex()
    {
        $manager = $this->getDashboardManager();

        $pendingDataProvider = new ArrayDataProvider([
            'allModels' => $manager->getPendingEvents(),
            'pagination' => false,
        ]);

        $actor = \Yii::$container->get('Actor');
        $students = ArrayHelper::map($actor->students, 'id', 'fullName');
        $students[0] = 'all children';
        ksort($students);

        $selectStudentForm = new SelectStudentForm();
        $selectStudentForm->studentId = \Yii::$app->session->get('selectedStudent', 0);
        $selectStudentForm->returnUrl = \Yii::$app->request->url;

        return $this->render('index', [
            'upcomingDataProvider' => $this->getUpcomingDataProvider(),
            'pendingDataProvider' => $pendingDataProvider,
            'events' => Json::encode(iterator_to_array($this->getCalendarItems())),
            'productHistoryDataProvider' => $this->getProductHistoryDataProvider(),
            'formHistoryDataProvider' => $this->getFormHistoryDataProvider(),
            'selectStudentForm' => $selectStudentForm,
            'students' => $students,
        ]);
    }

    public function actionUpcoming()
    {
        return $this->renderPartial('_upcoming-grid', [
            'dataProvider' => $this->getUpcomingDataProvider(),
        ]);
    }

    public function actionProductHistory()
    {
        return $this->renderPartial('_product-history', [
            'dataProvider' => $this->getProductHistoryDataProvider(),
        ]);
    }

    public function actionFormHistory()
    {
        return $this->renderPartial('_form-history', [
            'dataProvider' => $this->getFormHistoryDataProvider(),
        ]);
    }

    public function actionSelectStudent()
    {
        $form = new SelectStudentForm();
        if (!$form->load(\Yii::$app->request->post())) {
            return $this->redirect(Url::to(['guardian/index']));
        }
        \Yii::$app->session->set('selectedStudent', $form->studentId);
        return $this->redirect($form->returnUrl);
    }

    private function getUpcomingDataProvider()
    {
        $manager = $this->getDashboardManager();

        return new ArrayDataProvider([
            'allModels' => $manager->getUpcomingEvents(),
            'pagination' => false,
            'sort' => [
                'route' => '/guardian/index/upcoming',
                'attributes' => [
                    'start_date',
                    'title',
                    'first_name',
                    'forms_signed',
                    'products_purchased',
                    'status',
                ]
            ]
        ]);
    }

    private function getProductHistoryDataProvider()
    {
        $manager = $this->getDashboardManager();

        $route = '/guardian/index/product-history';

        return new ActiveDataProvider([
            'query' => $manager->getProductHistory(),
            'pagination' => [
                'route' => $route,
                'pageSize' => 5,
            ],
            'sort' => [
                'route' => $route,
                'attributes' => [
                    'date',
                    'product',
                    'event',
                    'quantity',
                    'action'
                ],
            ],
        ]);
    }

    private function getFormHistoryDataProvider()
    {
        $manager = $this->getDashboardManager();

        $route = '/guardian/index/form-history';

        return new ActiveDataProvider([
            'query' => $manager->getFormHistory(),
            'pagination' => [
                'route' => $route,
                'pageSize' => 5,
            ],
            'sort' => [
                'route' => $route,
                'attributes' => [
                    'date',
                    'form',
                    'event',
                    'action'
                ],
            ],
        ]);
    }

    private function newDateExpression($date)
    {
        return new JsExpression(sprintf('new Date("%s")', $date));
    }

    /**
     * @return \Generator
     * @throws \yii\web\ForbiddenHttpException
     */
    private function getCalendarItems()
    {
        $colors = [
            UpcomingEvent::STATUS_DECLINED => '#000',
            UpcomingEvent::STATUS_INCOMPLETE => '#1D84C6',
            UpcomingEvent::STATUS_PLANNED => '#1BB394',
            UpcomingEvent::STATUS_PENDING_OPTIONAL => '#F8AC5A',
            UpcomingEvent::STATUS_PENDING_REQUIRED => '#ED5666',
        ];

        $daysOfWeek = DocEvent::getDaysOfWeek();
        $holidayDates = Holiday::find()
            ->select('start_date, end_date')
            ->asArray()
            ->all();
        $holidays = [];
        foreach ($holidayDates as $dates) {
            $holidays = ArrayHelper::merge($holidays, DateHelper::getDaysBetweenDatesIncluded($dates['start_date'], $dates['end_date']));
        }
        $holidays = array_flip($holidays);


        foreach ($this->getDashboardManager()->getUpcomingEvents() as $event) {
            if (!empty($event->predecessor)) {
                $eventStartDate = Formatter::date($event->start_date);
                $eventEndDate = Formatter::date($event->end_date);
                $current = DateHelper::beginningOfWeek($eventStartDate);
                $startDate = null;
                $previousDate = null;
                foreach ($daysOfWeek as $dayOfWeek => $name) {
                    $date = $current
                        ->modify($name)
                        ->format(Formatter::DATE_FORMAT);
                    if (DateHelper::between($date, $eventStartDate, $eventEndDate) &&
                        !key_exists($date, $holidays) &&
                        (($event->repeat_on_days & $dayOfWeek) > 0)) {
                        if (empty($startDate)) {
                            $startDate = $date;
                        }
                        $previousDate = $date;
                        continue;
                    }

                    if (empty($startDate)) {
                        continue;
                    }

                    yield [
                        'title' => $event->title,
                        'start' => $this->newDateExpression($startDate . ' 00:00:00'),
                        'end' => $this->newDateExpression($previousDate . ' 23:23:59'),
                        'color' => $colors[$event->getStatus()],
                    ];
                    $startDate = null;
                }
                if (!empty($startDate)) {
                    yield [
                        'title' => $event->title,
                        'start' => $this->newDateExpression($startDate . ' 00:00:00'),
                        'end' => $this->newDateExpression($previousDate . ' 23:23:59'),
                        'color' => $colors[$event->getStatus()],
                    ];
                }

                continue;
            }

            $params = [
                'title' => $event->title,
                'start' => $this->newDateExpression($event->start_date),
                'end' => $this->newDateExpression($event->end_date),
                'color' => $colors[$event->getStatus()],
            ];
            if (
                empty($event->predecessor)
                && (BitHelper::testBits($event->type,
                    DocEvent::EVENT_TYPE_FIELD_TRIP | DocEvent::EVENT_TYPE_STOCK_THE_CLASSROOM)
                )) {
                $params['url'] = Url::toRoute($event->getRoute());
            }
            yield $params;
        }
    }
}