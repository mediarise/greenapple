<?php
namespace backend\controllers\greenappleadmin;

use backend\controllers\BaseController;

class IndexController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}