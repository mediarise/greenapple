<?php

namespace backend\controllers;

use common\components\bl\Actor;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use function Stringy\create as s;

class BaseController extends Controller
{
    /** @var Actor */
    public $actor;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'ghost-access'=> [
//                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => '@frontend/views/layouts/login.php',
                'view' => '@frontend/views/site/error.php',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'error') {
            return parent::beforeAction($action);
        }

        try {
            $this->actor = Yii::$container->get('Actor');
        } catch (\Exception $e) {
            throw new NotFoundHttpException('Seems you have no active profile. Please contact with administrator.');
        }
        $this->view->params['userRole'] = (string) s($this->actor->role)->toLowerCase();

        return parent::beforeAction($action);
    }
}
