<?php

namespace backend\controllers\schooladmin;

use backend\controllers\BaseController;
use common\components\AjaxResponse;
use common\components\bl\GuardianManager;
use common\components\ModelHelper;
use common\components\bl\SignupRequestNotifier;
use common\models\GuardianStudent;
use common\models\Student;
use common\models\User;
use Yii;
use common\models\Guardian;
use common\models\search\Guardian as GuardianSearch;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GuardianController implements the CRUD actions for Guardian model.
 */
class GuardianController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Guardian models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuardianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Guardian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $manager = new GuardianManager();

        list($guardian, $user) = $manager->newGuardian(Yii::$app->request->post());

        if (!$guardian->isNewRecord) {
            return $this->redirect(['update', 'id' => $guardian->id]);
        }
        return $this->render('create', [
            'guardian' => $guardian,
            'user' => $user,
        ]);
    }

    /**
     * Updates an existing Guardian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $guardian = $this->findModel($id);

        if ($guardian->load(Yii::$app->request->post()) && $guardian->save()) {
            return $this->redirect(['update', 'id' => $guardian->id]);
        }

        $studentsDataProvider = new ActiveDataProvider([
            'query' => $guardian->getStudents(),
            'sort' => false,
        ]);

        return $this->render('update', [
            'guardian' => $guardian,
            'user' => $guardian->user,
            'studentsDataProvider' => $studentsDataProvider,
            'guardianStudent' => new GuardianStudent(['guardian_id' => $guardian->id]),
        ]);
    }

    public function actionAssignStudent()
    {
        $guardianStudents = new GuardianStudent();
        $guardianStudents->load(Yii::$app->request->post());
        if ($guardianStudents->validate()){
            $guardianStudents->save(false);
            return $this->asJson(['result' => 'success']);
        }
        return $this->asJson(AjaxResponse::validationError(ModelHelper::errorsForForm($guardianStudents)));
    }

    public function actionRemoveRelationship($guardianId, $studentId)
    {
        $count = Yii::$app->db->createCommand()->delete('lnk_guardian_student', [
            'guardian_id' => $guardianId,
            'student_id' => $studentId
        ])->execute();

        if ($count > 0) {
            return $this->asJson(AjaxResponse::success());
        }
        return $this->asJson(AjaxResponse::error('Relationship don\'t exist.'));
    }

    public function actionGetStudents($q = '')
    {
        $students = Student::find()
            ->select(['last_name', 'first_name', 'oen', 'id'])
            ->orWhere(['ilike', 'first_name', $q])
            ->orWhere(['ilike', 'last_name', $q])
            ->orWhere(['ilike', 'oen', $q])
            ->limit(10)
            ->all();

        $result = function () use($students){
            foreach ($students as $student){
                yield [
                    'value' => $student->first_name . ' ' . $student->last_name,
                    'oen' => $student->oen,
                    'id' => $student->id,
                ];
            }
        };

        return $this->asJson($result());
    }

    public function actionInvite($id)
    {
        $manager = new GuardianManager();

        $notifier = new SignupRequestNotifier(
            $manager->getGuardian($id)->user->email,
            'guardianInvite');
        return $this->asJson($manager->invite($id, $notifier));
    }

    /**
     * @param $userId
     * @return \yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionSwitchStatus($userId)
    {
         $manager = new GuardianManager();
         $status = $manager->switchUserStatus($userId);

         $result = ['result' => 'success'];

         if ($status === User::STATUS_ACTIVE){
             $result['caption'] = 'Block user';
             $result['message'] = 'User activated successfully.';
         } else {
             $result['caption'] = 'Activate user';
             $result['message'] = 'User blocked successfully.';
         }

         return $this->asJson($result);
    }

    /**
     * Deletes an existing Guardian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Guardian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Guardian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Guardian::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
