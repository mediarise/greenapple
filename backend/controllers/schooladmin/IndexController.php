<?php

namespace backend\controllers\schooladmin;

use backend\controllers\BaseController;
use common\models\Product;
use common\models\schooladmin\DocEvent;
use yii\data\ArrayDataProvider;

class IndexController extends BaseController
{

    public function actionIndex($productId = null)
    {
        return $this->render('index', [
            'upcomingDataProvider' => $this->getUpcomingDataProvider($productId),
            'productModel' => empty($productId) ? null : Product::findOne($productId),
        ]);
    }

    /**
     * @return ArrayDataProvider
     */
    private function getUpcomingDataProvider($productId): ArrayDataProvider
    {
        return new ArrayDataProvider([
            'allModels' => DocEvent::find()
                ->forThisUser()
                ->upcoming()
                ->filterByProductId($productId)->all(),
            'pagination' => false,
            'sort' => [
                'route' => '/schooladmin/index/upcoming',
                'attributes' => [
                    'start_date',
                    'title',
                    'ordersRatio',
                    'formsRatio'
                ]
            ]
        ]);
    }

    public function actionUpcoming($productId = null)
    {
        return $this->renderPartial('_upcoming-grid', [
            'upcomingDataProvider' => $this->getUpcomingDataProvider($productId),
        ]);
    }
}