<?php

namespace backend\controllers\schooladmin;

use common\models\Holiday;

class HolidayController extends \backend\controllers\boardadmin\HolidayController
{
    public function getViewPath()
    {
        return '@backend/views/boardadmin/holiday';
    }

    public function setDefaultValues(Holiday $model)
    {
        $actor = $this->actor;
        $model->board_id = $actor['board_id'];
        $model->school_id = $actor['school_id'];
    }
}