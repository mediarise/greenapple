<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.04.18
 * Time: 15:27
 */

namespace backend\controllers\schooladmin;


use backend\controllers\BaseController;
use common\models\Form;
use common\models\FormSearch;
use Yii;
use yii\web\NotFoundHttpException;

class FormController extends \backend\controllers\teacher\FormController
{
    public function getViewPath()
    {
        return '@backend/views/teacher/form';
    }
//    public function actionIndex()
//    {
//        $filterModel = new FormSearch();
//        $dataProvider = $filterModel->search(Yii::$app->request->queryParams);
//        return $this->render('index', [
//            'filterModel' => $filterModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }
//
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        $returnUrl = Yii::$app->request->post('returnUrl', Url::to(['index']));
//
//        if ($model->load(Yii::$app->request->post())) {
//            if ($model->save()) {
//                Yii::$app->session->setFlash('success', 'Form successfully updated');
//            } else {
//                Yii::$app->session->setFlash('error', 'Form not saved');
//            }
//        }
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }
//
//    public function actionCreate()
//    {
//        $model = new Form();
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            Yii::$app->session->setFlash('success', 'Form successfully created');
//            return $this->redirect(['index']);
//        }
//        return $this->render('create', [
//            'model' => $model,
//        ]);
//    }
//
//    public function actionDelete($id)
//    {
//        $model = $this->findModel($id);
//        if ($model->delete() !== false) {
//            Yii::$app->session->setFlash('success', 'Form successfully deleted');
//            return $this->redirect(['index']);
//        }
//        Yii::$app->session->setFlash('error', 'Form is not deleted');
//    }
//
//    /**
//     * @param $id
//     * @return array|null|\yii\db\ActiveRecord
//     * @throws NotFoundHttpException
//     */
//    protected function findModel($id)
//    {
//        $model = Form::find()->where(['id' => $id])->one();
//        if ($model === null) {
//            throw new NotFoundHttpException();
//        };
//        return $model;
//    }
}