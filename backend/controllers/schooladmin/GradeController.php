<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.04.18
 * Time: 17:42
 */

namespace backend\controllers\schooladmin;


use backend\controllers\BaseController;
use common\models\Board;
use common\models\Grade;
use common\models\GradeSearch;
use common\models\School;
use common\models\SchoolAdmin;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class GradeController extends BaseController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new GradeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        $model = new Grade();
        $schoolAdminModel = SchoolAdmin::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($schoolAdminModel === null) {
            throw new ForbiddenHttpException();
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Grade successfully added');
            $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
            'schoolId' => $schoolAdminModel->school_id,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete() !== false) {
            Yii::$app->session->setFlash('success', 'Grade successfully deleted');
        }
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $schoolAdminModel = SchoolAdmin::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($schoolAdminModel === null) {
            throw new ForbiddenHttpException();
        }
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Grade successfully updated');
            $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
            'schoolId' => $schoolAdminModel->school_id,
        ]);
    }

    /**
     * @return array
     */
    public function actionGetSchools()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $boardId = $parents[0];
                $schools = School::find()->where(['board_id' => $boardId])->asArray()->all();
                foreach ($schools as $school) {
                    $out[] = ['id' => $school['id'], 'name' => $school['name']];
                }
                return ['output' => $out, 'selected' => ''];

            }
            return ['output' => '', 'selected' => ''];
        }
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Grade::find()->where(['id' => $id])->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        };
        return $model;
    }
}