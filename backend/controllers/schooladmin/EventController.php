<?php

namespace backend\controllers\schooladmin;

class EventController extends \backend\controllers\teacher\EventController
{
    public function getViewPath()
    {
        return '@backend/views/teacher/event';
    }

    public function getCreatorId()
    {
        return null;
    }
}