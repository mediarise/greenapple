<?php

namespace backend\controllers\schooladmin;

class EventStatisticController extends \backend\controllers\teacher\EventStatisticController
{
    public function getViewPath()
    {
        return '@backend/views/teacher/event-statistic';
    }
}