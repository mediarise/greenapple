<?php

namespace backend\controllers\schooladmin;

use common\helpers\BackendUrl;
use Yii;

class ProductController extends \backend\controllers\teacher\ProductController
{
    public function getViewPath()
    {
        return '@backend/views/teacher/product';
    }

    protected function getUploadPhotoUrl()
    {
        return BackendUrl::to(["/schooladmin/product/upload-photo"]);
    }

//    public function actionUploadPhoto()
//    {
//        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//
//        $photoSaver = Yii::$container->get('PhotoSaver');
//        if ($photoSaver->save()) {
//            return $photoSaver->fileSaver->getFileInfo();
//        }
//
//        return $photoSaver->getErrors();
//    }
}