<?php

namespace backend\controllers\schooladmin;

use backend\controllers\BaseController;
use common\components\bl\SchoolHierarchy;
use common\models\Guardian;
use common\models\GuardianStudent;
use common\models\SchoolForm;
use Yii;
use common\models\Student;
use common\models\search\Student as StudentSearch;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $student = new Student();

        $schoolForm = new SchoolForm();

        if ($student->load(Yii::$app->request->post()) && $student->save()) {
            return $this->redirect(['update', 'id' => $student->id]);
        }

        if (Yii::$app->request->isPost) {
            $schoolForm->load(Yii::$app->request->post());
        }

        return $this->render('create', [
            'student' => $student,
            'boards' => SchoolHierarchy::getBoards(),
            'schoolForm' => $schoolForm,
        ]);
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $student = $this->findModel($id);
        $schoolForm = new SchoolForm();

        if ($student->load(Yii::$app->request->post()) && $student->save()) {
            return $this->redirect(['update', 'id' => $student->id]);
        }

        $guardiansDataProvider = new ActiveDataProvider([
            'query' => $student->getGuardians(),
            'sort' => false,
        ]);

        if (Yii::$app->request->isPost) {
            $schoolForm->load(Yii::$app->request->post());
        } else {
            $schoolForm->loadFromGrade($student->grade);
        }

        return $this->render('update', [
            'student' => $student,
            'boards' => SchoolHierarchy::getBoards(),
            'schoolForm' => $schoolForm,
            'guardiansDataProvider' => $guardiansDataProvider,
            'guardianStudent' => new GuardianStudent(['student_id' => $student->id]),
        ]);
    }

    public function actionGetGuardians($q = '')
    {
        $guardians = Guardian::find()
            ->select(['greeting', 'last_name', 'first_name', 'id'])
            ->orWhere(['ilike', 'first_name', $q])
            ->orWhere(['ilike', 'last_name', $q])
            ->limit(10)
            ->all();

        $result = function () use($guardians){
            foreach ($guardians as $guardian){
                yield [
                    'value' => $guardian->getOfficialName(),
                    'id' => $guardian->id,
                ];
            }
        };

        return $this->asJson($result());
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
