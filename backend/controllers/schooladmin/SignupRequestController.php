<?php

namespace backend\controllers\schooladmin;

use backend\controllers\BaseController;
use common\components\AjaxResponse;
use common\components\bl\SchoolHierarchy;
use common\components\bl\SignupRequestManager;
use common\components\bl\SignupRequestNotifier;
use common\models\SchoolAdmin;
use common\models\SchoolForm;
use common\models\search\SignupRequestGuardian as SignupRequestGuardianSearch;
use common\models\SignupRequestRejectionForm;
use common\models\SignupRequestGuardian;
use common\models\SignupRequestStudent;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class SignupRequestController extends BaseController
{
    private function getSignupRequestManager($id = 0)
    {
        static $manager;
        if (empty($manager)) {
            $manager = new SignupRequestManager($id, $this->getSchoolAdmin());
        }
        return $manager;
    }

    public function actionIndex()
    {
        $filterModel = new SignupRequestGuardianSearch();
        $filterModel->schoolAdmin = $this->getSchoolAdmin();
        $dataProvider = $filterModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel,
        ]);
    }

    public function actionView($id)
    {
        $schoolAdmin = $this->getSchoolAdmin();

        $manager = $this->getSignupRequestManager($id);

        $rejectionForm = new SignupRequestRejectionForm();


        $guardian = $manager->getGuardian();
        if ($guardian === null) {
            $guardian = $manager->newGuardian();
        }

        $signupRequest = $manager->getSignupRequest();

        return $this->render('signup-request', [
            'guardian' => $guardian,
            'signupRequest' => $signupRequest,
            'studentsDataProvider' => $this->getStudentDataProvider($id, $schoolAdmin),
            'rejectionForm' => $rejectionForm,
            'canInvite' => $signupRequest->getAdminStatus() === SignupRequestGuardian::STATUS_NOT_INVITED,
        ]);
    }

    public function actionReject($id)
    {
        $manager = $this->getSignupRequestManager($id);

        $notifier = new SignupRequestNotifier(
            $manager->getSignupRequest()->email,
            'signupRequestRejected');
        $manager->reject($notifier, Yii::$app->request->post());
        return $this->redirect(['schooladmin/signup-request']);
    }

    public function getSchoolAdmin()
    {
        $schoolAdmin = SchoolAdmin::findOne([
            'user_id' => Yii::$app->user->id,
        ]);
        if (!$schoolAdmin) {
            throw new ForbiddenHttpException();
        }
        return $schoolAdmin;
    }

    public function actionApproveStudent($requestId, $id)
    {
        $manager = $this->getSignupRequestManager($requestId);

        $signupRequestStudent = $manager->getStudent($id);

        if ($signupRequestStudent->student_id) {
            throw new BadRequestHttpException('Student already approved.');
        }

        $student = null;
        $schoolForm = new SchoolForm();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post('Student');
            $student = $manager->approveStudent($id, $post);
            if (!$student->isNewRecord) {
                return $this->toSignupRequestView($signupRequestStudent->signup_request_id);
            }
            $schoolForm->load(Yii::$app->request->post());
        } else {
            $student = $manager->newStudent($signupRequestStudent);
            $schoolForm->loadFromGrade($signupRequestStudent->grade);
        }

        return $this->render('approve-student', [
            'student' => $student,
            'boards' => SchoolHierarchy::getBoards(),
            'schoolForm' => $schoolForm,
            'signupRequestStudent' => $signupRequestStudent,
        ]);
    }

    public function actionRejectStudent($requestId, $id)
    {
        $manager = $this->getSignupRequestManager($requestId);
        $manager->rejectStudent($id);
        return $this->asJson(AjaxResponse::success());
    }

    public function toSignupRequestView($requestId)
    {
        return $this->redirect([
            'schooladmin/signup-request/view',
            'id' => $requestId,
        ]);
    }

    public function actionInvite($id)
    {
        $manager = $this->getSignupRequestManager($id);
        $notifier = new SignupRequestNotifier(
            $manager->getSignupRequest()->email,
            'signupRequestApproved');
        return $this->asJson($manager->invite($notifier));
    }

    public function getStudentDataProvider($requestId)
    {
        $manager = $this->getSignupRequestManager($requestId);
        return new ArrayDataProvider([
            'allModels' => $manager->getStudents()
        ]);
    }
}