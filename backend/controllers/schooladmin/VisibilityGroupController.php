<?php

namespace backend\controllers\schooladmin;

use backend\controllers\BaseController;
use common\components\AjaxResponse;
use common\components\bl\VisibilityGroupManager;
use common\helpers\BackendUrl;
use common\models\Grade;
use common\models\Student;
use common\models\teacher\VisibilityGroupForm;
use Yii;
use common\models\VisibilityGroup;
use common\models\search\VisibilityGroup as VisibilityGroupSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ServerErrorHttpException;

/**
 * VisibilityGroupController implements the CRUD actions for VisibilityGroup model.
 */
class VisibilityGroupController extends BaseController
{
    protected $manager = null;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VisibilityGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VisibilityGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCheckBeforeCreate($redirectOnSuccess = true)
    {
        $returnUrl = Yii::$app->request->post('returnUrl', BackendUrl::to(['index']));
        BackendUrl::remember($returnUrl);
        Yii::$app->session->set('returnUrl', $returnUrl);

        if ($redirectOnSuccess) {
            return $this->redirect(BackendUrl::to(['create']));
        }
        return $this->asJson(AjaxResponse::success());
    }

    /**
     * @return string|\yii\web\Response
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {
        $model = new VisibilityGroupForm();
        $visibilityGroup = new VisibilityGroup();
        $manager = $this->getManager();

        $returnUrl = Yii::$app->session->get('returnUrl', BackendUrl::to(['index']));

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $visibilityGroup->load($model->getAttributes(), '');
            if ($manager->save($visibilityGroup, $model)) {
                Yii::$app->session->set('newlyCreatedVisibilityGroupId', $visibilityGroup->id);
                return $this->redirect($returnUrl);
            }
        }

        $gradeList = $manager->getGradeList();
        if (!$gradeList) {
            throw new ServerErrorHttpException(
                'Sorry, you are not linked to any grade. Please, contact with your school administrator.'
            );
        }
        $relatedGrades = $manager->getGradeQuery()->all();
        $currentYearRelatedGrades = $manager->getGradeQuery()
            ->andWhere(['year' => Grade::getCurrentAcademicYear()])
            ->all();
        $memberedGradeList = $manager->getGradeMemberedList($relatedGrades);
        $memberedCurrentYearGradeList = $manager->getGradeMemberedList($currentYearRelatedGrades);
        $relatedStudents = Student::find()->all();
        if (!$relatedStudents) {
            throw new ServerErrorHttpException(
                'Sorry, you have no students to assign. Please, contact with your school administrator.'
            );
        }
        $studentList = $manager->getStudentMemberedList($relatedStudents);

        $currentYearGrades = $manager->getCurrentYearGrades();

        $relatedTeachers = $manager->getTeacherQuery()->all();
        $teacherMemberedList = $manager->getTeacherMemberedList($relatedTeachers);

        return $this->render('create', [
            'model' => $model,
            'gradeList' => $gradeList,
            'studentList' => $studentList,
            'haveStudentMember' => true,
            'currentYearGrades' => $currentYearGrades,
            'memberedGradeList' => $memberedGradeList,
            'haveGradeMember' => false,
            'memberedCurrentYearGradeList' => $memberedCurrentYearGradeList,
            'memberedTeacherList' => $teacherMemberedList,
            'haveTeacherMember' => false,
        ]);
    }



    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionUpdate($id)
    {
        $model = new VisibilityGroupForm();
        $visibilityGroup = $this->findModel($id);
        $manager = $this->getManager();

        if (
            $model->load($visibilityGroup->getAttributes(), '') &&
            $model->load(Yii::$app->request->post()) &&
            $model->validate()
        ) {
            $visibilityGroup->load($model->getAttributes(), '');
            if ($manager->save($visibilityGroup, $model)) {
                return $this->redirect(['index']);
            }
        }

        $gradeList = $manager->getGradeList();
        $currentYearGrades = $manager->getCurrentYearGrades();

        $relatedGrades = $manager->getGradeQuery()->all();
        $currentYearRelatedGrades = $manager->getGradeQuery()
            ->andWhere(['year' => Grade::getCurrentAcademicYear()])
            ->all();
        $gradesInGroup = $manager->getGradeQuery()->asArray()->inVisibilityGroup($model->id)->all();
        $memberedGradeList = $manager->getGradeMemberedList($relatedGrades, $gradesInGroup);
        $memberedCurrentYearGradeList = $manager->getGradeMemberedList($currentYearRelatedGrades, $gradesInGroup);

        $relatedStudents = Student::find()->all();
        $studentsInGroup = Student::find()->asArray()->inVisibilityGroup($model->id)->all();
        $studentList = $manager->getStudentMemberedList($relatedStudents, $studentsInGroup);

        $teachersInGroup = $manager->getTeacherQuery()->asArray()->inVisibilityGroup($model->id)->all();
        $relatedTeachers = $manager->getTeacherQuery()->all();
        $teacherMemberedList = $manager->getTeacherMemberedList($relatedTeachers, $teachersInGroup);

        return $this->render('update', [
            'model' => $model,
            'gradeList' => $gradeList,
            'studentList' => $studentList,
            'haveStudentMember' => !empty($studentsInGroup),
            'currentYearGrades' => $currentYearGrades,
            'memberedGradeList' => $memberedGradeList,
            'haveGradeMember' => !empty($gradesInGroup),
            'memberedCurrentYearGradeList' => $memberedCurrentYearGradeList,
            'memberedTeacherList' => $teacherMemberedList,
            'haveTeacherMember' => !empty($teachersInGroup),
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \ErrorException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionDelete($id)
    {
        $manager = $this->getManager();
        if ($manager->delete($id)) {
            Yii::$app->session->setFlash('success', 'Visibility Group successfully deleted.');
        } else {
            Yii::$app->session->setFlash('error', 'Visibility Group not deleted. Please try again later.');
        }
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return VisibilityGroup
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if ($visibilityGroup = VisibilityGroup::findOne($id)) {
            return $visibilityGroup;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    protected function getManager()
    {
        if ($this->manager === null) {
            $this->manager = new VisibilityGroupManager(['actor' => $this->actor]);
        }
        return $this->manager;
    }
}
