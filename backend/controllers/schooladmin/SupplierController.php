<?php

namespace backend\controllers\schooladmin;


use common\models\Supplier;

class SupplierController extends \backend\controllers\boardadmin\SupplierController
{
    public function getViewPath()
    {
        return '@backend/views/boardadmin/supplier';
    }

    public function setDefaultValues(Supplier $model)
    {
        $actor = $this->actor;
        $model->board_id = $actor['board_id'];
        $model->school_id = $actor['school_id'];
    }
}