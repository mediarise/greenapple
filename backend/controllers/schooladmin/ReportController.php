<?php

namespace backend\controllers\schooladmin;

class ReportController extends \backend\controllers\teacher\ReportController
{
    public function getViewPath()
    {
        return '@backend/views/teacher/report';
    }

    protected function getUserId()
    {
        return null;
    }
}