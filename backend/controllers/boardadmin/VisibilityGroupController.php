<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.04.18
 * Time: 16:33
 */

namespace backend\controllers\boardadmin;


use backend\controllers\BaseController;
use common\models\School;
use yii\helpers\ArrayHelper;

class VisibilityGroupController extends BaseController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $schools = School::find()->asArray()->all();
        $schoolList = ArrayHelper::map($schools, 'id', 'name');
        return $this->render('index', [
            'schoolList' => $schoolList,
        ]);
    }
}