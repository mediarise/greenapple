<?php
namespace backend\controllers\boardadmin;

use backend\controllers\BaseController;

class IndexController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}