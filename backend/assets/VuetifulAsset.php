<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class VuetifulAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vue-data-grid.css',
        'css/vue2-scrollbar.css',
    ];
    public $js = [
        ['js/date-fns.js', 'position' => View::POS_HEAD],
        ['js/components.bundle.js', 'position' => View::POS_HEAD],
        ['js/vue/vue2-scrollbar.js', 'position' => View::POS_HEAD],
    ];
    public $depends = [
        'backend\assets\VueAsset',
    ];

}