<?php

namespace backend\assets;

use yii\web\AssetBundle;

class SummernoteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/summernote/summernote.css',
        'css/plugins/summernote/summernote-bs3.css',
    ];
    public $js = [
        'js/plugins/summernote/summernote.min.js',
        'js/form-editor-plugins/preview.js',
        'js/form-editor-plugins/insert-vars.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}