<?php

namespace backend\assets;

use yii\web\AssetBundle;

class CalendarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/fullcalendar/fullcalendar.css',
        [
            'css/plugins/fullcalendar/fullcalendar.print.css',
            'media' => 'print'
        ]
    ];
    public $js = [
        'js/plugins/fullcalendar/moment.min.js',
        'js/plugins/fullcalendar/fullcalendar.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];

}