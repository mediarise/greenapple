<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class VueAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        ['js/vue.js', 'position' => View::POS_HEAD],
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset'
    ];
}