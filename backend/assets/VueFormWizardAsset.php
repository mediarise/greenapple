<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class VueFormWizardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/vue-form-wizard/vue-form-wizard.min.css',
    ];
    public $js = [
        'js/plugins/vue-form-wizard/vue-form-wizard.js',
    ];
    public $depends = [
        'backend\assets\VueAsset',
    ];

}