<?php

namespace backend\assets;

use frontend\assets\InspiniaAsset;

/**
 * Main backend application asset bundle.
 */
class BackendAsset extends InspiniaAsset
{

}
