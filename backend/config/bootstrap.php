<?php

use common\components\bl\constraint\Constraint;
use common\components\role_factory\FactoryBase;
use common\query\EventQuery;
use common\query\FormQuery;
use common\query\ProductQuery;
use frontend\widgets\SideBarHeader;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\web\Application;
use yii\web\View;

$url = explode('/', $_SERVER['REQUEST_URI']);

if (count($url) < 3) {
    return;
}

/** @var FactoryBase $factory */
$factory = FactoryBase::create($url[2]);

if ($factory === null) {
    return;
}

$container->setSingleton('role-factory', $factory);
$container->setSingleton('Actor', $factory->getActorCreator());
$container->setSingleton(Constraint::class, $factory->getConstraintCreator());
$container->set(View::class, $factory->getViewCreator());
$container->set(SideBarHeader::class, $factory->getSideBarHeaderCreator());
$container->set(ProductQuery::class, $factory->getProductQueryCreator());
$container->set(FormQuery::class, $factory->getFormQueryCreator());
$container->set(EventQuery::class, $factory->getEventQueryCreator());
//$container->set(Breadcrumbs::class, $factory->getBreadcrumbsCreator());

$factory->bootstrap();

Event::on(Application::class, Application::EVENT_BEFORE_ACTION, function ($event) {
    /** @var Application $app */
    $app = $event->sender;
    /** @noinspection PhpUndefinedMethodInspection */
    $app->setHomeUrl($app->getUser()->getHomeUrl());
});

Event::on(ActiveRecord::class, ActiveRecord::EVENT_BEFORE_INSERT, function ($event) {
    $constraint = Yii::$container->get(Constraint::class);
    if (empty($constraint)) {
        return;
    }

    $model = $event->sender;
    $methodName = 'loadDefaultsFor' . (new ReflectionClass($model))->getShortName();
    if (method_exists($constraint, $methodName)) {
        call_user_func([$constraint, $methodName], $model);
    }
});