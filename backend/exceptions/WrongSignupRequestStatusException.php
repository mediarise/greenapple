<?php

namespace backend\exceptions;


class WrongSignupRequestStatusException extends \Exception
{
    /**
     * WrongSignupRequestStatusException constructor.
     */
    public function __construct()
    {
    }
}