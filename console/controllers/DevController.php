<?php

namespace console\controllers;

use common\components\bl\CartManager;
use common\components\bl\enum\ProductTax;
use common\components\bl\ledger\EventLedger;
use common\components\bl\ledger\GuardianEventLedger;
use common\components\bl\NameInquisitizor;
use common\components\bl\teacher\EventManager;
use common\models\Cart;
use common\models\DocEvent;
use common\models\EventProduct;
use common\models\Form;
use common\models\VisibilityGroup;
use Yii;
use yii\db\Exception;
use common\components\bl\Greeting;
use common\models\Product;
use common\models\SignupRequestGuardian;
use common\models\SignupRequestStudent;
use Faker\Factory;
use yii\db\Query;

class DevController extends base\Controller
{
    /**
     * @param $userName
     * @param $schoolId
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionAddSchoolAdmin($userName, $schoolId)
    {
        SeedController::addUser($userName, 'schoolAdmin', [
            'schoolId' => (int)$schoolId,
        ]);

    }

    public function actionAddSignupRequest()
    {
        $faker = Factory::create();

        $transaction = Yii::$app->db->beginTransaction();

        $signupRequest = new SignupRequestGuardian();
        $signupRequest->email = $faker->email;
        $signupRequest->first_name = NameInquisitizor::firstName();
        $signupRequest->last_name = NameInquisitizor::lastName();
        $signupRequest->greeting = $faker->numberBetween(1, count(Greeting::getNames()));
        $signupRequest->status = SignupRequestGuardian::STATUS_NEW;
        $signupRequest->save(false);

        foreach (func_get_args() as $gradeId) {
            $student = new SignupRequestStudent();
            $student->signup_request_id = $signupRequest->id;
            $student->relationship = 1;
            $student->oen = Yii::$app->security->generateRandomString(8);
            $student->grade_id = $gradeId;
            $student->first_name = NameInquisitizor::firstName();
            $student->last_name = NameInquisitizor::lastName();
            $student->birthday = $faker->date();
            $student->gender = $faker->colorName;
            $student->status = SignupRequestStudent::STATUS_NEW;
            $student->save(false);
        }

        try {
            $transaction->commit();
        } catch (Exception $e) {
            $this->printErrors([$e->getMessage()]);
        }
        $this->println($signupRequest->id);
    }

    public function actionAddProduct($count)
    {
        $faker = Factory::create();

        $transaction = Yii::$app->db->beginTransaction();

        try {
            for ($i = 0; $i < $count; $i++) {
                $product = new Product();
                $product->school_id = 1;
                $product->board_id = 1;
                $product->owner_id = null;
                $product->sku = $faker->regexify('[a-zA-Z0-9]{10}');
                $product->title = $faker->realText(80);
                $product->description = $faker->realText();

                $tax = $faker->randomElement(ProductTax::list());
                $price = $this->randomPrice($faker, 1, 20);
                $product->price = $price;
                $product->cost = $this->randomPrice($faker, 1, 20);
                $product->tax = $tax;
                if ($tax == ProductTax::HST_13_INCLUDED) {
                    $netPrice = $product->price * $product->getTaxRatio();
                    if ($netPrice - $product->cost < 0) {
                        $product->price += $product->cost;
//                        die($product->price . ' - ' . $product->cost . ' - ' . $cost . ' - ' . $netPrice);
                    }
                }

                $product->is_free = false;
                if (!$product->save()) {
                    throw new \ErrorException(var_dump($product->getErrors()));
                }
                $this->println("Product #{$product->id} created.");
            }

            $transaction->commit();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        return true;
    }

    private function randomPrice($faker, $min, $max)
    {
        return number_format($faker->randomFloat(2, $min, $max), 2, '.', '');
    }

    public function actionAddForm($count = 10)
    {
        $faker = Factory::create();

        $controlTypes = array_keys(Form::getControlFields());

        foreach (range(1, $count) as $i) {
            $form = new Form();
            $form->school_id = 1;
            $form->board_id = 1;
            $form->owner_id = null;
            $form->title = $faker->colorName;
            $form->description = $faker->realText();
            $form->html = $faker->realText(1000);
            $form->control = $faker->randomElement($controlTypes);
            $form->save();
        }
    }

    public function actionAddVisibilityGroup($count = 10)
    {
        $faker = Factory::create();
        $grades = Yii::$app->db->createCommand('select grade_id from lnk_teacher_grade where teacher_id = 1')->queryColumn();
        $grade = $faker->randomElement($grades);
        $students = Yii::$app->db->createCommand('select id from student where grade_id = ' . $grade)->queryColumn();

        foreach (range(1, $count) as $i) {
            $ids = $faker->randomElements($students, $faker->numberBetween(4, count($students)));
            SeedController::addVisibilityGroup(null, $grade, VisibilityGroup::MEMBER_TYPE_STUDENT, $ids, $faker->colorName);
        }
    }

    public function actionTestPostEvent($eventId)
    {
        echo EventLedger::createStatus($eventId);
    }

    public function actionTestPostRepeatable($eventId)
    {
        echo EventLedger::createRepeatableStatus($eventId);
    }

    public function actionTestPurchase($eventId, $guardianId, $studentId, $productId)
    {
        $ledger = new GuardianEventLedger($eventId, $guardianId, $studentId);

        $eventProduct = EventProduct::find()->where(['lnk_event_product.product_id' => $productId])->joinWith('product')->one();
        $ledger->purchaseProduct((int)$studentId, $eventProduct, null, 3, 1);
    }

    public function actionTestAcceptForm($eventId, $guardianId, $studentId, $formId)
    {
        $ledger = new GuardianEventLedger($guardianId);

        $ledger->acceptForm($eventId, $studentId, $formId);
        //sleep(1);
        //$ledger->cancelForm($formId, $studentId, $formId);

    }

    public function actionTestAcceptEvent($eventId, $guardianId, $studentId)
    {
        $ledger = new GuardianEventLedger($guardianId);

        $ledger->acceptEvent($eventId, $studentId);
        //sleep(1);
        //$ledger->declineEvent();
    }

    public function actionGarbageVisibilityGroup($count)
    {
        $faker = Factory::create();
        $generator = function () use ($faker, $count) {
            for ($i = 1; $i < $count; $i++) {
                yield [$faker->colorName];
            }
        };
        Yii::$app->db->createCommand()
            ->batchInsert('visibility_group', ['name'], $generator())
            ->execute();
    }

    public function actionGarbageVisibilityGroupMember($count)
    {
        $faker = Factory::create();
        $generator = function () use ($faker, $count) {
            for ($i = 1; $i < $count; $i++) {
                yield [$faker->numberBetween(3, 100000), $faker->numberBetween(1, 5), $faker->numberBetween(4, 50000)];
            }
        };
        Yii::$app->db->createCommand()
            ->batchInsert('visibility_group_member', ['visibility_group_id', 'member_type', 'member_id'], $generator())
            ->execute();
    }

    public function actionTestCart($productId)
    {
        $cartManager = new CartManager(1);
        $product = EventProduct::findOne($productId);
        $cartManager->addToCart(1, $product, 1);
    }

    public function actionTestOrderId()
    {
        $cartManager = new CartManager(1);
        $this->println($cartManager->generateOrderId());
    }

    public function actionTestPurchasedQuantity($productId)
    {
        $cartManager = new CartManager(1);
        $product = EventProduct::findOne($productId);
        $cartManager->getPurchasedQuantity(1, $product);
    }

    public function actionTestGetCount($productId)
    {
        $cartManager = new CartManager(1);
        $product = EventProduct::findOne($productId);
        $this->println($cartManager->getProductQuantity(1, $product));
    }

    public function actionTestUpdateCounters($eventId, $studentId)
    {
        $ledger = new GuardianEventLedger(1);
        $ledger->updateCounters($eventId, $studentId, 0, 1);
        $ledger->updateCounters($eventId, $studentId, 1, 0);
    }

    public function actionTestDays($startDate, $endDate)
    {
        print_r(iterator_to_array(EventManager::dayGenerator($startDate, $endDate, [
            //'sun',
            'Monday',
            //'tue',
            'Wednesday',
            //'thu',
            'Friday',
            //'sat',
        ])));
    }

    public function actionUnpublish($eventId)
    {
        $children = ['in', 'doc_event_id', (new Query())
            ->select('id')
            ->from('doc_event')
            ->where(['predecessor' => $eventId])
        ];

        $condition = [
            'or',
            ['doc_event_id' => $eventId],
            $children
        ];

        /** @noinspection PhpUnhandledExceptionInspection */
        Yii::$app->db->createCommand()
            ->delete('ldg_event_status', $condition)
            ->execute();
        /** @noinspection PhpUnhandledExceptionInspection */
        Yii::$app->db->createCommand()
            ->delete('ldg_event_student_status', $condition)
            ->execute();
        /** @noinspection PhpUnhandledExceptionInspection */
        Yii::$app->db->createCommand()
            ->delete('ldg_event_activity_purchase', $condition)
            ->execute();
        /** @noinspection PhpUnhandledExceptionInspection */
        Yii::$app->db->createCommand()
            ->delete('ldg_event_activity_general', $condition)
            ->execute();
        EventProduct::deleteAll($children);
        Cart::deleteAll($children);
        DocEvent::deleteAll(['predecessor' => $eventId]);
        DocEvent::updateAll(['status' => DocEvent::STATUS_SAVED], ['id' => $eventId]);
    }
}