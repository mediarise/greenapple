<?php

namespace console\controllers;

use common\components\bl\Greeting;
use common\components\bl\NameInquisitizor;
use common\models\BoardAdmin;
use common\models\DocEvent;
use common\models\EventProduct;
use common\models\Form;
use common\models\GuardianStudent;
use common\models\Product;
use common\models\SchoolAdmin;
use common\models\Board;
use common\models\School;
use common\models\Grade;
use common\models\Guardian;
use common\models\SignupRequestGuardian;
use common\models\SignupRequestStudent;
use common\models\Student;
use common\models\Teacher;
use common\models\User;
use common\models\VisibilityGroup;
use console\controllers\base\Controller;
use DateTime;
use Faker\Factory;
use webvimark\modules\UserManagement\models\User as rbacUser;
use Yii;
use yii\helpers\ArrayHelper;
use yii\base\ErrorException;
use yii\helpers\Console;
use function Stringy\create as s;

class SeedController extends Controller
{
    /**
     * @var \Faker\Generator
     */
    private static $genders = [1 => 'male', 2 => 'female'];
    private static $faker;
    private static $passwordHash;
    private static $greetings;
    private static $firstSchoolGrades;

    /**
     * @return \Faker\Generator
     */
    public static function getFaker()
    {
        if (!self::$faker) {
            self::$faker = Factory::create();
        }
        return self::$faker;
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    private static function getPasswordHash()
    {
        if (!self::$passwordHash) {
            self::$passwordHash = Yii::$app->security->generatePasswordHash('password_0');
        }
        return self::$passwordHash;
    }

    private static function getFirstSchoolGrades()
    {
        if (empty(self::$firstSchoolGrades)) {
            $year = Grade::getCurrentAcademicYear();
            self::$firstSchoolGrades = Yii::$app->db->createCommand("select id from grade where school_id = 1 and year = '$year'")->queryColumn();
        }
        return self::$firstSchoolGrades;
    }

    public function actionIndex()
    {
        \Yii::$app->runAction('migrate/fresh');
        \Yii::$app->runAction('seed/seed-school-hierarchy');
    }


    public function actionSeedSchoolHierarchy()
    {
        return;
        /** @noinspection PhpUnreachableStatementInspection */
        $this->println('--- begin actionSeedSchoolHierarchy');
        Console::startProgress(0, 5);

        $faker = self::getFaker();
        $itemCount = 5;

        $schoolId = 1;
        for ($boardId = 1; $boardId <= $itemCount; $boardId++) {
            $board = new Board();
            $board->id = $boardId;
            $board->name = $faker->name;
            $board->save(false);

            for ($i = 1; $i <= $itemCount; $i++, $schoolId++) {
                $school = new School();
                $school->id = $schoolId;
                $school->board_id = $boardId;
                $school->name = $faker->name;
                $school->save(false);

                for ($j = 1; $j <= 10; $j++) {
                    $grade = new Grade();
                    $grade->school_id = $schoolId;
                    $grade->name = $faker->name;
                    $grade->save(false);
                }
            }
            Console::updateProgress($boardId, $itemCount);
        }
        Console::endProgress();
        $this->println('--- end actionSeedSchoolHierarchy');
    }

    /**
     * @param int $count
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionSeedGuardians($count = 100)
    {
        $method = self::getMethod('guardian');
        foreach (range(1, $count) as $i) {
            self::addUserInner('new-guardian' . $i, 'guardian', [], $method);
        }
    }

    /**
     * @param int $count
     * @throws \yii\base\Exception
     */
    public function actionSeedStudents($count = 150)
    {
        $faker = self::getFaker();
        $gradesIds = self::getFirstSchoolGrades();

        $guardians = Yii::$app->db->createCommand('select id from guardian')->queryColumn();
        $relationshipGenerator = function ($studentId) use ($faker, $guardians) {
            foreach ($faker->randomElements($guardians, 2) as $guardianId) {
                yield [$guardianId, $studentId, $faker->numberBetween(1, 6)];
            }
        };

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach (range(1, $count) as $i) {
            $student = new Student();
            $student->oen = $faker->lexify('?????????');
            $student->grade_id = $faker->randomElement($gradesIds);

            $genderIdx = rand(1, 2);
            $gender = self::$genders[$genderIdx];
            $student->first_name = NameInquisitizor::firstName($gender);
            $student->last_name = NameInquisitizor::lastName();

            $student->birthday = $faker->dateTimeBetween('-17 years', '-7 years')->format('Y-m-d');
            $student->gender = $genderIdx;
            $student->save(false);

            Yii::$app->db->createCommand()
                ->batchInsert('lnk_guardian_student', [
                    'guardian_id',
                    'student_id',
                    'relationship'
                ], $relationshipGenerator($student->id))
                ->execute();
        }
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionSeedSpringfield()
    {
        /** @noinspection PhpIncludeInspection */
        $data = require Yii::getAlias('@console') . '/data/springfield.php';

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($data['boards'] as $board => $schools) {
                $newBoard = new Board();
                $newBoard->name = $board;
                if (!$newBoard->save()) {
                    throw new ErrorException('Unable to save board.');
                }
                $data['boards'][$board]['id'] = $newBoard->id;
                foreach ($schools as $school => $grades) {
                    $newSchool = new School();
                    $newSchool->name = $school;
                    $newSchool->board_id = $newBoard->id;
                    $newSchool->save();
                    $data['schools'][$school] = $newSchool->id;
                    foreach ($grades as $grade) {
                        $newGrade = new Grade();
                        $newGrade->name = $grade;
                        $newGrade->school_id = $newSchool->id;
                        $newGrade->year = Grade::getCurrentAcademicYear();
                        $newGrade->save();
                        $data['grades'][$grade] = ['id' => $newGrade->id, 'school_id' => $newSchool->id];
                    }
                }
            }

            $transaction->commit();
        } catch (\Exception $e) {
            var_dump($newBoard->getErrors());
        }

        foreach ($data['students'] as &$student) {
            $newStudent = new Student();
            $student['grade_id'] = $data['grades'][$student['grade']]['id'];
            $student['birthday'] = (new DateTime())
                ->sub(date_interval_create_from_date_string($student['age'] . ' years'))
                ->format('Y-m-d');
            $newStudent->load($student, '');
            $newStudent->oen = (string)rand(100000000, 999999999);
            $newStudent->save();
            $student['id'] = $newStudent->id;
            unset($student);
        }

        $this->seedUsersFromArray($data['guardians'], 'guardian', null, function ($item) use ($data) {
            foreach ($item['children'] as $child => $relationship) {
                $link = new GuardianStudent();
                $link->guardian_id = $item['id'];
                $link->student_id = $data['students'][$child]['id'];
                $link->relationship = $relationship;
                $link->save();
            }
        });

        $this->seedUsersFromArray($data['teachers'], 'teacher', function ($item) use ($data) {
            $item['school_id'] = $data['schools'][$item['school']];
            $item['doNotLinkGrades'] = true;
            return $item;
        }, function ($item) use ($data) {
            foreach ($item['grades'] as $grade) {
                /** @noinspection MissedFieldInspection */
                Yii::$app->db->createCommand()
                    ->insert('lnk_teacher_grade', ['teacher_id' => $item['id'], 'grade_id' => $data['grades'][$grade]['id']])
                    ->execute();
            }
        });

        $this->seedUsersFromArray($data['school-admins'], 'schoolAdmin', function ($item) use ($data) {
            $item['school_id'] = $data['schools'][$item['school']];
            return $item;
        });

        $this->seedUsersFromArray($data['board-admins'], 'boardAdmin', function ($item) use ($data) {
            $item['board_id'] = $data['boards'][$item['board']]['id'];
            return $item;
        });

        foreach ($data['guardians'] as $guardian) {
            $signupRequest = new SignupRequestGuardian();
            $signupRequest->load($guardian, '');
            $signupRequest->email = $guardian['first_name'] . $guardian['last_name'] . '@signup.ca';
            $signupRequest->status = SignupRequestGuardian::STATUS_NEW;
            $signupRequest->save();
            foreach ($guardian['children'] as $childName => $relationship) {
                $signupRequestStudent = new SignupRequestStudent();
                $signupRequestStudent->load($data['students'][$childName], '');
                $signupRequestStudent->relationship = $relationship;
                $signupRequestStudent->oen = (string)rand(100000000, 999999999);
                $signupRequestStudent->status = SignupRequestStudent::STATUS_NEW;
                $signupRequestStudent->link('signupRequest', $signupRequest);
            }
        }
    }

    /**
     * @param $array
     * @param $roleName
     * @param null $beforeSave
     * @param null $afterSave
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    private function seedUsersFromArray($array, $roleName, $beforeSave = null, $afterSave = null)
    {
        $method = self::getMethod($roleName);

        foreach ($array as &$item) {
            $options = $item;
            if ($beforeSave !== null) {
                $options = call_user_func($beforeSave, $options);
            }
            $userName = $item['first_name'] . $item['last_name'];
            $newItem = self::addUserInner($userName, $roleName, $options, $method);
            $item['id'] = $newItem->id;
            if ($afterSave !== null) {
                call_user_func($afterSave, $item);
            }
            unset($item);
        }
    }

    private static function greeting($gender)
    {
        if ($gender === 'male') {
            return Greeting::MR;
        }

        if (!self::$greetings) {
            self::$greetings = array_keys(Greeting::getNames());
        }

        return self::$greetings[rand(1, 3)];
    }

    /**
     * @throws \yii\base\Exception
     */
    public function actionSeedUsers()
    {
        self::addUsers('schooladmin', 'schoolAdmin');
        self::addUsers('activeuser', 'activeUser');
        self::addUsers('teacher', 'teacher');
        self::addUsers('guardian', 'guardian');
        self::addUsers('boardadmin', 'boardAdmin');
        self::addUsers('greenappleadmin', 'greenappleAdmin');
    }

    public static function getMethod($role)
    {
        $method = 'add' . (string)s($role)->upperCaseFirst();
        if (method_exists(self::class, $method)) {
            $method = [self::class, $method];
        }
        return $method;
    }

    /**
     * @param $userName
     * @param $roleName
     * @param array $options
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public static function addUsers($userName, $roleName, $options = [])
    {
        $method = self::getMethod($roleName);
        foreach (range(1, 10) as $i) {
            self::addUserInner($userName . $i, $roleName, $options, $method);
        }
    }

    /**
     * @param $userName
     * @param $roleName
     * @param array $options
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public static function addUser($userName, $roleName, $options = [])
    {
        $method = self::getMethod($roleName);
        self::addUserInner($userName, $roleName, $options, $method);
    }

    /**
     * @param $userName
     * @param $roleName
     * @param $options
     * @param $method
     * @return mixed|null
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    private static function addUserInner($userName, $roleName, $options, $method)
    {
        $userName = "{$userName}@{$roleName}.ca";
        $transaction = \Yii::$app->db->beginTransaction();

        $user = new User();
        $user->status = User::STATUS_ACTIVE;
        $user->username = $userName;
        $user->email = $userName;
        $user->password_hash = self::getPasswordHash();
        $user->generateAuthKey();
        $user->save();

        rbacUser::assignRole($user->id, $roleName);

        $result = null;

        if (is_array($method)) {
            $faker = self::getFaker();
            $options = array_merge([
                'first_name' => NameInquisitizor::firstName(),
                'last_name' => NameInquisitizor::lastName()
            ], $options);
            $result = call_user_func($method, $user, $options);
        }

        $transaction->commit();

        return $result;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private static function addBoardAdmin($user, $options)
    {
        $options = array_merge([
            'board_id' => 1,
        ], $options);
        $boardAdmin = new BoardAdmin();
        $boardAdmin->load($options, '');
        $boardAdmin->link('user', $user);

        return $boardAdmin;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private static function addSchoolAdmin($user, $options)
    {
        $options = array_merge([
            'school_id' => 1,
        ], $options);
        $schoolAdmin = new SchoolAdmin();
        $schoolAdmin->load($options, '');
        $schoolAdmin->link('user', $user);

        return $schoolAdmin;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param $user
     * @param $options
     * @return Guardian
     * @throws \yii\base\Exception
     */
    private static function addGuardian($user, $options)
    {
        $faker = self::getFaker();
        $gender = $faker->randomElement(self::$genders);
        $options = array_merge([
            'first_name' => NameInquisitizor::firstName($gender),
            'last_name' => NameInquisitizor::lastName(),
            'greeting' => self::greeting($gender),
            'invite_token' => Yii::$app->security->generateRandomString(),
        ], $options);
        $guardian = new Guardian();
        $guardian->load($options, '');
        $guardian->link('user', $user);

        return $guardian;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private static function addTeacher($user, $options)
    {
        $faker = self::getFaker();
        $gender = $faker->randomElement(self::$genders);
        $options = array_merge([
            'school_id' => 1,
            'first_name' => NameInquisitizor::firstName($gender),
            'last_name' => NameInquisitizor::lastName(),
        ], $options);
        $teacher = new Teacher();
        $teacher->load($options, '');
        $teacher->link('user', $user);

        if (ArrayHelper::remove($options, 'doNotLinkGrades', false)) {
            return $teacher;
        }

        $grades = self::getFirstSchoolGrades();

        $teacherGradeGenerator = function ($grades) use ($teacher) {
            foreach ($grades as $grade) {
                yield [
                    $teacher->id,
                    $grade,
                ];
            }
        };

        Yii::$app->db->createCommand()
            ->batchInsert('lnk_teacher_grade', ['teacher_id', 'grade_id'], $teacherGradeGenerator($grades))
            ->execute();

        return $teacher;
    }

    public function actionSeedEvents($teacherId, $count = 10)
    {
        $maxProductId = Yii::$app->db->createCommand('select max(id) from product')->queryScalar();
        if ($maxProductId < 20) {
            Yii::$app->runAction('dev/add-product', [20]);
        }
        $maxFormId = Yii::$app->db->createCommand('select max(id) from form')->queryScalar();
        if ($maxFormId < 20) {
            Yii::$app->runAction('dev/add-form', [20]);
        }

        $maxStudentId = Yii::$app->db->createCommand('select max(id) from student')->queryScalar();
        if ($maxStudentId < 100) {
            Yii::$app->runAction('seed/seed-students');
            Yii::$app->runAction('dev/add-visibility-group');
        }

        $products = Product::find()->all();
        $forms = Form::find()->all();

        $faker = self::getFaker();
        $transaction = Yii::$app->db->beginTransaction();

        $grades = Yii::$app->db->createCommand('select grade_id from lnk_teacher_grade where teacher_id = 1')->queryColumn();
        $gradeId = $faker->randomElement($grades);
        $students = Yii::$app->db->createCommand('select id from student where grade_id = ' . $gradeId)->queryColumn();

        $visibilityGroups = Yii::$app->db->createCommand('select id from visibility_group where doc_event_id is null')->queryColumn();
        if (count($visibilityGroups) < 10) {
            Yii::$app->runAction('dev/add-visibility-group');
            $visibilityGroups = Yii::$app->db->createCommand('select id from visibility_group where doc_event_id is null')->queryColumn();
        }

        $sections = [
            function ($event) use ($gradeId) {
                self::addVisibilityGroup($event->id, $gradeId, VisibilityGroup::MEMBER_TYPE_GRADE, [$gradeId]);
            },
            function ($event) use ($students, $gradeId, $faker) {
                self::addVisibilityGroup($event->id, $gradeId, VisibilityGroup::MEMBER_TYPE_STUDENT, $faker->randomElements($students, 5));
            },
            function ($event) use ($gradeId, $visibilityGroups, $faker) {
                self::addVisibilityGroup($event->id, $gradeId, VisibilityGroup::MEMBER_TYPE_VISIBILITY_GROUP, [$faker->randomElement($visibilityGroups)]);
            },
        ];

        foreach (range(1, $count) as $i) {
            $event = new DocEvent();
            $event->creator_id = $teacherId;
            $event->type = DocEvent::EVENT_TYPE_FIELD_TRIP;
            $event->ledger_account_id = 0;
            $event->title = $faker->colorName;
            $event->description = $faker->realText();
            $event->is_required = true;
            $event->is_parent_action_required = true;
            $startDate = $faker->dateTimeBetween('-10 days', '+30 days')->format('Y-m-d h:i:s');
            $event->start_date = $startDate;
            $event->end_date = $faker->dateTimeBetween($startDate, '+35 days')->format('Y-m-d h:i:s');
            $event->due_date = $faker->dateTimeBetween('now', '+30 days')->format('Y-m-d h:i:s');
            $event->duration = rand(60, 200);
            $event->is_repeatable = false;
            $event->repeat_on_days = '';
            $event->frequency = 1;
            $event->save();

            $randomProducts = $faker->randomElements($products, $faker->numberBetween(1, 5));

            foreach ($randomProducts as $product) {
                $eventProduct = new EventProduct();
                $eventProduct->load($product->attributes, '');
                $eventProduct->doc_event_id = $event->id;
                $eventProduct->product_id = $product->id;
                $eventProduct->min_quantity = 1;
                $eventProduct->max_quantity = 1;
                $eventProduct->min_interval = 0;
                $eventProduct->save();
            }

            $randomForms = $faker->randomElements($forms, $faker->numberBetween(1, 3), false);
            foreach ($randomForms as $form) {
                $event->link('forms', $form, ['is_mandatory' => true]);
            }
            call_user_func($faker->randomElement($sections), $event);
        }
        $transaction->commit();
    }

    public static function addVisibilityGroup($eventId, $gradeId, $type, $ids, $name = null)
    {
        $vg = new VisibilityGroup([
            'doc_event_id' => $eventId,
            'name' => $name,
            'board_id' => 1,
            'school_id' => 1,
            'grade_id' => $gradeId,
        ]);
        $vg->save();

        $rowGenerator = function ($ids) use ($vg, $type) {
            foreach ($ids as $itemId) {
                yield [
                    $vg->id,
                    $type,
                    $itemId
                ];
            }
        };

        Yii::$app->db->createCommand()
            ->batchInsert('visibility_group_member', ['visibility_group_id', 'member_type', 'member_id'],
                $rowGenerator($ids))
            ->execute();
    }
}