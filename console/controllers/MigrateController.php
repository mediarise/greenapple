<?php

namespace console\controllers;

use yii\console\ExitCode;

class MigrateController extends \yii\console\controllers\MigrateController
{
    public function actionFresh()
    {
        if (YII_ENV_PROD) {
            $this->stdout("YII_ENV is set to 'prod'.\nRefreshing migrations is not possible on production systems.\n");
            return ExitCode::OK;
        }

        if ($this->confirm(

            "Are you sure you want to reset the database and start the migration from the beginning?\nAll data will be lost irreversibly!")) {
            require __DIR__ . '/../migrations/m180501_134844_create_functions.php';
            \m180501_134844_create_functions::dropFunctions($this);
            $this->truncateDatabase();
            $this->actionUp();
        } else {
            $this->stdout('Action was cancelled by user. Nothing has been performed.');
        }
    }

    public function execute($sql)
    {
        \Yii::$app->db->createCommand($sql)->execute();
        $this->stdout("$sql\n");
    }
}