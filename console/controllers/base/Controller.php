<?php
/**
 * Created by PhpStorm.
 * User: crunc
 * Date: 3/31/2018
 * Time: 1:42 PM
 */

namespace console\controllers\base;


use yii\helpers\Console;

class Controller extends \yii\console\Controller
{
    protected function println($text)
    {
        echo $text;
        echo PHP_EOL;
    }

    protected function printErrors($errors)
    {
        if (!is_array($errors) || count($errors) === 0) {
            return true;
        }
        foreach ($errors as $attributeName => $attributeErrors) {
            foreach ($attributeErrors as $attributeError) {
                $this->stdout($attributeError . "\n", Console::FG_RED);
            }
        }
        return true;
    }

}