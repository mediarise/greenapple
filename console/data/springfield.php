<?php

use common\components\bl\Greeting;
use common\components\bl\Gender;
use common\components\bl\FamilyRelationship;

return [
    'guardians' => [
        'Homer Simpson' => [
            'first_name' => 'Homer',
            'last_name' => 'Simpson',
            'greeting' => Greeting::MR,
            'children' => [
                'Bart Simpson' => FamilyRelationship::PARENT,
                'Lisa Simpson' => FamilyRelationship::PARENT,
                'Maggie Simpson' => FamilyRelationship::PARENT,
            ]
        ],
        'Marge Simpson' => [
            'first_name' => 'Marge',
            'last_name' => 'Simpson',
            'greeting' => Greeting::MRS,
            'children' => [
                'Bart Simpson' => FamilyRelationship::PARENT,
                'Lisa Simpson' => FamilyRelationship::PARENT,
                'Maggie Simpson' => FamilyRelationship::PARENT,
            ]
        ],
        'Jacqueline Bouvier' => [
            'first_name' => 'Jacqueline',
            'last_name' => 'Bouvier',
            'greeting' => Greeting::MRS,
            'children' => [
                'Bart Simpson' => FamilyRelationship::GRANDPARENT,
                'Lisa Simpson' => FamilyRelationship::GRANDPARENT,
                'Maggie Simpson'=> FamilyRelationship::GRANDPARENT,
            ]
        ],
        'Patty Bouvier' => [
            'first_name' => 'Patty',
            'last_name' => 'Bouvier',
            'greeting' => Greeting::MRS,
            'children' => [
                'Bart Simpson' => FamilyRelationship::AUNT_UNCLE,
                'Lisa Simpson' => FamilyRelationship::AUNT_UNCLE,
                'Maggie Simpson'=> FamilyRelationship::AUNT_UNCLE,
            ]
        ],
        'Selma Bouvier' => [
            'first_name' => 'Selma',
            'last_name' => 'Bouvier',
            'greeting' => Greeting::MRS,
            'children' => [
                'Bart Simpson' => FamilyRelationship::AUNT_UNCLE,
                'Lisa Simpson' => FamilyRelationship::AUNT_UNCLE,
                'Maggie Simpson' => FamilyRelationship::AUNT_UNCLE,
                'Ling Bouvier' => FamilyRelationship::ADOPTIVE_PARENT,
            ]
        ],
        'Mr. Starbeam' => [
            'first_name' => 'Noname',
            'last_name' => 'Starbeam',
            'greeting' => Greeting::MR,
            'children' => [
                'Dolph Starbeam' => FamilyRelationship::PARENT
            ]
        ],
        'Starla Starbeam' => [
            'first_name' => 'Starla',
            'last_name' => 'Starbeam',
            'greeting' => Greeting::MRS,
            'children' => [
                'Dolph Starbeam' => FamilyRelationship::PARENT,
            ]
        ],
        'Ned Flanders' => [
            'first_name' => 'Ned',
            'last_name' => 'Flanders',
            'greeting' => Greeting::MR,
            'children' => [
                'Rod Flanders' => FamilyRelationship::PARENT,
                'Todd Flanders' => FamilyRelationship::PARENT,
            ]
        ],
        'Maude Flanders' => [
            'first_name' => 'Maude',
            'last_name' => 'Flanders',
            'greeting' => Greeting::MRS,
            'children' => [
                'Rod Flanders' => FamilyRelationship::PARENT,
                'Todd Flanders' => FamilyRelationship::PARENT,
            ]
        ],
        'Edna Krabappel' => [
            'first_name' => 'Edna',
            'last_name' => 'Krabappel',
            'greeting' => Greeting::MRS,
            'children' => [
                'Rod Flanders' => FamilyRelationship::STEPPARENT,
                'Todd Flanders' => FamilyRelationship::STEPPARENT,
            ]
        ],
        'Ginger Flanders' => [
            'first_name' => 'Ginger',
            'last_name' => 'Flanders',
            'greeting' => Greeting::MRS,
            'children' => [
                'Rod Flanders' => FamilyRelationship::STEPPARENT,
                'Todd Flanders' => FamilyRelationship::STEPPARENT,
            ]
        ],
        'Agnes Flanders' => [
            'first_name' => 'Agnes',
            'last_name' => 'Flanders',
            'greeting' => Greeting::MRS,
            'children' => [
                'Rod Flanders' => FamilyRelationship::GRANDPARENT,
                'Todd Flanders' => FamilyRelationship::GRANDPARENT,
            ]
        ],
        'Nedsel Flanders' => [
            'first_name' => 'Nedsel',
            'last_name' => 'Flanders',
            'greeting' => Greeting::MR,
            'children' => [
                'Rod Flanders' => FamilyRelationship::GRANDPARENT,
                'Todd Flanders' => FamilyRelationship::GRANDPARENT,
            ]
        ],
        'Eddie Muntz' => [
            'first_name' => 'Eddie',
            'last_name' => 'Muntz',
            'greeting' => Greeting::MR,
            'children' => [
                'Nelson Muntz' => FamilyRelationship::PARENT,
                'Maureen Muntz' => FamilyRelationship::PARENT,
                'Reilly Muntz' => FamilyRelationship::AUNT_UNCLE,
                'Chuck Muntz' => FamilyRelationship::AUNT_UNCLE,
            ]
        ],
        'Mrs. Muntz' => [
            'first_name' => 'noname',
            'last_name' => 'Muntz',
            'greeting' => Greeting::MRS,
            'children' => [
                'Nelson Muntz' => FamilyRelationship::PARENT,
                'Maureen Muntz' => FamilyRelationship::PARENT,
                'Reilly Muntz' => FamilyRelationship::AUNT_UNCLE,
                'Chuck Muntz' => FamilyRelationship::AUNT_UNCLE,
            ]
        ],
        'Judge Muntz' => [
            'first_name' => 'Judge',
            'last_name' => 'Muntz',
            'greeting' => Greeting::MR,
            'children' => [
                'Nelson Muntz' => FamilyRelationship::GRANDPARENT,
                'Maureen Muntz' => FamilyRelationship::GRANDPARENT,
                'Reilly Muntz' => FamilyRelationship::GRANDPARENT,
                'Chuck Muntz' => FamilyRelationship::GRANDPARENT,
            ]
        ],
    ],
    'students' => [
        'Bart Simpson' => [
            'first_name' => 'Bart',
            'last_name' => 'Simpson',
            'age' => 11,
            'gender' => Gender::MALE,
            'grade' => '4th Grade',
        ],
        'Lisa Simpson' => [
            'first_name' => 'Lisa',
            'last_name' => 'Simpson',
            'age' => 8,
            'gender' => Gender::FEMALE,
            'grade' => '2nd Grade',
        ],
        'Maggie Simpson' => [
            'first_name' => 'Maggie',
            'last_name' => 'Simpson',
            'age' => 1,
            'gender' => Gender::FEMALE,
            'grade' => '1st Grade',
        ],
        'Wendell Borton' => [
            'first_name' => 'Wendell',
            'last_name' => 'Borton',
            'age' => 11,
            'gender' => Gender::MALE,
            'grade' => '4th Grade',
        ],
        'Ling Bouvier' => [
            'first_name' => 'Ling',
            'last_name' => 'Bouvier',
            'age' => 2,
            'gender' => Gender::FEMALE,
            'grade' => '1st Grade',
        ],
        'Kyle Database' => [
            'first_name' => 'Kyle',
            'last_name' => 'Database',
            'age' => 14,
            'gender' => Gender::MALE,
            'grade' => '4th Grade',
        ],
        'Dolph Starbeam' => [
            'first_name' => 'Dolph',
            'last_name' => 'Starbeam',
            'age' => 17,
            'gender' => Gender::MALE,
            'grade' => '6th Grade',
        ],
        'Rod Flanders' => [
            'first_name' => 'Rod',
            'last_name' => 'Flanders',
            'age' => 17,
            'gender' => Gender::MALE,
            'grade' => '4th Grade',
        ],
        'Todd Flanders' => [
            'first_name' => 'Todd',
            'last_name' => 'Flanders',
            'age' => 17,
            'gender' => Gender::MALE,
            'grade' => '4th Grade',
        ],
        'Nelson Muntz' => [
            'first_name' => 'Nelson',
            'last_name' => 'Muntz',
            'age' => 17,
            'gender' => Gender::MALE,
            'grade' => '4th Grade',
        ],
        'Maureen Muntz' => [
            'first_name' => 'Maureen',
            'last_name' => 'Muntz',
            'age' => 17,
            'gender' => Gender::FEMALE,
            'grade' => '4th Grade',
        ],
        'Reilly Muntz' => [
            'first_name' => 'Reilly',
            'last_name' => 'Muntz',
            'age' => 17,
            'gender' => Gender::MALE,
            'grade' => '4th Grade',
        ],
        'Chuck Muntz' => [
            'first_name' => 'Chuck',
            'last_name' => 'Muntz',
            'age' => 17,
            'gender' => Gender::FEMALE,
            'grade' => '4th Grade',
        ],
    ],
    'teachers' => [
//        'Teresa Albright' => [
//            'first_name' => 'Teresa',
//            'last_name' => 'Albright'
//        ],
        'Edna Krabappel' => [
            'first_name' => 'Edna',
            'last_name' => 'Krabappel',
            'school' => 'Springfield Elementary School',
            'grades' => [
                '4th Grade',
            ]
        ]
    ],
    'board-admins' => [
        'Gary Chalmers' => [
            'first_name' => 'Gary',
            'last_name' => 'Chalmers',
            'board' => 'Springfield board',
        ]
    ],
    'school-admins' => [
        'Seymour Skinner' => [
            'first_name' => 'Seymour',
            'last_name' => 'Skinner',
            'school' => 'Springfield Elementary School',
        ]
    ],
    'boards' => [
        'Springfield board' => [
            'Springfield Elementary School' => [
                '1st Grade',
                '2nd Grade',
                '4th Grade',
                '6th Grade',
            ]
        ]
    ]
];