<?php

use console\base\Migration;

/**
 * Handles the creation of table `event_photo`.
 */
class m180430_213813_create_event_photo_table extends Migration
{
    public $tableName = 'event_photo';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'event_id' => $this->bigInteger(),
            'url' => $this->string(255),
            'is_main' => $this->boolean(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'event_id' => 'doc_event'
        ];
    }
}
