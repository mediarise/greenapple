<?php

use console\base\Migration;

/**
 * Handles the creation of table `greenapple_admin`.
 */
class m180408_161932_create_greenapple_admin_table extends Migration
{
    public $tableName = 'greenapple_admin';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->bigInteger()->notNull()->unique(),
            'last_name' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'user_id' => 'user',
        ];
    }
}
