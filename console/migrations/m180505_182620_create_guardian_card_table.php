<?php

use console\base\Migration;

/**
 * Handles the creation of table `guardian_card`.
 */
class m180505_182620_create_guardian_card_table extends Migration
{
    public $tableName = 'guardian_card';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'guardian_id' => $this->bigInteger()->notNull(),
            'token' => $this->string()->notNull(),
            'mask' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    public function getFkColumns()
    {
        return [
            'guardian_id' => 'guardian',
        ];
    }
}
