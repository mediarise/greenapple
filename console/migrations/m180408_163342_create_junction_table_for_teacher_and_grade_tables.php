<?php

use console\base\Migration;

/**
 * Handles the creation of table `teacher_grade`.
 * Has foreign keys to the tables:
 *
 * - `teacher`
 * - `grade`
 */
class m180408_163342_create_junction_table_for_teacher_and_grade_tables extends Migration
{
    public $tableName = 'lnk_teacher_grade';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'teacher_id' => $this->integer(),
            'grade_id' => $this->integer(),
            'UNIQUE(teacher_id, grade_id)',
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'teacher_id' => 'teacher',
            'grade_id' => 'grade',
        ];
    }
}
