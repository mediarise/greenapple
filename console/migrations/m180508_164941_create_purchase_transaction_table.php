<?php

use console\base\Migration;

/**
 * Handles the creation of table `purchase_transaction`.
 */
class m180508_164941_create_purchase_transaction_table extends Migration
{
    public $tableName = 'purchase_transaction';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'guardian_card_id' => $this->bigInteger(),
            'amount' => $this->money(9, 2)->notNull(),

            'response_code' => $this->smallInteger(3),
            'iso' => $this->tinyInteger(2),
            'date_time' => $this->dateTime(),
            'type' => $this->tinyInteger(2),
            'number' => $this->string(20),

            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'guardian_card_id' => 'guardian_card',
        ];
    }
}
