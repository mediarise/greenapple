<?php

use console\base\Migration;

/**
 * Handles the creation of table `doc_event`.
 */
class m180100_161943_create_doc_event_table extends Migration
{

    public $tableName = 'doc_event';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'predecessor' => $this->bigInteger(),
            'creator_id' => $this->bigInteger(),
            'type' => $this->smallInteger()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'featured_image' => $this->string(),
            'is_required' => $this->boolean(),
            'is_parent_action_required' => $this->boolean(),
            'umbrella_account' => $this->integer(),
            'ledger_account_id' => $this->integer(),
            'start_date' => $this->timestamp()->notNull(),
            'end_date' => $this->timestamp()->notNull(),
            'due_date' => $this->timestamp(),
            'due_lead_period' => $this->integer(),
            'due_unit' => $this->integer(),
            'duration' => $this->integer(),
            'duration_unit' => $this->tinyInteger(),
            'is_repeatable' => $this->boolean(),
            'repeat_on_week' => $this->integer(),
            'repeat_on_days' => $this->integer(),
            'frequency' => $this->integer(),
            'remind_for_pending' => $this->boolean(),
            'remind_for_upcoming' => $this->boolean(),
            'pending_reminder_interval' => $this->integer(),
            'pending_reminder_interval_unit' => $this->smallInteger(),
            'upcoming_reminder_interval' => $this->integer(),
            'upcoming_reminder_interval_unit' => $this->smallInteger(),
            'text_for_pending_reminder' => $this->text(),
            'text_for_upcoming_reminder' => $this->text(),

            'fundraising_message' => $this->text(),
            'fundraising_min' => $this->money(9, 2),
            'fundraising_max' => $this->money(9, 2),
            'fundraising_step' => $this->money(9, 2),

            'status' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'predecessor' => 'doc_event',
        ];
    }
}
