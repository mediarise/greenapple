<?php

use console\base\Migration;

/**
 * Handles the creation of table `cart`.
 */
class m180428_055104_create_cart_table extends Migration
{
    public $tableName = 'cart';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'doc_event_id' => $this->bigInteger()->notNull(),
            'guardian_id' => $this->bigInteger()->notNull(),
            'student_id' => $this->bigInteger()->notNull(),
            'expiry_date' => $this->dateTime()->notNull(),
            'product_id' => $this->bigInteger()->notNull(),
            'product_variation_id' => $this->bigInteger(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->money(9, 2),
            'tax' => $this->integer(),
            'total' => $this->money(9, 2),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
            'UNIQUE(doc_event_id, guardian_id, student_id, product_id)'
        ]);
        $this->createForeignKeysForColumns($this->getPkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getPkColumns());
        $this->dropTable($this->tableName);
    }

    private function getPkColumns()
    {
        return [
            'doc_event_id' => 'doc_event',
            'guardian_id' => 'guardian',
            'student_id' => 'student',
            'product_id' => 'product',
            'product_variation_id' => 'product_variation'
        ];
    }
}
