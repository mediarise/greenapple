<?php

use yii\db\Migration;
use webvimark\modules\UserManagement\models\rbacDB\Permission;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use webvimark\modules\UserManagement\models\rbacDB\Route;
use function Stringy\create as s;

/**
 * Class m180407_093422_create_base_roles
 */
class m180407_093422_create_base_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    const P_LOGIN_AND_LOGOUT = 'loginAndLogout';

    /**
     * @return bool|void
     * @throws Exception
     */
    public function safeUp()
    {
        Route::refreshRoutes();

        Permission::create(self::P_LOGIN_AND_LOGOUT, null, 'userCommonPermissions');

        Permission::assignRoutes(self::P_LOGIN_AND_LOGOUT, [
            '/site/logout',
            '/site/login',
            '/site/error',
            '/base/error'
        ], null, 'userCommonPermissions');

        foreach ($this->getRoles() as $roleName => $route){
            $this->addRole($roleName, $route);
        }
    }

    /**
     * @param $roleName
     * @param $route
     * @throws Exception
     */
    private function addRole($roleName, $route)
    {
        Role::create($roleName); //create role

        $permissionName = $this->getVisitPermissionNameByRoleName($roleName);

        Role::assignRoutesViaPermission($roleName, $permissionName, [
            "/$route/*",
            "/$route/index/*"
        ]);

        Permission::addChildren($permissionName, self::P_LOGIN_AND_LOGOUT); //add loginAndLogout to new role

        $method = 'migrate' . (string) s($roleName)->upperCaseFirst();

        if (method_exists($this, $method)){
            call_user_func([$this, $method], $roleName);
        }
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param $roleName
     * @throws Exception
     */
    private function migrateActiveUser($roleName)
    {
        $this->assignPermissions($roleName, [
            'purchaseProduct',
            'acceptEvent',
            'DeclineEvent',
        ]);
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param $roleName
     * @throws Exception
     */
    private function migrateTeacher($roleName)
    {
        Role::assignRoutesViaPermission($roleName, 'manageVisibilityGroup', [
            '/teacher/visibility-group/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageEvents', [
            '/teacher/event/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageProducts', [
            '/teacher/product/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageForms', [
            '/teacher/form/*'
        ]);

        $this->assignPermissions($roleName, [
            'createEvent',
            'updateEvent',
            'deleteEvent',
            'addCustomUserGroup',
            'editCustomUserGroup',
            'runGradeReports',
        ]);
        Role::addChildren($roleName, 'activeUser');
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param $roleName
     * @throws Exception
     */
    private function migrateSchoolAdmin($roleName)
    {
        Role::assignRoutesViaPermission($roleName, 'manageGrades', [
            '/schooladmin/grade/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageForms', [
            '/schooladmin/form/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageSignupRequests', [
            '/schooladmin/signup-request/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageVisibilityGroup', [
            '/schooladmin/visibility-group/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageStudents', [
            '/schooladmin/student/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageGuardians', [
            '/schooladmin/guardian/*'
        ]);

        $this->assignPermissions($roleName, [
            'addTeacher',
            'editTeacher',
            'addStudent',
            'editStudent',
            'addGrade',
            'editGrade',
            'addGuardian',
            'editGuardian',
            'assignGuardianToStudent',
            'assignStudentToGrade',
            'acceptGuardianApplication',
            'inviteGuardianToTheSystem',
            'runSchoolReports',
        ]);

        Role::addChildren($roleName, 'teacher');
    }


    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param $roleName
     * @throws Exception
     */
    private function migrateBoardAdmin($roleName)
    {
        Role::assignRoutesViaPermission($roleName, 'manageVisibilityGroup', [
            '/boardadmin/visibility-group/*'
        ]);

        Role::assignRoutesViaPermission($roleName, 'manageSchools', [
            '/boardadmin/school/*'
        ]);

        $this->assignPermissions($roleName, [
            'addSchool',
            'editSchool',
            'addSchoolAdmin',
            'editSchoolAdmin',
            'runBoardReports',
        ]);
        Role::addChildren($roleName, 'schoolAdmin');
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * @param $roleName
     * @throws Exception
     */
    private function migrateGreenappleAdmin($roleName)
    {
        $this->assignPermissions($roleName, [
            'addBoard',
            'editBoard',
            'addBoardAdmin',
            'editBoardAdmin',
        ]);

        Role::addChildren($roleName, 'boardAdmin');
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param $roleName
     * @throws Exception
     */
    private function migrateGuardian($roleName)
    {
        Role::assignRoutesViaPermission($roleName, 'manageEvents', [
            '/guardian/event/*'
        ]);

        $this->assignPermissions($roleName, [
            'submitForm',
        ]);

        Role::addChildren($roleName, 'activeUser');
    }

    /**
     * @param $roleName
     * @param $permissionNames
     * @throws Exception
     */
    private function assignPermissions($roleName, $permissionNames)
    {
        foreach ($permissionNames as $permissionName){
            Permission::create($permissionName);
        }
        Role::addChildren($roleName, $permissionNames);
    }

    private function getVisitPermissionNameByRoleName($roleName)
    {
        return sprintf('visit%sDashboard', (string) s($roleName)->upperCaseFirst());

    }

    private function getPermissionNames($roles){
        foreach ($roles as $roleName){
            yield $this->getVisitPermissionNameByRoleName($roleName);
        }
    }

    private function removeRoles($roles){
        Permission::deleteAll([
            'name' => iterator_to_array($this->getPermissionNames($roles)),
        ]);
        Role::deleteAll([
            'name' => $roles
        ]);
    }

    private function getRoles()
    {
        // [roleName] = dashboard
        return [
            'activeUser' => 'activeuser',
            'teacher' => 'teacher',
            'schoolAdmin' => 'schooladmin',
            'boardAdmin' => 'boardadmin',
            'greenappleAdmin' => 'greenappleadmin',
            'guardian' => 'guardian',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Permission::deleteAll(['name' => [
            //guardian
            'submitForm',

            //greenappleAdmin
            'addBoard',
            'editBoard',
            'addBoardAdmin',
            'editBoardAdmin',

            //boardAdmin
            'addSchool',
            'editSchool',
            'addSchoolAdmin',
            'editSchoolAdmin',
            'runBoardReports',

            //schoolAdmin
            'addTeacher',
            'editTeacher',
            'addStudent',
            'editStudent',
            'addGrade',
            'editGrade',
            'addGuardian',
            'editGuardian',
            'assignGuardianToStudent',
            'assignStudentToGrade',
            'acceptGuardianApplication',
            'inviteGuardianToTheSystem',
            'runSchoolReports',

            //teacher
            'createEvent',
            'updateEvent',
            'deleteEvent',
            'addCustomUserGroup',
            'editCustomUserGroup',
            'runGradeReports',

            //activeUser
            'purchaseProduct',
            'acceptEvent',
            'DeclineEvent',
        ]]);
        $this->removeRoles(array_keys($this->getRoles()));
        Permission::deleteAll(['name' => [self::P_LOGIN_AND_LOGOUT]]);
    }
}
