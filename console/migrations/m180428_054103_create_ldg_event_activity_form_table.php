<?php

use console\base\Migration;


/**
 * Handles the creation of table `ldg_event_activity_form`.
 */
class m180428_054103_create_ldg_event_activity_form_table extends Migration
{
    public $tableName = 'ldg_event_activity_form';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'doc_event_id' => $this->bigInteger()->notNull(),
            'guardian_id' => $this->bigInteger()->notNull(),
            'student_id' => $this->bigInteger()->notNull(),
            'form_id' => $this->bigInteger()->notNull(),
            'action_type' => $this->smallInteger()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
        ]);
        $this->createForeignKeysForColumns($this->getPkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getPkColumns());
        $this->dropTable($this->tableName);
    }

    private function getPkColumns()
    {
        return [
            'doc_event_id' => 'doc_event',
            'guardian_id' => 'guardian',
            'student_id' => 'student',
            'form_id' => 'form',
        ];
    }
}
