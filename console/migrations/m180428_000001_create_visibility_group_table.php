<?php

use console\base\Migration;

/**
 * Handles the creation of table `visibility_group`.
 */
class m180428_000001_create_visibility_group_table extends Migration
{
    public $tableName = 'visibility_group';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(),
            'doc_event_id' => $this->bigInteger(),
            'board_id' => $this->bigInteger(),
            'school_id' => $this->bigInteger(),
            'grade_id' => $this->bigInteger(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'board_id' => 'board',
            'school_id' => 'school',
            'grade_id' => 'grade',
            'doc_event_id' => 'doc_event'
        ];
    }
}
