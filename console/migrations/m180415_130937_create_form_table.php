<?php

use console\base\Migration;

/**
 * Handles the creation of table `form`.
 */
class m180415_130937_create_form_table extends Migration
{
    public $tableName = 'form';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'html' => $this->text()->notNull(),
            'control' => $this->smallInteger()->notNull(),
            'owner_id' => $this->bigInteger(),
            'board_id' => $this->bigInteger(),
            'school_id' => $this->bigInteger(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    /**
     * @return array
     */
    private function getFkColumns()
    {
        return [
            'owner_id' => 'user',
            'board_id' => 'board',
            'school_id' => 'school'
        ];
    }
}
