<?php

use console\base\Migration;

/**
 * Handles the creation of table `ldg_event_activity_purchase`.
 */
class m180428_053005_create_ldg_event_activity_purchase_table extends Migration
{
    public $tableName = 'ldg_event_activity_purchase';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'doc_event_id' => $this->bigInteger()->notNull(),
            'guardian_id' => $this->bigInteger()->notNull(),
            'student_id' => $this->bigInteger()->notNull(),
            'action_type' => $this->smallInteger()->notNull(),
            'product_id' => $this->bigInteger()->notNull(),
            'product_variation_id' => $this->bigInteger(),
            'order_id' => $this->string(50),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->money(9, 2),
            'tax' => $this->integer(),
            'total' => $this->money(9, 2),
            'created_at' => $this->dateTime()->notNull(),
        ]);
        $this->createForeignKeysForColumns($this->getPkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getPkColumns());
        $this->dropTable($this->tableName);
    }

    private function getPkColumns()
    {
        return [
            'doc_event_id' => 'doc_event',
            'guardian_id' => 'guardian',
            'student_id' => 'student',
            'product_id' => 'product',
            'product_variation_id' => 'product_variation',
        ];
    }
}
