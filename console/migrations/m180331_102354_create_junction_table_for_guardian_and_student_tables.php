<?php

use console\base\Migration;

/**
 * Handles the creation of table `lnk_guardian_student`.
 * Has foreign keys to the tables:
 *
 * - `guardian`
 * - `student`
 */
class m180331_102354_create_junction_table_for_guardian_and_student_tables extends Migration
{
    public $tableName = 'lnk_guardian_student';
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'guardian_id' => $this->bigInteger(),
            'student_id' => $this->bigInteger(),
            'relationship' => $this->integer()->notNull(),
            'UNIQUE(guardian_id, student_id)',
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'guardian_id' => 'guardian',
            'student_id' => 'student',
        ];
    }
}
