<?php

use console\base\Migration;

/**
 * Handles the creation of table `holiday`.
 */
class m180522_110452_create_holiday_table extends Migration
{
    public $tableName = 'holiday';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'board_id' => $this->bigInteger()->notNull(),
            'school_id' => $this->bigInteger(),
            'start_date' => $this->date()->notNull()->unique(),
            'end_date' => $this->date()->notNull()->unique(),
            'title' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'board_id' => 'board',
            'school_id' => 'school',
        ];
    }
}
