<?php

use yii\db\Migration;

/**
 * Handles the creation of table `board`.
 */
class m180329_192233_create_board_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('board', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('board');
    }
}
