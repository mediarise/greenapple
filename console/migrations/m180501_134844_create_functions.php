<?php

use common\models\DocEvent;
use common\models\guardian\UpcomingEvent;
use common\models\VisibilityGroup;
use yii\db\Migration;

/**
 * Class m180501_134844_create_functions
 */
class m180501_134844_create_functions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        self::dropFunctions($this);

        $es_published = DocEvent::STATUS_PUBLISHED;

        $this->execute(/** @lang SQL */
            "CREATE FUNCTION event_is_active(e doc_event)
  RETURNS BOOLEAN AS $$
BEGIN
  RETURN e.end_date > NOW() AND e.status = $es_published;
END;
$$
LANGUAGE 'plpgsql';");

        $this->execute(/** @lang SQL */
            'create function has_opened_children_events(event bigint, student bigint)
  returns boolean
language plpgsql
as $$
BEGIN
  RETURN (SELECT EXISTS(
      SELECT 1
      FROM doc_event ce
        LEFT JOIN ldg_event_student_status l ON ce.id = l.doc_event_id AND l.student_id = student
      WHERE ce.predecessor = event AND (l.status IS NULL OR l.status != 1) AND ce.due_date > NOW()
  ));
END;
$$;');

        $s_plannedOrDeclined = UpcomingEvent::STATUS_PLANNED | UpcomingEvent::STATUS_DECLINED;
        $s_pending = UpcomingEvent::STATUS_PENDING_OPTIONAL;

        $this->execute(/** @lang SQL */
            "CREATE FUNCTION nearest_due_date(event_id BIGINT)
  RETURNS TIMESTAMP AS $$
BEGIN
  RETURN (
    SELECT min(due_date)
    FROM doc_event e
      LEFT JOIN ldg_event_student_status l on e.id = l.doc_event_id
    WHERE predecessor = event_id AND due_date > NOW() AND (coalesce(l.status, $s_pending) & $s_plannedOrDeclined = 0)
  );
END;
$$
LANGUAGE 'plpgsql';");

        $mt_student = VisibilityGroup::MEMBER_TYPE_STUDENT;
        $mt_teacher = VisibilityGroup::MEMBER_TYPE_TEACHER;
        $mt_grade = VisibilityGroup::MEMBER_TYPE_GRADE;
        $mt_school = VisibilityGroup::MEMBER_TYPE_SCHOOL;
        $mt_group = VisibilityGroup::MEMBER_TYPE_VISIBILITY_GROUP;

        $this->execute(/** @lang SQL */
            "CREATE FUNCTION get_upcoming_events(students_ids bigint [])
  RETURNS TABLE(event_id      bigint, predecessor bigint, student_id bigint, start_date timestamp, end_date timestamp,
                title         varchar(255), first_name varchar(255), last_name varchar(255), forms_signed int,
                forms_to_sign int, products_purchased int, products_to_purchase int,
                gender        int, status smallint, required boolean, img_url varchar(255), \"type\" smallint,
                due_date      timestamp, is_repeatable boolean, repeat_on_days integer) AS $$
BEGIN
  RETURN QUERY
  WITH members AS (
    -- select student info:
      SELECT
        s.id,
        s.grade_id,
        g.school_id,
        ltg.teacher_id
      FROM
        student s
        LEFT JOIN grade g ON s.grade_id = g.id
        LEFT JOIN lnk_teacher_grade ltg on g.id = ltg.grade_id
      WHERE
        s.id = ANY (students_ids)
  ), groups AS (
    -- select all groups
      SELECT vgm.*
      FROM visibility_group vg
        LEFT JOIN visibility_group_member vgm ON (vg.id = vgm.visibility_group_id)
        INNER JOIN members m ON (vgm.member_type = $mt_student AND vgm.member_id = m.id) OR
                                (vgm.member_type = $mt_teacher AND vgm.member_id = m.teacher_id) OR
                                (vgm.member_type = $mt_grade AND vgm.member_id = m.grade_id) OR
                                (vgm.member_type = $mt_school AND vgm.member_id = m.school_id)
      WHERE vg.doc_event_id IS NULL
    -- union students and groups
  ), all_members AS (
      SELECT
        m.*,
        g.visibility_group_id group_id
      FROM members m
        LEFT JOIN groups g ON (g.member_type = $mt_student AND g.member_id = m.id) OR
                              (g.member_type = $mt_teacher AND g.member_id = m.teacher_id) OR
                              (g.member_type = $mt_grade AND g.member_id = m.grade_id) OR
                              (g.member_type = $mt_school AND g.member_id = m.school_id)
  )
  SELECT
    DISTINCT ON (doc_event.start_date, doc_event.id, student.id)
    doc_event.id,
    doc_event.predecessor,
    student.id,
    doc_event.start_date,
    doc_event.end_date,
    doc_event.title,
    student.first_name,
    student.last_name,
    less.forms_signed,
    les.forms_to_sign,
    less.products_purchased,
    les.products_to_purchase,
    student.gender,
    less.status,
    doc_event.is_parent_action_required,
    ph.url,
    doc_event.type,
    CASE WHEN doc_event.is_repeatable
      THEN nearest_due_date(doc_event.id)
    ELSE doc_event.due_date END due_date,
    doc_event.is_repeatable,
    p.repeat_on_days
  FROM
    (doc_event
      LEFT JOIN
      visibility_group vg ON doc_event.id = vg.doc_event_id OR doc_event.predecessor = vg.doc_event_id
      LEFT JOIN
      visibility_group_member vgm on vg.id = vgm.visibility_group_id)
    INNER JOIN all_members m ON (vgm.member_type = $mt_student AND vgm.member_id = m.id) OR
                                (vgm.member_type = $mt_teacher AND vgm.member_id = m.teacher_id) OR
                                (vgm.member_type = $mt_grade AND vgm.member_id = m.grade_id) OR
                                (vgm.member_type = $mt_school AND vgm.member_id = m.school_id) OR
                                (vgm.member_type = $mt_group AND vgm.member_id = m.group_id)
    INNER JOIN ldg_event_status les ON doc_event.id = les.doc_event_id
    LEFT JOIN student ON (m.id = student.id)
    LEFT JOIN ldg_event_student_status less on doc_event.id = less.doc_event_id AND student.id = less.student_id
    LEFT JOIN event_photo ph ON doc_event.id = ph.event_id AND ph.is_main
    LEFT JOIN doc_event p ON doc_event.predecessor = p.id
  WHERE event_is_active(doc_event)
        AND (doc_event.predecessor IS NULL OR less.status = 1)
        AND (NOT coalesce(doc_event.is_repeatable, false) OR has_opened_children_events(doc_event.id, student.id))
  ORDER BY doc_event.start_date;
END;
$$
LANGUAGE 'plpgsql';
        ");
        $this->execute(/** @lang SQL */
            "
            CREATE FUNCTION get_event_member_count()
              RETURNS TABLE(event_id bigint, student_count bigint) AS $$
            BEGIN
              RETURN QUERY
              WITH vsm AS (
                  SELECT
                    v.doc_event_id,
                    m2.member_type,
                    m2.member_id
                  FROM visibility_group v
                    LEFT JOIN visibility_group_member m2 ON v.id = m2.visibility_group_id
                  WHERE doc_event_id IN (SELECT id FROM doc_event e WHERE event_is_active(e))
              ), vgm AS (
                SELECT
                  vsm.doc_event_id,
                  m2.member_type,
                  m2.member_id
                FROM vsm
                  LEFT JOIN visibility_group_member m2 ON vsm.member_id = m2.visibility_group_id
                WHERE vsm.member_type = $mt_group
                UNION
                SELECT *
                FROM vsm
                WHERE vsm.member_type != $mt_group
              )
                  SELECT
                    doc_event_id,
                    count(student_id) quantity
                  FROM
                    (SELECT DISTINCT
                       doc_event_id,
                       CASE WHEN vgm.member_type = $mt_student
                         THEN vgm.member_id
                       ELSE s.id END student_id
                     FROM vgm
                       LEFT JOIN lnk_teacher_grade l ON vgm.member_type = $mt_teacher AND vgm.member_id = l.teacher_id
                       LEFT JOIN grade g ON vgm.member_type = $mt_school AND vgm.member_id = g.school_id
                       LEFT JOIN student s ON (vgm.member_type = $mt_teacher AND l.grade_id = s.grade_id)/*by teacher*/ OR
                                              (vgm.member_type = $mt_grade AND vgm.member_id = s.grade_id) /*by grade*/ OR
                                              (vgm.member_type = $mt_school AND g.id = s.grade_id) /*by school*/) s
                  GROUP BY doc_event_id;
            END;
            $$
            LANGUAGE 'plpgsql';
        ");

        $this->execute(/** @lang SQL */
            'CREATE FUNCTION update_parent_status(event_id BIGINT, student BIGINT, status_planned INT, status_declined INT)
  RETURNS VOID AS $$
DECLARE   new_status     INTEGER;
  DECLARE predecessor_id INTEGER;
BEGIN
  SELECT predecessor
  INTO predecessor_id
  FROM doc_event e
  WHERE e.id = event_id;
  IF predecessor_id IS NULL
  THEN RETURN; END IF;
  SELECT CASE WHEN ls.quantity = ls.total
    THEN
      CASE WHEN (ls.status & ~(status_planned | status_declined)) > 0
        THEN 0
      WHEN ls.status & status_planned > 0
        THEN status_planned
      ELSE status_declined END
         ELSE 0 END
  INTO new_status
  FROM (SELECT
          count(e.id)      total,
          count(l.id)      quantity,
          bit_or(l.status) status
        FROM doc_event e
          LEFT JOIN ldg_event_student_status l on e.id = l.doc_event_id AND l.student_id = student
        WHERE e.predecessor = predecessor_id) ls;
  IF (new_status > 0)
  THEN
    INSERT INTO ldg_event_student_status
    (doc_event_id, student_id, status, forms_signed, products_purchased)
    VALUES (predecessor_id, student, new_status, 0, 0);
  END IF;

END;
$$
LANGUAGE \'plpgsql\';
');

        $this->execute(/** @lang SQL */
            "CREATE FUNCTION update_status_counters(event_id BIGINT, guardian BIGINT, student BIGINT, purchased INT,
                                       signed   INT, status_planned INT, status_declined INT, status_incomplete INT)
  RETURNS VOID AS $$
DECLARE new_status INTEGER;
BEGIN
  SELECT CASE WHEN products_to_purchase = coalesce(ss.products_purchased, 0) + purchased AND
                   forms_to_sign = coalesce(ss.forms_signed, 0) + signed
    THEN status_planned
         ELSE status_incomplete END
  INTO new_status
  FROM ldg_event_status s
    LEFT JOIN ldg_event_student_status ss
      ON s.doc_event_id = ss.doc_event_id AND ss.student_id = student
  WHERE s.doc_event_id = event_id;
  INSERT INTO ldg_event_student_status AS ss
  (doc_event_id, student_id, status, forms_signed, products_purchased)
  VALUES (event_id, student, new_status, signed, purchased)
  ON CONFLICT (doc_event_id, student_id)
    DO UPDATE SET
      status             = new_status,
      products_purchased = ss.products_purchased + purchased,
      forms_signed       = ss.forms_signed + signed;
  UPDATE ldg_event_status
  SET
    products_purchased = products_purchased + purchased,
    forms_signed       = forms_signed + signed
  WHERE doc_event_id = event_id;
  IF new_status = status_planned
  THEN
    INSERT INTO ldg_event_activity_general
    (doc_event_id, guardian_id, student_id, action_type, created_at)
    VALUES (event_id, guardian, student, 1, NOW());
    PERFORM update_parent_status(event_id, student, status_planned, status_declined);
  END IF;
END;
$$
LANGUAGE 'plpgsql';
");
    }

    public static function dropFunctions($instance)
    {
        $instance->execute(/** @lang SQL */
            'DROP FUNCTION IF EXISTS get_upcoming_events(bigint []);');
        $instance->execute(/** @lang SQL */
            'DROP FUNCTION IF EXISTS get_event_member_count();');
        $instance->execute(/** @lang SQL */
            'DROP FUNCTION IF EXISTS nearest_due_date( BIGINT );');
        $instance->execute(/** @lang SQL */
            'DROP FUNCTION IF EXISTS has_opened_children_events( BIGINT, BIGINT );');
        $instance->execute(/** @lang SQL */
            'DROP FUNCTION IF EXISTS event_is_active(e doc_event);');
        $instance->execute(/** @lang SQL */
            'DROP FUNCTION IF EXISTS update_parent_status( BIGINT, BIGINT, INT, INT );');
        $instance->execute(/** @lang SQL */
            'DROP FUNCTION IF EXISTS update_status_counters( BIGINT, BIGINT, BIGINT, INT, INT, INT, INT, INT);'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        m180501_134844_create_functions::dropFunctions($this);
    }
}
