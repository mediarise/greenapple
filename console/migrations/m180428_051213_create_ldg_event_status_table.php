<?php

use console\base\Migration;


/**
 * Handles the creation of table `ldg_event_status`.
 */
class m180428_051213_create_ldg_event_status_table extends Migration
{
    public $tableName = 'ldg_event_status';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'doc_event_id' => $this->bigInteger()->unique()->notNull(),
            'events_accepted' => $this->integer()->notNull(),
            'events_declined' => $this->integer()->notNull(),
            'forms_signed' => $this->integer()->notNull(),
            'products_purchased' => $this->integer()->notNull(),
            'forms_to_sign' => $this->integer()->notNull(),
            'products_to_purchase' => $this->integer()->notNull(),
        ]);
        $this->createForeignKeysForColumns($this->getPkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getPkColumns());
        $this->dropTable($this->tableName);
    }

    private function getPkColumns()
    {
        return [
            'doc_event_id' => 'doc_event',
        ];
    }
}
