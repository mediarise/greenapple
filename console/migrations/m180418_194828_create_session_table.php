<?php

use yii\db\Migration;

/**
 * Handles the creation of table `session`.
 */
class m180418_194828_create_session_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%session}}', [
            'id'     => 'char(40) not null, primary key (id)',
            'expire' => 'integer default null',
            'data'   => 'bytea default null',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%session}}');
    }
}
