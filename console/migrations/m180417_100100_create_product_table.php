<?php

use common\models\Product;
use console\base\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180417_100100_create_product_table extends Migration
{
    public $tableName = 'product';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'sku' => $this->string(50),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'is_free' => $this->boolean(),
            'price' => $this->money(9, 2),
            'tax' => $this->smallInteger(),
            'cost' => $this->money(9, 2),
            'board_id' => $this->bigInteger(),
            'school_id' => $this->bigInteger(),
            'grade_id' => $this->bigInteger(),
            'owner_id' => $this->bigInteger(),
            'supplier_id' => $this->bigInteger(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->batchInsert($this->tableName,
            ['title', 'description', 'is_free', 'created_at', 'updated_at', 'price', 'cost'],
            [
                [Product::PREDEFINED_DONATION, '', true, 'NOW()', 'NOW()', 0, 0],
            ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    /**
     * @return array
     */
    private function getFkColumns()
    {
        return [
            'owner_id' => 'user',
            'board_id' => 'board',
            'school_id' => 'school',
            'grade_id' => 'grade',
            'supplier_id' => 'supplier'
        ];
    }
}
