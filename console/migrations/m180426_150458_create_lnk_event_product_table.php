<?php

use console\base\Migration;

/**
 * Handles the creation of table `lnk_event_product`.
 * Has foreign keys to the tables:
 *
 * - `doc_event`
 * - `product`
 */
class m180426_150458_create_lnk_event_product_table extends Migration
{
    public $tableName = 'lnk_event_product';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'doc_event_id' => $this->bigInteger()->notNull(),
            'product_id' => $this->bigInteger()->notNull(),
            'price' => $this->money(9, 2)->notNull(),
            'cost' => $this->money(9, 2)->notNull(),
            'min_quantity' => $this->integer()->notNull(),
            'max_quantity' => $this->integer()->notNull(),
            'min_interval' => $this->integer(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'UNIQUE(doc_event_id, product_id)',
        ]);
        
        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'doc_event_id' => 'doc_event',
            'product_id' => 'product',
        ];
    }
}
