<?php

use console\base\Migration;


/**
 * Handles the creation of table `guardian`.
 */
class m180331_101509_create_guardian_table extends Migration
{
    public $tableName = 'guardian';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->bigInteger()->unique(),
            'greeting' => $this->integer()->notNull(),
            'last_name' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'invite_token' => $this->string()->unique(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getPkColumns());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getPkColumns());

        $this->dropTable($this->tableName);
    }

    private function getPkColumns()
    {
        return [
            'user_id' => 'user',
        ];
    }
}
