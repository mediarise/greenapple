<?php

use console\base\Migration;

/**
 * Handles the creation of table `grade`.
 */
class m180329_192907_create_grade_table extends Migration
{
    public $tableName = 'grade';
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'school_id' => $this->bigInteger()->notNull(),
            'name' => $this->string()->notNull(),
            'year' => $this->smallInteger()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'UNIQUE(school_id, name, year)'
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'school_id' => 'school'
        ];
    }
}
