<?php

use console\base\Migration;


/**
 * Handles the creation of table `school`.
 */
class m180329_192655_create_school_table extends Migration
{
    public $tableName = 'school';
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'board_id' => $this->bigInteger()->notNull(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'UNIQUE(board_id, name)'
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'board_id' => 'board',
        ];
    }
}
