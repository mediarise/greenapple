<?php

use console\base\Migration;

/**
 * Handles the creation of table `visibility_group_member`.
 */
class m180428_000002_create_visibility_group_member_table extends Migration
{
    public $tableName = 'visibility_group_member';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'visibility_group_id' => $this->bigInteger()->notNull(),
            'member_type' => $this->integer()->notNull(),
            'member_id' => $this->bigInteger()->notNull(),
            'UNIQUE(visibility_group_id, member_type, member_id)',
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'visibility_group_id' => 'visibility_group',
        ];
    }
}
