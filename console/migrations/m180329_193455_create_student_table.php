<?php

use console\base\Migration;

/**
 * Handles the creation of table `student`.
 */
class m180329_193455_create_student_table extends Migration
{
    public $tableName = 'student';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'oen' => $this->char(9)->unique()->notNull(),
            'grade_id' => $this->bigInteger()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'birthday' => $this->dateTime()->notNull(),
            'gender' => $this->integer(),
            'photo_path' => $this->string(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'grade_id' => 'grade'
        ];
    }
}
