<?php

use console\base\Migration;

/**
 * Handles the creation of table `product_variation`.
 */
class m180428_000003_create_product_variation_table extends Migration
{
    public $tableName = 'product_variation';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'product_id' => $this->bigInteger()->notNull(),
            'name' => $this->string()->notNull(),
            'price' => $this->money(9,2),
            'cost' => $this->money(9, 2),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'UNIQUE(product_id, name)',
        ]);
        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'product_id' => 'product',
        ];
    }
}
