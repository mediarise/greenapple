<?php

use console\base\Migration;

/**
 * Handles the creation of table `signup_request_student`.
 */
class m180406_092027_create_signup_request_student_template_table extends Migration
{
    public $tableName = 'signup_request_student';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'signup_request_id' => $this->bigInteger()->notNull(),
            'student_id' => $this->bigInteger()->unique(),
            'relationship' => $this->integer()->notNull(),
            'oen' => $this->char(9),
            'grade_id' => $this->bigInteger()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'birthday' => $this->dateTime()->notNull(),
            'gender' => $this->integer(),
            'status' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'signup_request_id' => 'signup_request',
            'grade_id' => 'grade',
            'student_id' => 'student',
        ];
    }
}
