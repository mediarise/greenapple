<?php

use console\base\Migration;

/**
 * Handles the creation of table `ldg_event_student_status`.
 */
class m180428_052443_create_ldg_event_student_status_table extends Migration
{
    public $tableName = 'ldg_event_student_status';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'doc_event_id' => $this->bigInteger()->notNull(),
            'student_id' => $this->bigInteger()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'forms_signed' => $this->integer()->notNull(),
            'products_purchased' => $this->integer()->notNull(),
            'cancellation_reason' => $this->text(),
            'UNIQUE(doc_event_id, student_id)'
        ]);
        $this->createForeignKeysForColumns($this->getPkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getPkColumns());
        $this->dropTable($this->tableName);
    }

    private function getPkColumns()
    {
        return [
            'doc_event_id' => 'doc_event',
            'student_id' => 'student',
        ];
    }
}
