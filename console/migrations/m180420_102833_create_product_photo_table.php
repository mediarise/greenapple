<?php

use console\base\Migration;

/**
 * Handles the creation of table `product_photo`.
 */
class m180420_102833_create_product_photo_table extends Migration
{
    public $tableName = 'product_photo';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'product_id' => $this->bigInteger(),
            'url' => $this->string(255),
            'is_main' => $this->boolean(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'product_id' => 'product'
        ];
    }
}
