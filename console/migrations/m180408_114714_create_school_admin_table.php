<?php

use console\base\Migration;


/**
 * Handles the creation of table `school_admin`.
 */
class m180408_114714_create_school_admin_table extends Migration
{
    public $tableName = 'school_admin';

    private function getFkColumns()
    {
        return [
            'user_id' => 'user',
            'school_id' => 'school',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->bigInteger()->notNull()->unique(),
            'school_id' => $this->bigInteger()->notNull(),
            'last_name' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKeysForColumns($this->getFkColumns());
        $this->dropTable($this->tableName);
    }
}
