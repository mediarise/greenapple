<?php

use yii\db\Migration;

/**
 * Class m180404_174753_boards_and_schools
 */
class m180404_174753_boards_and_schools extends Migration
{
    private $date;
    private $zeroDate = '1970-01-01 08:00:00+08';

    private $boardIds;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->date = date('Y-m-d H:i:s');
        $this->batchInsert('board', ['name', 'created_at', 'updated_at'], [
            ['York Catholic District School Board', $this->zeroDate, $this->date],
            ['Halton District School Board', $this->zeroDate, $this->date]
        ]);

        $boardIds = (new \yii\db\Query())
            ->select(['id', 'name'])
            ->from('board')
            ->where(['created_at' => $this->zeroDate])
            ->all();
        $this->boardIds = \yii\helpers\ArrayHelper::map($boardIds, 'name', 'id');

        /** @noinspection PhpParamsInspection */
        $this->batchInsert('school', ['board_id', 'name', 'created_at', 'updated_at'], $this->schoolGenerator());

        $schoolIds = (new \yii\db\Query())
            ->select('id')
            ->from('school')
            ->where(['created_at' => $this->zeroDate])
            ->all();
        $schoolIds = array_column($schoolIds, 'id');
        /** @noinspection PhpParamsInspection */
        $this->batchInsert('grade', ['school_id', 'name', 'year', 'created_at', 'updated_at'], $this->gradeGenerator($schoolIds));
    }

    private function schoolGenerator()
    {
        $boardId = $this->boardIds['York Catholic District School Board'];
        yield from $this->rowGenerator($boardId, $this->getYorkCatholicDistrictSchoolSchools());
        $boardId = $this->boardIds['Halton District School Board'];
        yield from $this->rowGenerator($boardId, $this->getHaltonDistrictSchoolBoardsSchools());
    }

    private function rowGenerator($boardId, $array)
    {
        foreach ($array as $school) {
            yield [$boardId, $school, $this->zeroDate, $this->date];
        }
    }

    private function gradeGenerator($schoolIds)
    {
        foreach ($schoolIds as $schoolId) {
            for ($grade = 1; $grade <= 11; $grade++) {
                yield [$schoolId, "grade " . $grade, \common\models\Grade::getCurrentAcademicYear(), $this->zeroDate, $this->date];
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('grade', ['created_at' => $this->zeroDate]);
        $this->delete('school', ['created_at' => $this->zeroDate]);
        $this->delete('board', ['created_at' => $this->zeroDate]);
    }

    private function getYorkCatholicDistrictSchoolSchools()
    {
        return [
            //Aurora
            'Holy Spirit',
            'Light of Christ',
            'Our Lady of Grace',
            'St. Jerome',
            'St. Joseph (Aurora)',
            //East Gwillimbury
            'Good Shepherd',
            'Our Lady of Good Counsel',
            //Georgina
            'Prince of Peace',
            'St. Bernadette',
            'St. Thomas Aquinas',
            //King
            'Holy Name',
            'St. Mary',
            'St. Patrick (Schomberg)',
            //Markham
            'All Saints',
            'San Lorenzo Ruiz',
            'Sir Richard W. Scott',
            'St. Anthony',
            'St. Benedict ',
            'St. Edward',
            'St. Francis Xavier',
            'St. John XXIII',
            'St. Joseph (Markham)',
            'St. Julia Billiart',
            'St. Justin Martyr',
            'St. Kateri Tekakwitha',
            'St. Matthew',
            'St. Michael',
            'St. Monica',
            'St. Mother Teresa',
            'St. Patrick (Markham)',
            'St. René Goupil - St Luke',
            'St. Vincent de Paul',
            //Newmarket
            'Canadian Martyrs',
            'Notre Dame',
            'St. Elizabeth Seton',
            'St. John Chrysostom',
            'St. Nicholas',
            'St. Paul',
            //Richmond Hill
            'Christ the King ',
            'Corpus Christi',
            'Father Frederick McGinn',
            'Father Henri Nouwen',
            'Our Lady Help of Christians',
            'Our Lady of Hope',
            'Our Lady of the Annunciation',
            'St. Anne',
            'St. Charles Garnier',
            'St. John Paul II',
            'St. Joseph (Richmond Hill)',
            'St. Marguerite D’Youville',
            'St. Mary Immaculate',
            //Stouffville
            'St. Brendan',
            'St. Brigid',
            'St. Mark',
            //Vaughan
            'Blessed Scalabrini',
            'Blessed Trinity',
            'Divine Mercy',
            'Father John Kelly',
            'Guardian Angels',
            'Holy Jubilee',
            'Immaculate Conception',
            'Our Lady of Fatima',
            'Our Lady of the Rosary',
            'Pope Francis',
            'San Marco',
            'St. Agnes of Assisi',
            'St. Andrew',
            'St. Angela Merici',
            'St. Catherine of Siena',
            'St. Cecilia',
            'St. Clare',
            'St. Clement',
            'St. David',
            'St. Emily',
            'St. Gabriel the Archangel',
            'St. Gregory the Great',
            'St. James',
            'St. John Bosco',
            'St. Joseph the Worker',
            'St. Margaret Mary',
            'St. Mary of the Angels',
            'St. Michael the Archangel',
            'St. Padre Pio',
            'St. Peter',
            'St. Raphael the Archangel',
            'St. Stephen',
            'St. Veronica',
            //Secondary Schools
            'Cardinal Carter CHS',
            'Father Bressani CHS',
            'Father Michael McGivney Catholic Academy',
            'Holy Cross Catholic Academy',
            'Jean Vanier CHS',
            'Our Lady of the Lake Catholic College School (Grades 7-12)',
            'Sacred Heart CHS',
            'St. Augustine CHS',
            'St. Brother Andre CHS',
            'St. Elizabeth CHS',
            'St. Jean de Brebeuf CHS',
            'St. Joan of Arc CHS',
            'St. Luke - North',
            'St. Luke - West ',
            'St. Maximilian Kolbe CHS',
            'St. Robert CHS',
            'St. Theresa of Lisieux CHS',
        ];

    }

    private function getHaltonDistrictSchoolBoardsSchools()
    {
        return [
            'Abbey Lane Public School',
            'Abbey Park High School',
            'Acton District High School',
            'Aldershot Elementary',
            'Aldershot High School',
            'Alexander\'s Public School',
            'Alton Village Public School',
            'Anne J. MacArthur Public School',
            'Boyne Public School',
            'Brant Hills Public School',
            'Brookdale Public School',
            'Brookville Public School',
            'Bruce T. Lindley Public School',
            'Bruce Trail Public School',
            'Burlington Central Elementary (Gr.7 & Gr.8)',
            'Burlington Central High School',
            'C.H. Norton Public School',
            'Captain R. Wilson',
            'Centennial Public School',
            'Central PS (JK-Gr.6)',
            'Charles R. Beaudoin Public School',
            'Chris Hadfield Public School',
            'Clarksdale Public School',
            'Craig Kielburger Secondary School',
            'Dr. Charles Best Public School',
            'Dr. Frank J. Hayden Secondary School',
            'E. J. James Public School',
            'E. W. Foster Public School',
            'Eastview Public School',
            'Emily Carr Public School',
            'Escarpment View Public School',
            'Ethel Gardiner Public School',
            'Falgarwood Public School',
            'Florence Meares Public School',
            'Forest Trail Public School',
            'Frontenac Public School',
            'Garth Webb Secondary School',
            '"Gary Allan High School(Formally Adult, Alt. Ed)"',
            'George Kennedy Public School',
            'Georgetown District High School',
            'Gladys Speers Public School',
            'Glen Williams Public School',
            'Glenview Public School',
            'Harrison Public School',
            'Hawthorne Village Public School',
            'Heritage Glen Public School',
            'Irma Coulson Public School',
            'Iroquois Ridge High School',
            'J.M. Denyes Public School',
            'James W. Hill Public School',
            'John T. Tuck Public School',
            'John William Boich Public School',
            'Joseph Gibbons Public School',
            'Joshua Creek Public School',
            'Kilbride Public School',
            'King\'s Road Public School',
            'Lakeshore Public School',
            'Lester B. Pearson High School',
            'Limehouse Public School',
            'M. M. Robinson High School',
            'Maple Grove Public School',
            'Maplehurst Public School',
            'Martin Street Public School',
            'McKenzie-Smith Bennett Public School',
            'Milton District High School',
            'Mohawk Gardens Public School',
            'Montclair Public School',
            'Munn\'s Public School',
            'Nelson High School',
            'New Central Public School',
            'Night School',
            'Oakville Trafalgar High School',
            'Oakwood Public School',
            'Oodenawi Public School',
            'Orchard Park Public School',
            'P.L. Robertson Public School',
            'Palermo Public School',
            'Park Public School',
            'Paul A. Fisher Public School',
            'Pauline Johnson Public School',
            'Pilgrim Wood Public School',
            'Pine Grove Public School',
            'Pineland Public School',
            'Pineview Public School',
            'Post\'s Corners Public School',
            'River Oaks Public School',
            'Robert Baldwin Public School',
            'Robert Bateman High School',
            'Robert Little Public School',
            'Rolling Meadows Public School',
            'Ryerson Public School',
            'Sam Sherratt Public School',
            'Sheridan Public School',
            'Silver Creek Public School',
            'Sir E. MacMillan Public School',
            'Stewarttown Public School',
            'Summer School',
            'Sunningdale Public School',
            'Syl Apps School and Section 20 Programs',
            'T. A. Blakelock High School',
            'Tecumseh Public School',
            'Tiger Jeet Singh Public School',
            'Tom Thomson Public School',
            'W. H. Morden Public School',
            'W. I. Dick Middle School',
            'West Oak Public School',
            'White Oaks Secondary School',
        ];
    }
}
