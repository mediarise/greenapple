<?php

use console\base\Migration;

/**
 * Handles the creation of table `doc_event_form`.
 * Has foreign keys to the tables:
 *
 * - `doc_event`
 * - `form`
 */
class m180424_164026_create_junction_table_for_doc_event_and_form_tables extends Migration
{
    public $tableName = 'lnk_event_form';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'doc_event_id' => $this->bigInteger()->notNull(),
            'form_id' => $this->bigInteger()->notNull(),
            'is_mandatory' => $this->boolean(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'UNIQUE(doc_event_id, form_id)'
        ]);

        $this->createForeignKeysForColumns($this->getFkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropForeignKeysForColumns($this->getFkColumns());

        $this->dropTable($this->tableName);
    }

    private function getFkColumns()
    {
        return [
            'doc_event_id' => 'doc_event',
            'form_id' => 'form',
        ];
    }
}
