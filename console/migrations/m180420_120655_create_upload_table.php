<?php

use console\base\Migration;

/**
 * Handles the creation of table `upload`.
 */
class m180420_120655_create_upload_table extends Migration
{
    public $tableName = 'upload';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'path' => $this->string(255),
            'filename' => $this->string(50),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
