<?php

use console\base\Migration;

/**
 * Handles the creation of table `signup_request`.
 */
class m180406_090502_create_signup_request_table extends Migration
{
    public $tableName = 'signup_request';
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'guardian_id' => $this->bigInteger()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'greeting' => $this->integer()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'status' => $this->integer()->notNull(),
            'note' => $this->text(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
        $this->createForeignKeysForColumns($this->getPkColumns());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }

    private function getPkColumns()
    {
        return [
            'guardian_id' => 'guardian'
        ];
    }
}
