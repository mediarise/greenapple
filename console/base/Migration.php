<?php

namespace console\base;

use yii\db\Migration as yiiMigration;

abstract class Migration extends yiiMigration
{
    public $tableName = '';

    private static $defaultOptions = [
        'column' => 'id',
        'delete' => 'RESTRICT',
        'update' => null,
    ];

    protected function createForeignKeysForColumns($columns)
    {
        foreach ($columns as $column => $refTable) {
            $options = self::$defaultOptions;

            if (is_array($refTable)){
                $options = array_merge($options, $refTable);
            } else {
                $options['table'] = $refTable;
            }
            $this->createIndex(
                $this->idxName($column),
                $this->tableName,
                $column
            );

            $this->addForeignKey(
                $this->fkName($column),
                $this->tableName,
                $column,
                $options['table'],
                $options['column'],
                $options['delete'],
                $options['update']
            );
        }
    }

    protected function dropForeignKeysForColumns($columns)
    {
        foreach (array_reverse(array_keys($columns)) as $column){
            $this->dropForeignKey(
                $this->fkName($column),
                $this->tableName
            );

            $this->dropIndex(
                $this->idxName($column),
                $this->tableName
            );
        }
    }

    private function idxName($columnName){
        return sprintf("idx-%s-%s", $this->tableName, $columnName);
    }

    private function fkName($columnName){
        return sprintf('fk-%s-%s', $this->tableName, $columnName);
    }
}