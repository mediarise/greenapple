"use strict";

$(function () {
    var addChildLadda = null;
    var addChildBtn = document.getElementById("add-child-btn");
    if (addChildBtn != null) {
        addChildLadda = Ladda.create(addChildBtn);
    }
    var dataCache = {};

    function updateSelect($select2, rows) {
        if (!rows) {
            return false;
        }

        var options = document.createDocumentFragment();
        $.each(rows, function (i, row) {
            options.appendChild(new Option(row.name, row.id, false, false));
        });

        $select2
            .append(options)
            .val(null)
            .trigger('change')
            .select2('enable');
        return true;
    }

    // noinspection JSUnusedGlobalSymbols
    document.SignupForm = {
        loadOptions: function (idOfSelect, dataId, url) {
            var $select2 = $('#' + idOfSelect);
            $select2.select2('enable', false);
            $select2.empty();

            if (!dataId) {
                $select2.trigger('change');
                // $('#add-child-form').yiiActiveForm('updateAttribute', idOfSelect, null);
                // window.setTimeout(function () {
                //     $select2.parent('.form-group')
                //         .removeClass('has-success')
                //         .removeClass('has-error');
                //
                // }, 1);
                return;
            }

            if (idOfSelect in dataCache) {
                if (updateSelect($select2, dataCache[idOfSelect][dataId])) {
                    return;
                }
            }

            $.ajax({
                method: 'post',
                url: url + dataId,
                data: {
                    board_id: dataId
                }
            })
                .done(function (rows) {
                    updateSelect($select2, rows);
                    if (!(idOfSelect in dataCache)) {
                        dataCache[idOfSelect] = [];
                    }
                    dataCache[idOfSelect][dataId] = rows;
                });
        }
    };

    function onRemoveButtonClick() {
        if (!confirm('Are you sure you want to delete this item?')) {
            return false;
        }
        var $a = $(this);
        $.ajax({
            type: 'post',
            url: $a.attr('href')
        })
            .done(function (data) {
                if (data.success) {
                    $a.parents('tr').remove();
                    togglGrid();
                }
            });
        return false;
    }

    function togglGrid() {
        var $wrapper = $("#grid-wrapper");
        var rowCount = $wrapper.find("tbody").children().length;
        if (rowCount > 0) {
            $wrapper.show();
        } else {
            $wrapper.hide();
        }
    }

    $("#add-child-btn").on("click", function () {
        addChildLadda.start();
        var $yiiform = $("#add-child-form");

        greenApple.ajax.submitForm($yiiform, {
            complete: function () {
                addChildLadda.stop();
                this.base("complete");
            },
            resultSuccess: function (data) {
                this.base("resultSuccess", data);
                $('#children-grid tbody')
                    .append(data.row);
                togglGrid();
            }
        });
        return false;
    });

    $("#children-grid").on("click", "a", onRemoveButtonClick);

    togglGrid();
});