"use strict";
$(function () {
    $("#linked-grid")
        //when clicked invite guardian in signup request card
        .on("click", "#invite-guardian", function (e) {
            e.preventDefault();
            greenApple.ajax.command($(this), {
                resultSuccess: function () {
                    $.pjax.reload({container: "#linked-grid"});
                    greenApple.alert.success("Invite sent successfully.");
                }
            });
            return false;
        })
        //when clicked block/activate in guardian card
        .on("click", "#switch-user-status", function (e) {
            e.preventDefault();
            greenApple.ajax.command($(this), {
                resultSuccess: function (data) {
                    $.pjax.reload({container: "#linked-grid"});
                    greenApple.alert.success(data.message);
                }
            });
            return false;
        })
        //when clicked reject student in signup request card
        .on("click", ".reject", function (e) {
            e.preventDefault();
            greenApple.ajax.command($(this), {
                resultSuccess: function () {
                    $.pjax.reload({container: "#linked-grid"});
                    greenApple.alert.success("Student rejected.");
                }
            });
            return false;
        })
        .on("click", ".remove-relationship", function (e) {
            e.preventDefault();
            greenApple.ajax.command($(this), {
                resultSuccess: function () {
                    $.pjax.reload({container: "#linked-grid"});
                    greenApple.alert.success("Relationship removed successfully.");
                }
            });
            return false;
        });

    //when add guardian/student in student/guardian card
    $("#guardian-student-link-form").on("submit", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        greenApple.ajax.submitForm($(this), {
            resultSuccess: function (data) {
                this.base("resultSuccess", data);
                $.pjax.reload({container: "#linked-grid"});
                greenApple.alert.success("Record assigned successfully.");
            }
        });
        return false;
    });

    //for rejection reason templates
    $("#rejection-templates a").click(function (e) {
        e.preventDefault();
        $("#signuprequestrejectionform-rejectreason").val($(this).find("span").html());
        return false;
    })
})
;