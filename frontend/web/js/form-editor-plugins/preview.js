(function (factory) {
        /* Global define */
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define(['jquery'], factory);
        } else if (typeof module === 'object' && module.exports) {
            // Node/CommonJS
            module.exports = factory(require('jquery'));
        } else {
            // Browser globals
            factory(window.jQuery);
        }
    }(function ($) {
        /**
         * @class plugin.showPreview
         *
         */
        previewEventTitle = previewEventTitle || 'AGO visit';
        $.extend(true, $.summernote.lang, {
            'en-US': {
                /* US English(Default Language) */
                showPreview: {
                    exampleText: 'Preview',
                    dialogTitle: 'Preview',
                    dialogHeaderText: 'Here you can see the final contents',
                    okButton: 'OK'
                }
            }
        });
        $.extend($.summernote.options, {
            showPreview: {
                icon: '<i class="fa fa-eye"/>',
                tooltip: 'Show preview',
                text: 'Preview',
                replacementList: {
                    '{studentName}': "Bard Simpson",
                    '{parentName}': "Homer Simpson",
                    '{teacherName}': "Edna Krabappel",
                    '{school}': "Springfield Elementary",
                    '{grade}': "Grade 4-1",
                    '{eventTitle}': previewEventTitle,
                    '{eventStartDate}': "2018-10-26",
                    '{eventEndDate}': "2018-10-26",
                    '{eventLocation}': "317 Dundas St W, Toronto, ON M5T 1G4, "
                }
            }
        });

        $.extend($.summernote.plugins, {
            /**
             *  @param {Object} context - context object has status of editor.
             */
            'showPreview': function (context) {
                var self = this,

                    // ui has renders to build ui elements
                    // for e.g. you can create a button with 'ui.button'
                    ui = $.summernote.ui,
                    $note = context.layoutInfo.note,

                    // contentEditable element
                    $editor = context.layoutInfo.editor,
                    $editable = context.layoutInfo.editable,
                    $toolbar = context.layoutInfo.toolbar,

                    // options holds the Options Information from Summernote and what we extended above.
                    options = context.options,

                    // lang holds the Language Information from Summernote and what we extended above.
                    lang = options.langInfo;

                context.memo('button.showPreview', function () {

                    // Here we create a button
                    var button = ui.button({

                        // icon for button
                        contents: options.showPreview.icon + ' ' + options.showPreview.text,
                        // tooltip for button
                        tooltip: lang.showPreview.tooltip,
                        click: function (e) {
                            context.invoke('showPreview.show');
                        }
                    });
                    return button.render();
                });
                this.show = function () {
                    this.toggleModalBox();
                    this.showModal();
                };

                this.showModal = function () {
                    $editor.parent().children('.preview').modal({});
                };
                this.toggleModalBox = function () {
                    $editor.parent().children('.preview').remove();

                    $editor.after('<div class="preview modal inmodal fade in" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">\n' +
                        '<div class="modal-dialog modal-lg">\n' +
                        '<div class="modal-content animated bounceInRight">\n' +
                        '<div class="modal-header">\n' +
                        '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>\n' +
                        '<i class="fa fa-eye modal-icon"></i>\n' +
                        '<h4 class="modal-title">' + lang.showPreview.dialogTitle + '</h4>\n' +
                        '<small class="font-bold">' + lang.showPreview.dialogHeaderText + '</small>\n' +
                        '</div>\n' +
                        '<div class="modal-body">\n' +
                        this.getEditedContent() +
                        '</div>\n' +
                        '<div class="modal-footer">\n' +
                        '<button type="button" class="btn btn-primary btn-xl" data-dismiss="modal">Close</button>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>');

                };
                this.getEditedContent = function () {
                    var content = $editable.html();
                    $.each(options.showPreview.replacementList, function (index, value) {
                        content = content.replace(new RegExp(index, 'g'), value);
                    });
                    return content;
                }
            }
        })
    })
);