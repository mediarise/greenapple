"use strict";

(function () {

    var classes = {
        formControl: "form-control",
        formGroup: "form-group",
        success: "has-success",
        error: "has-warning",
        warning: "has-warning",
        errorBlock: "help-block",
        disabled: "disabled"
    };

    window.greenApple = {
        alert: {
            success: function (message) {
                toastr.success(message);
            },
            error: function (message) {
                toastr.error(message);
            },
            info: function (message) {
                toastr.info(message);
            },
            warning: function (message) {
                toastr.warning(message);
            }
        },
        form: {
            clearErrors: function ($form) {
                $form.find("." + classes.formGroup)
                    .removeClass([classes.success, classes.error].join(" "))
                    .find("." + classes.errorBlock)
                    .html("");
            },
            clearTabularFormErrors: function ($forms) {
                $.each($forms, function (i, form) {
                    $(form)
                        .find('input')
                        .popover('destroy')
                        .removeClass('has-error');
                });
            },
            setErrors: function ($form, errors) {
                this.clearErrors($form);
                $.each(errors, function (elementId, error) {
                    $form
                        .find("#" + elementId)
                        .parents("." + classes.formGroup)
                        .removeClass(classes.success)
                        .addClass(classes.error)
                        .find("." + classes.errorBlock)
                        .html(error);
                });
            },
            setTabularFormErrors: function ($forms, errors) {
                var self = this;
                self.clearTabularFormErrors($forms);
                $.each($forms, function (i, form) {
                    $.each(errors, function (elementId, error) {
                        $(form)
                            .find('.' + elementId)
                            .addClass('has-error')
                            .popover({
                                trigger: 'hover',
                                placement: 'top',
                                content: error[0],
                                container: 'body'
                            });
                    });
                });
            },
            clear: function ($form) {
                this.clearErrors($form);
                $form.find("." + classes.formControl).val(null).trigger("change.select2");
            }
        },
        ajax: {
            perform: function (url, data, options) {
                data = typeof data === 'undefined' ? {} : data;
                options = typeof options === 'undefined' ? {} : options;
                options.__proto__ = new AjaxRequestOptions(url, data);
                $.ajax(options);
            },
            command: function ($a, options) {
                options = typeof options === 'undefined' ? {} : options;
                options.__proto__ = new AjaxRequestCommandOptions($a);
                if ($a.hasClass(classes.disabled)) {
                    return false;
                }
                $a.addClass(classes.disabled);
                $.ajax(options);
            },
            submitForm: function ($form, options) {
                options = typeof options === 'undefined' ? {} : options;
                options.__proto__ = new AjaxRequestFormOptions($form);
                if ($form.hasClass(classes.disabled)) {
                    return false;
                }
                $form.addClass(classes.disabled);
                $.ajax(options)
            }
        }
    };

    function getArgs(args) {
        args = Array.apply(null, args);
        args.shift();
        return args;
    }

    function AjaxRequestOptions(url, data) {
        this.url = url;
        this.method = "POST";
        if (data !== undefined) {
            this.data = data;
        }
    }

    AjaxRequestOptions.prototype.resultSuccess = function (data) {
        greenApple.alert.success(data.message);
    };
    AjaxRequestOptions.prototype.resultValidation = function (data) {
    };
    AjaxRequestOptions.prototype.resultError = function (data) {
        greenApple.alert.error(data.message);
    };
    AjaxRequestOptions.prototype.resultNotification = function (data) {
        switch (data.type) {
            case 'info':
                greenApple.alert.info(data.message);
                break;
            default:
                greenApple.alert.warning(data.message);
                break;
        }
    };
    AjaxRequestOptions.prototype.success = function (data) {
        switch (data.result) {
            case "success" :
                this.resultSuccess(data);
                break;
            case "validation" :
                this.resultValidation(data);
                break;
            case "notification" :
                this.resultNotification(data);
                break;
            default:
                this.resultError(data);
        }
    };
    AjaxRequestOptions.prototype.error = function (xhr) {
        if (xhr.statusCode() >= 400) {
            greenApple.alert.error(xhr.responseText);
        }
    };
    AjaxRequestOptions.prototype.base = function (method) {
        AjaxRequestOptions.prototype[method].apply(this, getArgs(arguments));
    };

    function AjaxRequestCommandOptions($a) {
        this.$a = $a;
        AjaxRequestOptions.call(this, $a.attr("href"));
    }

    AjaxRequestCommandOptions.prototype = Object.create(AjaxRequestOptions.prototype);
    AjaxRequestCommandOptions.prototype.constructor = AjaxRequestCommandOptions;
    AjaxRequestCommandOptions.prototype.complete = function () {
        this.$a.removeClass(classes.disabled);
    };
    AjaxRequestCommandOptions.prototype.base = function (method) {
        AjaxRequestCommandOptions.prototype[method].apply(this, getArgs(arguments));
    };

    function AjaxRequestFormOptions($form) {
        this.$form = $form;
        AjaxRequestOptions.call(this, $form.attr("action"), $form.serialize());
    }

    AjaxRequestFormOptions.prototype = Object.create(AjaxRequestOptions.prototype);
    AjaxRequestFormOptions.prototype.constructor = AjaxRequestFormOptions;
    AjaxRequestFormOptions.prototype.complete = function () {
        this.$form.removeClass(classes.disabled);
    };
    AjaxRequestFormOptions.prototype.resultValidation = function (data) {
        greenApple.form.setErrors(this.$form, data.validation);
    };
    AjaxRequestFormOptions.prototype.resultSuccess = function () {
        greenApple.form.clear(this.$form);
    };
    AjaxRequestFormOptions.prototype.base = function (method) {
        AjaxRequestFormOptions.prototype[method].apply(this, getArgs(arguments));
    };

})();

$.extend({
    redirectPost: function(location, args) {
        var form = '';

        var csrfParam = $('meta[name=csrf-param]').attr('content');
        args[csrfParam] = $('meta[name=csrf-token]').attr('content');

        $.each(args, function(key, value) {
            form += '<input type="hidden" name="'+key+'" value="'+value+'">';
        });

        $('<form action="' + location + '" method="POST">' + form + '</form>')
            .appendTo($(document.body))
            .submit();
    }
});