"use strict";

Vue.component('datepicker', {
    inheritAttrs: false,
    props: ['options', 'value', 'placeholder', 'cssClass'],
    template:
        '<div class="input-group date">' +
        '<input type="text" ref="input" :placeholder="placeholder" :class="cssClass">' +
        '<span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar"></span></span>' +
        '</div>',
    mounted: function () {
        var vm = this;
        var options = _.merge({
            autoclose: true,
            startView: 2,
            maxViewMode: 'years',
            format: 'mm/dd/yyyy',
            clearBtn: true
        }, this.options);

        $(this.$refs.input)
            .datepicker(options)
            .val(this.value)
            .on('change', function () {
                vm.$emit('input', this.value);
                vm.$emit('date-picked', this.value);
            });
    },
    watch: {
        value: function (value) {
            $(this.$refs.input).val(value);
        }
    },
    destroyed: function () {
        $(this.$el).off().datepicker('destroy')
    }
});

Vue.component('touch-spin', {
    props: {
        value: {
            type: Number
        },
        min: {
            type: Number
        },
        max: {
            type: Number
        },
        step: {
            type: Number,
            default: 1
        }
    },
    template: '<div><input ref="input"></div>',
    mounted: function () {
        var vm = this;
        $(this.$refs.input)
            .TouchSpin({
                min: this.min,
                max: this.max,
                step: this.step,
                buttonup_class: 'input-group-addon on-right',
                buttondown_class: 'input-group-addon on-left',
                buttonup_txt: '\u003Ci class=\u0022glyphicon glyphicon-plus\u0022\u003E\u003C\/i\u003E',
                buttondown_txt: '\u003Ci class=\u0022glyphicon glyphicon-minus\u0022\u003E\u003C\/i\u003E'
            })
            .val(this.value)
            .on('change', function () {
                vm.$emit('input', this.value);
            });
    },
    watch: {
        value: function (value) {
            $(this.$refs.input).val(value);
        }
    },
    destroyed: function () {
        $(this.$refs.input).off().TouchSpin("destroy");
    }
});

Vue.component("select2", {
    props: ['placeholder', 'options', 'value', 'allowClear', 'disabled'],
    template: '<div><select ref="select"></select></div>',
    mounted: function () {
        var vm = this;
        $(this.$refs.select)
            .select2({
                data: this.options,
                width: '100%',
                allowClear: this.allowClear,
                theme: 'default',
                minimumResultsForSearch: Infinity,
                placeholder: this.placeholder,
                disabled: this.isDisabled()
            })
            .val(this.value)
            .trigger('change')
            .on('change', function () {
                vm.$emit('input', this.value);
                vm.$emit('select2-selected', $(this).select2('data')[0]);
            })
    },
    watch: {
        value: function (value) {
            $(this.$refs.select)
                .val(value)
                .trigger('change');
        },
        options: function (options) {
            $(this.$refs.select).empty().select2({
                data: options,
                placeholder: this.placeholder,
                value: this.value,
                allowClear: this.allowClear,
                theme: 'default',
                width: '100%',
                minimumResultsForSearch: Infinity,
                disabled: this.disabled
            })
        }
    },
    destroyed: function () {
        $(this.$refs.select).off().select2("destroy")
    },
    methods: {
        isDisabled: function () {
            return this.options.length == 1;
        }
    }
});