(function (factory) {
        /* Global define */
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define(['jquery'], factory);
        } else if (typeof module === 'object' && module.exports) {
            // Node/CommonJS
            module.exports = factory(require('jquery'));
        } else {
            // Browser globals
            factory(window.jQuery);
        }
    }(function ($) {
        /**
         * @class plugin.insertVars
         *
         */

        $.extend(true, $.summernote.lang, {
            'en-US': {
                /* US English(Default Language) */
                insertVars: {}
            }
        });
        $.extend($.summernote.options, {
            insertVars: {
                icon: '<i class="fa fa-bars"/>',
                tooltip: 'Student name insert plugin',
                text: 'Insert variable',
                insertValue: '{studentName}',
                items: {
                    '{studentName}': 'Student name',
                    '{parentName}': 'Parent name',
                    '{teacherName}': 'Teacher name',
                    '{school}': 'School',
                    '{grade}': 'Grade',
                    '{eventTitle}': 'Event title',
                    '{eventStartDate}': 'Event start date',
                    '{eventEndDate}': 'Event end date'
                    // '{eventLocation}': 'Event location'
                }
            }
        });

        $.extend($.summernote.plugins, {
            /**
             *  @param {Object} context - context object has status of editor.
             */
            'insertVars': function (context) {
                var self = this,

                    // ui has renders to build ui elements
                    // for e.g. you can create a button with 'ui.button'
                    ui = $.summernote.ui,
                    $note = context.layoutInfo.note,

                    // contentEditable element
                    $editor = context.layoutInfo.editor,
                    $editable = context.layoutInfo.editable,
                    $toolbar = context.layoutInfo.toolbar,

                    // options holds the Options Information from Summernote and what we extended above.
                    options = context.options,

                    // lang holds the Language Information from Summernote and what we extended above.
                    lang = options.langInfo;

                context.memo('button.insertVars', function () {

                    var contentList = '';
                    $.each(options.insertVars.items, function (index, value) {
                        contentList += '<li><a data-value="' + index + '"><p>' + value + '</p></a></li>';
                    });


                    // Here we create a button
                    var buttonGroup = ui.buttonGroup([
                        ui.button({
                            className: 'dropdown-toggle',
                            contents: options.insertVars.icon + ' ' + options.insertVars.text,
                            tooltip: 'hello',
                            data: {
                                toggle: 'dropdown'
                            },
                            click: function () {
                                // Cursor position must be saved because is lost when dropdown is opened.
                                context.invoke('editor.saveRange');
                            }
                        }),
                        ui.dropdown({
                            className: 'drodown-style',
                            contents: contentList,
                            callback: function ($dropdown) {
                                $dropdown.find('li').each(function () {
                                    $(this).click(function (e) {
                                        // We restore cursor position and text is inserted in correct pos.
                                        context.invoke('editor.restoreRange');
                                        context.invoke('editor.focus');
                                        context.invoke("editor.insertText", $(this).children('a').attr('data-value'));
                                        e.preventDefault();
                                    });
                                });
                            }
                        })
                    ]);

                    return buttonGroup.render();   // return button as jquery object
                });
                this.insertName = function () {
                    context.invoke('editor.insertText', options.insertVars.insertValue);
                };
                this.getDropdownContent = function () {
                    return "<ol><li>Text.1</li><li>Text.2</li><li>Text.3</li></ol>";
                }
            }
        })
    })
);