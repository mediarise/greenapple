<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.04.18
 * Time: 17:28
 */

namespace frontend\widgets;


use backend\assets\BackendAsset;
use backend\assets\CropperAsset;
use frontend\assets\InspiniaAsset;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class Cropper
 *
 * Render image cropper
 *
 * For example:
 * echo Cropper::widget([
 *      'imageSrc' => Url::to(['path/to/image']),
 *      'uploadUrl' => Url::to(["path/to/upload-photo"]),
 * ]);
 *
 * @package frontend\widgets
 */
class Cropper extends Widget
{
    /**
     * Cropped image path
     */
    public $imageSrc;

    /**
     * Placeholder path
     */
    public $placeholderSrc;

    /**
     * Url for image upload
     */
    public $uploadUrl;

    /**
     * @var array
     * Options for cropper container
     */
    public $containerOptions = [];

    /**
     * @var string
     */
    public $modalTitle = 'Crop image';

    /**
     * @var string
     */
    public $croppedImageInputId = 'cropped-image-input';


    /**
     * Init the widget
     */
    public function init()
    {
        parent::init();
        CropperAsset::register($this->view);
        Html::addCssClass($this->containerOptions, ['col-md-12', 'col-lg-6', 'col-xl-4', 'pr-0']);
    }

    /**
     * Render the widget
     * @return string
     */
    public function run()
    {
        return $this->render('cropper', [
            'placeholderSrc' => $this->placeholderSrc,
            'imageSrc' => $this->imageSrc,
            'uploadUrl' => $this->uploadUrl,
            'containerOptions' => $this->containerOptions,
            'modalTitle' => $this->modalTitle,
            'croppedImageInputId' => $this->croppedImageInputId,
        ]);
    }
}