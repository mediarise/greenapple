<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.04.18
 * Time: 20:08
 */

namespace frontend\widgets;


use backend\assets\SummernoteAsset;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\InputWidget;

class HtmlEditor extends InputWidget
{

    public $enableDefaultSettings = true;
    public $previewEventTitle = 'AGO visit';

    protected $customSettings = [
        ['custom', ['insertVars']],
        ['custom', ['showPreview']],
    ];

    protected $defaultSettings = [
        ["style", ["style"]],
        ["font", ["bold", "underline", "clear"]],
        ["fontname", ["fontname"]],
        ["color", ["color"]],
        ["para", ["ul", "ol", "paragraph"]],
        ["table", ["table"]],
        // ["insert", ["link", "picture", "video"]],
        ["view", ["fullscreen", "codeview"]],
    ];

    /**
     * Init the widget
     */
    public function init()
    {
        parent::init();
        $this->options['id'] = $this->getId();
        $this->registerAssets();
    }

    /**
     * Render the widget
     */
    public function run()
    {
        if ($this->hasModel()) {
            return Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            return Html::textarea($this->name, '', $this->options);
        }
    }

    public function setCustomSettings($params)
    {
        $this->customSettings = $params;
    }

    public function registerAssets()
    {
        $view = $this->getView();
        SummernoteAsset::register($view);
        $inputId = $this->getId();

        $defaultSettings = $this->enableDefaultSettings ? $this->defaultSettings : [];
        $toolbarSettings = ArrayHelper::merge($defaultSettings, $this->customSettings);
        $view->registerJsVar('previewEventTitle', $this->previewEventTitle);
        $view->registerJsVar("summerNoteToolbarParams_{$inputId}", $toolbarSettings);
        $script = <<<JS
$(document).ready(function(){
    $('#{$inputId}').summernote({
    height: 200,
    toolbar: summerNoteToolbarParams_{$inputId}
    });
});
JS;
        $view->registerJs($script);

    }

}