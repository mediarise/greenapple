<?php

namespace frontend\widgets;

use common\widgets\Select2;
use Yii;
use yii\base\InvalidConfigException;
use yii\grid\GridView as GridViewBase;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\Pjax;

/**
 * Customized GridView with external filtering form
 *
 * For example:
 * ...
 * 'filterRowOptions' => [
 *      'class' => 'some-filters'
 *  ],
 * 'filterWrapperSelector' => '.some-filters',
 * 'filters' => [
 *      [
 *          'attributes' => ['name', 'email'],
 *          'type' => '...',
 *          'filter' => [...],
 *      ],
 * ],
 */
class GridView extends GridViewBase
{
    const FILTER_TYPE_SELECT_2 = 'Select2';
    const FILTER_TYPE_RADIO_LIST = 'RadiolList';
    const FILTER_TYPE_CHECKBOX_LIST = 'CheckBoxList';
    const FILTER_TYPE_TEXT_INPUT = 'TextInput';

    private $textInputFilterTemplate = '{label}{input}';
    private $select2FilterTemplate = '{label}{widget}';

    public $filtersTemplate = "{wrapperBegin}<div class=\"form-horizontal\">{filters}</div>{wrapperEnd}";
    public $filterWrapperSelector = '.filters';
    public $enablePjax = false;
    public $pjaxOptions = [];
    public $summary = false;

    /**
     * External filter list
     */
    public $filters = [];

    public function init()
    {
        parent::init();

    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo $this->renderExternalFilters();
        if ($this->enablePjax) {
            Pjax::begin($this->pjaxOptions);
        }
        parent::run();
        if ($this->enablePjax) {
            Pjax::end();
        }
        $this->registerJs();
    }

    protected function registerJs()
    {
        $gridId = $this->getId();
        $this->view->registerJs("
            $('body').on('change keyup', '{$this->filterWrapperSelector} input', function(e) {
                var value = $(this).val();
                if (value.length >=2 || value.length == 0) {
                  $(\"#{$gridId}\").yiiGridView('applyFilter');
                }
            });
        ");
    }

    /**
     * Render form with external filters
     */
    protected function renderExternalFilters()
    {
        if (empty($this->filters)) {
            return '';
        }
        if (!is_array($this->filters)) {
            throw new InvalidConfigException();
        }
        $formItems = [];
        foreach ($this->filters as $externalFilter) {
            $formItems[] = $this->renderExternalFilter($externalFilter);
        }

        $filters = implode("\n", $formItems);
        $wrapperBegin = $this->getWrapperBegin();
        $wrapperEnd = $this->getWrapperEnd();
        $pairs = ['{wrapperBegin}' => $wrapperBegin, '{wrapperEnd}' => $wrapperEnd, '{filters}' => $filters];
        return strtr($this->filtersTemplate, $pairs);
    }

    /**
     * Render item for filter form
     */
    protected function renderExternalFilter($filter)
    {
        if (!is_array($filter)) {
            return $filter;
        }
        if (!isset($filter['type'])) {
            throw new InvalidConfigException();
        }
        switch ($filter['type']) {
            case self::FILTER_TYPE_CHECKBOX_LIST :
                return '';
                break;
            case self::FILTER_TYPE_RADIO_LIST :
                return '';
                break;
            case self::FILTER_TYPE_SELECT_2 :
                return $this->renderSelect2Filter($filter);
                break;
            case self::FILTER_TYPE_TEXT_INPUT :
                return $this->renderInputTextFilter($filter);
                break;
            default:
                return '';
                break;
        }
    }

    protected function renderSelect2Filter($filter)
    {
        if (!isset($filter['widgetConfig'])) {
            throw new InvalidConfigException();
        }
        $widgetConfig = $filter['widgetConfig'];
        if (!isset($widgetConfig['name'])) {
            throw new InvalidConfigException();
        }
        $value = $this->getSelect2FilterValue($widgetConfig['name']);
        ArrayHelper::setValue($widgetConfig, 'value', $value);

        if ($this->filterModel !== null) {
            $widgetConfig['name'] = Html::getInputName($this->filterModel, $widgetConfig['name']);
        }
        $labelContent = ArrayHelper::getValue($filter, 'label', '');
        $labelOptions = ArrayHelper::getValue($filter, 'labelOptions', []);
        $label = Html::label($labelContent, null, $labelOptions);
        $widget = Select2::widget($widgetConfig);
        $template = ArrayHelper::getValue($filter, 'template', $this->select2FilterTemplate);
        $pairs = ['{label}' => $label, '{widget}' => $widget];
        return strtr($template, $pairs);
    }

    private function getSelect2FilterValue($name)
    {
        $queryParams = Yii::$app->request->queryParams;
        if ($this->filterModel !== null) {
            $filterModelName = $this->getFilterModelShortName();
            if (!isset($queryParams[$filterModelName])) {
                return [];
            }
            return ArrayHelper::getValue($queryParams[$filterModelName], $name, []);
        } else {
            return ArrayHelper::getValue($queryParams, $name, []);
        }
    }


    /**
     * Render input text filter
     */
    protected function renderInputTextFilter($filter)
    {
        $name = $filter['name'];
        if ($this->filterModel !== null) {
            $value = $this->filterModel->{$name};
            $name = Html::getInputName($this->filterModel, $name);
        } else {
            $value = Yii::$app->request->getQueryParam($name);
        }
        $options = ArrayHelper::getValue($filter, 'options', []);
        $label = ArrayHelper::getValue($filter, 'label', '');
        $labelOptions = ArrayHelper::getValue($filter, 'labelOptions', []);
        $label = Html::label($label, null, $labelOptions);
        Html::addCssClass($options, 'form-control');
        $input = Html::textInput($name, $value, $options);
        $template = ArrayHelper::getValue($filter, 'template', $this->textInputFilterTemplate);
        $pairs = ['{label}' => $label, '{input}' => $input];
        return strtr($template, $pairs);
    }


    protected function getFilterModelShortName()
    {
        return StringHelper::basename(get_class($this->filterModel));
    }

    public function renderFilters()
    {
        return '';
    }

    protected function getWrapperBegin()
    {
        return '<div ' . Html::renderTagAttributes($this->filterRowOptions) . '>';
    }

    protected function getWrapperEnd()
    {
        return '</div>';
    }

}