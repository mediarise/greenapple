<?php

namespace frontend\widgets;

use yii\base\Widget;

class SideBarHeader extends Widget
{
    public function run()
    {
        return $this->render('sidebar-header');
    }

}