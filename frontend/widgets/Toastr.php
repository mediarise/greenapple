<?php


namespace frontend\widgets;


use Yii;
use yii\base\Widget;
use yii\helpers\Json;

class Toastr extends Widget
{
    /**
     * @var array
     */
    public $options = [];

    /**
     * Default toaster options
     * @var array
     */
    protected $defaultOptions = [
        "closeButton" => true,
        "debug" => false,
        "progressBar" => true,
        "preventDuplicates" => true,
        "positionClass" => "toast-bottom-left",
        "onclick" => null,
        "showDuration" => "400",
        "hideDuration" => "1000",
        "timeOut" => "7000",
        "extendedTimeOut" => "1000",
        "showEasing" => "swing",
        "hideEasing" => "linear",
        "showMethod" => "fadeIn",
        "hideMethod" => "fadeOut"
    ];

    public function run()
    {
        $this->registerAssets();
        $this->registerToastr('error');
        $this->registerToastr('success');
        $this->registerToastr('info');
        $this->registerToastr('warning');
    }

    public function registerToastr($type)
    {
        $messages = Yii::$app->session->getFlash($type, []);

        if (is_string($messages)) {
            $messages = [$messages];
        }

        foreach ($messages as $message) {
            $this->view->registerJs("toastr.$type('$message');", yii\web\View::POS_READY);
        }
    }

    protected function registerAssets()
    {
        $this->view->registerJs("toastr.options={$this->toasterOptions}");
    }

    protected function getToasterOptions()
    {
        $options = empty($this->options) ? $this->defaultOptions : $this->options;
        return Json::encode($options);
    }
}