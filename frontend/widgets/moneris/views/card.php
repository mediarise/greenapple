<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

/**
 * @var \yii\web\View $this
 * @var array $buttonOptions
 * @var \common\models\BankCard $card
 */

?>

<?php Modal::begin([
    'header' => '<h2>Add new card</h2>',
    //'toggleButton' => $buttonOptions,
    'id' => 'add-card-modal'
]); ?>

<?php $form = ActiveForm::begin(['id' => 'card-form', 'action' => ['make-payment'], 'enableClientValidation' => false]); ?>
<!--<form role="form" id="payment-form">-->
<div class="row">
    <div class="col-xs-12">
        <?= $form->field($card, 'number', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><i class="fa fa-credit-card"></i></span></div>',
        ])->textInput(['placeholder' => 'Valid Card Number', 'autocomplete' => 'cc-number'])
            ->label('CARD NUMBER') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-7 col-md-7">
        <?= $form
            ->field($card, 'expireDate')
            ->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '99/99',
                'options' => [
                    'placeholder' => 'MM / YY',
                    'autocomplete' => 'cc-exp',
                    'class' => 'form-control',
                ],
            ])
            ->label('EXPIRATION DATE') ?>
    </div>
    <div class="col-xs-5 col-md-5 pull-right">
        <?= $form
            ->field($card, 'cvd')
            ->textInput(['placeholder' => 'CVD', 'autocomplete' => 'cc-csc'])
            ->label('CV CODE') ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?= \yii\helpers\Html::button('Make a payment!', ['class' => 'btn btn-success btn-xl', 'id' => 'make-payment']) ?>
        <?php $this->registerJs(/** @lang JavaScript */
            <<<JS
var makePaymentLadda = Ladda.create(document.getElementById("make-payment"));
$("#make-payment").click(function () {
    makePaymentLadda.start();
    // noinspection JSUnusedGlobalSymbols
greenApple.ajax.submitForm($("#card-form"), {
        complete: function () {
            this.base("complete");
            makePaymentLadda.stop();
        }
    });
});
JS
        ) ?>
    </div>
</div>
<!--</form>-->
<?php ActiveForm::end() ?>

<?php Modal::end() ?>
