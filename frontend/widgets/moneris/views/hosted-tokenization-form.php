<?php

/* @var \yii\web\View $this */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\spinner\Spinner;

/* @var string $url */
/* @var array $buttonOptions */
?>

    <script>
        "use strict";

        function doMonerisSubmit() {
            var monFrameRef = document.getElementById('monerisFrame').contentWindow;
            monFrameRef.postMessage('', 'https://esqa.moneris.com/HPPtoken/index.php');
        }

        var respMsg = function (e) {
            var respData = eval("(" + e.data + ")");
            if (typeof respData.errorMessage === "undefined") {
                $("#data-key").val(respData.dataKey);
                $("#token-form").submit();
            } else {
                document.getElementById("monerisResponse").innerHTML = e.origin + " SENT " + " - " +
                    respData.responseCode + "-" + respData.dataKey + "-" + respData.errorMessage;
            }
        };

        window.onload = function () {
            if (window.addEventListener) {
                window.addEventListener("message", respMsg, false);
            }
            else {
                if (window.attachEvent) {
                    window.attachEvent("onmessage", respMsg);
                }
            }
        }
    </script>

<?php $modal = Modal::begin([
    'header' => '<h2>Add new card</h2>',
    'toggleButton' => $buttonOptions,
    'id' => 'add-card-modal'
]); ?>
    <div class="pos-r">
        <div id="block-content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="moneris-frame" style="width: 300px; margin: 0 auto">
                        <?= Html::tag('iframe', '', [
                            'id' => 'monerisFrame',
                            'style' => [
                                'width' => '100%',
                                'height' => '175px',
                            ],
                            'frameborder' => 0,
                        ]) ?>
                        <?php ActiveForm::begin([
                            'id' => 'token-form',
                            'method' => 'post',
                            'action' => Url::to(['add-card'])]) ?>
                        <?= Html::hiddenInput('data_key', '', ['id' => 'data-key']) ?>
                        <?php ActiveForm::end() ?>
                        <button class="btn btn-primary col-xs-6 col-xs-offset-3" onClick="doMonerisSubmit()">Submit
                        </button>
                        <div id="monerisResponse"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="block-spinner" class="spinner-wrapper">
            <?= /** @noinspection PhpUnhandledExceptionInspection */
            Spinner::widget([
                'preset' => 'large',
                'align' => 'center',
                'color' => '#3CB392',
            ]); ?>
        </div>
    </div>
    <div class="mb-25"></div>

<?php Modal::end() ?>

<?php $this->registerJs(<<<JS
$("#{$modal->id}").on("show.bs.modal", function() {
    $("#block-spinner").removeClass("invisible"); 
    $("#block-content").addClass("invisible");
    var monFrameRef = document.getElementById('monerisFrame');
    monFrameRef.src = "$url";
});
var monFrameRef = document.getElementById('monerisFrame');
monFrameRef.onload = function () { 
    $("#block-spinner").addClass("invisible"); 
    $("#block-content").removeClass("invisible"); 
};
JS
);