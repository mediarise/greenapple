<?php

namespace frontend\widgets\moneris;

use common\models\BankCard;
use yii\base\Widget;

class HostedTokenizationForm extends Widget
{
    public $buttonOptions;

    public function run()
    {
        $card = new BankCard();
        return $this->render('card', [
            'card' => $card,
        ]);
    }

}