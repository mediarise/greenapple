<?php

namespace frontend\widgets;

use frontend\assets\SimplebarAsset;
use yii\base\Widget;
use yii\helpers\Html;

class Simplebar extends Widget
{
    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        SimplebarAsset::register($this->view);
        $content = ob_get_clean();
        echo Html::tag('div', $content, ['id' => $this->id, 'style' => 'max-height: 650px;']);
        $this->view->registerJs(sprintf('new SimpleBar(document.getElementById("%s"))', $this->id));
    }
}