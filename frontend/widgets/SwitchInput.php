<?php


namespace frontend\widgets;


use backend\assets\BackendAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\InputWidget;
use function Stringy\create as s;

class SwitchInput extends InputWidget
{
    const COLOR_PRIMARY = '#5e5fc6';
    const COLOR_SUCCESS = '#62bb46';

    public $checked = false;
    public $color = self::COLOR_PRIMARY;
    public $secondaryColor = '#dfdfdf';
    public $jackColor = '#fff';
    public $jackSecondaryColor = '#fff';
    public $className = 'switchery';
    public $disabled = false;
    public $disabledOpacity = 0.5;
    public $speed = '0.4s';
    public $size = 'default';
    public $wrapperOptions = [];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->registerAsset();
        Html::addCssClass($this->options, 'switch-input');
        $this->options['id'] = $this->getId();
    }

    public function run()
    {
        $checkbox = $this->getCheckbox();
        return $this->wrap($checkbox);
    }


    protected function wrap($content)
    {
        if (isset($this->wrapperOptions['tag']) && $this->wrapperOptions['tag'] !== false) {
            $tag = $this->wrapperOptions['tag'];
            unset($this->wrapperOptions['tag']);
            return Html::tag($tag, $content, $this->wrapperOptions);
        }
        return $content;
    }

    protected function getCheckbox()
    {
        if ($this->hasModel()) {
            return Html::activeCheckbox($this->model, $this->attribute, $this->options);
        } else {
            return Html::checkbox($this->name, $this->checked, $this->options);
        }
    }

    protected function registerAsset()
    {
        $view = $this->view;
        $view->registerCssFile('css/plugins/switchery/switchery.css', ['depends' => BackendAsset::class]);
        $view->registerJsFile('js/plugins/switchery/switchery.js', ['depends' => BackendAsset::class]);
        $switcherOption = Json::encode($this->getSwitcheryOptions());
        $varName = 'switchery_' . s($this->getId())->replace('-', '_');
        $checkboxId = $this->getId();
        $disabled = intval($this->disabled);
        $script = <<<JS
var {$varName} = new Switchery(document.querySelector('.switch-input#{$checkboxId}'), {$switcherOption});
if ({$disabled}) {
    {$varName}.disable();
}
      
JS;
        $view->registerJs($script, View::POS_END);
    }

    protected function getSwitcheryOptions()
    {
        $options = [
            'color' => $this->color,
            'secondaryColor' => $this->secondaryColor,
            'jackColor' => $this->jackColor,
            'className' => $this->className,
            'disabledOpacity' => $this->disabledOpacity,
            'speed' => $this->speed,
            'size' => $this->size,
        ];
        return $options;
    }
}