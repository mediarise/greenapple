<?php

namespace frontend\widgets;

use yii\bootstrap\Html;

class Radio extends CheckBox
{
    public $items = [];

    public function run()
    {
        echo $this->renderRadio();
    }

    public function renderRadio()
    {
        if (empty($this->items)) {
            if ($this->hasModel()) {
                return Html::activeRadio($this->model, $this->attribute, $this->options);
            }
            return Html::radio($this->name, $this->value, $this->options);
        }
        if ($this->hasModel()){
            return Html::activeRadioList($this->model, $this->attribute, $this->items, $this->options);
        }
        return Html::radioList($this->name, $this->value, $this->items, $this->options);
    }
}