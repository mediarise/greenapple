<?php


namespace frontend\widgets;


use backend\assets\BackendAsset;
use common\helpers\ArrayHelper;
use yii\base\Widget;
use yii\helpers\Html;

class Wizard extends Widget
{
    public $steps = [];
    public $wizardId = 'wizard';

    public function run()
    {
        $this->registerAssets();
        return Html::tag('div', $this->renderSteps(), ['id' => $this->wizardId]);
    }

    protected function renderSteps()
    {
        if (!is_array($this->steps) || empty($this->steps)) {
            return '';
        }
        $renderedSteps = [];
        foreach ($this->steps as $stepNumber => $step) {
            $renderedSteps[] = $this->renderStep($step, $stepNumber);
        }
        return implode('', $renderedSteps);
    }

    public function renderStep($step, $stepNumber)
    {
        $showedStepNumber = $stepNumber + 1;
        $title = ArrayHelper::getValue($step, 'title', "Step {$showedStepNumber}");
        $content = ArrayHelper::getValue($step, 'content', '');
        $titleContainer = Html::tag('h1', $title);
        $contentContainer = Html::tag('fieldset', $content);
        return $titleContainer . $contentContainer;
    }

    protected function registerAssets()
    {
        $view = $this->view;
        $view->registerCssFile('css/plugins/steps/jquery.steps.css', ['depends' => BackendAsset::class]);
        $view->registerJsFile('js/plugins/steps/jquery.steps.min.js', ['depends' => BackendAsset::class]);
        $script = <<<JS
$(document).ready(function(){
            $("#{$this->wizardId}").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    return true;
                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            });
       });
JS;

        $view->registerJs($script);
    }
}