<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.04.18
 * Time: 19:07
 */

namespace frontend\widgets;


use backend\assets\BackendAsset;
use frontend\assets\InspiniaAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\InputWidget;

class ClockPicker extends InputWidget
{
    /**
     * @var string the addon markup if you wish to display the input as a component. If you don't wish to render as a
     * component then set it to null or false.
     */
    public $addon = '<i class="fa fa-clock-o"></i>';

     /**
     * @var string the template to render the input.
     */
    public $template = '{input}{addon}';

    public $containerOptions = [];



    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, ['form-control']);
        Html::addCssClass($this->containerOptions, ['clockpicker', 'input-group']);

        if (!isset($this->containerOptions['data-autoclose'])) {
            $this->containerOptions = ArrayHelper::merge(
                $this->containerOptions,
                ['data-autoclose' => 'true']
            );
        }
        $this->registerAssets();
    }

    public function run()
    {
        $input = $this->hasModel()
            ? Html::activeTextInput($this->model, $this->attribute, $this->options)
            : Html::textInput($this->name, $this->value, $this->options);

         if ($this->addon) {
            $addon = Html::tag('span', $this->addon, ['class' => 'input-group-addon']);
            $input = strtr($this->template, ['{input}' => $input, '{addon}' => $addon]);
            $input = Html::tag('div', $input, $this->containerOptions);
        }
        return $input;
    }

    protected function registerAssets()
    {
        $view = $this->view;
        $view->registerJsFile('js/plugins/clockpicker/clockpicker.js', ['depends' => BackendAsset::class]);
        $view->registerCssFile('css/plugins/clockpicker/clockpicker.css', ['depends' => BackendAsset::class]);
        $view->registerJs('$(\'.clockpicker\').clockpicker();');
    }
}