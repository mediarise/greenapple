<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08.04.18
 * Time: 16:55
 */

namespace frontend\widgets;

use kartik\typeahead\Typeahead;
use yii\web\JsExpression;

/**
 * Class AutocompletedInput
 * @package frontend\widgets
 *
 */
class AutocompletedInput extends Typeahead
{
    /**
     * Address of the list with results
     * The page must contain an json with result items. Each item must contain parameter 'value'
     */
    public $dataUrl;

    /**
     * Template for result item
     */
    public $itemTemplate = '<p>{{value}}}</p>';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setDefaultDataset();
    }

    /**
     * @return string|void
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        parent::run();
    }

    /**
     * Adjusts the parameter 'dataset'
     */
    protected function setDefaultDataset()
    {
        $this->dataset[] = [
            'prefetch' => $this->dataUrl,
            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
            'display' => 'value',
            'templates' => [
                'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                'suggestion' => new JsExpression("Handlebars.compile('{$this->itemTemplate}')")
            ],
            'remote' => [
                'url' => $this->dataUrl . '?q=%QUERY',
                'wildcard' => '%QUERY'
            ]
        ];
    }

}