<?php

namespace frontend\widgets;

use common\helpers\ArrayHelper;
use frontend\assets\InspiniaAsset;
use yii\helpers\Html;
use yii\widgets\InputWidget;

class CheckBox extends InputWidget
{
    const PAIR_LABEL_BEGIN = '{labelBegin}';
    const PAIR_LABEL_CONTENT = '{labelContent}';
    const PAIR_LABEL_END = '{labelEnd}';
    const PAIR_INPUT = '{input}';
    const PAIR_ERROR = '{error}';
    const PAIR_HINT = '{hint}';

    public $template = "";
    public $label = null;
    public $assetDepends = [InspiniaAsset::class];

    protected $pairs = [];

    /**
     * Init widget
     */
    public function init()
    {
        parent::init();
        $this->registerCss();
        $this->registerJs();
        $this->setDefaultPairs();
        $this->setDefaultTemplate();
    }

    /**
     * Run the widget
     */
    public function run()
    {

        if ($this->hasModel()) {
            echo $this->renderActiveCheckbox($this->model, $this->attribute, $this->options);
        } else {
            echo $this->renderCheckbox($this->name, $this->value, $this->options);
        }
    }

    protected function renderActiveCheckbox($model, $attribute, $options)
    {
        $name = isset($options['name']) ? $options['name'] : html::getInputName($model, $attribute);
        $value = html::getAttributeValue($model, $attribute);

        if (!array_key_exists('value', $options)) {
            $options['value'] = '1';
        }
        if (!array_key_exists('uncheck', $options)) {
            $options['uncheck'] = '0';
        } elseif ($options['uncheck'] === false) {
            unset($options['uncheck']);
        }
        if (!array_key_exists('label', $options)) {
            $options['label'] = html::encode($model->getAttributeLabel(html::getAttributeName($attribute)));
        } elseif ($options['label'] === false) {
            unset($options['label']);
        }
        if (isset($options['template'])) {
            unset($options['template']);
        }
        $checked = "$value" === "{$options['value']}";

        if (!array_key_exists('id', $options)) {
            $options['id'] = html::getInputId($model, $attribute);
        }
        if (isset($options['labelOptions'])) {
            $options['labelOptions'] = ArrayHelper::merge($options['labelOptions'], ['for' => $options['id']]);
        } else {
            $options['labelOptions']['for'] = $options['id'];
        }

        return $this->renderCheckbox($name, $checked, $options);
    }

    protected function renderCheckbox($name, $checked = false, $options = [])
    {
        $options['checked'] = (bool)$checked;
        $value = array_key_exists('value', $options) ? $options['value'] : '1';
        if (isset($options['uncheck'])) {
            // add a hidden field so that if the checkbox is not selected, it still submits a value
            $hiddenOptions = [];
            if (isset($options['form'])) {
                $hiddenOptions['form'] = $options['form'];
            }
            $hidden = Html::hiddenInput($name, $options['uncheck'], $hiddenOptions);
            unset($options['uncheck']);
        } else {
            $hidden = '';
        }
        $labelContent = '';
        $labelOptions = [];

        if (isset($options['label'])) {
            $labelContent = $options['label'];
            $labelOptions = isset($options['labelOptions']) ? $options['labelOptions'] : [];
            unset($options['label'], $options['labelOptions']);
        }
        if ($this->label !== null) {
            $labelContent = $this->label;
        }
        $input = $hidden . Html::input('checkbox', $name, $value, $options);

        $this->pairs[self::PAIR_LABEL_BEGIN] = $this->renderLabelBegin($labelOptions);
        $this->pairs[self::PAIR_LABEL_CONTENT] = $labelContent;
        $this->pairs[self::PAIR_INPUT] = $input;

        $widget = strtr($this->template, $this->pairs);
        return $widget;
    }

    protected function setDefaultPairs()
    {
        $this->pairs = [
            self::PAIR_LABEL_BEGIN => $this->renderLabelBegin([]),
            self::PAIR_LABEL_CONTENT => '',
            self::PAIR_LABEL_END => $this->renderLabelEnd(),
            self::PAIR_INPUT => Html::checkbox($this->name),
            self::PAIR_ERROR => '',
            self::PAIR_HINT => '',
        ];
    }

    protected function setDefaultTemplate()
    {
        $labelBegin = self::PAIR_LABEL_BEGIN;
        $labelContent = self::PAIR_LABEL_CONTENT;
        $labelEnd = self::PAIR_LABEL_END;
        $input = self::PAIR_INPUT;
        $error = self::PAIR_ERROR;
        $hint = self::PAIR_HINT;
        $this->template = "<div class=\"i-checks\">\n{$labelBegin}\n{$input}<i></i>\n{$labelContent}\n{$labelEnd}\n{$error}\n{$hint}\n</div>";
    }

    protected function renderLabelBegin($option)
    {
        return "<label " . Html::renderTagAttributes($option) . ">";
    }

    protected function renderLabelEnd()
    {
        return '</label>';
    }


    /**
     * Register the required css
     */
    protected function registerCss()
    {
        $view = $this->getView();
        $view->registerCssFile('@web/css/plugins/iCheck/custom.css', ['depends' => $this->assetDepends]);
    }

    /**
     * Register js scripts and files
     */
    protected function registerJs()
    {
        $view = $this->getView();
        $view->registerJsFile('@web/js/plugins/iCheck/icheck.min.js', ['depends' => $this->assetDepends]);
        $script = <<<JS
  $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
JS;
        $view->registerJs($script);

    }
}