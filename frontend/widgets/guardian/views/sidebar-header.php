<?php

/* @var $this \yii\web\View */
use common\widgets\Select2;

/* @var \common\models\guardian\SelectStudentForm $model */
/* @var $students  */ ?>

<li class="nav-header">
    <div class="dropdown profile-element">
        <span><img alt="image" src="/images/logo_v2.png" style="width: 100%"/></span>
    </div>
</li>


<li style="padding-bottom: 40px;"></li>

<li style="height: 64px;">
    <div style="margin: 10px;">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'id' => 'select-student-form',
            'action' => \yii\helpers\Url::to(['/guardian/index/select-student']),
            'validateOnBlur' => false,
            'validateOnChange' => false]); ?>
        <?= \yii\helpers\Html::activeHiddenInput($model, 'returnUrl') ?>
        <?= $form->field($model, 'studentId')->label('')->widget(Select2::class, [
            'data' => $students,
            'hideSearch' => true,
            'options' => [
                'id' => 'student-select'
            ],
            'pluginOptions' => [
                'allowClear' => false,
            ],
            'pluginEvents' => [
                'select2:select' => 'function() { $("#select-student-form").submit(); }',
            ]
        ]) ?>
        <?php \yii\widgets\ActiveForm::end(); ?>
    </div>
</li>

