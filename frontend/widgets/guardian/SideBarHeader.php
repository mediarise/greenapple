<?php

namespace frontend\widgets\guardian;

use common\models\guardian\SelectStudentForm;
use yii\helpers\ArrayHelper;

class SideBarHeader extends \frontend\widgets\SideBarHeader
{
    public function run()
    {
        $actor = \Yii::$container->get('Actor');
        $students = ArrayHelper::map($actor->students, 'id', 'fullName');
        $students[0] = 'all children';
        ksort($students);

        $form = new SelectStudentForm();
        $form->studentId = \Yii::$app->session->get('selectedStudent', 0);
        $form->returnUrl = \Yii::$app->request->url;

        return $this->render('sidebar-header', [
            'model' => $form,
            'students' => $students,
        ]);
    }
}