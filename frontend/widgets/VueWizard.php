<?php


namespace frontend\widgets;


use common\helpers\ArrayHelper;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class VueWizard
 *
 * Render navigation tab from vue form wizard
 *
 * Example:
 *
 * echo VueWizard::widget([
 *      'items' => [
 *              [
 *                  'title' => 'Step 1 title',
 *                  'icon' => 'fa fa-some-icon',
 *                  'isActive' => true,
 *              ],
 *              [
 *                  'title' => 'Step 2 title',
 *                  'icon' => 'fa fa-some-icon',
 *                  'isActive' => false,
 *                  'url' => '#',
 *                  'titleOptions' => [...],
 *                  'options' => [... //options for li element ],
 *                  'linkOptions' => [... //options for a element],
 *                  'tabOptions' => [... //option for tab div element],
 *              ],
 *              [
 *                  'title' => 'Step 3 title',
 *                  'icon' => 'fa fa-some-icon',
 *                  'isActive' => false,
 *              ],
 *          ],
 *      'options' => [ ... //main block options],
 *      'listOption' => [ ... //options for ul element ],
 *      'colorActive' => '#fafafa', //color of active tab
 * ]);
 *
 * @package frontend\widgets
 */
class VueWizard extends Widget
{
    public $items = [];
    public $options = [];
    public $colorActive = '#5e5fc6';
    public $colorDefault = '#f3f2ee';
    public $listOptions = [];

    /**
     * @return string
     */
    public function run()
    {
        $this->registerAssets();
        $navContainer = Html::tag('div', $this->renderItems(), ['class' => 'wizard-navigation']);
        Html::addCssClass($this->options, 'vue-form-wizard');
        return Html::tag('div', $navContainer, $this->options);
    }

    /**
     * @return array|string
     */
    protected function renderItems()
    {
        $items = [];
        if (!is_array($this->items)) {
            return $this->items;
        }
        foreach ($this->items as $index => $item) {
            $items[] = $this->renderItem($item, $index);
        }

        Html::addCssClass($this->listOptions, 'wizard-nav wizard-nav-pills');
        $ul = Html::tag('ul', implode("\n", $items), $this->listOptions);
        return $ul;
    }

    /**
     * @param $item
     * @param $index
     * @return string
     */
    protected function renderItem($item, $index)
    {
        if (!is_array($item)) {
            return $item;
        }
        $tabOptions = ArrayHelper::getValue($item, 'tabOptions', []);
        Html::addCssClass($tabOptions, 'wizard-icon-circle tab_shape');

        $icon = ArrayHelper::getValue($item, 'icon', $index);
        $icon = $this->renderIcon($icon);

        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
        $url = ArrayHelper::getValue($item, 'url', '#');

        $options = ArrayHelper::getValue($item, 'options', []);
        $isActive = ArrayHelper::getValue($item, 'isActive', false);

        $titleContent = ArrayHelper::getValue($item, 'title', '');
        $titleOptions = ArrayHelper::getValue($item, 'titleOptions', []);
        Html::addCssClass($titleOptions, 'stepTitle');


        if ($isActive) {
            Html::addCssClass($options, 'active');
            $tabBg = Html::tag('div', $icon, [
                'class' => 'wizard-icon-container tab_shape',
                'style' => "background-color: {$this->colorActive}",
            ]);
            $tab = Html::tag('div', $tabBg, $tabOptions);
            $titleOptions['style'] = "color: {$this->colorActive}";
        } else {
            $tab = Html::tag('div', $icon, $tabOptions);
        }

        $title = Html::tag('span', $titleContent, $titleOptions);

        $link = Html::a($tab . $title, $url, $linkOptions);
        $li = Html::tag('li', $link, $options);

        return $li;

    }

    /**
     * @param $icon
     * @return string
     */
    protected function renderIcon($icon)
    {
        $content = '';
        $options = ['class' => 'wizard-icon'];
        if (is_int($icon)) {
            $content = $icon + 1;
        }
        if (is_string($icon)) {
            Html::addCssClass($options, $icon);
        }
        $iTag = Html::tag('i', $content, $options);
        return $iTag;
    }


    protected function registerAssets()
    {
        $view = $this->view;
        $view->registerCssFile('/css/plugins/vue-form-wizard/vue-form-wizard.min.css');
    }
}