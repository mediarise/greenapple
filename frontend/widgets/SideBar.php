<?php

namespace frontend\widgets;

use yii\base\Widget;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/**
 * Nav renders a nav HTML component.
 *
 * For example:
 *
 * ```php
 * echo SideBar::widget([
 *     'items' => [
 *         [
 *             'label' => 'Home',
 *             'url' => ['site/index'],
 *             'linkOptions' => [...],
 *         ],
 *         [
 *             'label' => 'Dropdown',
 *             'items' => [
 *                  ['label' => 'Level 1 - Dropdown A', 'url' => '#'],
 *                  '<li class="divider"></li>',
 *                  '<li class="dropdown-header">Dropdown Header</li>',
 *                  ['label' => 'Level 1 - Dropdown B', 'url' => '#'],
 *             ],
 *         ],
 *         [
 *             'label' => 'Login',
 *             'url' => ['site/login'],
 *             'visible' => Yii::$app->user->isGuest
 *         ],
 *     ],
 *     'options' => ['class' =>'nav-pills'],
 * ]);
 * ```
 *
 * For add badge:
 * SideBar::setBadge($itemName, $value);
 *
 * Note: Multilevel dropdowns beyond Level 1 are not supported in Bootstrap 3.
 *
 * @see http://getbootstrap.com/components/#dropdowns
 * @see http://getbootstrap.com/components/#nav
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @since 2.0
 */
class SideBar extends Widget
{
    /**
     * @var array list of items in the nav widget. Each array element represents a single
     * menu item which can be either a string or an array with the following structure:
     *
     * - label: string, required, the nav item label.
     * - url: optional, the item's URL. Defaults to "#".
     * - visible: bool, optional, whether this menu item is visible. Defaults to true.
     * - linkOptions: array, optional, the HTML attributes of the item's link.
     * - options: array, optional, the HTML attributes of the item container (LI).
     * - active: bool, optional, whether the item should be on active state or not.
     * - dropDownOptions: array, optional, the HTML options that will passed to the [[Dropdown]] widget.
     * - items: array|string, optional, the configuration array for creating a [[Dropdown]] widget,
     *   or a string representing the dropdown menu. Note that Bootstrap does not support sub-dropdown menus.
     * - encode: bool, optional, whether the label will be HTML-encoded. If set, supersedes the $encodeLabels option for only this item.
     *
     * If a menu item is a string, it will be rendered directly without HTML encoding.
     */
    public $items = [];
    /**
     * @var bool whether the nav items labels should be HTML-encoded.
     */
    public $encodeLabels = true;
    /**
     * @var bool whether to automatically activate items according to whether their route setting
     * matches the currently requested route.
     * @see isItemActive
     */
    public $activateItems = true;

    /**
     * @var string the route used to determine if a menu item is active or not.
     * If not set, it will use the route of the current request.
     * @see params
     * @see isItemActive
     */
    public $route;
    /**
     * @var array the parameters used to determine if a menu item is active or not.
     * If not set, it will use `$_GET`.
     * @see route
     * @see isItemActive
     */
    public $params;
    /**
     * @var string this property allows you to customize the HTML which is used to generate the drop down caret symbol,
     * which is displayed next to the button text to indicate the drop down functionality.
     * Defaults to `null` which means `<span class="caret"></span>` will be used. To disable the caret, set this property to be an empty string.
     */
    public $dropDownCaret;

    /**
     * @var array main options
     */
    public $options;

    /**
     * @var array options of sidebar item list element
     */
    public $listOptions;

    /**
     * @var string this parameter set sidebar header element
     */
    public $header = '';

    const BADGE_LIST_NAME = 'sidebar-badges';

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();
        if ($this->route === null && Yii::$app->controller !== null) {
            $this->route = Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = Yii::$app->request->getQueryParams();
        }
        if ($this->dropDownCaret === null) {
            $this->dropDownCaret = '<span class="fa arrow"></span>';
        }
        $this->initOptions();
    }

    /**
     * Renders the widget.
     * @return string
     * @throws InvalidConfigException
     */
    public function run()
    {
        return $this->renderNav();
    }

    /**
     * Render main nav
     * @return string
     * @throws InvalidConfigException
     */
    public function renderNav()
    {
        $itemList = $this->renderItems();
        $collapseBlock = Html::tag('div', $itemList, ['class' => 'sidebar-collapse']);
        return Html::tag('nav', $collapseBlock, $this->options);
    }

    /**
     * Renders widget items.
     * @return string
     * @throws InvalidConfigException
     */
    public function renderItems()
    {
        $items = [];
        $items[] = $this->header;
        foreach ($this->items as $itemName => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                continue;
            }
            $items[] = $this->renderItem($item, $itemName);
        }
        Html::addCssClass($this->listOptions, ['nav metismenu']);
        $this->listOptions = ArrayHelper::merge($this->listOptions, ['id' => 'side-menu']);
        return Html::tag('ul', implode("\n", $items), $this->listOptions);
    }

    /**
     * Renders a widget's item.
     * @param string|array $item the item to render.
     * @return string the rendering result.
     * @throws InvalidConfigException
     */
    public function renderItem($item, $itemName)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $label = Html::tag('span', $label, ['class' => 'nav-label']);
        $options = ArrayHelper::getValue($item, 'options', []);
        $iconClass = ArrayHelper::getValue($item, 'iconClass', '');
        $icon = $iconClass != '' ? Html::tag('i', '', ['class' => $iconClass]) : '';
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
        $badge = $this->renderBadge(ArrayHelper::getValue($item, 'badge', []), $itemName);

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if (empty($items)) {
            $items = '';
        } else {
            if ($this->dropDownCaret !== '' && $badge == '') {
                $label .= ' ' . $this->dropDownCaret;
            }
            if (is_array($items)) {
                $items = $this->isChildActive($items, $active);
                $items = $this->renderSecondLevelItems($items);
            }
        }

        if ($active) {
            Html::addCssClass($options, 'active');
        }

        return Html::tag('li', Html::a($icon . $label . $badge, $url, $linkOptions) . $items, $options);
    }

    /**
     * Render badge
     * @param $badgeParams
     * @return string
     * @throws InvalidConfigException
     */
    protected function renderBadge($badgeParams, $itemName = null)
    {
        if ($this->getBudgeValue($itemName) !== null) {
            $badgeParams = $this->getBudgeValue($itemName);
        }
        if (empty($badgeParams)) {
            return '';
        }
        if (!is_array($badgeParams)) {
            $label = $badgeParams;
            return Html::tag('span', $label, ['class' => 'label pull-right']);
        }
        if (!isset($badgeParams['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $label = $this->encodeLabels ? Html::encode($badgeParams['label']) : $badgeParams['label'];
        $options = ArrayHelper::getValue($badgeParams, 'options', []);
        Html::addCssClass($options, ['label', 'pull-right']);
        return Html::tag('span', $label, $options);
    }

    /**
     * @param $itemName
     * @return null
     */
    protected function getBudgeValue($itemName)
    {
        if ($itemName == null) {
            return null;
        }
        if (!isset(Yii::$app->params[self::BADGE_LIST_NAME])) {
            return null;
        }
        if (isset(Yii::$app->params[self::BADGE_LIST_NAME][$itemName])) {
            return Yii::$app->params[self::BADGE_LIST_NAME][$itemName];
        }
        return null;
    }

    /**
     * @param $itemName
     * @param $value
     */
    public static function setBadge($itemName, $value)
    {
        if (!isset(Yii::$app->params[self::BADGE_LIST_NAME])) {
            Yii::$app->params[self::BADGE_LIST_NAME] = [];
        }
        Yii::$app->params[self::BADGE_LIST_NAME][$itemName] = $value;
    }

    /**
     * Render sub menu
     * @param $secondLevelItems
     * @return string
     * @throws InvalidConfigException
     */
    protected function renderSecondLevelItems($secondLevelItems)
    {
        $items = [];
        foreach ($secondLevelItems as $item) {
            $items[] = $this->renderSecondLevelItem($item);
        }
        return Html::tag('ul', implode("\n", $items), ['class' => 'nav nav-second-level collapse']);
    }

    /**
     * Render sub menu item
     * @param $item
     * @return string
     * @throws InvalidConfigException
     */
    protected function renderSecondLevelItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        return Html::tag('li', Html::a($label, $url, $linkOptions), $options);
    }

    /**
     * Check to see if a child item is active optionally activating the parent.
     * @param array $items @see items
     * @param bool $active should the parent be active too
     * @return array @see items
     */
    protected function isChildActive($items, &$active)
    {
        foreach ($items as $i => $child) {
            if (is_array($child) && !ArrayHelper::getValue($child, 'visible', true)) {
                continue;
            }
            if (ArrayHelper::remove($items[$i], 'active', false) || $this->isItemActive($child)) {
                Html::addCssClass($items[$i]['options'], 'active');
                $active = true;
            }
        }
        return $items;
    }

    /**
     * Checks whether a menu item is active.
     * This is done by checking if [[route]] and [[params]] match that specified in the `url` option of the menu item.
     * When the `url` option of a menu item is specified in terms of an array, its first element is treated
     * as the route for the item and the rest of the elements are the associated parameters.
     * Only when its route and parameters match [[route]] and [[params]], respectively, will a menu item
     * be considered active.
     * @param array $item the menu item to be checked
     * @return bool whether the menu item is active
     */
    protected function isItemActive($item)
    {
        if (!$this->activateItems) {
            return false;
        }
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $route = $item['url'][0];
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }
            $route = ltrim($route, '/');
            if ($route[0] === '#') {
                return false;
            }
            $route = explode('/', $route);
            if ($route[0] . '/' . $route[1] !== Yii::$app->controller->id) {
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1) {
                $params = $item['url'];
                unset($params[0]);
                foreach ($params as $name => $value) {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Set default options
     */
    protected function initOptions()
    {
        Html::addCssClass($this->options, ['navbar-default navbar-static-side']);
    }
}