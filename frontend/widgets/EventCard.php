<?php

namespace frontend\widgets;

use common\models\DocEvent;
use Yii;
use yii\base\Widget;

/**
 * Widget for render event card
 */
class EventCard extends Widget
{
    const OBLIGATION_REQUIRED = 'required';
    const OBLIGATION_OPTIONAL = 'optional';

    /**
     * Type of event
     */
    public $obligation;

    public $studentFirstName;
    public $studentLastName;

    public $formsToSign;
    public $productsToPurchase;
    public $required;

    /**
     * Price quoted in the event
     */
    public $price = null;

    /**
     * Period of the event
     */
    public $period = null;

    /**
     * Event date
     */
    public $date = null;

    /**
     * Due date
     */
    public $dueDate = null;

    /**
     * Event name
     */
    public $title;

    /**
     * Address of the event image
     */
    public $previewImageSrc;

    /**
     * Url for title link
     */
    public $url = '#';

    /**
     * Link on parent event
     */
    public $eventLink = null;

    public $eventType;

    /**
     * Link to confirm participation in the event or for payment
     */
    public $acceptLink;

    /**
     * Render the widget
     */
    public function run()
    {
        $attendCaption = 'Buy';

        switch ($this->eventType){
            case DocEvent::EVENT_TYPE_FIELD_TRIP:
                $attendCaption = 'Attend';
                break;
            case DocEvent::EVENT_TYPE_FILL_OUT_FORM:
                $attendCaption = 'Sign';
                break;
            case DocEvent::EVENT_TYPE_STOCK_THE_CLASSROOM:
                $attendCaption = 'Donate';
                break;
        }


        return $this->render('event-card', [
            'obligation' => $this->obligation,
            'price' => $this->price,
            'period' => $this->period,
            'date' => $this->date,
            'dueDate' => $this->dueDate,
            'title' => $this->title,
            'previewImageSrc' => $this->previewImageSrc ?? Yii::$app->params['eventPhotoPlaceholder'],
            'url' => $this->url,
            'eventLink' => $this->eventLink,
            'acceptLink' => $this->acceptLink,
            'studentFirstName' => $this->studentFirstName,
            'studentLastName' => $this->studentLastName,
            'formsToSign' => $this->formsToSign,
            'productsToPurchase' => $this->productsToPurchase,
            'required' => $this->required,
            'eventType' => $this->eventType,
            'attendCaption' => $attendCaption,
        ]);
    }
}