<?php

namespace frontend\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Renders HTML component for ibox block
 *
 * For example:
 * <?php IBox::begin([
 *      'title' => 'Heading',
 *      'encodeContent' => false,
 *      'settingItems' => [
 *          [
 *              'label' => 'Config option 1',
 *              'url' => '#'
 *          ],
 *          [
 *              'label' => 'Config option 2',
 *              'url' => ['path/to']
 *          ],
 *          [
 *              'label' => 'Config option 3'
 *          ],
 *      ],
 * ]);
 * ?>
 *
 * Your content
 *
 * <?php
 * IBox::end();
 * ?>
 *
 */
class IBox extends Widget
{
    /**
     * Title for ibox block
     */
    public $title = '';

    /**
     * Specifies whether to encode content
     */
    public $encodeContent = false;

    /**
     * Array with items from the settings menu
     */
    public $settingItems = [];

    /**
     * Init the widget
     */
    public function init()
    {
        parent::init();
        ob_start();
    }

    /**
     * Render the widget
     */
    public function run()
    {
        if ($this->title === '') {
            throw new InvalidConfigException();
        }
        $content = ob_get_clean();
        $content = $this->encodeContent ? Html::encode($content) : $content;
        return $this->render('ibox', [
            'title' => $this->title,
            'settingItems' => $this->settingItems,
            'content' => $content,
        ]);
    }
}