<?php
/**
 * @var $this \yii\web\View
 * @var $content
 * @var $formId
 */

?>
<div id="<?= $formId ?>">
    <?= $content ?>
</div>

