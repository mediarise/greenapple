<?php

/**
 * @var $this \yii\web\View
 * @var array $errorMessages
 * @var array $successMessages
 */

foreach ($errorMessages as $errorMessage) {
    $this->registerJs("toastr.error('$errorMessage');", yii\web\View::POS_READY);
}
foreach ($successMessages as $successMessage) {
    $this->registerJs("toastr.success('$successMessage');", yii\web\View::POS_READY);
}

foreach ($successMessages as $successMessage) {
    $this->registerJs("toastr.success('$successMessage');", yii\web\View::POS_READY);
}