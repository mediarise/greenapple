<?php
/**
 * @var $this \yii\web\View
 * @var string $placeholderSrc
 * @var string|null $imageSrc
 * @var string $uploadUrl
 * @var array $containerOptions
 * @var string $modalTitle
 * @var $croppedImageInputId
 */

use common\extensions\bootstrap\ButtonLink;
use common\extensions\fileUpload\FileUpload;
use yii\bootstrap\Modal;
use yii\helpers\Html;

?>

<div <?= Html::renderTagAttributes($containerOptions)?>>

    <div class="row">
        <div class="product-main-photo-container mb">
            <?= Html::img($imageSrc ?: $placeholderSrc, ['id' => 'main-photo']) ?>
        </div>
    </div>

    <div class="row">
        <div class="mb px-0 pt-05">

            <?= FileUpload::widget([
                'enabledCsrfValidation' => false,
                'name' => 'BlobImageUploadForm',
                'attribute' => 'productPhoto',
                'url' => $uploadUrl,
                'options' => [
                    'accept' => 'image/*',
                    'class' => 'full-height',
                ],
                'label' => 'Upload Photo',
                'icon' => false,
                'cssClass' => 'btn btn-xl btn-success mr-125',
                'clientOptions' => [
                    'maxFileSize' => Yii::$app->params['maxUploadFileSize'],
                ],
                'clientEvents' => [
                    'fileuploadsubmit' => "function(e, data) {
                      toastr.clear();
                      window.fileButton = $('.fileinput-button').ladda();
                      window.fileButton.ladda('start').attr('disabled', true);
                    }",
                    'fileuploaddone' => "function(e, data) {
                        toastr.clear();
                        window.fileButton.ladda('stop');
                        var photo = new Image();
                        photo.src = data.result.url;
                        photo.onload = function(cropOptions) {
                            $('#main-photo, #main-photo-cropper').attr('src', photo.src);
                            $('#main-photo-url-input').val(data.result.url);
                        };
                    }",
                    'fileuploadfail' => "function(e, data) {
                        window.fileButton.ladda('stop');
                        toastr.error('File uploading failed.');
                    }",
                ],
            ]); ?>

            <?= ButtonLink::widget([
                'label' => 'Crop Photo',
                'icon' => false,
                'options' => [
                    'id' => 'btn-cropper-modal',
                    'class' => 'btn btn-xl btn-primary mr-2'
                ]
            ]); ?>
            <?= Html::input('hidden', 'cropped-image', '', ['id' => $croppedImageInputId])?>
        </div>
    </div>
</div>

<?php
    $this->registerJs("
        var \$btnCropperModal = $('#btn-cropper-modal');
        \$btnCropperModal.click(function(e) {
            e.preventDefault();
            if ($('#main-photo').attr('src') == '$placeholderSrc') {
                toastr.error('Please, upload photo before cropping.');
                return true;
            }
            $('#cropper-modal').modal('show');
        });
    ", yii\web\View::POS_READY);
?>


<?php Modal::begin([
    'header' => "<h4 class=\"modal-title\">{$modalTitle}</h4>",
    'options' => ['id' => 'cropper-modal'],
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false
    ],
    'footer' => '
        <div class="modal-actions text-left">
            <button type="button" id="save-crop-btn" class="btn btn-xl btn-success ml-175" disabled>
                <i class="fa fa-check mr-025"></i> Save
            </button>
            <button type="button" class="btn btn-xl btn-primary" data-dismiss="modal">Cancel</button>
        </div>  
    '
]); ?>

    <div class="row">
        <div class="col-sm-12">

            <div class="row">
                <div class="col-md-12">
                    <div class="mb-2">
                        <?= Html::img($imageSrc, ['id' => 'main-photo-cropper', 'style' => 'width: 100%;']) ?>
                    </div>
                </div>
            </div>

            <div class="row px-0 docs-buttons">
                <div class="col-md-12">

                    <button id="crop-btn" type="button" class="ladda-button btn btn-xl btn-success mr" title="Crop" data-style="zoom-out">
                        <i class="fa fa-scissors"></i> Crop
                    </button>

                    <div class="btn-group mr">
                        <button type="button" class="btn btn-xl btn-primary" data-action="zoom" data-option="0.1" title="Zoom In">
                        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, 0.1)">
                          <span class="fa fa-search-plus"></span>
                        </span>
                        </button>
                        <button type="button" class="btn btn-xl btn-primary" data-action="zoom" data-option="-0.1" title="Zoom Out">
                        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, -0.1)">
                          <span class="fa fa-search-minus"></span>
                        </span>
                        </button>
                        <button type="button" class="btn btn-xl btn-primary" data-action="rotate" data-option="-45" title="Rotate Left">
                        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, -45)">
                          <span class="fa fa-rotate-left"></span>
                        </span>
                        </button>
                        <button type="button" class="btn btn-xl btn-primary" data-action="rotate" data-option="45" title="Rotate Right">
                        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, 45)">
                          <span class="fa fa-rotate-right"></span>
                        </span>
                        </button>
                    </div>
                </div>
            </div><!--row-->

        </div>
    </div>

<?php Modal::end(); ?>

<?php
    $this->registerJsVar('uploadPhotoUrl', $uploadUrl);
?>

<?php $this->registerJs(/** @lang JavaScript */
    <<<'JS'
    
$(function () {

  'use strict';

  var console = window.console || { log: function () {} };
  var URL = window.URL || window.webkitURL;
  var $preview = $('#main-photo');
  var $image = $('#main-photo-cropper');
  var $cropperModal = $('#cropper-modal');
  var $dataX = $('#dataX');
  var $dataY = $('#dataY');
  var $dataHeight = $('#dataHeight');
  var $dataWidth = $('#dataWidth');
  var $dataRotate = $('#dataRotate');
  var $dataScaleX = $('#dataScaleX');
  var $dataScaleY = $('#dataScaleY');
  var options = {
    viewMode: 3,
    aspectRatio: 295 / 295,
    minContainerWidth: 295,
    minContainerHeight: 0,
    crop: function (e) {
      $dataX.val(Math.round(e.detail.x));
      $dataY.val(Math.round(e.detail.y));
      $dataHeight.val(Math.round(e.detail.height));
      $dataWidth.val(Math.round(e.detail.width));
      $dataRotate.val(e.detail.rotate);
      $dataScaleX.val(e.detail.scaleX);
      $dataScaleY.val(e.detail.scaleY);
    }
  };

  window.getCropperOptions = function () {
    return options
  };

  var originalImageURL = $image.attr('src');
  var uploadedImageName = 'cropped.jpg';
  var uploadedImageType = 'image/jpeg';
  var uploadedImageURL;


  // Methods
  $('.docs-buttons').on('click', '[data-action]', function () {
    var $this = $(this);
    var data = $this.data();
    var cropper = $image.data('cropper');
    var cropped;
    var $target;
    var result;

    if ($this.prop('disabled') || $this.hasClass('disabled')) {
      return;
    }

    if (cropper && data.action) {
      data = $.extend({}, data); // Clone a new one

      if (typeof data.target !== 'undefined') {
        $target = $(data.target);

        if (typeof data.option === 'undefined') {
          try {
            data.option = JSON.parse($target.val());
          } catch (e) {
            console.log(e.message);
          }
        }
      }

      cropped = cropper.cropped;

      switch (data.action) {
        case 'rotate':
          if (cropped && options.viewMode > 0) {
            $image.cropper('clear');
          }

          break;

        case 'getCroppedCanvas':
          if (uploadedImageType === 'image/jpeg') {
            if (!data.option) {
              data.option = {};
            }

            data.option.fillColor = '#fff';
          }

          break;
      }

      result = $image.cropper(data.action, data.option, data.secondOption);

      switch (data.action) {
        case 'rotate':
          if (cropped && options.viewMode > 0) {
            $image.cropper('crop');
          }

          break;

        case 'scaleX':
        case 'scaleY':
          $(this).data('option', -data.option);
          break;

        case 'getCroppedCanvas':
          if (result) {
            // Bootstrap's Modal
            $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

            if (!$download.hasClass('disabled')) {
              download.download = uploadedImageName;
              $download.attr('href', result.toDataURL(uploadedImageType));
            }
          }

          break;

        case 'destroy':
          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
            uploadedImageURL = '';
            $image.attr('src', originalImageURL);
          }

          break;
      }

      if ($.isPlainObject(result) && $target) {
        try {
          $target.val(JSON.stringify(result));
        } catch (e) {
          console.log(e.message);
        }
      }

    }
  });


  // Import image
  var $inputImage = $('#inputImage');

  if (URL) {
    $inputImage.change(function () {
      var files = this.files;
      var file;

      if (!$image.data('cropper')) {
        return;
      }

      if (files && files.length) {
        file = files[0];

        if (/^image\/\w+$/.test(file.type)) {
          uploadedImageName = file.name;
          uploadedImageType = file.type;

          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
          }

          uploadedImageURL = URL.createObjectURL(file);
          $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
          $inputImage.val('');
        } else {
          window.alert('Please choose an image file.');
        }
      }
    });
  } else {
    $inputImage.prop('disabled', true).parent().addClass('disabled');
  }

  var $saveCropBtn = $('#save-crop-btn');
  var $btnCropperModal = $('#btn-cropper-modal');
  $btnCropperModal.click(function() {
    $saveCropBtn.attr('disabled', true);
  });

  var $cropBtn = $('#crop-btn').ladda();
  $cropBtn.click(function() {
    $cropBtn.ladda('start');
    $image.cropper('getCroppedCanvas').toBlob(function (blob) {

      var formData = new FormData();
      formData.append('BlobImageUploadForm', blob);

      $.ajax(uploadPhotoUrl, {
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          $cropBtn.ladda('stop');
          $saveCropBtn.attr('disabled', false);
          $('#main-photo').val(data.url);
          var photo = new Image();
          photo.src = data.url;
          photo.onload = function() {
            $image.cropper('destroy').attr('src', photo.src).cropper(options);
          };
        },
        error: function () {
          $cropBtn.ladda('stop');
          toastr.error('File uploading failed.');
        }
      });
    });
  });

  $saveCropBtn.click(function() {
    var croppedImageUrl = $image.attr('src');
    $preview.attr('src', croppedImageUrl);
    $('#main-photo-url-input').val(croppedImageUrl);
    $cropperModal.modal('hide');
  });

  $cropperModal.on('show.bs.modal', function(event) {
    var photo = new Image();
    photo.src = $preview.attr('src');
    options.minContainerHeight = photo.height / (photo.width / 380);
    $image.cropper(options);
  }).on('hidden.bs.modal', function() {
    $image.cropper('destroy');
  });

});


JS
); ?>
