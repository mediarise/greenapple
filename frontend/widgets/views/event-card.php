<?php

/**
 * @var $this \yii\web\View
 * @var $eventType
 * @var $obligation
 * @var $studentFirstName
 * @var $studentLastName
 * @var $price
 * @var $period
 * @var $date
 * @var $dueDate
 * @var $title
 * @var $previewImageSrc
 * @var $url
 * @var $formsToSign
 * @var $productsToPurchase
 * @var $required
 * @var $eventLink
 * @var $acceptLink
 * @var $attendCaption
 */

use common\models\guardian\DocEvent;
use frontend\widgets\EventCard;
use yii\helpers\Html;

?>

<div class="ibox">
    <div class="ibox-content product-box">

        <div class="event-pending-section">

            <div class="due-date">
                <?= $dueDate ? 'Due ' . (new DateTime($dueDate))->format('M d') : 'No due date' ?>
            </div>

            <div class="required-badge-area">
                <?php if ($required): ?>
                    <div class="required-badge">Required</div>
                <?php endif; ?>
            </div>

            <div class="card-body">
                <div class="row px-0">

                    <div class="col-lg-7 pr">

                        <div class="student-name">
                            <?= $studentFirstName ?>
                        </div>

                        <div class="event-type">
                            <?= $title ?>
                        </div>

                        <div class="event-date">
                            <?= $date ?>
                        </div>

                        <div class="event-info">
                            Forms: <?= $formsToSign ?><br>
                            Products: <?= $productsToPurchase ?>
                        </div>

                    </div>
                    <div class="col-lg-5">
                        <div class="text-center">

                            <div class="event-photo">
                                <img class="img-thumbnail" src="<?= $previewImageSrc ?>" style="max-width: 100%">
                            </div>

                            <?= Html::a($attendCaption, $acceptLink, ['class' => 'btn btn-xl btn-success mt-2', 'style' => 'width: 100%']) ?>

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>


