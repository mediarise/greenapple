<?php

/* @var $this \yii\web\View */

?>

<li class="nav-header">
    <div class="dropdown profile-element">
        <span><img alt="image" src="/images/logo_v2.png" style="width: 100%"/></span>
    </div>
</li>

<?php if (isset($this->blocks['student-selector'])): ?>
    <li>
        <?= $this->blocks['student-selector']; ?>
    </li>
<?php endif; ?>

<li style="padding-top: 40px;">

</li>
