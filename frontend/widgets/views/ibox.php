<?php

/**
 * @var $this \yii\web\View
 * @var string $title
 * @var array $settingItems
 * @var $content
 */

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<div class="ibox">
    <div class="ibox-title">
        <h5><?= Html::encode($title) ?></h5>

        <div class="ibox-tools">
            <?php if (!empty($settingItems)) : ?>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <?php
                    foreach ($settingItems as $settingItem) :
                        if (!isset($settingItem['label'])) {
                            throw new InvalidConfigException();
                        }
                        if (is_array($settingItem)) {
                            $label = Html::encode($settingItem['label']);
                            $url = ArrayHelper::getValue($settingItem, 'url', '#');
                            $options = ArrayHelper::getValue($settingItem, 'options', []);
                            $itemContent = Html::a($label, $url, $options);
                        } else {
                            $itemContent = $settingItem;
                        }
                        ?>
                        <li><?= $itemContent ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox-content">
        <?= $content ?>
    </div>
</div>
