<?php
namespace frontend\widgets;

use frontend\assets\InspiniaAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\InputWidget;
use function Stringy\create as s;

class DualListBox extends InputWidget
{
 /**
     * @var array listbox items
     */
    public $items = [];

    /**
     * @var string|array selected items
     */
    public $selection;

    /**
     * @var array listbox options
     */
    public $options = [];

    /**
     * @var array dual listbox options
     */
    public $clientOptions = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();

        Html::addCssClass($this->options, 'form-control');
        $this->options['multiple'] = true;

        if ($this->hasModel()) {
            return Html::activeListBox($this->model, $this->attribute, $this->items, $this->options);
        }

        return Html::listBox($this->name, $this->selection, $this->items, $this->options);
    }

    /**
     * Registers the required JavaScript.
     */
    public function registerClientScript()
    {
        $view = $this->getView();
        $view->registerCssFile('css/plugins/dualListbox/bootstrap-duallistbox.min.css', ['depends' => InspiniaAsset::class]);
        $view->registerJsFile('js/plugins/dualListbox/jquery.bootstrap-duallistbox.js', ['depends' => InspiniaAsset::class]);
        $id = (array_key_exists('id', $this->options)) ? $this->options['id'] : Html::getInputId($this->model, $this->attribute);
        $options = empty($this->clientOptions) ? '' : Json::encode($this->clientOptions);

        $dualListBox = (string) s($id)->camelize();
        $view->registerJs("var $dualListBox = jQuery('#$id').bootstrapDualListbox($options);", View::POS_END);
    }
}