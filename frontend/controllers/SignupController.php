<?php

namespace frontend\controllers;

use common\components\AjaxResponse;
use common\components\bl\SignupRequestManager;
use common\components\ModelHelper;
use common\models\Board;
use common\models\School;
use common\models\Grade;
use common\models\User;
use frontend\base\EulaNotAcceptedException;
use frontend\base\UserAlreadyRegisteredException;
use common\models\EulaForm;
use common\models\InviteForm;
use common\models\GuardianAddStudentTemplateForm;
use common\models\GuardianSignupForm;
use common\models\SignupRequestGuardian;
use common\models\SignupRequestStudent;
use Yii;
use yii\base\InvalidArgumentException;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

class SignupController extends Controller
{
    public $layout = 'login';

    /**
     * @return mixed
     */
    private function getStepVars()
    {
        $result = Yii::$app->session['signup'];
        if (!is_array($result)) {
            $result = [];
        }


        if (YII_ENV_DEV && !array_key_exists('guardianId', $result)) {
            $guardianId = Yii::$app->request->get('guardianId');
            if ($guardianId) {
                $result['guardianId'] = $guardianId;
            }
        }

        if (!array_key_exists('guardianId', $result)) {
            return $this->redirect(['signup/index']);
        }

        return $result;
    }

    /**
     * @return string|Response
     */
    public function actionIndex()
    {
        $model = new GuardianSignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->create()) {
                Yii::$app->session['signup'] = [
                    'guardianId' => $model->id
                ];
                return $this->redirect(['signup/add-child']);
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|Response
     * @throws \yii\db\Exception
     */
    public function actionAddChild()
    {
        $stepVars = $this->getStepVars();
        if (is_object($stepVars)) {
            return $stepVars;
        }

        $guardianId = $stepVars['guardianId'];

        $model = new GuardianAddStudentTemplateForm();

        //if not ajax return view
        if (!Yii::$app->request->isAjax) {
            $boards = Board::find()->all();
            return $this->render('add-child', [
                'model' => $model,
                'boards' => ArrayHelper::map($boards, 'id', 'name'),
                'childrenDataProvider' => new ActiveDataProvider([
                    'query' => SignupRequestGuardian::getSignupRequestStudentTemplateById($guardianId),
                    'sort' => false,
                ]),
            ]);
        }

        //else add child and return result

        $model->signup_request_id = $guardianId;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson(AjaxResponse::success([
                'row' => $this->renderPartial('_children-grid', [
                    'isAjax' => true,
                    'childrenDataProvider' => new ActiveDataProvider([
                        'query' => SignupRequestGuardian::getSignupRequestStudentTemplateById($guardianId)
                            ->andWhere(['signup_request_student.id' => $model->id])
                    ])
                ])
            ]));
        }
        return $this->asJson(AjaxResponse::validationError(ModelHelper::errorsForForm($model)));
    }

    public function actionRequestInvite()
    {
        $stepVars = $this->getStepVars();
        if (is_object($stepVars)) {
            return $stepVars;
        }

        $signupRequest = SignupRequestGuardian::findOne($stepVars['guardianId']);
        $signupRequest->status = SignupRequestGuardian::STATUS_NEW;
        $signupRequest->save();

        return $this->redirect(['signup/finish']);
    }

    public function actionFinish()
    {
        $stepVars = $this->getStepVars();
        if (is_object($stepVars)) {
            return $stepVars;
        }

        unset(Yii::$app->session['signup']);

        return $this->render('finish');
    }

    /**
     * @param $id
     * @return string|Response
     * @throws BadRequestHttpException
     * @throws \yii\db\Exception
     */
    public function actionEula($id)
    {
        $user = User::findOne((int)$id);
        if ($user && $user->status != User::STATUS_EULA_NOT_ACCEPTED) {
            throw new BadRequestHttpException('Wrong user id');
        }

        $model = new EulaForm();
        $model->guardianId = $id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->guardian->acceptEula();

            return $this->goHome();
        }
        return $this->render('eula', ['model' => $model]);
    }

    /**
     * @param $inviteToken
     * @return string|Response
     * @throws BadRequestHttpException
     * @throws \yii\base\Exception
     */
    public function actionInvite($inviteToken)
    {
        try {
            $model = new InviteForm($inviteToken);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        } catch (EulaNotAcceptedException $e) {
            return $this->redirect(['signup/eula', 'id' => $e->userId]);
        } catch (UserAlreadyRegisteredException $e) {
            return $this->redirect(['site/login', 'inviteToken' => $inviteToken]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->accept()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->redirect(['signup/eula', 'id' => $model->getUserId()]);
        }

        return $this->render('invite', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws BadRequestHttpException
     * @throws \yii\base\Exception
     * @throws \Throwable
     */
    public function actionTestInvite($id)
    {
        if (!YII_ENV_DEV) {
            throw new BadRequestHttpException();
        }

        $manager = new SignupRequestManager($id);

        $request = $manager->getSignupRequest();

        //$invite_token = $manager->approve($request->attributes, '');
        $invite_token = $request->invite();

        return $this->redirect(['signup/invite', 'inviteToken' => $invite_token]);
    }

    public function actionTestApprove($id)
    {
        if (!YII_ENV_DEV) {
            throw new BadRequestHttpException();
        }

        $manager = new SignupRequestManager($id);

        $result = [];

        foreach ($manager->getStudents() as $student) {
            $result[] = $manager->approveStudent($student->attributes, $student->id, '');
        }

        return $this->asJson($result);
    }

    public function actionGetSchools($boardId)
    {
        $schools = School::find()
            ->where(['board_id' => $boardId])
            ->select(['id', 'name'])
            ->asArray()
            ->all();

        return $this->asJson($schools);
    }

    public function actionGetGrades($schoolId)
    {
        $grades = Grade::find()
            ->where(['school_id' => $schoolId])
            ->andWhere(['year' => Grade::getCurrentAcademicYear()])
            ->select(['id', 'name'])
            ->asArray()
            ->all();

        return $this->asJson($grades);
    }

    public function actionRemoveChild($id)
    {
        $rowCount = SignupRequestStudent::deleteAll(['id' => $id]);
        return $this->asJson([
            'success' => $rowCount == 1,
        ]);
    }
}
