<?php

namespace frontend\controllers;

use frontend\models\GridViewTest;
use Yii;
use yii\web\Controller;

class GridviewTestController extends Controller
{
    public function actionTest()
    {
        $filterModel = new GridViewTest();
        $dataProvider = $filterModel->search(Yii::$app->request->getQueryParams());
        return $this->render('test', [
            'filterModel' => $filterModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}