<?php

namespace frontend\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;

class SampleController extends Controller
{

    public function actionCropper()
    {
        return $this->render('cropper');
    }

    public function actionNotification()
    {
        return $this->render('notification');
    }

    public function actionWtcywygWidget()
    {
        return $this->render('wtcywyg');
    }

    public function actionDashboard()
    {
        $dataProvider = $this->getUpcomingDataProvider();
        return $this->render('dashboard', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Autocomplete sample page
     *
     * @return string
     */
    public function actionSearchWidget()
    {
        return $this->render('search-widget');
    }

    /**
     * Page with json data for autocomplete input widget
     * Each item must contain an parameter 'value'
     *
     * @param string $q
     * @return array
     */
    public function actionGetSearchData($q = '')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            ['name' => "Russia", 'code' => '123', 'value' => 'Ru'],
            ['name' => "Ukraine", 'code' => '321', 'value' => 'Ukr'],
            ['name' => "Belorussia", 'code' => '123', 'value' => 'Bel'],
            ['name' => "Uzbekistan", 'code' => '234', 'value' => 'Uz'],
            ['name' => "Romania", 'code' => '234', 'value' => 'Rom']
        ];
    }

    /**
     * Upcoming sample page
     *
     * @return string
     */
    public function actionUpcoming()
    {
        $dataProvider = $this->getUpcomingDataProvider();
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('upcoming', [
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->render('upcoming', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Event card sample page
     */
    public function actionEvents()
    {
        return $this->render('events');
    }

    /**
     * Event view sample page
     */
    public function actionEventView()
    {
        return $this->render('event-view', [
            'workingdayDateList' => $this->getWorkdayDateList(),
        ]);
    }

    /**
     * Return data for upcoming table
     *
     * @return ArrayDataProvider
     */
    private function getUpcomingDataProvider()
    {
        $upcomingData = [
            [
                'date' => 'Oct 1 - Oct 5',
                'title' => 'Milk',
                'student' => Html::img('/images/lisa.png', ['class' => 'img-circle', 'width' => '30px']) . ' Lisa Simpson',
                'forms' => Html::a('n/a', '#', ['class' => 'btn btn-xs btn-w-m btn-default']),
                'products' => Html::a('5/5', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
                'status' => Html::a('Planned', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
            ], [
                'date' => 'Oct 3',
                'title' => 'Pizza Day',
                'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
                'forms' => Html::a('n/a', '#', ['class' => 'btn btn-xs btn-w-m btn-default']),
                'products' => Html::a('1/1', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
                'status' => Html::a('Planned', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
            ], [
                'date' => 'Oct 8 - Oct 12',
                'title' => 'Milk',
                'student' => Html::img('/images/lisa.png', ['class' => 'img-circle', 'width' => '30px']) . ' Lisa Simpson',
                'forms' => Html::a('n/a', '#', ['class' => 'btn btn-xs btn-w-m btn-default']),
                'products' => Html::a('5/5', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
                'status' => Html::a('Planned', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
            ], [
                'date' => 'Oct 10',
                'title' => 'Pizza Day',
                'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
                'forms' => Html::a('n/a', '#', ['class' => 'btn btn-xs btn-w-m btn-default']),
                'products' => Html::a('1/1', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
                'status' => Html::a('Planned', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
            ], [
                'date' => 'Oct 11, 4:30 PM',
                'title' => 'Parent-teacher behaviour meeting',
                'student' => Html::img('/images/lisa.png', ['class' => 'img-circle', 'width' => '30px']) . ' Homer Simpson for Bart Simpson',
                'forms' => Html::a('n/a', '#', ['class' => 'btn btn-xs btn-w-m btn-default']),
                'products' => Html::a('n/a', '#', ['class' => 'btn btn-xs btn-w-m btn-default']),
                'status' => Html::a('Accepted', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
            ], [
                'date' => 'Oct 17',
                'title' => 'Pizza Day',
                'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
                'forms' => Html::a('n/a', '#', ['class' => 'btn btn-xs btn-w-m btn-default']),
                'products' => Html::a('1/1', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
                'status' => Html::a('Planned', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
            ], [
                'date' => 'Oct 23',
                'title' => 'AGO Visit',
                'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
                'forms' => Html::a('0/1', '#', ['class' => 'btn btn-xs btn-w-m btn-danger']),
                'products' => Html::a('0/1', '#', ['class' => 'btn btn-xs btn-w-m btn-danger']),
                'status' => Html::a('Pending', '#', ['class' => 'btn btn-xs btn-w-m btn-danger']),
            ], [
                'date' => 'Oct 31, 12:00 AM',
                'title' => 'Stock the classroom',
                'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
                'forms' => Html::a('n/a', '#', ['class' => 'btn btn-xs btn-w-m btn-default']),
                'products' => Html::a('18/30', '#', ['class' => 'btn btn-xs btn-w-m btn-danger']),
                'status' => Html::a('Active', '#', ['class' => 'btn btn-xs btn-w-m btn-success']),
            ],
        ];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $upcomingData,
            'sort' => [
                'attributes' => [
                    'date',
                    'title',
                    'student',
                    'forms',
                    'products',
                    'status',
                ]
            ],
        ]);
        return $dataProvider;
    }

    private function getWorkdayDateList()
    {
        return [
            'Oct 1- Oct 5',
            'Oct 8- Oct 12',
            'Oct 15 - Oct 19',
            'Oct 22 - Oct 26',
            'Oct 29 - Nov 2',
            'Nov 5 - Nov 9',
            'Nov 12 - Nov 17',
        ];
    }
}