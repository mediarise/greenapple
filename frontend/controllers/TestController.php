<?php

namespace frontend\controllers;

use common\components\bl\SchoolHierarchy;
use common\components\bl\SignupRequestManager;
use common\models\SignupRequestGuardian;
use common\models\SignupRequestRejectionForm;
use common\models\SignupRequestStudent;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Board;

class TestController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        $manager = new SignupRequestManager($id);

        $rejectionForm = new SignupRequestRejectionForm();

        $studentsDataProvider = new ActiveDataProvider([
            'query' => SignupRequestGuardian::getSignupRequestStudentTemplateById($id),
            'sort' => false
        ]);

        return $this->render('@frontend/views/sample/signup-request', [
            'guardian' => $manager->getSignupRequest(),
            'studentsDataProvider' => $studentsDataProvider,
            'rejectionForm' => $rejectionForm,
        ]);
    }

    public function actionStudent($id)
    {
        $manager = new SignupRequestManager(0);

        $rejectionForm = new SignupRequestRejectionForm();

        $student = $manager->getStudent($id);
        $guardiansDataProvider = new ActiveDataProvider([
            'query' => SignupRequestGuardian::find()->where(['id' => $student->signup_request_id]),
            'sort' => false
        ]);

        return $this->render('@frontend/views/sample/student', [
            'student' => $student,
            'guardiansDataProvider' => $guardiansDataProvider,
            'rejectionForm' => $rejectionForm,
            'boards' => SchoolHierarchy::getBoards(),
        ]);
    }
}