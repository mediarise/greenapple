<?php
namespace frontend\base;

class EulaNotAcceptedException extends \Exception
{
    public $userId = 0;

    public function __construct($userId)
    {
        $this->userId = $userId;
        parent::__construct("", 0, null);
    }
}