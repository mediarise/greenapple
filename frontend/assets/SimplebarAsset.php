<?php
/**
 * Created by PhpStorm.
 * User: crunc
 * Date: 6/16/2018
 * Time: 1:30 PM
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class SimplebarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/simplebar/simplebar.css',
    ];
    public $js = [
        'js/plugins/simplebar/simplebar.js',
    ];
    public $depends = [
    ];
}