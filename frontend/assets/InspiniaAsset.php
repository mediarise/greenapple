<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use kartik\select2\Select2Asset;
use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class InspiniaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/animate.css',
        'css/plugins/toastr/toastr.min.css',
        'css/plugins/ladda/ladda-themeless.min.css',
        'css/plugins/sweetalert/sweetalert.css',
        'css/theme.css'
    ];
    public $js = [
        'js/plugins/lodash/lodash.min.js',
        'js/common.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/inspinia.js',
        //'js/plugins/pace/pace.min.js',
        'js/plugins/toastr/toastr.min.js',
        'js/plugins/ladda/spin.min.js',
        'js/plugins/ladda/ladda.min.js',
        'js/plugins/ladda/ladda.jquery.min.js',
        'js/plugins/sweetalert/sweetalert.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
