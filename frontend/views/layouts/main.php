<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\InspiniaAsset;
use frontend\widgets\SideBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use function Stringy\create as s;

InspiniaAsset::register($this);

$userRole = s(Yii::$app->user->getHighestRole())->toLowerCase();
$menuItemsFile = Yii::getAlias("@backend/views/{$userRole}/_menu-items.php");
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="wrapper">
    <?php

    $menuItems = [
        [
            'label' => 'Dashboard',
            'url' => ['/sample/dashboard'],
            'iconClass' => 'fa fa-th-large',
        ],
        [
            'label' => 'Messages',
            'url' => ['/'],
            'iconClass' => 'fa fa-envelope-open-o',
            'badge' => '2',
        ],
        [
            'label' => 'Account Settings',
            'url' => ['/'],
            'iconClass' => 'fa fa-wrench',
        ],
        [
            'label' => 'Wtcywyg widget',
            'url' => ['/sample/wtcywyg-widget'],
            'iconClass' => 'fa fa-envelope-open-o',
        ],
    ];


    ?>

    <?= SideBar::widget([
        'options' => [
            'role' => 'role-name'
        ],
        'header' => '<li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="/images/greenapple-avatar.png" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">GreenApple</strong>
                        </a>
                      
                    </div>
                    <div class="logo-element">
                         <img alt="image" class="img-circle" src="/images/greenapple-avatar.png" />
                    </div>
                </li>',
        'items' => isset($this->params['sideBarItems']) ? $this->params['sideBarItems'] : $menuItems,
    ]) ?>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top gray-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="#">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2><?= Html::encode($this->title) ?></h2>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="row wrapper wrapper-content animated fadeIn">
            <?= $content ?>
        </div>


        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company © 2014-2017
            </div>
        </div>

    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
