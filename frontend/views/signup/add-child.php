<?php

use common\components\bl\FamilyRelationship;
use common\components\bl\Gender;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\GuardianAddStudentTemplateForm */
/* @var $form ActiveForm */
/* @var $boards array */
/* @var \yii\data\ActiveDataProvider $childrenDataProvider */

$this->registerJsFile('js/add-child.js', ['depends' => 'yii\web\JqueryAsset']);

$selectFieldOptions = [
    'validateOnChange' => false,
    'validateOnBlur' => false,
];

$jsChangeCallbackFunctionDefinition = 'function(e) { document.SignupForm.loadOptions("%s", e.target.value, "%s") }';

?>
<div class="large-box signup-add-child request-wizard login-box pa-2 ibox-content">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'id' => 'add-child-form',
            'class' => 'wizard-big wizard clearfix',
        ]
    ]); ?>

    <?= $this->render('_master-header', ['currentStep' => 2]) ?>

    <div class="help-block"></div>
    <div class="row">
        <div class="col-sm-12 text-left">

            <p>Please enter information about your children</p>

            <div class="clearfix"></div>

            <?= $form->field($model, 'oen')->textInput(['placeholder' => 'oen'])->label(false) ?>

            <div class="row px-0">
                <div class="col-sm-6">
                    <?= $form->field($model, 'first_name')->textInput(['placeholder' => 'First Name'])->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Last Name'])->label(false) ?>
                </div>
            </div>

            <div class="row px-0">
                <div class="col-sm-6">
                    <?= $form->field($model, 'birthday')->widget(DatePicker::class, [
                        'clientOptions' => [
                            'autoclose' => true,
                            'maxViewMode' => 'years',
                            'startView' => 2,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'options' => [
                            'placeholder' => 'Select birth date'
                        ]
                    ])
                        ->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'gender')
                        ->widget(Select2::class, [
                            'data' => Gender::getNames(),
                            'options' => [
                                'placeholder' => 'Select a gender ...',
                            ],
                        ])
                        ->label(false) ?>
                </div>
            </div>

            <div class="row px-0">
                <div class="col-sm-6">
                    <?= $form->field($model, 'relationship')->widget(Select2::class, [
                        'data' => FamilyRelationship::getNames(),
                        'options' => [
                            'placeholder' => 'Select a relationship ...',
                        ],
                    ])->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form
                        ->field($model, 'boardId', $selectFieldOptions)
                        ->widget(Select2::class, [
                            'data' => $boards,
                            'options' => [
                                'placeholder' => 'Select a board ...',
                            ],
                            'pluginEvents' => [
                                'change' => sprintf(
                                    $jsChangeCallbackFunctionDefinition,
                                    Html::getInputId($model, 'schoolID'),
                                    Url::to(['signup/get-schools', 'boardId' => ''])
                                ),
                            ],
                        ])
                        ->label(false) ?>
                </div>
            </div>

            <div class="row px-0">
                <div class="col-sm-6">
                    <?= $form
                        ->field($model, 'schoolId', $selectFieldOptions)
                        ->widget(Select2::class, [
                            'options' => [
                                'placeholder' => 'Select a school ...',
                                'class' => ['dependent-select']
                            ],
                            'pluginEvents' => [
                                'change' => sprintf(
                                    $jsChangeCallbackFunctionDefinition,
                                    Html::getInputId($model, 'grade_id'),
                                    Url::to(['signup/get-grades', 'schoolId' => ''])
                                ),
                            ],
                        ])
                        ->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form
                        ->field($model, 'grade_id', $selectFieldOptions)
                        ->widget(Select2::class, [
                            'options' => [
                                'placeholder' => 'Select a class ...',
                                'class' => ['dependent-select']
                            ],
                        ])
                        ->label(false)
                    ?>
                </div>
            </div>


            <?php $this->registerJs('$(".dependent-select").select2("enable", false);') ?>
        </div>
    </div>
    <div class="col-md-12 px-0 pt">
        <div class="pull-right">
            <?= Html::button('Add child', ['class' => 'btn btn-primary btn-xl', 'id' => 'add-child-btn']) ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="grid-wrapper" class="col-md-12" style="display: none;">
        <p>Your children</p>
        <?= $this->render('_children-grid', [
            'childrenDataProvider' => $childrenDataProvider,
            'isAjax' => false,
        ]) ?>
        <div class="actions pull-right">
            <ul>
                <li>
                    <?= Html::a('Next >', Url::to(['signup/request-invite']), ['class' => 'btn btn-success btn-xl']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>

    <?php ActiveForm::end(); ?>

</div><!-- signup-add-child -->
