<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;

?>

<div class="request-wizard login-box pa-2">
    <div class="wizard-big wizard finish clearfix">

        <?= $this->render('_master-header', ['currentStep' => 4]) ?>

        <div class="content clearfix">
            <div class="help-block"></div>
            <div class="logo mb-4 text-center"><img src="/images/ga-v2.png" width="100%" style="max-width: 378px" alt=""></div>
            <div class="col-xs-12">
                <p class="thank-you">Thank you for your sign-up request. School Administaror will process your request and send you notification by email.</p>
            </div>

        </div>

    </div>
</div>
