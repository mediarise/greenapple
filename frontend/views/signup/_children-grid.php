<?php

use common\models\Guardian;
use common\widgets\AjaxGridView;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\helpers\Html;

/** @var $this \yii\web\View */
/** @var \yii\data\ActiveDataProvider $childrenDataProvider */
/** @var bool $isAjax */

$column = function ($columnName, $options = []) {
    return array_merge([
        'class' => DataColumn::class,
        'attribute' => $columnName,
        'enableSorting' => false
    ], $options);
};

/** @noinspection PhpUnhandledExceptionInspection */
echo AjaxGridView::widget([
    'id' => 'children-grid',
    'renderRowsOnly' => $isAjax,
    'dataProvider' => $childrenDataProvider,
    'layout' => '{items}',
    'emptyText' => false,
    'columns' => [
        [
            'class' => DataColumn::class,
            'attribute' => 'fullName',
            'label' => 'student'
        ],
        $column('grade.school.name', ['label' => 'School']),
        $column('grade.name', ['label' => 'grade']),
        [
            'class' => DataColumn::class,
            'attribute' => 'status',
            'label' => 'Actions',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a('Remove', ['remove-child', 'id' => $model->id]);
            }
        ]
    ]
]);