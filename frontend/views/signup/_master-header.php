<?php

/* @var $this \yii\web\View */

use frontend\widgets\VueWizard;

/* @var $currentStep integer */

$beginLi = function ($step) use ($currentStep) {
    if ($step === $currentStep) {
        return '<li class="current" role="tab" aria-disabled="false" aria-selected="true">';
    }
    return '<li class="disabled" role="tab" aria-disabled="true">';
};

$endLi = '</li>';

?>

<?= VueWizard::widget([
    'items' => [
        [
            'title' => 'Account',
            'isActive' => $currentStep == 1,
            'icon' => 'fa fa-user',
        ],
        [
            'title' => 'Children',
            'isActive' => $currentStep == 2,
            'icon' => 'fa fa-graduation-cap',
        ],
        [
            'title' => 'Confirmation',
            'isActive' => $currentStep == 4,
            'icon' => 'fa fa-check-square-o'
        ],
    ],
]) ?>