<?php

use common\components\bl\Greeting;
use common\widgets\Select2;
use himiklab\yii2\recaptcha\ReCaptcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GuardianSignupForm */
/* @var $form ActiveForm */
?>
<div class="login-box signup-index request-wizard pa-2 ibox-content">
    <?php $form = ActiveForm::begin(['options' => [
        'class' => 'wizard-big wizard'
    ]]); ?>

    <?= $this->render('_master-header', ['currentStep' => 1]) ?>

    <div class="">
        <div class="help-block"></div>
        <div class="col-md-12 px-0">
            <?= $form->field($model, 'first_name')
                ->textInput(['autofocus' => true, 'placeholder' => 'First Name'])
                ->label(false) ?>

            <?= $form->field($model, 'last_name')
                ->textInput(['placeholder' => 'Last Name'])
                ->label(false) ?>

            <?= $form->field($model, 'greeting')
                ->widget(Select2::class, [
                    'data' => Greeting::getNames(),
                    'options' => [
                        'placeholder' => 'Select a greeting ...',
                    ],
                ])->label(false) ?>

            <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
            <?= $form->field($model, 'confirmEmail')->textInput(['placeholder' => 'Confirm Email'])->label(false) ?>
            <div class="pull-right">
                <?= $form->field($model, 'reCaptcha')->widget(
                    ReCaptcha::class
                )->label('') ?>
            </div>

        </div>
    </div>

    <?= Html::submitButton('Next', ['class' => 'btn btn-primary btn-xl pull-right']) ?>

    <?php ActiveForm::end(); ?>


</div><!-- signup-index -->
