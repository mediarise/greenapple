<?php

/** @var integer $currentStep */
/** @var $this \yii\web\View */

$endLi = '</li>';

$beginLi = function ($step) use ($currentStep) {
    if ($step === $currentStep) {
        return '<li class="current" role="tab" aria-disabled="false" aria-selected="true">';
    }
    return '<li class="disabled" role="tab" aria-disabled="true">';
};


?>
<div class="steps clearfix">
    <ul role="tablist">
        <?= $beginLi(1) ?>
        <a>
            <span class="number">1.</span>
            Password
        </a>
        <?= $endLi ?>
        <?= $beginLi(2) ?>
        <a>
            <span class="number">2.</span>
            EULA
        </a>
        <?= $endLi ?>
    </ul>
</div>