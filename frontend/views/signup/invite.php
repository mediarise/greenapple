<?php

/* @var $this \yii\web\View */

use himiklab\yii2\recaptcha\ReCaptcha;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model \common\models\ResetPasswordForm */
?>

<div class="middle-box signup-invite invite-wizard">
    <?php $form = ActiveForm::begin(['options' => [
        'class' => 'wizard-big wizard'
    ]]); ?>

    <?= $this->render('_invite-master-header', ['currentStep' => 1]) ?>

    <div class="content">
        <div class="help-block"></div>
        <div class="col-md-12">
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
            <?= $form->field($model, 'reCaptcha')->widget(
                ReCaptcha::class
            )->label('') ?>
        </div>

    </div>

    <div class="actions clearfix">
        <ul>
            <li>
                <?= Html::submitButton('Next', ['class' => 'btn btn-primary']) ?>
            </li>
        </ul>
    </div>

    <?php ActiveForm::end(); ?>


</div><!-- signup-invite -->