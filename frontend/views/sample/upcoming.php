<?php

/**
 * @var $this View
 * @var $dataProvider ArrayDataProvider
 */
use frontend\widgets\IBox;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\Pjax;

$this->title = 'Upcoming';
$this->params['breadcrumbs'][] = ['label' => 'Upcoming'];
?>
<div class="upcoming-sample-page">
    <div class="col-lg-12">
        <?php IBox::begin([
            'title' => 'Upcoming',

        ]) ?>
        <?php Pjax::begin() ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => false,
            'tableOptions' => [
                'class' => 'table table-striped'
            ],
            'columns' => [
                'date',
                'title',
                'student:html',
                'forms:html',
                'products:html',
                'status:html',
            ],
        ]) ?>
        <?php Pjax::end(); ?>
        <?php IBox::end(); ?>
    </div>

</div>
