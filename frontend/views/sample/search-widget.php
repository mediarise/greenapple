<?php
/**
 * @var $this \yii\web\View
 *
 */

use frontend\widgets\AutocompletedInput;
use yii\helpers\Url;

$this->title = 'Search widget';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="search-widget-sample-page">

    <?= AutocompletedInput::widget([
        'name' => 'twitter_oss',
        'dataUrl' => Url::toRoute(['/sample/get-search-data'], true),
        'options' => ['placeholder' => 'Filter as you type ...'],
        'itemTemplate' => "<div><b>{{name}}</b><br><i class=\"fa fa-user\"></i> {{code}}</div>",
    ]); ?>


</div>