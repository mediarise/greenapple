<?php
/**
 * @var $this \yii\web\View
 */

$this->title = 'Notification test';

$script = <<<JS
toastr.options = {
    closeButton: true,
    debug: false,
    progressBar: true,
    preventDuplicates: false,
    positionClass: "toast-top-right",
    onclick: null,
    showDuration: "400",
    hideDuration: "1000",
    timeOut: "7000",
    extendedTimeOut: "1000",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut"
};
    $('body').on('click', '#show-notification-btn', function() {
        toastr.success('Without any options','Simple notification!');
    })
JS;
$this->registerJs($script);

?>

<div class="test-notification-page">
    <a id="show-notification-btn" class="btn btn-success">Show notification</a>
</div>