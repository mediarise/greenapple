<?php
/**
 * @var $this \yii\web\View
 */

use frontend\assets\InspiniaAsset;
use frontend\widgets\EventCard;
use frontend\widgets\GridView;
use frontend\widgets\IBox;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Dashboard';

?>

<div class="dashboard-sample-page">
    <div class="col-lg-12">
        <?php IBox::begin([
            'title' => 'Pending',
        ]) ?>
        <div class="row">
            <div class="col-lg-9">
                <div class="col-lg-3">
                    <?= EventCard::widget([
                        'title' => 'Milk',
                        'url' => Url::toRoute(['/guardian/index/event-view']),
                        'obligation' => EventCard::OBLIGATION_OPTIONAL,
                        'price' => '5 x $1',
                        'previewImageSrc' => '/images/event-pic/milk.png',
                        'period' => 'Oct 15 - Oct 19',
                        'dueDate' => '8 Oct',
                        'acceptLink' => Html::a('Buy', '#', ['class' => 'btn btn-xs btn-outline btn-primary']),
                        'student' => Html::img('/images/lisa.png', ['class' => 'img-circle', 'width' => '30px']) . ' Lisa Simpson',
                        'eventLink' => Html::a('Milk Program', '#')
                    ]) ?>
                </div>
                <div class="col-lg-3">
                    <?= EventCard::widget([
                        'title' => 'AGO Visit',
                        'url' => Url::toRoute(['/guardian/index/event-view']),
                        'obligation' => EventCard::OBLIGATION_REQUIRED,
                        'previewImageSrc' => '/images/event-pic/ticket.png',
                        'date' => 'Oct 23',
                        'dueDate' => '15 Oct',
                        'acceptLink' => Html::a('Attend', '#', ['class' => 'btn btn-xs btn-outline btn-primary']),
                        'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
                    ]) ?>
                </div>
                <div class="col-lg-3">
                    <?= EventCard::widget([
                        'title' => 'Pizza Slice',
                        'url' => Url::toRoute(['/guardian/index/event-view']),
                        'obligation' => EventCard::OBLIGATION_OPTIONAL,
                        'price' => '$2',
                        'previewImageSrc' => '/images/event-pic/pizza.png',
                        'period' => 'Oct 24',
                        'dueDate' => 'Oct 17',
                        'acceptLink' => Html::a('Buy', '#', ['class' => 'btn btn-xs btn-outline btn-primary']),
                        'student' => Html::img('/images/lisa.png', ['class' => 'img-circle', 'width' => '30px']) . ' Lisa Simpson',
                        'eventLink' => Html::a('Pizza Day', '#')
                    ]) ?>
                </div>
                <div class="col-lg-3">
                    <?= EventCard::widget([
                        'title' => 'Classroom supplies list',
                        'url' => Url::toRoute(['/guardian/index/event-view']),
                        'obligation' => EventCard::OBLIGATION_OPTIONAL,
                        'price' => '$N/A',
                        'previewImageSrc' => '/images/event-pic/stationery.png',
                        'dueDate' => 'Oct 31',
                        'acceptLink' => Html::a('Buy', '#', ['class' => 'btn btn-xs btn-outline btn-primary']),
                        'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
                        'eventLink' => Html::a('Stock the Classroom', '#')
                    ]) ?>
                </div>
            </div>
        </div>
        <?php IBox::end(); ?>
    </div>

    <div class="mb-5 col-lg-12">
        <?php IBox::begin([
            'title' => 'Upcoming',
        ]) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => false,
            'tableOptions' => [
                'class' => 'table table-striped'
            ],
            'columns' => [
                'date',
                'title',
                'student:html',
                'forms:html',
                'products:html',
                'status:html',
            ],
        ]) ?>
        <?php IBox::end(); ?>
    </div>


    <div class="col-lg-12">
        <?php IBox::begin([
            'title' => 'Events on Calendar View',
        ]) ?>
        <div id="calendar"></div>
        <?php
        $script = <<<JS
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            drop: function() {
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1)
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d-5),
                    end: new Date(y, m, d-2)
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d-3, 16, 0),
                    allDay: false
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d+4, 16, 0),
                    allDay: false
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d+1, 19, 0),
                    end: new Date(y, m, d+1, 22, 30),
                    allDay: false
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/'
                }
            ]
        });
JS;
        $this->registerJs($script);
        $this->registerJsFile('js/plugins/fullcalendar/moment.min.js', ['depends' => InspiniaAsset::class]);
        $this->registerJsFile('js/plugins/fullcalendar/fullcalendar.min.js', ['depends' => InspiniaAsset::class]);
        $this->registerCssFile('css/plugins/fullcalendar/fullcalendar.css', ['depends' => InspiniaAsset::class]);
        $this->registerCssFile('css/plugins/fullcalendar/fullcalendar.print.css', ['depends' => InspiniaAsset::class, 'media' => 'print']);
        ?>
        <?php IBox::end(); ?>
    </div>


    <div class="col-lg-12">
        <?php IBox::begin([
            'title' => 'History',
        ]) ?>
        <div class="row">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1"> Product</a></li>
                    <li class=""><a data-toggle="tab" href="#tab-2">Forms</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Product</th>
                                    <th>Event</th>
                                    <th>Quantity</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Sep 12</td>
                                    <td>ROM Ticket</td>
                                    <td>Field Trip: ROM Visit</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>Sep 19</td>
                                    <td>Mr. Sketch Markers</td>
                                    <td>Sale: Mr. Sketch Markers</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>Sep 22</td>
                                    <td>Box of Tissue</td>
                                    <td>Initiative: Stock the classroom</td>
                                    <td>2</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Form</th>
                                    <th>Event</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Sep 12</td>
                                    <td>Parent Consent Form</td>
                                    <td>Field Trip: ROM Visit</td>
                                </tr>
                                <tr>
                                    <td>Sep 21</td>
                                    <td>Police clearance received/td>
                                    <td>N/A</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php IBox::end(); ?>
    </div>
</div>
