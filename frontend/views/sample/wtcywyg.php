<?php
/**
 * @var $this \yii\web\View
 */

use frontend\widgets\HtmlEditor;

$this->title = 'Wtcywyg widget';
?>

<div class="wtcywyg-widget-sample-page">
    <?= HtmlEditor::widget([
        'inputId' => 'sample-editor',
        'name' => 'some-field',
    ]); ?>
</div>
