<?php

/**
 * @var $this \yii\web\View
 */

use frontend\widgets\EventCard;
use yii\helpers\Html;

$this->title = 'Pending';
$this->params['breadcrumbs'][] = ['label' => 'Pending'];
?>

<div class="events-sample-page row">
    <div class="col-lg-9">
        <div class="col-lg-3">
            <?= EventCard::widget([
                'title' => 'Milk',
                'obligation' => EventCard::OBLIGATION_OPTIONAL,
                'price' => '5 x $1',
                'previewImageSrc' => '/images/event-pic/milk.png',
                'period' => 'Oct 15 - Oct 19',
                'dueDate' => '8 Oct',
                'acceptLink' => Html::a('Buy', '#', ['class' => 'btn btn-xs btn-outline btn-primary']),
                'student' => Html::img('/images/lisa.png', ['class' => 'img-circle', 'width' => '30px']) . ' Lisa Simpson',
                'eventLink' => Html::a('Milk Program', '#')
            ])?>
        </div>
        <div class="col-lg-3">
            <?= EventCard::widget([
                'title' => 'AGO Visit',
                'obligation' => EventCard::OBLIGATION_REQUIRED,
                'previewImageSrc' => '/images/event-pic/ticket.png',
                'date' => 'Oct 23',
                'dueDate' => '15 Oct',
                'acceptLink' => Html::a('Attend', '#', ['class' => 'btn btn-xs btn-outline btn-primary']),
                'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
            ])?>
        </div>
        <div class="col-lg-3">
            <?= EventCard::widget([
                'title' => 'Pizza Slice',
                'obligation' => EventCard::OBLIGATION_OPTIONAL,
                'price' => '$2',
                'previewImageSrc' => '/images/event-pic/pizza.png',
                'period' => 'Oct 24',
                'dueDate' => 'Oct 17',
                'acceptLink' => Html::a('Buy', '#', ['class' => 'btn btn-xs btn-outline btn-primary']),
                'student' => Html::img('/images/lisa.png', ['class' => 'img-circle', 'width' => '30px']) . ' Lisa Simpson',
                'eventLink' => Html::a('Pizza Day', '#')
            ])?>
        </div>
        <div class="col-lg-3">
            <?= EventCard::widget([
                'title' => 'Classroom supplies list',
                'obligation' => EventCard::OBLIGATION_OPTIONAL,
                'price' => '$N/A',
                'previewImageSrc' => '/images/event-pic/stationery.png',
                'dueDate' => 'Oct 31',
                'acceptLink' => Html::a('Buy', '#', ['class' => 'btn btn-xs btn-outline btn-primary']),
                'student' => Html::img('/images/bart.png', ['class' => 'img-circle', 'width' => '30px']) . ' Bart Simpson',
                'eventLink' => Html::a('Stock the Classroom', '#')
            ])?>
        </div>
    </div>
</div>
