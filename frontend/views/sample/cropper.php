<?php
/**
 * @var $this \yii\web\View
 */

use frontend\widgets\Cropper;
use yii\helpers\Url;

$this->title = 'Cropper sample';
?>

<div class="cropper-sample-page">
    <?= Cropper::widget([
        'imageSrc' => Url::toRoute('@web/images/photo-placeholder.png'),
    ]) ?>
</div>


