<?php

/**
 * @var $this \yii\web\View
 * @var array $workingdayDateList
 */
use yii\helpers\Html;

$this->title = 'Event view';
$this->params['breadcrumbs'][] = ['label' => 'Event view'];
$this->params['sideBarItems'] = [
    [
        'label' => 'Dashboard',
        'url' => ['/'],
        'iconClass' => 'fa fa-th-large',
    ],
    [
        'label' => 'Messages',
        'url' => ['/'],
        'iconClass' => 'fa fa-envelope-open-o',
    ],
    [
        'label' => 'Account Settings',
        'url' => ['/'],
        'iconClass' => 'fa fa-wrench',
    ],

];
?>

<div class="sample-event-view-page row">
    <div class="col-lg-8">
        <div class="ibox">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table shoping-cart-table">
                        <tbody>
                        <tr>
                            <td width="90">
                                <?= Html::img('/images/event-pic/milk.png', ['width' => '130px']) ?>
                            </td>
                            <td class="desc">
                                <h3>
                                    <a href="#" class="text-navy">
                                        Milk Program
                                    </a>
                                </h3>

                                <p class="small">
                                    Starts Oct 1, 11:00 AM
                                </p>

                                <p class="small">
                                    Repeats Every Weekday
                                </p>

                                <div class="m-t-sm">
                                    <?= Html::img('/images/lisa.png', ['class' => 'img-circle', 'width' => '30px']) ?>
                                    Lisa Simpson
                                </div>
                            </td>

                            <td>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="ibox">
            <div class="ibox-title">
                Products and Forms
            </div>
            <div class="ibox-content">
                <div class="">
                    <a href="#" class="btn btn-primary">
                        <i class="fa fa-check"></i>
                        Buy selected
                    </a>
                    <a href="#" class="btn btn-danger">
                        <i class="fa fa-times"></i>
                        Decline selected
                    </a>
                </div>
                <br>

                <div class="">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    <input type="checkbox">
                                </th>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Product
                                </th>
                                <th>
                                    Event
                                </th>
                                <th>
                                    Options
                                </th>
                                <th>
                                    Quantity
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Total
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($workingdayDateList as $key => $dateRange) :
                                $disabled = $key < 2;
                                ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" <?= $disabled ? 'disabled' : '' ?>>
                                    </td>
                                    <td>
                                        <a href="#" class="<?= $disabled ? 'text-muted' : '' ?>">
                                            <?= $dateRange; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="<?= $disabled ? 'text-muted' : '' ?>">
                                            Milk
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="<?= $disabled ? 'text-muted' : '' ?>">
                                            Milk Program
                                        </a>
                                    </td>
                                    <td>
                                        <select class="form-control" <?= $disabled ? 'disabled' : '' ?>>
                                            <option selected>White</option>
                                            <option>Chocolate</option>
                                        </select>
                                    </td>
                                    <td>
                                        5
                                    </td>
                                    <td>
                                        $1
                                    </td>
                                    <td>
                                        $5
                                    </td>
                                </tr>
                            <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
