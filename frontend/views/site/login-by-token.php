<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use frontend\widgets\CheckBox;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <h1 class=""><?= Html::encode($this->title) ?></h1>

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>

            <p>Login in. To see it in action.</p>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => [
                    'class' => 'm-t'
                ],
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{hint}\n{error}",
                    'labelOptions' => [
                        'class' => 'col-lg-2 control-label'
                    ],
                ],
            ]); ?>

            <p>Please fill out the following fields to login:</p>

            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'password'])->label(false) ?>

            <?= $form->field($model, 'rememberMe')->widget(CheckBox::class, [])->label('') ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'login-button']) ?>
            </div>

            <?= Html::a('<small>Forgot password?</small>', ['site/request-password-reset']) ?>

            <p class="text-muted text-center">
                <small>Do not have an account?</small>
            </p>

            <?= Html::a('Create an account', ['site/signup'], [ 'class' => 'btn btn-sm btn-white btn-block']) ?>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
