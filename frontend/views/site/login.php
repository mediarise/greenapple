<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

/* @var $user \yii\web\User */

use frontend\widgets\CheckBox;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <div class="animated text-left fadeInDown login-box pa-2 ibox-content">
        <div>

            <div class="logo mb-4"><?= Html::img('/images/ga-v2.png', ['width' => '100%']); ?></div>

            <?php if (!$user->isGuest): ?>
                <p>
                    You are logged as <?= Html::encode($user->username) ?><br>
                    <?= Html::a('Back to dashboard', $user->homeUrl) ?>
                </p>
            <?php endif; ?>

            <?php $form = ActiveForm::begin([
                'enableClientValidation' => false,
                'id' => 'login-form',
                'options' => [
                    'class' => 'm-t'
                ],
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{hint}\n{error}",
                    'labelOptions' => [
                        'class' => 'col-lg-2 control-label'
                    ],
                ],
            ]); ?>

            <h2 class="pt-2 pb-2">Online school payments <br> made <span class="text-success">easy</span>.</h2>

            <?= $form->field($model, 'username')->textInput(['autofocus' => false, 'placeholder' => 'Email'])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false) ?>

            <div class="row">
                <div class="col-xs-6">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-success btn-xl full-width', 'name' => 'login-button']) ?>
                </div>
                <div class="col-xs-6">
                    <?= Html::a('Create account', ['/signup'], ['class' => 'btn btn-xl btn-primary full-width']) ?>
                </div>
            </div>
            <div class="row mt-15 pl-15">
                <?= Html::a('Forgot password?', ['site/request-password-reset'], ['class' => 'link dark']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
