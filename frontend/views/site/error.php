<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception yii\web\HttpException */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="middle-box text-center animated fadeInDown">
    <h1><?= $exception->statusCode; ?></h1>
    <h3 class="font-bold">
        <?= $exception->getName(); ?>
    </h3>

    <div class="error-desc">
        <?= nl2br(Html::encode($message)) ?>
    </div>
</div>