<?php

/**
 * @var $this \yii\web\View
 * @var $filterModel \yii\base\Model
 * @var $dataProvider ArrayDataProvider
 */

use common\widgets\Select2;
use frontend\widgets\IBox;
use yii\data\ArrayDataProvider;
use frontend\widgets\GridView;
use yii\widgets\Pjax;

?>

<div class="tables-example-page">
    <h1>Example of table</h1>

    <div class="col-lg-6">
        <?php echo Select2::widget([
            'name' => 'state_10',
            'data' => ['lola', 'peter', 'sasha'],
            'options' => [
                'placeholder' => 'Select provinces ...',
                'multiple' => true
            ],
        ]);?>

        <?php IBox::begin([
            'title' => 'Table example'
        ]) ?>

        <!--        --><?php //Pjax::begin() ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $filterModel,
            'columns' => [
                'id',
                'name',
            ],
            'tableOptions' => [
                'class' => 'table table-bordered'
            ],
            'filters' => [
                [
                    'name' => 'search',
                    'type' => GridView::FILTER_TYPE_TEXT_INPUT,
                    'label' => 'Search',
                ],
                [
                    'type' => GridView::FILTER_TYPE_SELECT_2,
                    'label' => 'Just name filter',
                    'widgetConfig' => [
                        'name' => 'name',
                        'data' => ['lola', 'peter', 'sasha'],
                        'options' => [
                            'multiple' => true,
//                            'placeholder' => 'Select states ...',
                        ],
                    ]
                ],
            ]
        ]) ?>
        <!--        --><?php //Pjax::end() ?>

        <?php IBox::end(); ?>
    </div>


</div>
