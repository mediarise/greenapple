<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ArrayDataProvider;

class GridViewTest extends Model
{
    public $search;
    public $name;
    public $id;

    public function rules()
    {
        return [
            [['search', 'name'], 'string']
        ];
    }

    public function search($params)
    {
        $data = $this->getData();
        $this->load($params);
        if ($this->search !== null && ($this->search !== '')) {
            $data = $this->arrayLike($data, 'name', $this->search);
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => ['id', 'name']
            ]
        ]);
        return $dataProvider;
    }

    private function arrayLike($array, $attributeName, $needle)
    {
        $output = [];
        foreach ($array as $key => $row) {
            if (stripos($row[$attributeName], $needle) !== false)
            {
                $output[] = $row;
            }
        }
        return $output;
    }

    private function getData()
    {
        return [
            [
                'id' => '1',
                'name' => 'Lola'
            ],
            [
                'id' => '2',
                'name' => 'Peter'
            ],
            [
                'id' => '3',
                'name' => 'Jon'
            ],
            [
                'id' => '4',
                'name' => 'Igor'
            ],
            [
                'id' => '5',
                'name' => 'Andrey'
            ],
            [
                'id' => '6',
                'name' => 'Kostya'
            ],
        ];
    }
}