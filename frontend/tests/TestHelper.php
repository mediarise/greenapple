<?php

namespace frontend\tests;

class TestHelper
{

    public static function getMailClientUrl()
    {
        $smtp = getenv('SMTP_IP');
        if (empty($smtp)) {
            $smtp = 'smtp';
        }

        return 'http://' . $smtp;
    }

    public static function getSiteUrl()
    {
        $php = getenv('PHP_IP');
        if (empty($php)) {
            $php = 'php';
        }

        return 'http://' . $php;
    }

    public static function loginAs(AcceptanceTester $I, $userName)
    {
        $I->amOnPage('/');
        $I->fillField('#loginform-username', $userName);
        $I->fillField('#loginform-password', 'password_0');
        $I->click('login-button');
        $I->waitForText('Events');
    }
}