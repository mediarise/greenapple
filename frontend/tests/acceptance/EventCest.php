<?php

use Codeception\Util\Fixtures;
use common\components\Formatter;
use common\fixtures\DocEventFixture;
use common\fixtures\FormFixture;
use common\fixtures\ProductFixture;
use common\fixtures\StudentFixture;
use frontend\tests\AcceptanceTester;

class EventCest
{
    /*----- helper functions */

    private static function goToEvent(AcceptanceTester $I, $eventTitle)
    {
        $dataKey = $I->executeJS(<<<JS
return $("#events-grid-view td a:contains('$eventTitle')").closest("tr").attr('data-key');
JS
        );
        $I->click("#events-grid-view [data-key='$dataKey'] .btn-view");
        $I->waitForText('Update event');
    }

    private static function fillDuration(AcceptanceTester $I, $duration, $durationUnit)
    {
        $I->fillField('DocEventForm[duration]', $duration);
        $I->select2('doceventform-duration_unit', $durationUnit);
    }

    private static function loginAsTeacherAndGoToEvents(AcceptanceTester $I)
    {
        $teacher = Fixtures::get('teacher1');
        $I->loginAs($teacher['username'], $teacher['password']);
        $I->see('Events');
        $I->click('Events');
        $I->waitForText('Create Event');
    }

    private static function goToCreateNewEvent(AcceptanceTester $I)
    {
        $I->click('Create Event');
    }

    private static function completeCreation(AcceptanceTester $I)
    {
        $I->click('.event-save-btn');
        $I->waitForText('Event successfully created');
        $I->logout();
    }

    private static function setRequired(AcceptanceTester $I)
    {
        $I->click('.field-doceventform-is_parent_action_required .switchery');
    }

    private static function fillLedgerAccount(AcceptanceTester $I, $ledgerAccount)
    {
        $I->wait(1);
        $I->select2('doceventform-ledger_account_id', $ledgerAccount);
    }

    private static function fillUmbrellaAccount(AcceptanceTester $I, $umbrellaAccount)
    {
        $I->select2('doceventform-umbrella_account', $umbrellaAccount);
    }

    private static function fillEventType(AcceptanceTester $I, $type)
    {
        $I->select2('doceventform-type', $type);
    }

    private static function fillDescription(AcceptanceTester $I, $description)
    {
        $I->fillField('DocEventForm[description]', $description);
    }

    private static function fillTitle(AcceptanceTester $I, $title)
    {
        $I->fillField('DocEventForm[title]', $title);
    }

    /*----- end helper functions */

    public function _fixtures()
    {
        return [
            'docEvent' => [
                'class' => DocEventFixture::class,
                'dataFile' => codecept_data_dir() . 'doc_event_data.php',
            ],
            'visibilityGroup' => [
                'class' => \common\fixtures\VisibilityGroupFixture::class,
                'dataFile' => codecept_data_dir() . 'visibility_group.php'
            ],
            'product' => [
                'class' => ProductFixture::class,
                'dataFile' => codecept_data_dir() . 'product_data.php',
            ],
            'form' => [
                'class' => FormFixture::class,
                'dataFile' => codecept_data_dir() . 'form_data.php',
            ],
            'student' => [
                'class' => StudentFixture::class,
                'dataFile' => codecept_data_dir() . 'student_data.php',
            ],
        ];
    }

    /*----- creation tests */

    public function testCreateFieldTripEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);
        self::goToCreateNewEvent($I);

        self::fillTitle($I, 'new Field trip');
        self::fillDescription($I, 'This is field trip 1');
        self::fillEventType($I, 'Field trip');
        self::fillUmbrellaAccount($I, 'Departments');
        self::fillLedgerAccount($I, 'Library');
        self::setRequired($I);
        self::fillDuration($I, '2', 'Days');


        self::completeCreation($I);
    }

    public function testCreateOneTimeNutritionEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);
        self::goToCreateNewEvent($I);

        self::fillTitle($I, 'new One Time Nutrition');
        self::fillDescription($I, 'One Time Nutrition');
        self::fillEventType($I, 'Nutrition (one time)');
        self::fillUmbrellaAccount($I, 'Student Activities');
        self::fillLedgerAccount($I, 'Nutrition Program');
        self::setRequired($I);

        self::fillDuration($I, '2', 'Days');
        self::completeCreation($I);
    }

    public function testCreateRecurringNutritionEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);
        self::goToCreateNewEvent($I);

        self::fillTitle($I, 'new Recurring Nutrition');
        self::fillDescription($I, 'Recurring Nutrition');
        self::fillEventType($I, 'Nutrition (recurring)');
        self::fillUmbrellaAccount($I, 'Student Activities');
        self::fillLedgerAccount($I, 'Nutrition Program');
        self::setRequired($I);

        //Frequency
        $I->select2('doceventform-frequency', 'Weekly');
        $I->checkOption('#doceventform-repeatondays [value="1"]');
        $I->checkOption('#doceventform-repeatondays [value="4"]');
        $I->checkOption('#doceventform-repeatondays [value="16"]');

        //Due lead
        $I->fillField('#doceventform-due_lead_period', '6');
        $I->select2('doceventform-due_unit', 'Days');

        $date = new DateTime('+ 1 month');
        $I->fillField('#doceventform-enddate', Formatter::date($date));

        self::completeCreation($I);
    }

    public function testCreateStockTheClassroomEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);
        self::goToCreateNewEvent($I);

        self::fillTitle($I, 'new Stock The Classroom');
        self::fillDescription($I, 'Stock The Classroom');
        self::fillEventType($I, 'Stock the classroom');
        self::fillUmbrellaAccount($I, 'Fund Raising');
        self::fillLedgerAccount($I, 'Donations');
        self::setRequired($I);

        self::completeCreation($I);
    }

    public function testCreateSellProductEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);
        self::goToCreateNewEvent($I);

        self::fillTitle($I, 'new Sell Product Event');
        self::fillDescription($I, 'Sell Product Event');
        self::fillEventType($I, 'Sell product');
        self::fillUmbrellaAccount($I, 'Fund Raising');
        self::fillLedgerAccount($I, 'Donations');
        self::setRequired($I);

        self::completeCreation($I);
    }

    public function testCreateFillOutForm(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);
        self::goToCreateNewEvent($I);

        self::fillTitle($I, 'new Fill Out Form Event');
        self::fillDescription($I, 'Fill Out Form Event');
        self::fillEventType($I, 'Fill out form');
        self::fillUmbrellaAccount($I, 'Fund Raising');
        self::fillLedgerAccount($I, 'Donations');
        self::setRequired($I);

        self::completeCreation($I);
    }

    /*----- end creation tests */

    /*----- publishing tests */

    public function testPublishFieldTripEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);

        self::goToEvent($I, 'Field trip');

        self::checkParticipants($I, 'First1 Last1');
        self::checkProducts($I, 'Product');
        self::checkForms($I, 'Form 1');

        self::completePublishing($I);
    }

    public function testPublishOneTimeNutritionEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);

        self::goToEvent($I, 'One time nutrition');

        self::checkParticipants($I, 'First1 Last1');
        self::checkProducts($I, 'Product');

        self::completePublishing($I);
    }

    public function testPublishRecurringNutritionEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);

        self::goToEvent($I, 'Recurring Nutrition');

        self::checkParticipants($I, 'First1 Last1');
        self::checkProducts($I, 'Product');

        self::completePublishing($I);
    }

    public function testPublishStockTheClassroomEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);

        self::goToEvent($I, 'Stock the classroom');

        self::checkParticipants($I, 'First1 Last1');
        self::checkProducts($I, 'Free product');

        self::completePublishing($I);
    }

    public function testPublishSellProductEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);

        self::goToEvent($I, 'Sell product');

        self::checkParticipants($I, 'First1 Last1');
        self::checkProducts($I, 'Product');

        self::completePublishing($I);
    }

    public function testPublishFillOutFormEvent(AcceptanceTester $I)
    {
        self::loginAsTeacherAndGoToEvents($I);

        self::goToEvent($I, 'Fill out form');

        self::checkParticipants($I, 'First1 Last1');
        self::checkForms($I, 'Form 1');

        self::completePublishing($I);
    }

    /*----- end publishing tests */

    private static function checkGridRow(AcceptanceTester $I, $gridSelector, $value, $hasAnchor)
    {
        $td = 'td';
        if ($hasAnchor) {
            $td = 'td a';
        }
        $dataKey = $I->executeJS(<<<JS
return $("$gridSelector $td:contains('$value')").closest("tr").attr('data-key');
JS
        );
        $I->checkOption("$gridSelector tr[data-key='$dataKey'] input[type='checkbox']");

    }

    private static function checkParticipants(AcceptanceTester $I, $name)
    {
        $I->click('Participants', '.event-form-page');
        self::checkGridRow($I, '#students-grid', $name, false);
    }

    private static function checkProducts(AcceptanceTester $I, $name)
    {
        $I->click('Products', '.event-form-page');
        self::checkGridRow($I, '#products-grid', $name, true);
    }

    private static function checkForms(AcceptanceTester $I, $name)
    {
        $I->click('Forms', '.event-form-page');
        self::checkGridRow($I, '#forms-grid', $name, true);
    }

    private static function completePublishing(AcceptanceTester $I)
    {
        $I->click('Event info');
        $I->click('.event-publish-btn');
        $I->waitForText('Event successfully published');
        $I->logout();
    }

}