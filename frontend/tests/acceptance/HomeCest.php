<?php
//namespace frontend\tests\acceptance;

use common\fixtures\SignupRequestGuardianFixture;
use common\fixtures\SignupRequestStudentFixture;
use frontend\tests\AcceptanceTester;
use frontend\tests\TestHelper;
use yii\helpers\Url;
use Codeception\Util\Locator;

class FirstCest

{
    // Test data begin

    protected $newGuardian;

    protected $students;

    protected $appURL;

    // Test data end

    public function _fixtures()
    {
        return [
            'signupRequestGuardian' => [
                'class' => SignupRequestGuardianFixture::class,
                'dataFile' => codecept_data_dir() . 'signup_request_guardian_data.php'
            ],
            'signupRequestStudent' => [
                'class' => SignupRequestStudentFixture::class,
                'dataFile' => codecept_data_dir() . 'signup_request_student_data.php'
            ]
        ];
    }


    public function _before(AcceptanceTester $I)
    {


    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function setupData()
    {
        // Set Test Data


        $this->newGuardian =
            [
                'email' => 'tester_' . rand() . '@cappers.ca',
                //'password' => 'Abc!23$B'.rand(100000,9999999),
                'password' => 'password_0',
                'firstName' => 'New Guardian First',
                'lastName' => 'New Guardian Last',
                'greeting' => 'Mr.'
            ];

        $this->students =
            [
                'student_1' => [
                    'oen' => rand(100000000, 999999999),
                    'first_name' => "Test Student 1 First Name",
                    'last_name' => "Test Student 1 Last Name",
                    'birth_date' => '2010-02-01',
                    'gender' => 'Male',
                    'relationship' => 'Parent',
                    'board' => 'York Catholic District School Board',
                    'school' => 'Holy Spirit',
                    'grade' => 'grade 5'
                ],
                'student_2' => [
                    'oen' => rand(100000000, 999999999),
                    'first_name' => "Test Student 2 First Name",
                    'last_name' => "Test Student 2 Last Name",
                    'birth_date' => '2010-03-03',
                    'gender' => 'Male',
                    'relationship' => 'Parent',
                    'board' => 'York Catholic District School Board',
                    'school' => 'Holy Spirit',
                    'grade' => 'grade 6'
                ],
                'student_3' => [
                    'oen' => rand(100000000, 999999999),
                    'first_name' => "Test Student 3 First Name",
                    'last_name' => "Test Student 3 Last Name",
                    'birth_date' => '2010-04-05',
                    'gender' => 'Male',
                    'relationship' => 'Parent',
                    'board' => 'York Catholic District School Board',
                    'school' => 'Holy Spirit',
                    'grade' => 'grade 7'
                ]
            ];
    }

    public function testLoginLogoutExistingSchoolAdmin(AcceptanceTester $I)
    {
        $schoolAdmin = \Codeception\Util\Fixtures::get('school-admin1');
        $I->loginAs($schoolAdmin['username'], $schoolAdmin['password']);
        $I->logout();
    }

    public function testLoginLogoutExistingSchoolTeacher(AcceptanceTester $I)
    {
        $teacher = \Codeception\Util\Fixtures::get('teacher1');
        $I->loginAs($teacher['username'], $teacher['password']);
        $I->logout();
    }

    public function testLoginLogoutExistingGuardian(AcceptanceTester $I)
    {
        $guardian = \Codeception\Util\Fixtures::get('guardian1');
        $I->loginAs($guardian['username'], $guardian['password']);
        $I->logout();
    }

    public function testGuardianSignup(AcceptanceTester $I)
    {
        $this->guardianSignUp($I);
        $this->schoolAdminSeesNewSignUpRequest($I);
        $this->schoolAdminApprovesStudentsAndInvitesGuardian($I);
        $this->parentReceivesInvitationEmail($I);
        $this->newGuardianLoginLogout($I);
    }

    private function guardianSignUp(AcceptanceTester $I)
    {
        // Sign-up
        $I->amOnPage('/');
        $I->wait(0.1);
        $I->see('Online school payments made easy');
        $I->amGoingTo('Create Sign-up Request');
        $I->click('Create account');
        $I->waitForText('Account');

        // Step 1, Account

        $I->fillField('#guardiansignupform-first_name', $this->newGuardian['firstName']);
        $I->fillField('#guardiansignupform-last_name', $this->newGuardian['lastName']);

        // Greeting select2 tricks...
        $I->click('#select2-guardiansignupform-greeting-container');
        $searchField = '.select2-search__field';
        $I->waitForElementVisible($searchField);
        $I->fillField($searchField, $this->newGuardian['greeting']);
        $I->pressKey($searchField, WebDriverKeys::ENTER);

        $I->fillField('#guardiansignupform-email', $this->newGuardian['email']);
        $I->fillField('#guardiansignupform-confirmemail', $this->newGuardian['email']);
        $I->click('Next');
        $I->waitForText('Please enter information about your children');

        // Step 2. Children
        foreach ($this->students as $key => $student) {
            $I->see('Please enter information about your children');
            $I->fillField('#guardianaddstudenttemplateform-oen', $student['oen']);
            $I->fillField('#guardianaddstudenttemplateform-first_name', $student['first_name']);
            $I->fillField('#guardianaddstudenttemplateform-last_name', $student['last_name']);
            // BirthDate tricks...
            $I->fillField('#guardianaddstudenttemplateform-birthday', $student['birth_date']);
            $I->pressKey('#guardianaddstudenttemplateform-birthday', WebDriverKeys::ENTER);

            // Gender select2 tricks...
            $I->click('#select2-guardianaddstudenttemplateform-gender-container');
            $searchField = '.select2-search__field';
            $I->waitForElementVisible($searchField);
            $I->fillField($searchField, $student['gender']);
            $I->pressKey($searchField, WebDriverKeys::ENTER);

            // Relationship select2 tricks...
            $I->click('#select2-guardianaddstudenttemplateform-relationship-container');
            $searchField = '.select2-search__field';
            $I->waitForElementVisible($searchField);
            $I->fillField($searchField, $student['relationship']);
            $I->pressKey($searchField, WebDriverKeys::ENTER);

            // Board select2 tricks...
            $I->click('#select2-guardianaddstudenttemplateform-boardid-container');
            $searchField = '.select2-search__field';
            $I->waitForElementVisible($searchField);
            $I->fillField($searchField, $student['board']);
            $I->pressKey($searchField, WebDriverKeys::ENTER);

            // School select2 tricks...
            $I->wait(0.1);
            $I->waitForElementVisible('#select2-guardianaddstudenttemplateform-schoolid-container');
            $I->click('#select2-guardianaddstudenttemplateform-schoolid-container');
            $searchField = '.select2-search__field';
            $I->waitForElementVisible($searchField);
            $I->fillField($searchField, $student['school']);
            $I->pressKey($searchField, WebDriverKeys::ENTER);

            // Grade select2 tricks...
            $I->wait(0.1);
            $I->waitForElementVisible('#select2-guardianaddstudenttemplateform-grade_id-container');
            $I->click('#select2-guardianaddstudenttemplateform-grade_id-container');
            $searchField = '.select2-search__field';
            $I->waitForElementVisible($searchField);
            $I->fillField($searchField, $student['grade']);
            $I->pressKey($searchField, WebDriverKeys::ENTER);

            $I->click('Add child');
            $I->waitForText($student['first_name'] . ' ' . $student['last_name']);
        }
        $I->see('Remove');
        $I->click('Next >');
        $I->waitForText('Thank you');
    }

    private function schoolAdminSeesNewSignUpRequest(AcceptanceTester $I)
    {

        $schoolAdmin = \Codeception\Util\Fixtures::get('school-admin1');
        $I->loginAs($schoolAdmin['username'], $schoolAdmin['password']);

        $I->waitForText('Events');
        $I->see('User menu');
        $I->click('User menu');
        $I->click('Signup Request');
        $I->see($this->newGuardian['email']);
        $I->click($this->newGuardian['email']);
        $I->waitForText('First Name');
        $I->see($this->students['student_1']['first_name']);
        $I->click('Log out');
    }

    private function schoolAdminApprovesStudentsAndInvitesGuardian(AcceptanceTester $I)
    {
        $schoolAdmin = \Codeception\Util\Fixtures::get('school-admin1');
        $I->loginAs($schoolAdmin['username'], $schoolAdmin['password']);

        $I->waitForText('Events');
        $I->see('User menu');
        $I->click('User menu');
        $I->click('Signup Request');
        $I->see($this->newGuardian['email']);
        $I->click($this->newGuardian['email']);
        $I->waitForText('First Name');
        foreach ($this->students as $key => $student) {
            $I->see('Create & Approve');
            $I->click('Create & Approve');
            $I->waitForText('Approve student');
            $I->see('Approve student');
            $I->click('Approve student');
            $I->waitForText('Email');
        }
        $I->see('Invite guardian');
        $I->click('Invite guardian');
//        $toastCloseButton = '.toast-close-button';
//        $I->waitForElement($toastCloseButton);
//        $I->clickWithLeftButton($toastCloseButton);
//        $I->waitForElementNotVisible($toastCloseButton);
        $I->see('Log out');
        $I->click('Log out');
    }

    private function parentReceivesInvitationEmail(AcceptanceTester $I)
    {
        $mailClientUrl = TestHelper::getMailClientUrl();
        $I->amOnUrl($mailClientUrl);
        $I->see('MailCatcher');
        $I->waitForText($this->newGuardian['email']);
        // MailCatcher opens email text in iFrame without name or id => cannot switch to it using WebDriver...
        $selector = "//tr[descendant::td[contains(text(),'" . $this->newGuardian['email'] . "')]]";
        $message_id = $I->grabAttributeFrom(Locator::firstElement($selector), 'data-message-id');
        $I->amOnUrl($mailClientUrl . "/messages/" . $message_id . ".html");
        $I->see('inviteToken');
        $I->click('inviteToken');
        $I->wait(5);
        $I->see('Confirm Password');
        $I->fillField('Password', $this->newGuardian['password']);
        $I->fillField('Confirm Password', $this->newGuardian['password']);
        $I->click('Next');
        $I->waitForText('Agree');
        $I->see('Agree');
        $I->click('.iCheck-helper');
        $I->click('Next');
        $I->waitForText('Login');
        $I->see('Login');
    }

    private function newGuardianLoginLogout(AcceptanceTester $I)
    {
        $siteUrl = TestHelper::getSiteUrl();
        $I->amOnUrl($siteUrl);
        $I->loginAs($this->newGuardian['email'], $this->newGuardian['password']);
        $I->logout();
    }
}

