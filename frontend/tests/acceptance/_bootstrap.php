<?php
/**
 * Here you can initialize variables via \Codeception\Util\Fixtures class
 * to store data in global array and use it in Cepts.
 *
 * ```php
 * // Here _bootstrap.php
 * \Codeception\Util\Fixtures::add('user1', ['name' => 'davert']);
 * ```
 *
 * In Cept
 *
 * ```php
 * \Codeception\Util\Fixtures::get('user1');
 * ```
 */

\Codeception\Util\Fixtures::add('school-admin1', [
    'username' => 'schooladmin1@schoolAdmin.ca',
    'password' => 'password_0'
]);

\Codeception\Util\Fixtures::add('teacher1', [
    'username' => 'teacher1@teacher.ca',
    'password' => 'password_0'
]);

\Codeception\Util\Fixtures::add('guardian1', [
    'username' => 'guardian1@guardian.ca',
    'password' => 'password_0'
]);