<?php
namespace frontend\tests\acceptance;

return;

use frontend\tests\AcceptanceTester;
use frontend\tests\TestHelper;

class GradeCrudCest
{
    public function testAddLinkButton(AcceptanceTester $I)
    {
        TestHelper::loginAs($I, 'schooladmin1@schoolAdmin.ca');
        $I->amOnPage('/in/schooladmin/grade/index');
        $I->click('Add new');
        $I->waitForText('CREATE GRADE FORM');
    }

    public function testCreateGrade(AcceptanceTester $I)
    {
        TestHelper::loginAs($I, 'schooladmin1@schoolAdmin.ca');
        $I->amOnPage('/in/schooladmin/grade/create');
        $I->fillField('Grade[name]', 'New grade 1');
        $I->fillField('Grade[year]', '50');
        $I->click('Save');
        $I->waitForText('LIST OF GRADES');
    }

    public function testRemoveGrade(AcceptanceTester $I)
    {
        TestHelper::loginAs($I, 'schooladmin1@schoolAdmin.ca');
        $I->amOnPage('/in/schooladmin/grade/index');
        $I->fillField('GradeSearch[name]', 'New grade 1');
        $I->wait(1);
        $I->click('[title="Delete grade"]');
        $I->acceptPopup();
        $I->waitForText('Grade list');
    }
}