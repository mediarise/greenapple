<?php

namespace frontend\tests\acceptance;

use Codeception\Util\Locator;
use common\fixtures\SignupRequestGuardianFixture;
use common\fixtures\SignupRequestStudentFixture;
use frontend\tests\AcceptanceTester;
use frontend\tests\TestHelper;

class SignupRequestCest
{
    public function _fixtures()
    {
        return [
            'signupRequestGuardian' => [
                'class' => SignupRequestGuardianFixture::class,
                'dataFile' => codecept_data_dir() . 'signup_request_guardian_data.php'
            ],
            'signupRequestStudent' => [
                'class' => SignupRequestStudentFixture::class,
                'dataFile' => codecept_data_dir() . 'signup_request_student_data.php'
            ]
        ];
    }

    public function testReject(AcceptanceTester $I)
    {
        TestHelper::loginAs($I, 'schooladmin1@schoolAdmin.ca');
        $I->amOnPage('/in/schooladmin/signup-request/index');
        $I->see('Homer Simpson');
        $I->click('Homer Simpson');
        $I->waitForText('Bart Simpson');
        $I->click('Reject request');
        $I->waitForText('Reject signup request');
        $I->wait(0.1);
        $I->fillField('SignupRequestRejectionForm[rejectReason]', 'Reject because of some problems with a guardian identification');
        $I->seeInField('SignupRequestRejectionForm[rejectReason]', 'Reject because of some problems with a guardian identification');
        $I->click('Reject signup request');
        $I->waitForText('Rejected');
        $I->see('Rejected', 'tr[data-key="1000001"]');

        //Check Email
        $mailClientUrl = TestHelper::getMailClientUrl();
        $I->amOnUrl($mailClientUrl);
        $I->see('MailCatcher');
        $I->waitForText('HomerSimpson@mail.ca');
        // MailCatcher opens email text in iFrame without name or id => cannot switch to it using WebDriver...
        $selector = "//tr[descendant::td[contains(text(),'HomerSimpson@mail.ca')]]";
        $message_id = $I->grabAttributeFrom(Locator::firstElement($selector), 'data-message-id');
        $I->amOnUrl($mailClientUrl . "/messages/" . $message_id . ".html");
        $I->see('Hello Homer Simpson');
        $I->see('Your signup request was rejected');
    }
}