<?php

namespace frontend\tests;

use WebDriverKeys;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    public function loginAs($username, $password)
    {
        $this->amOnPage('/');
        $this->wait(0.1);
        $this->see('Online school payments made easy');
        $this->fillField('#loginform-username', $username);
        $this->fillField('#loginform-password', $password);
        $this->click('login-button');
        $this->waitForText('Log out');
    }

    public function logout()
    {
        $this->click('Log out');
        $this->waitForText('Online school payments');
    }

    public function select2($id, $value)
    {
        $this->click("#select2-{$id}-container");
        $this->waitForElementVisible('.select2-results__options');
        $elementId = $this->executeJS(
            "return $(\".select2-results__options li:contains('$value')\").attr(\"id\");"
        );
        $this->click('#' . $elementId);
    }
}
