<?php

use common\components\Formatter;
use common\models\DocEvent;

$now = date('Y-m-d H:i:s');

return [
    [
        'id' => 1000001,
        'doc_event_id' => 1000001,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000002,
        'doc_event_id' => 1000002,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000003,
        'doc_event_id' => 1000003,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000004,
        'doc_event_id' => 1000004,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000005,
        'doc_event_id' => 1000005,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000006,
        'doc_event_id' => 1000006,
        'created_at' => $now,
        'updated_at' => $now,
    ],
];