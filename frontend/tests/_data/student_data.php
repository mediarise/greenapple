<?php

use common\components\Formatter;
use common\models\DocEvent;

$now = date('Y-m-d H:i:s');
$birthday = Formatter::date('-7 years');

return [
    [
        'id' => 1000001,
        'oen' => 111111111,
        'grade_id' => 1,
        'first_name' => 'First1',
        'last_name' => 'Last1',
        'birthday' => $birthday,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000002,
        'oen' => 111111112,
        'grade_id' => 1,
        'first_name' => 'First2',
        'last_name' => 'Last2',
        'birthday' => $birthday,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000003,
        'oen' => 111111113,
        'grade_id' => 1,
        'first_name' => 'First3',
        'last_name' => 'Last3',
        'birthday' => $birthday,
        'created_at' => $now,
        'updated_at' => $now,
    ],
];