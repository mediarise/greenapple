<?php

use common\components\bl\Greeting;
use common\models\SignupRequestGuardian;

$now = date('Y-m-d H:i:s');

return [
    [
        'id' => 1000001,
        'first_name' => 'Homer',
        'last_name' => 'Simpson',
        'greeting' => Greeting::MR,
        'email' => 'HomerSimpson@mail.ca',
        'status' => SignupRequestGuardian::STATUS_NEW,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000002,
        'first_name' => 'Marge',
        'last_name' => 'Simpson',
        'greeting' => Greeting::MR,
        'email' => 'MargeSimpson@mail.ca',
        'status' => SignupRequestGuardian::STATUS_NEW,
        'created_at' => $now,
        'updated_at' => $now,
    ]
];