<?php

use common\components\Formatter;
use common\models\DocEvent;

$now = date('Y-m-d H:i:s');

return [
    [
        'id' => 1000001,
        'sku' => 111111111,
        'title' => 'Product',
        'description' => 'Description',
        'is_free' => false,
        'price' => 1.0,
        'cost' => 1.0,
        'tax' => 4,
        'board_id' => 1,
        'school_id' => 1,
        'owner_id' => 22,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000002,
        'sku' => 111111112,
        'title' => 'Free product',
        'description' => 'Description',
        'is_free' => true,
        'board_id' => 1,
        'school_id' => 1,
        'owner_id' => 22,
        'created_at' => $now,
        'updated_at' => $now,
    ],
];