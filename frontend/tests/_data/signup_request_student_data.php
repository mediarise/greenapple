<?php

use common\components\bl\FamilyRelationship;
use common\components\bl\Gender;
use common\models\SignupRequestStudent;

$now = date('Y-m-d H:i:s');

return [
    [
        'signup_request_id' => 1000001,
        'relationship' => FamilyRelationship::PARENT,
        'oen' => rand(100000000, 999999999),
        'grade_id' => 1,
        'first_name' => 'Bart',
        'last_name' => 'Simpson',
        'birthday' => '2005-11-12',
        'gender' => Gender::MALE,
        'status' => SignupRequestStudent::STATUS_NEW,
        'created_at' => $now,
        'updated_at' => $now,
    ],
];