<?php

use common\components\Formatter;
use common\models\DocEvent;

$now = date('Y-m-d H:i:s');

return [
    [
        'id' => 1000001,
        'title' => 'Form 1',
        'description' => 'Description',
        'html' => '<p>Form html template 1</p>',
        'control' => 1,
        'owner_id' => 22,
        'board_id' => 1,
        'school_id' => 1,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000002,
        'title' => 'Form 2',
        'description' => 'Description',
        'html' => '<p>Form html template 2</p>',
        'control' => 1,
        'owner_id' => 22,
        'board_id' => 1,
        'school_id' => 1,
        'created_at' => $now,
        'updated_at' => $now,
    ],
    [
        'id' => 1000003,
        'title' => 'Form 3',
        'description' => 'Description',
        'html' => '<p>Form html template 3</p>',
        'control' => 1,
        'owner_id' => 22,
        'board_id' => 1,
        'school_id' => 1,
        'created_at' => $now,
        'updated_at' => $now,
    ],
];