<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'driverName' => 'pgsql',
            'dsn' => 'pgsql:host=postgres;port=5432;dbname=xxx;',
            'username' => 'postgres',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'htmlLayout' => 'layouts/html',
            'textLayout' => 'layouts/text',
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['noreply@greenapplepay.com' => 'Green apple pay'],
            ],
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp',
                'username' => '',
                'password' => '',
                'port' => '25',
                'encryption' => '',
            ]
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LcO3E8UAAAAAMHbnKBaWOdkckp_klSdZxuKuzj7',
            'secret' => '6LcO3E8UAAAAAF0gBvSgD_MvCgVVBIuBOQUQM0SO',
        ],
    ],
];
