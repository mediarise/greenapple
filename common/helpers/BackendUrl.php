<?php
namespace common\helpers;

use Yii;
use yii\helpers\Url;

class BackendUrl extends Url
{
    /**
     * @return null|object|\yii\web\UrlManager
     * @throws \yii\base\InvalidConfigException
     */
    protected static function getUrlManager()
    {
        $app = Yii::$app;
        if ($app->has('urlManagerBackend')){
            return $app->get('urlManagerBackend');
        }
        return $app->getUrlManager();
    }
}