<?php
namespace common\helpers;

class ArrayHelper extends \yii\helpers\ArrayHelper
{
    public static function map($array, $from, $to, $group = null)
    {
        $result = [];
        foreach ($array as $element) {
            $key = $from ? static::getValue($element, $from) : null;
            $value = static::getValue($element, $to);
            if ($group !== null) {
                if ($key) {
                    $result[static::getValue($element, $group)][$key] = $value;
                } else {
                    $result[static::getValue($element, $group)][] = $value;
                }
            } else {
                if ($key) {
                    $result[$key] = $value;
                } else {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    public static function getFirstValue($array)
    {
        if (!is_array($array)) {
            return null;
        }
        $key = array_keys($array)[0];

        return ArrayHelper::getValue($array, $key, null);
    }

    public static function getFirstKey($array)
    {
        if (!is_array($array)) {
            return null;
        }

        return array_keys($array)[0];
    }

    public static function getColumn($array, $name, $keepKeys = true, $defaultValue = [])
    {
        if (!is_array($array)) {
            return $defaultValue;
        }

        $result = [];
        if ($keepKeys) {
            foreach ($array as $k => $element) {
                $result[$k] = static::getValue($element, $name);
            }
        } else {
            foreach ($array as $element) {
                $result[] = static::getValue($element, $name);
            }
        }

        if (!$result) {
            return $defaultValue;
        }

        return $result;
    }
}