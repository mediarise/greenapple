<?php
namespace common\helpers;

use Yii;
use yii\helpers\Url;

class FrontendUrl extends Url
{
    /**
     * @return null|object|\yii\web\UrlManager
     * @throws \yii\base\InvalidConfigException
     */
    protected static function getUrlManager()
    {
        $app = Yii::$app;
        if ($app->has('urlManagerFrontend')){
            return $app->get('urlManagerFrontend');
        }
        return $app->getUrlManager();
    }
}