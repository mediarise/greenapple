<?php

namespace common\models;


use yii\web\ForbiddenHttpException;

/**
 * This is the ActiveQuery class for [[Holiday]].
 *
 * @see Holiday
 */
class HolidayQuery extends \yii\db\ActiveQuery
{
    /**
     * @throws ForbiddenHttpException
     */
    protected function restrictByRole()
    {
        throw new ForbiddenHttpException();
    }

    /**
     * {@inheritdoc}
     * @return Holiday[]|array
     * @throws ForbiddenHttpException
     */
    public function all($db = null)
    {
        $this->restrictByRole();
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Holiday|array|null
     * @throws ForbiddenHttpException
     */
    public function one($db = null)
    {
        $this->restrictByRole();
        return parent::one($db);
    }
}
