<?php


namespace common\models\guardian;


use common\components\bl\ledger\GuardianEventLedger;
use yii\base\Model;

class DeclineEventForm extends Model
{
    public $reason;
    public $guardianId;
    public $eventId;
    public $studentId;

    public function rules()
    {
        return [
            [['reason'], 'string'],
            [['guardianId', 'eventId', 'studentId'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'reason' => 'Cancellation reason'
        ];
    }

    /**
     * @throws \Throwable
     * @throws \common\exception\LogicException
     * @throws \yii\db\Exception
     */
    public function save()
    {
        $ledger = new GuardianEventLedger($this->guardianId);
        $ledger->declineEvent($this->eventId, $this->studentId, $this->reason);
        return true;
    }
}