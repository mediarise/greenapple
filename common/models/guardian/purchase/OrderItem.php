<?php

namespace common\models\guardian\purchase;

interface OrderItem
{
    public function getEventId();
    public function getGuardianId();
    public function getStudentId();
    public function getActionType();
    public function getProductId();
    public function getProductVariationId();
    public function getOrderId();
    public function getQuantity();
    public function getPrice();
    public function getTax();
    public function getTotal();
}