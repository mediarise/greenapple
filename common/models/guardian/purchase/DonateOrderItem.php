<?php

namespace common\models\guardian\purchase;


use common\components\bl\ledger\GuardianEventLedger;
use yii\base\Model;

/**
 * Class DonateOrderItem
 * @property int eventId
 * @property int guardianId
 * @property int studentId
 * @property int productId
 * @property int quantity
 * @package common\models\guardian\purchase
 */
class DonateOrderItem extends Model implements OrderItem
{
    private $_eventId;
    private $_guardianId;
    private $_studentId;
    private $_productId;
    private $_quantity;

    public function rules()
    {
        return [
            [['eventId', 'guardianId', 'studentId', 'productId', 'quantity'], 'safe']
        ];
    }

    public function getEventId()
    {
        return $this->_eventId;
    }

    public function getGuardianId()
    {
        return $this->_guardianId;
    }

    public function getStudentId()
    {
        return $this->_studentId;
    }

    public function getActionType()
    {
        return GuardianEventLedger::ACTION_PURCHASE_DONATE;
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    public function getProductVariationId()
    {
        return null;
    }

    public function getOrderId()
    {
        return null;
    }

    public function getQuantity()
    {
        return $this->_quantity;
    }

    public function getPrice()
    {
        return null;
    }

    public function getTax()
    {
        return null;
    }

    public function getTotal()
    {
        return null;
    }

    /**
     * @param mixed $eventId
     */
    public function setEventId($eventId)
    {
        $this->_eventId = $eventId;
    }

    /**
     * @param mixed $guardianId
     */
    public function setGuardianId($guardianId)
    {
        $this->_guardianId = $guardianId;
    }

    /**
     * @param mixed $studentId
     */
    public function setStudentId($studentId)
    {
        $this->_studentId = $studentId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->_productId = $productId;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->_quantity = $quantity;
    }
}