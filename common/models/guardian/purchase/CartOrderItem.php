<?php

namespace common\models\guardian\purchase;

use common\components\bl\ledger\GuardianEventLedger;
use common\models\Cart;

class CartOrderItem implements OrderItem
{
    /**
     * @var Cart
     */
    private $cartItem;
    private $orderId;

    public function __construct(Cart $cartItem, $orderId)
    {
        $this->cartItem = $cartItem;
        $this->orderId = $orderId;
    }

    public function getEventId()
    {
        return $this->cartItem->doc_event_id;
    }

    public function getGuardianId()
    {
        return $this->cartItem->guardian_id;
    }

    public function getStudentId()
    {
        return $this->cartItem->student_id;
    }

    public function getActionType()
    {
        return GuardianEventLedger::ACTION_PURCHASE_PURCHASE;
    }

    public function getProductId()
    {
        return $this->cartItem->product_id;
    }

    public function getProductVariationId()
    {
        return $this->cartItem->product_variation_id;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function getQuantity()
    {
        return $this->cartItem->quantity;
    }

    public function getPrice()
    {
        return $this->cartItem->price;
    }

    public function getTax()
    {
        return $this->cartItem->tax;
    }

    public function getTotal()
    {
        return $this->cartItem->total;
    }
}