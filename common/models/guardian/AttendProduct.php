<?php

namespace common\models\guardian;

use common\models\EventProduct;
use common\models\ProductVariation;
use yii\db\Query;

class AttendProduct extends EventProduct
{
    /** @var string */
    public $title;
    /** @var string */
    public $description;
    /** @var string */
    public $url;
    /** @var int */
    public $quantity;

    public static function findByEvent($eventId, $guardianId, $studentId)
    {
        $cartSubQuery = (new Query())
            ->select('product_id, quantity')
            ->from('cart')
            ->where(['doc_event_id' => $eventId, 'guardian_id' => $guardianId, 'student_id' => $studentId]);

        return AttendProduct::find()
            ->alias('ep')
            ->select([
                'ep.id',
                'ep.product_id',
                'p.title',
                'p.description',
                'ep.price',
                'ep.min_quantity',
                'ep.max_quantity',
                'ph.url',
                'c.quantity',
            ])
            ->joinWith('product p', false)
            ->joinWith('product.mainPhoto ph', false)
            ->with('variations')
            ->leftJoin(['c' => $cartSubQuery], 'p.id = c.product_id')
            ->where(['ep.doc_event_id' => $eventId])
            ->orderBy('ph.url')
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariations()
    {
        return $this->hasMany(ProductVariation::class, ['product_id' => 'id'])
            ->viaTable('product', ['id' => 'product_id']);
    }
}