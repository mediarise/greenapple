<?php

namespace common\models\guardian;

use common\components\Formatter;
use common\models\EventProduct;

class NutritionProduct extends EventProduct
{
    public $start_date;
    public $end_date;
    public $title;
    public $bought;
    public $bought_variation;
    public $bought_price;
    public $bought_total;

    public function getDate(DocEvent $event)
    {
        if ($this->getDay($event)) {
            return $this->getDay($event);
        }

        return Formatter::period($this->start_date, $this->end_date);
    }

    public function getIsBought()
    {
        return $this->bought > 0;
    }

    public function getName()
    {
        return 'product' . $this->id;
    }

    public function getMinQuantity()
    {
        return max(1, $this->min_quantity);
    }

    public function getQuantity()
    {
        return max(1, $this->getMinQuantity());
    }

    /**
     * Returns the date for one day of the week.
     * @param DocEvent $event
     * @return bool|string
     */
    public function getDay(DocEvent $event)
    {
        $mapDays = [DocEvent::MONDAY, DocEvent::TUESDAY, DocEvent::WEDNESDAY, DocEvent::THURSDAY, DocEvent::FRIDAY, DocEvent::SATURDAY, DocEvent::SUNDAY];
        $in = in_array($event->repeat_on_days, $mapDays);
        if ($in) {
            return DocEvent::getDayWeek($event->repeat_on_days) . ', '
                . Formatter::toDay($this->start_date, array_keys($mapDays, $event->repeat_on_days)[0]);
        }
        return false;
    }
}