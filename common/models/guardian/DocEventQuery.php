<?php

namespace common\models\guardian;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[DocEvent]].
 *
 * @see DocEvent
 */
class DocEventQuery extends ActiveQuery
{
    public function pending()
    {
        return $this->andWhere('end_date > now()');
    }
}
