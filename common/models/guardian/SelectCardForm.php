<?php

namespace common\models\guardian;

use yii\base\Model;

class SelectCardForm extends Model
{
    /** @var int */
    public $tokenId;

    public function rules()
    {
        return [
            [['tokenId'], 'safe']
        ];
    }
}