<?php

namespace common\models\guardian;

use common\models\EventProduct;

class StockProduct extends EventProduct
{
    public $title;
    public $contributed;
    public $quantity = 0;

}