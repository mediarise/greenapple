<?php

namespace common\models\guardian;

use yii\base\Model;

class SelectStudentForm extends Model
{
    public $studentId;
    public $returnUrl;

    public function rules()
    {
        return [
            [['returnUrl', 'studentId'], 'safe']
        ];
    }
}