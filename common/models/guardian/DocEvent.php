<?php

namespace common\models\guardian;

class DocEvent extends \common\models\DocEvent
{
    public $student_status;
    /**
     * @return DocEventQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new DocEventQuery(get_called_class());
    }
}