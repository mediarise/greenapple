<?php

namespace common\models\guardian;

use common\models\DocEvent;
use common\models\DocEventForm;
use yii\base\Model;

/** @property mixed status */

/** @property mixed statusLabel
 */
class UpcomingEvent extends Model
{
    const STATUS_PLANNED = 0x1;
    const STATUS_DECLINED = 0x2;
    const STATUS_INCOMPLETE = 0x4;
    const STATUS_PENDING_OPTIONAL = 0x8;
    const STATUS_PENDING_REQUIRED = 0x10;

    /** @var int */
    private $_status;
    private $statusNames = [
        self::STATUS_PLANNED => 'Planned',
        self::STATUS_DECLINED => 'Declined',
        self::STATUS_INCOMPLETE => 'Incomplete',
        self::STATUS_PENDING_OPTIONAL => 'Pending',
        self::STATUS_PENDING_REQUIRED => 'Pending',
    ];

    /** @var int */
    public $event_id;
    /** @var int */
    public $predecessor;
    /** @var int */
    public $student_id;
    /** @var string */
    public $start_date;
    /** @var string */
    public $end_date;
    /** @var string */
    public $title;
    /** @var string */
    public $first_name;
    /** @var string */
    public $last_name;
    /** @var int */
    public $forms_signed;
    /** @var int */
    public $forms_to_sign;
    /** @var int */
    public $products_purchased;
    /** @var int */
    public $products_to_purchase;
    /** @var bool */
    public $required;
    /** @var int */
    public $gender;
    /** @var string */
    public $img_url;
    /** @var string */
    public $due_date;
    /** @var int */
    public $type;
    /** @var bool */
    public $is_repeatable;
    /** @var int */
    public $repeat_on_days;

    public function setStatus($value)
    {
        $this->_status = $value;
    }

    public function getStatus()
    {
        if (empty($this->_status)) {
            return $this->required ? self::STATUS_PENDING_REQUIRED : self::STATUS_PENDING_OPTIONAL;
        }
        return $this->_status;
    }

    public function getStatusLabel()
    {
        return $this->statusNames[$this->getStatus()];
    }

    public function getRoute()
    {
        $url = '';
        switch ($this->type) {
            case DocEvent::EVENT_TYPE_FIELD_TRIP:
            case DocEvent::EVENT_TYPE_FILL_OUT_FORM:
            case DocEvent::EVENT_TYPE_SELL_PRODUCT:
            case DocEvent::EVENT_TYPE_NUTRITION_ONE_TIME :
            case DocEvent::EVENT_TYPE_FUNDRAISING_SELL:
                $url = '/guardian/event/view';
                break;
            case DocEvent::EVENT_TYPE_NUTRITION:
                if ($this->predecessor !== null) {
                    $url = 'guardian/recurring-event/view-child';
                } else {
                    $url = 'guardian/recurring-event/view';
                }
                break;
            case DocEvent::EVENT_TYPE_STOCK_THE_CLASSROOM:
                $url = 'guardian/stock-event/view';
                break;
            case DocEvent::EVENT_TYPE_FUNDRAISING:
                $url = '/guardian/event/fundraising';
                break;
        }
        return [$url, 'eventId' => $this->event_id, 'studentId' => $this->student_id];
    }
}