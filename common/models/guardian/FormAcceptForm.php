<?php


namespace common\models\guardian;


use common\components\bl\ledger\GuardianEventLedger;
use Yii;
use yii\base\Model;

class FormAcceptForm extends Model
{
    public $isAccept;

    public $formId;

    public $involvedForms;

    public function rules()
    {
        return [
            [['isAccept'], 'boolean'],
            [['isAccept'], 'required'],
            [['formId'], 'integer'],
        ];
    }

    public function setAccepting($formId)
    {
        if (!isset($this->involvedForms[$formId])) {
            $this->isAccept = false;
            return;
        }
        if ($this->involvedForms[$formId] == GuardianEventLedger::ACTION_FORM_SIGN) {
            $this->isAccept = 1;
            return;
        }
        if (
            ($this->involvedForms[$formId] == GuardianEventLedger::ACTION_FORM_CANCEL)
            || ($this->involvedForms[$formId] == GuardianEventLedger::ACTION_FORM_SKIP)
        ) {
            $this->isAccept = 0;
            return;
        }
    }
}