<?php

namespace common\models\schooladmin;


use yii\db\Expression;

class HolidayQuery extends \common\models\HolidayQuery
{
    protected function restrictByRole()
    {
        $actor = \Yii::$container->get('Actor');
        $this->andWhere([
            'or',
            ['school_id' => $actor['school_id']],
            [
                'and',
                new Expression('school_id is null'),
                ['board_id' => $actor['board_id']],
            ]
        ]);
    }
}