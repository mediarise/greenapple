<?php

namespace common\models\schooladmin;

use yii\db\ActiveQuery;

class DocEventQuery extends ActiveQuery
{
    public function linkedWithTeacher($teacherId)
    {
        return $this;
    }

    /**
     * @inheritdoc
     * @return DocEvent[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DocEvent|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function count($q = '*', $db = null)
    {
        return parent::count($q, $db);
    }

    /**
     * @return DocEventQuery
     */
    public function upcoming()
    {
        return $this
            ->select([
                'e.id',
                'start_date',
                'end_date',
                'title',
                'type',
                'student_count',
                'forms_signed',
                'forms_to_sign',
                'products_purchased',
                'products_to_purchase',
                't.last_name',
                't.first_name',
            ])
            ->alias('e')
            ->leftJoin('get_event_member_count() ec', 'e.id=ec.event_id')
            ->leftJoin('ldg_event_status l', 'e.id=l.doc_event_id')
            ->leftJoin('teacher t', 't.user_id = e.creator_id')
            ->andWhere(['>', 'e.creator_id', 0])
            ->andWhere('event_is_active(e)');
    }

    public function filterByProductId($productId)
    {
        if ($productId == null) {
            return $this;
        }
        return $this
            ->alias('e')
            ->leftJoin('lnk_event_product ep', 'e.id=ep.doc_event_id')
            ->andWhere(['ep.product_id' => $productId]);
    }
}
