<?php

namespace common\models;

use yii\base\Model;

class Fundraising extends Model
{
    public $min;
    public $max;
    public $step;
    public $note;
    public $value;

    public function rules()
    {
        return [
            ['value', 'number', 'min' => $this->min, 'max' => $this->max]
        ];
    }

    public static function fromEvent(DocEvent $event)
    {
        $fundraising = new Fundraising();
        $fundraising->min = $event->fundraising_min;
        $fundraising->max = $event->fundraising_max == 0 ? 'infinite' : $event->fundraising_max;
        $fundraising->step = $event->fundraising_step;
        $fundraising->note = $event->fundraising_message;
        $fundraising->value = $fundraising->min;
        return $fundraising;
    }
}