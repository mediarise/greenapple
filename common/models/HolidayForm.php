<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11.07.18
 * Time: 19:46
 */

namespace common\models;


use common\helpers\ArrayHelper;

class HolidayForm extends Holiday
{
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['start_date', 'earlyWhenEndDate'],
        ]);
    }

    public function earlyWhenEndDate($attribute, $params)
    {
        $endDate = new \DateTime($this->end_date);
        $startDate = new \DateTime($this->{$attribute});
        $diff = $startDate->diff($endDate);
        if ($diff->invert == 1) {
            $this->addError($attribute, $this->getAttributeLabel($attribute) . ' must be early than End Date.');
            return false;
        }

        return true;
    }
}