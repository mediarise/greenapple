<?php

namespace common\models;
use common\components\CommaDividedListConverter;
use yii\base\ErrorException;
use yii\behaviors\TimestampBehavior;
use yii\validators\NumberValidator;

/**
 * @property integer $id
 * @property integer $doc_event_id
 * @property int $created_at
 * @property int $updated_at
 */
class EventVisibilitySection extends VisibilityGroup
{
    const FILTER_BY_STUDENTS = 0x1;
    const FILTER_BY_GRADES = 0x2;
    const FILTER_BY_VISIBILITY_GROUPS = 0x4;

    public $teacherId;
    public $eventId;
    public $filter_by;

    public $students = [];
    public $grades = [];
    public $visibility_groups = [];
    public $newlyCreatedVisibilityGroup;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * EventVisibilitySection constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['students', 'grades', 'visibility_groups'], 'each', 'rule' => ['number']],
            ['filter_by', 'in', 'range' => array_keys(self::getFilterVariants())],
            ['teacherId', 'safe'],
            [['doc_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocEvent::class,
                'targetAttribute' => ['doc_event_id' => 'id']],
        ];
    }

    public function getSelectedItems()
    {
        switch($this->filter_by) {
            case self::FILTER_BY_STUDENTS:
               return $this->students;
            case self::FILTER_BY_GRADES:
                return $this->grades;
            case self::FILTER_BY_VISIBILITY_GROUPS:
                return $this->visibility_groups;
            default:
                throw new ErrorException('Unable to get selected items because of invalid filter type.');
        }
    }

    public function getMemberType()
    {
        switch($this->filter_by) {
            case self::FILTER_BY_STUDENTS:
                return VisibilityGroup::MEMBER_TYPE_STUDENT;
            case self::FILTER_BY_GRADES:
                return VisibilityGroup::MEMBER_TYPE_GRADE;
            case self::FILTER_BY_VISIBILITY_GROUPS:
                return VisibilityGroup::MEMBER_TYPE_VISIBILITY_GROUP;
            default:
                throw new ErrorException('Unable to get member type because of invalid filter type.');
        }
    }

    public static function getFilterVariants()
    {
        return [
            self::FILTER_BY_STUDENTS => 'Individual Students',
            self::FILTER_BY_GRADES => 'Grades',
            self::FILTER_BY_VISIBILITY_GROUPS => 'Visibility Groups',
        ];
    }

    public function getFormName()
    {
        switch($this->filter_by) {
            case self::FILTER_BY_STUDENTS:
                return Student::class;
            case self::FILTER_BY_GRADES:
                return Grade::class;
            case self::FILTER_BY_VISIBILITY_GROUPS:
                return VisibilityGroup::class;
            default:
                throw new ErrorException('Unable to get form name because of invalid filter type.');
        }
    }
}