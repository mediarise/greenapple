<?php

namespace common\models;

/**
 * This is the model class for table "lnk_guardian_student".
 *
 * @property int $id
 * @property int $guardian_id
 * @property int $student_id
 * @property int $relationship
 *
 * @property Guardian $guardian
 * @property Student $student
 */
class GuardianStudent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lnk_guardian_student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['guardian_id', 'student_id', 'relationship'], 'integer'],
            [['guardian_id', 'student_id', 'relationship'], 'required'],
            //[['guardian_id', 'student_id'], 'unique', 'targetAttribute' => ['guardian_id', 'student_id']],
            //[['guardian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Guardian::class, 'targetAttribute' => ['guardian_id' => 'id']],
            //[['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::class, 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'guardian_id' => 'Guardian ID',
            'student_id' => 'Student ID',
            'relationship' => 'Relationship',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuardian()
    {
        return $this->hasOne(Guardian::class, ['id' => 'guardian_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::class, ['id' => 'student_id']);
    }
}
