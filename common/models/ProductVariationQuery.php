<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ProductVariation]].
 *
 * @see ProductVariation
 */
class ProductVariationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ProductVariation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ProductVariation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
