<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "cart".
 *
 * @property int $id
 * @property int $doc_event_id
 * @property int $guardian_id
 * @property int $student_id
 * @property string $expiry_date
 * @property int $product_id
 * @property int $product_variation_id
 * @property int $quantity
 * @property string $price
 * @property int $tax
 * @property int $total
 * @property int $created_at
 * @property int $updated_at
 *
 * @property DocEvent $docEvent
 * @property Guardian $guardian
 * @property Product $product
 * @property EventProduct $eventProduct
 * @property Student $student
 */
class Cart extends \yii\db\ActiveRecord
{
    public $photo;
    public $event_photo;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cart';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doc_event_id', 'guardian_id', 'student_id', 'product_id', 'quantity', 'tax', 'total'], 'default', 'value' => null],
            [['doc_event_id', 'guardian_id', 'student_id', 'product_id', 'quantity', 'tax', 'total'], 'integer'],
            [['expiry_date', 'created_at'], 'safe'],
            [['price'], 'number'],
            [['doc_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocEvent::class, 'targetAttribute' => ['doc_event_id' => 'id']],
            [['guardian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Guardian::class, 'targetAttribute' => ['guardian_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['product_id', 'exist', 'skipOnError' => true, 'targetClass' => EventProduct::class, 'targetAttribute' => [
                'doc_event_id' => 'doc_event_id',
                'product_id' => 'product_id',
            ]]],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::class, 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doc_event_id' => 'Doc Event ID',
            'guardian_id' => 'Guardian ID',
            'student_id' => 'Student ID',
            'expiry_date' => 'Expiry Date',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'tax' => 'Tax',
            'total' => 'Total',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocEvent()
    {
        return $this->hasOne(DocEvent::class, ['id' => 'doc_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuardian()
    {
        return $this->hasOne(Guardian::class, ['id' => 'guardian_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventProduct()
    {
        return $this->hasOne(EventProduct::class, ['doc_event_id' => 'doc_event_id', 'product_id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::class, ['id' => 'student_id']);
    }

    public function getPhoto()
    {
        if ($this->docEvent->type == DocEvent::EVENT_TYPE_FUNDRAISING) {
            return $this->event_photo;
        }
        return $this->photo;
    }
}
