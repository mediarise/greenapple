<?php

namespace common\models;

use common\components\bl\Gender;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "signup_request_student".
 *
 * @property int $id
 * @property int $signup_request_id
 * @property int $student_id
 * @property int $relationship
 * @property string $oen
 * @property int $grade_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birthday
 * @property integer $gender
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Grade $grade
 * @property SignupRequestGuardian $signupRequest
 * @property string $statusName
 * @property string $fullName
 * @property School $school
 * @property Board $board
 * @property int $schoolId
 * @property int $boardId
 */
class SignupRequestStudent extends ActiveRecord
{
    const STATUS_NEW = 0x1;
    const STATUS_APPROVED = 0x2;
    const STATUS_REJECTED = 0x4;
    //const NEXT_STATUS = 0x8;
    //const NEXT_STATUS = 0x10;

    private static $statusNames = [
        self::STATUS_NEW => 'new',
        self::STATUS_REJECTED => 'rejected',
        self::STATUS_APPROVED => 'approved',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'signup_request_student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['signup_request_id', 'relationship', 'grade_id', 'first_name', 'last_name', 'birthday', 'status'], 'required'],
            [['signup_request_id', 'relationship', 'grade_id', 'student_id', 'oen'], 'default', 'value' => null],
            [['signup_request_id', 'relationship', 'grade_id', 'student_id', 'status', 'gender'], 'integer'],

            [['birthday'], 'date', 'format' => 'Y-m-d'],
            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['oen'], 'string', 'length' => 9],

            [['grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::class, 'targetAttribute' => ['grade_id' => 'id']],
            [['signup_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => SignupRequestGuardian::class, 'targetAttribute' => ['signup_request_id' => 'id']],

            [['student_id'], 'unique'],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::class, 'targetAttribute' => ['student_id' => 'id']],

            [['status'], 'in', 'range' => array_keys(self::$statusNames)],
            [['gender'], 'in', 'range' => array_keys(Gender::getNames())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'signup_request_id' => 'Signup Request ID',
            'relationship' => 'Relationship',
            'oen' => 'OEN',
            'grade_id' => 'Class',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'status' => 'Status',
            'statusName' => 'Status'
        ];
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getBoardId()
    {
        return $this->board->id;
    }

    public function getSchoolId()
    {
        return $this->school->id;
    }

    public function getBoard()
    {
        return $this->school->board;
    }

    public function getSchool()
    {
        return $this->grade->school;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::class, ['id' => 'grade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignupRequest()
    {
        return $this->hasOne(SignupRequestGuardian::class, ['id' => 'signup_request_id']);
    }

    public function getStudent()
    {
        return $this->hasOne(Student::class, ['id' => 'student_id']);
    }

    public function getStatusName()
    {
        return self::$statusNames[$this->status];
    }

    public static function getStatusNameByCode($code)
    {
        return self::$statusNames[$code];
    }

    public static function getStatusNames()
    {
        return self::$statusNames;
    }

    public function isOpen()
    {
        return $this->status == self::STATUS_NEW && $this->guardianTemplate->isOpen();
    }
}
