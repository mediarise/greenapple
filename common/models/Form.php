<?php

namespace common\models;

use common\query\FormQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "grade".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $html
 * @property string $control
 * @property integer $owner_id
 * @property integer $school_id
 * @property integer $board_id
 * @property $owner User
 * @property int $created_at
 * @property int $updated_at
 *
 * @property LnkEventForm[] $lnkEventForms
 *
 */
class Form extends ActiveRecord
{
    const CONTROL_CHECKBOX_AGREE = 0x1;
    const CONTROL_AGREE_DECLINE = 0x2;
    const CONTROL_YES_NO = 0x4;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'html', 'control'], 'required'],
            [['title', 'description', 'html'], 'string'],
            ['description', 'string', 'max' => 255],
            [['owner_id', 'board_id', 'school_id', 'control'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'html' => 'Html',
            'control' => 'Control',
            'owner_id' => 'Owner Id'
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \Throwable
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            // todo for production leave only ` $this->setJunctionData(); `
            if (Yii::$app->id === 'app-console') {
                $this->owner_id = null;
            } else {
                $this->setJunctionData();
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function setJunctionData()
    {
        $user = Yii::$container->get('Actor');
        $board = isset($user->board) ? $user->board : null;
        $school = isset($user->school) ? $user->school : null;

        $this->owner_id = $user->role === 'teacher' ? $user->user_id : null;
        $this->school_id = $school !== null ? $school->id : null;
        $this->board_id = $board !== null ? $board->id : null;
    }

    /**
     * @return array
     */
    public static function getControlFields()
    {
        return [
            self::CONTROL_CHECKBOX_AGREE => '"I agree" option',
            self::CONTROL_AGREE_DECLINE => '"Agree/Decline" option',
            self::CONTROL_YES_NO => '"Yes/No" option',
        ];
    }

    /**
     * @return mixed|string
     */
    public function getControlField()
    {
        $fieldList = self::getControlFields();
        return isset($fieldList[$this->control]) ? $fieldList[$this->control] : 'n/a';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkEventForms()
    {
        return $this->hasMany(LnkEventForm::class, ['form_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(DocEvent::class, ['id' => 'doc_event_id'])
            ->via('lnkEventForms');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Teacher::class, ['user_id' => 'owner_id']);
    }

    public function getSchool()
    {
        return $this->hasOne(School::class, ['id' => 'school_id']);
    }

    /**
     * @return FormQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        return Yii::createObject(FormQuery::class, [get_called_class()]);
    }
}
