<?php

namespace common\models;

use common\components\bl\Actor;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "board_admin".
 *
 * @property int $id
 * @property int $user_id
 * @property int $board_id
 * @property string $first_name
 * @property string $last_name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Board $board
 * @property User $user
 */
class BoardAdmin extends Actor
{
    public $role = 'boardAdmin';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'board_admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'board_id'], 'default', 'value' => null],
            [['user_id', 'board_id'], 'integer'],
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name'], 'string'],
            [['user_id'], 'unique'],
            [['board_id'], 'exist', 'skipOnError' => true, 'targetClass' => Board::class, 'targetAttribute' => ['board_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'board_id' => 'Board ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoard()
    {
        return $this->hasOne(Board::class, ['id' => 'board_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
