<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "grade".
 *
 * @property int $id
 * @property int $school_id
 * @property string $name
 * @property int $year
 * @property int $created_at
 * @property int $updated_at
 *
 * @property School $school
 * @property Student[] $students
 */
class Grade extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grade';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id'], 'required'],
            [['school_id'], 'default', 'value' => null],
            [['school_id', 'year'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['name', 'required'],
            [['school_id'], 'exist', 'skipOnError' => true, 'targetClass' => School::class, 'targetAttribute' => ['school_id' => 'id']],
            ['name', 'unique', 'targetAttribute' => ['school_id', 'name', 'year'], 'message' => 'The combination of grade name and year must be unique.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School ID',
            'name' => 'Name',
            'year' => 'Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(School::class, ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::class, ['grade_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(Teacher::class, ['id' => 'teacher_id'])
            ->viaTable('lnk_teacher_grade', ['grade_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisibilityGroups()
    {
        return $this->hasMany(VisibilityGroup::class, ['id' => 'visibility_group_id'])
            ->viaTable('visibility_group_member', ['member_id' => 'id'], function (ActiveQuery $q) {
                $q->where(['visibility_group_member.member_type' => VisibilityGroup::MEMBER_TYPE_GRADE]);
            });
    }


    /**
     * @return array
     */
    public static function getAllYears($formatted = false)
    {
        $data = Grade::find()->select(['year'])->groupBy(['year'])->orderBy('year')->asArray()->all();
        if ($formatted) {
            $output = [];
            foreach ($data as $item) {
                $output[$item['year']] = $item['year'] . '-' . ($item['year'] + 1);
            }
            return $output;
        }
        return ArrayHelper::map($data, 'year', 'year');
    }

    /**
     * @return string
     */
    public function getFormattedYear()
    {
        return $this->year . '-' . ($this->year + 1);
    }

    /**
     * @return string
     */
    public static function getCurrentAcademicYear()
    {
        $currentYear = (int)date('Y', time());
        $currentMonth = (int)date('m', time());
        if (($currentMonth >= 7) && ($currentMonth) <= 12) {
            return $currentYear;
        }

        return $currentYear - 1;
    }

    /**
     * @inheritdoc
     * @return GradeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GradeQuery(get_called_class());
    }
}
