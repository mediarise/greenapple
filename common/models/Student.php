<?php

namespace common\models;

use common\components\bl\Gender;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "student".
 *
 * @property int $id
 * @property string $oen
 * @property int grade_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birthday
 * @property string $gender
 * @property string $photo_path
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Guardian[] $guardians
 * @property Grade $grade
 */
class Student extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grade_id', 'first_name', 'last_name', 'birthday', 'oen'], 'required'],
            [['grade_id'], 'default', 'value' => null],
            [['grade_id', 'gender'], 'integer'],
            [['grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::class, 'targetAttribute' => ['grade_id' => 'id']],

            [['birthday'], 'date', 'format' => 'Y-m-d'],
            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['oen'], 'string', 'length' => 9],
            [['oen'], 'unique'],

            [['gender'], 'in', 'range' => array_keys(Gender::getNames())],

            [['photo_path'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oen' => 'OEN',
            'grade_id' => 'Class',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
        ];
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuardians()
    {
        return $this->hasMany(Guardian::class, ['id' => 'guardian_id'])
            ->viaTable('lnk_guardian_student', ['student_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisibilityGroups()
    {
        return $this->hasMany(VisibilityGroup::class, ['id' => 'visibility_group_id'])
            ->viaTable('visibility_group_member', ['member_id' => 'id'], function (ActiveQuery $q) {
                $q->where(['visibility_group_member.member_type' => VisibilityGroup::MEMBER_TYPE_STUDENT]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::class, ['id' => 'grade_id']);
    }

    public function getGenderName()
    {
        return Gender::getName($this->gender);
    }

    /**
     * @inheritdoc
     * @return StudentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StudentQuery(get_called_class());
    }
}
