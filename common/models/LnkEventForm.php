<?php


namespace common\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * Class LnkEventForm
 * @package common\models
 * @property $id
 * @property $doc_event_id
 * @property $form_id
 * @property $is_mandatory
 */
class LnkEventForm extends ActiveRecord
{
    public static function tableName()
    {
        return 'lnk_event_form';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['form_id', 'is_mandatory'], 'safe'],
            [['is_mandatory'], 'in', 'range' => [0, 1]]
        ];
    }

    public function getDocEvent()
    {
        return $this->hasMany(DocEvent::class, ['id' => 'doc_event_id']);
    }

    public function getForm()
    {
        return $this->hasMany(Form::class, ['id' => 'form_id']);
    }
}