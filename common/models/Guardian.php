<?php

namespace common\models;

use common\components\bl\Actor;
use common\components\bl\Greeting;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;

/**
 * This is the model class for table "guardian".
 *
 * @property int $id
 * @property int $user_id
 * @property integer $greeting
 * @property string $last_name
 * @property string $first_name
 * @property string $invite_token
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Student[] $students
 * @property User $user
 * @property string $greetingName
 * @property string $fullName

 */
class Guardian extends Actor
{
    public $role = 'guardian';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guardian';
    }

    public static function findUserByInviteToken($token)
    {
        $guardian = static::findOne([
            'invite_token' => $token
        ]);
        if ($guardian){
            return $guardian->user;
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'default', 'value' => null],
            [['user_id', 'greeting'], 'integer'],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],

            [['last_name', 'first_name'], 'string', 'max' => 255],
            [['last_name', 'first_name'], 'required'],

            [['greeting'], 'in', 'range' => array_keys(Greeting::getNames())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'last_name' => 'Last Name',
            'first_name' => 'First Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::class, ['id' => 'student_id'])->viaTable('lnk_guardian_student', ['guardian_id' => 'id']);
    }

    public function getCards()
    {
        return $this->hasMany(GuardianCard::class, ['guardian_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param $guardianId
     * @return Query
     */
    public static function getChildren($guardianId)
    {
        $query = new Query();

        return $query->select([
            'lnk_guardian_student.student_id id',
            'student.oen oen',
            'student.first_name first_name',
            'student.last_name last_name',
            'student.birthday birthday',
            'student.gender gender',
            'lnk_guardian_student.relationship relationship',
            'board.name board',
            'school.name school',
            'grade.name grade',
        ])->from('lnk_guardian_student')
            ->leftJoin('student', 'lnk_guardian_student.student_id = student.id')
            ->leftJoin('grade', 'student.grade_id = grade.id')
            ->leftJoin('school', 'grade.school_id = school.id')
            ->leftJoin('board', 'school.board_id = board.id')
            ->where(['lnk_guardian_student.guardian_id' => $guardianId]);
    }

    /**
     */
    public function acceptEula()
    {
        $user = $this->user;
        $user->status = User::STATUS_ACTIVE;
        $user->save();
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getOfficialName()
    {
        return $this->getGreetingName() . ' ' . $this->getFullName();
    }

    public function getGreetingName()
    {
        return Greeting::getName($this->greeting);
    }
}
