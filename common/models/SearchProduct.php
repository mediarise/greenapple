<?php

namespace common\models;

use common\helpers\ArrayHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * SearchProduct represents the model behind the search form of `common\models\Product`.
 */
class SearchProduct extends Model
{
    public $search;
    public $id;
    public $tax;
    public $sku;
    public $title;
    public $description;
    public $price;
    public $cost;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tax'], 'integer'],
            [['sku', 'title', 'description'], 'safe'],
            [['price', 'cost'], 'number'],
            [['search'], 'string']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function search($params)
    {
        $query = Product::find()->with('lnkEventProducts');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $user = Yii::$container->get('Actor');
        $board = $user->board;
        $school = $user->school;

        $query
            ->withoutPredefined()
            ->restrictByBoard($user->board->id)
            ->restrictBySchool($user->school->id)
            ->restrictByUser($user->user_id);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'tax' => $this->tax,
            'cost' => $this->cost,
        ]);

        $query->andFilterWhere([
            'or',
            ['ilike', 'sku', $this->search],
            ['ilike', 'title', $this->search],
            ['ilike', 'description', $this->search]
        ]);

        return $dataProvider;
    }

    /**
     * @param $params
     * @param $eventId
     * @return ArrayDataProvider
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function searchForEvent($params, $eventId)
    {
        $queryChecked = Product::find();
        $queryUnchecked = Product::find();
        $this->load($params);

        $user = Yii::$container->get('Actor');
        $board = $user->board;
        $school = $user->school;

        $queryChecked
            ->joinWith('lnkEventProducts')
            ->where(['lnk_event_product.doc_event_id' => $eventId])
            ->withoutPredefined()
            ->restrictByBoard($board->id)
            ->restrictBySchool($school->id)
            ->restrictByUser($user->user_id)
            ->andFilterWhere([
                'or',
                ['ilike', 'product.sku', $this->search],
                ['ilike', 'product.title', $this->search],
                ['ilike', 'product.description', $this->search],
            ]);

        $checkedData = $queryChecked->indexBy('id')->all();
        $checkedIds = array_keys($checkedData);

        $queryUnchecked
            ->where(['not in', 'id', $checkedIds])
            ->withoutPredefined()
            ->restrictByBoard($board->id)
            ->restrictBySchool($school->id)
            ->restrictByUser($user->user_id)
            ->andFilterWhere([
                'or',
                ['ilike', 'product.sku', $this->search],
                ['ilike', 'product.title', $this->search],
                ['ilike', 'product.description', $this->search],
            ]);

        $uncheckedData = $queryUnchecked->indexBy('id')->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => ArrayHelper::merge($checkedData, $uncheckedData),
        ]);

        return $dataProvider;
    }
}