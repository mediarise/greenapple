<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Grade]].
 *
 * @see Grade
 */
class GradeQuery extends ActiveQuery
{
    public function linkedWithTeacher($teacherId)
    {
        return $this
            ->joinWith([
                'teachers' => function(ActiveQuery $query) use($teacherId) {
                    $query->alias('teacher');
                    $query->where(['teacher.id' => $teacherId]);
                },
            ]);
    }

    public function linkedWithStudent()
    {
        return $this
            ->joinWith([
                'students' => function(ActiveQuery $query) {
                    $query->alias('student');
                }
            ]);
    }

    public function linkedWithEvent($eventId)
    {
        return $this->joinWith([
            'visibilityGroups' => function(ActiveQuery $q) use ($eventId) {
                $q->alias('visibilityGroup');
                $q->joinWith([
                    'docEvent' => function(ActiveQuery $q2) use ($eventId) {
                        $q2->alias('docEvent');
                        $q2->where(['docEvent.id' => $eventId]);
                    }
                ]);
            }
        ]);
    }

    public function inVisibilityGroup($visibilityGroupId)
    {
        return $this->joinWith([
            'visibilityGroups' => function(ActiveQuery $q) use ($visibilityGroupId) {
                $q->alias('visibilityGroup');
                $q->where(['visibilityGroup.id' => $visibilityGroupId]);
                return $q;
            }
        ]);
    }

    /**
     * @inheritdoc
     * @return Grade[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Grade|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
