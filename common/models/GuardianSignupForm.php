<?php
namespace common\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator;

class GuardianSignupForm extends SignupRequestGuardian
{
    public $confirmEmail;
    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(),  [
            ['confirmEmail', 'trim'],
            ['confirmEmail', 'compare', 'compareAttribute' => 'email'],

            ['reCaptcha', ReCaptchaValidator::class],
        ]);
    }

    public function create()
    {
        $this->status = self::STATUS_CREATED;
        return $this->save();
    }
}