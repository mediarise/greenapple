<?php

namespace common\models\boardadmin;

class HolidayQuery extends \common\models\HolidayQuery
{
    protected function restrictByRole()
    {
        $actor = \Yii::$container->get('Actor');
        $this->andWhere([
            'board_id' => $actor['board_id'],
        ]);
    }
}