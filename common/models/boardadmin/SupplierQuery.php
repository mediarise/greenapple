<?php

namespace common\models\boardadmin;


class SupplierQuery extends \common\models\SupplierQuery
{
    protected function restrictByRole()
    {
        $actor = \Yii::$container->get('Actor');
        $this->andWhere([
            'board_id' => $actor['board_id'],
        ]);
    }
}