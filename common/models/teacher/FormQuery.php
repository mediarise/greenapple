<?php

namespace common\models\teacher;

/**
 * This is the ActiveQuery class for [[FormQuery]].
 *
 * @see Form
 */
class FormQuery extends \yii\db\ActiveQuery
{
    public function forTeacher($teacherUserId)
    {
        return $this->where(['t.owner_id' => [$teacherUserId, 0]]); // TODO: check that 0 is a right value
    }

    /**
     * @inheritdoc
     * @return Form[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Form|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
