<?php


namespace common\models\teacher\statistics;


use common\components\bl\teacher\EventStatisticHelper;
use common\models\DocEvent;
use yii\base\Model;

class NutritionParentEventStatistic extends Model
{
    public $start_date;
    public $end_date;
    public $title;
    public $products_purchased;
    public $products_to_purchase;
    public $doc_event_id;
    public $predecessor;

    /**
     * @param $eventId
     * @return array
     */
    public static function findByEvent($eventId)
    {
        $statistics = DocEvent::find()
            ->alias('e')
            ->select([
                'e.start_date',
                'e.end_date',
                'e.title',
                'e.id as doc_event_id',
                'e.predecessor',
                'les.products_to_purchase',
                'les.products_purchased'
            ])
            ->leftJoin('ldg_event_status les', 'les.doc_event_id = e.id')
            ->asArray()
            ->where(['predecessor' => $eventId])
            ->orderBy('abs(extract(epoch from age(e.start_date)))')
            ->all();

        $eventStatistics = [];
        foreach ($statistics as $eventState) {
            $eventStatistics[] = new static($eventState);
        }
        return $eventStatistics;
    }

    public function getStudentCount()
    {
        $count = count(EventStatisticHelper::getStudentIds($this->predecessor));
        return $count;
    }

    public function getTotalOrders()
    {
        return (int)$this->products_to_purchase * $this->getStudentCount();
    }
}