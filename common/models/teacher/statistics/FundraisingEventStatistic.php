<?php


namespace common\models\teacher\statistics;


use common\components\bl\teacher\EventStatisticHelper;
use common\models\EventProduct;
use common\models\LnkEventForm;
use common\models\Student;
use yii\base\Model;
use yii\db\Expression;

class FundraisingEventStatistic extends Model
{
    const STATUS_PLANNED = 0x1;
    const STATUS_DECLINED = 0x2;
    const STATUS_INCOMPLETE = 0x4;
    const STATUS_PENDING_OPTIONAL = 0x8;
    const STATUS_PENDING_REQUIRED = 0x10;

    private $statusNames = [
        self::STATUS_PLANNED => 'Planned',
        self::STATUS_DECLINED => 'Declined',
        self::STATUS_INCOMPLETE => 'Incomplete',
        self::STATUS_PENDING_OPTIONAL => 'Pending',
        self::STATUS_PENDING_REQUIRED => 'Pending'
    ];

    /** @var int */
    public $event_id;
    /** @var int */
    public $student_id;
    /** @var string */
    public $first_name;
    /** @var string */
    public $last_name;

    /** @var float */
    public $total;

    public $status;

    public function getStatus()
    {
        if (empty($this->status)) {
            return self::STATUS_PENDING_REQUIRED;
        }
        return $this->status;
    }

    public function getStatusLabel()
    {
        return $this->statusNames[$this->getStatus()];
    }

    public static function findByEvent($eventId)
    {
        $studentIds = EventStatisticHelper::getStudentIds($eventId);

        $students = Student::find()
            ->alias('s')
            ->select([
                's.id as student_id',
                's.last_name',
                's.first_name',
                'event_id' => 'lap.doc_event_id',
                'total' => new Expression('SUM(lap.total)'),
            ])
            ->leftJoin('ldg_event_activity_purchase lap', 'lap.doc_event_id=:event_id AND lap.student_id=s.id', [
                ':event_id' => $eventId,
            ])
            ->where(['s.id' => $studentIds])
            ->groupBy(['s.id', 'lap.doc_event_id'])
            ->asArray()
            ->all();

        $eventStatistics = [];
        foreach ($students as $eventState) {
            $eventStatistics[] = new static($eventState);
        }

        return $eventStatistics;
    }
}