<?php


namespace common\models\teacher\statistics;


use common\components\bl\teacher\EventStatisticHelper;
use common\models\DocEvent;
use common\models\Student;
use yii\base\Model;
use yii\db\Query;
use yii\web\NotFoundHttpException;

class NutritionChildEventStatistic extends Model
{
    const STATUS_PLANNED = 0x1;
    const STATUS_DECLINED = 0x2;
    const STATUS_INCOMPLETE = 0x4;
    const STATUS_PENDING_OPTIONAL = 0x8;
    const STATUS_PENDING_REQUIRED = 0x10;

    private $statusNames = [
        self::STATUS_PLANNED => 'Planned',
        self::STATUS_DECLINED => 'Declined',
        self::STATUS_INCOMPLETE => 'Incomplete',
        self::STATUS_PENDING_OPTIONAL => 'Pending',
        self::STATUS_PENDING_REQUIRED => 'Pending'
    ];

    public $start_date;
    public $end_date;
    public $title;
    public $last_name;
    public $first_name;
    public $product_name;
    public $products_purchased;
    public $products_to_purchase;
    public $doc_event_id;
    public $student_id;
    public $status;
    public $variation_name;
    public $total;
    public $min_quantity;

    /**
     * @param $eventId
     * @return array
     * @throws NotFoundHttpException
     */
    public static function findByEvent($eventId)
    {
        $event = self::findEvent($eventId);
        $studentIds = EventStatisticHelper::getStudentIds($event->predecessor);

        $totalQuery = (new Query())
            ->select([
                'l.student_id',
                'l.product_id',
                'l.product_variation_id',
                'sum(l.quantity) total'
            ])
            ->from('ldg_event_activity_purchase l')
            ->where(['l.doc_event_id' => $eventId, 'l.student_id' => $studentIds])
            ->groupBy('l.student_id, l.product_id, l.product_variation_id');

        $statistics = Student::find()
            ->alias('s')
            ->select([
                's.id as student_id',
                's.first_name',
                's.last_name',
                'e.title',
                'e.start_date',
                'e.end_date',
                'e.id as doc_event_id',
                //'les.products_to_purchase',
                //'ldg.products_purchased',
                'ldg.status',
                'p.title as product_name',
                'pv.name as variation_name',
                'bought.total',
                'lep.min_quantity'
            ])
            ->asArray()
            ->leftJoin('doc_event e', 'e.id = :eventId', [
                ':eventId' => $eventId,
            ])
            ->leftJoin('ldg_event_student_status ldg', 'ldg.student_id = s.id AND ldg.doc_event_id = e.id')
            ->leftJoin('lnk_event_product lep', 'lep.doc_event_id = e.id')
            ->leftJoin('product p', 'p.id = lep.product_id')
            //->leftJoin('ldg_event_status les', 'les.doc_event_id = e.id ')
            ->leftJoin(['bought' => $totalQuery], 's.id = bought.student_id AND lep.product_id=bought.product_id')
            ->leftJoin('product_variation pv', 'pv.id = bought.product_variation_id')
            ->where(['s.id' => $studentIds])
            ->all();

        $eventStatistics = [];
        foreach ($statistics as $eventState) {
            $eventStatistics[] = new static($eventState);
        }
        return $eventStatistics;
    }


    public function getStatus()
    {
        if (empty($this->status)) {
            return self::STATUS_PENDING_REQUIRED;
        }
        return $this->status;
    }

    public function getStatusLabel()
    {
        return $this->statusNames[$this->getStatus()];
    }

    /**
     * @param $eventId
     * @return array|null|DocEvent
     * @throws NotFoundHttpException
     */
    protected static function findEvent($eventId)
    {
        $event = DocEvent::find()->where(['id' => $eventId])->one();
        if ($event === null) {
            throw new NotFoundHttpException();
        }
        return $event;
    }
}