<?php


namespace common\models\teacher\statistics;


use common\components\bl\teacher\EventStatisticHelper;
use common\models\EventProduct;
use common\models\Student;
use yii\base\Model;

class StockTheClassroomEventStatistic extends Model
{
    /** @var int */
    public $event_id;
    /** @var int */
    public $student_id;
    /** @var string */
    public $student_name;

    public $products;


    public static function getDonateProgress($eventId)
    {
        $data = EventProduct::find()
            ->alias('ep')
            ->select([
                'p.title',
                'ep.max_quantity',
                'sum(lap.quantity) as quantity',
            ])
            ->leftJoin('product p', 'p.id = ep.product_id')
            ->leftJoin('ldg_event_activity_purchase lap', 'lap.doc_event_id = :eventId AND lap.product_id = p.id', [
                ':eventId' => $eventId,
            ])
            ->groupBy('p.id, ep.id, lap.product_id')
            ->where(['ep.doc_event_id' => $eventId])
            ->asArray()
            ->all();

        return $data;
    }

    protected function loadByData($data)
    {
        $this->event_id = $data['event_id'];
        $this->student_id = $data['student_id'];
        $this->student_name = $data['first_name'] . ' ' . $data['last_name'];
        $this->products[] = $this->formatProduct($data['product_title'], $data['quantity']);
    }

    protected static function formatProduct($productTitle, $quantity)
    {
        $quantity = (int)$quantity;
        return "{$productTitle} x {$quantity}";
    }

    public static function findByEvent($eventId)
    {
        $studentIds = EventStatisticHelper::getStudentIds($eventId);

        $students = Student::find()
            ->alias('s')
            ->select([
                's.id as student_id',
                's.last_name',
                's.first_name',
                'e.id as event_id',
                'p.title as product_title',
                'lap.quantity',
            ])
            ->leftJoin('doc_event e', 'e.id = :eventId', [
                ':eventId' => $eventId,
            ])
            ->leftJoin('ldg_event_student_status ldg', 'ldg.student_id = s.id AND ldg.doc_event_id = :eventId', [
                ':eventId' => $eventId,
            ])
            ->leftJoin('lnk_event_product ep', 'ep.doc_event_id = :eventId', [
                ':eventId' => $eventId,
            ])
            ->leftJoin('product p', 'p.id = ep.product_id')
            ->leftJoin('ldg_event_activity_purchase lap', 'lap.doc_event_id = :eventId AND lap.student_id = s.id AND lap.product_id = p.id', [
                ':eventId' => $eventId,
            ])
            ->where(['s.id' => $studentIds])
            ->andWhere(['>', 'lap.quantity', 0])
            ->asArray()
            ->all();

        $eventStatistics = [];
        foreach ($students as $key => $eventState) {
            $model = new static();
            $model->loadByData($eventState);
            $eventStatistics[] = $model;
        }

        return $eventStatistics;
    }
}