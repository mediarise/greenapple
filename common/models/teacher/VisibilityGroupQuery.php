<?php

namespace common\models\teacher;

use common\helpers\ArrayHelper;
use common\models\Grade;
use common\models\Student;
use yii\db\ActiveQuery;
use common\models\VisibilityGroup;

/**
 * This is the ActiveQuery class for [[VisibilityGroup]].
 *
 * @see VisibilityGroup
 */
class VisibilityGroupQuery extends \yii\db\ActiveQuery
{
    public function linkedWithGrades(array $grades)
    {
        return $this->where(['in', 'grade_id', $grades]);
    }

    public function linkedWithEvent($eventId)
    {
        return $this->joinWith([
            'docEvent' => function (ActiveQuery $q2) use ($eventId) {
                $q2->alias('docEvent');
                $q2->where(['docEvent.id' => $eventId]);
                $q2->andWhere(['docEvent.id' => $eventId]);
            }
        ]);
    }

    public function visibleForTeacher(array $grades)
    {
        return $this
            ->alias('vg')
            ->distinct('vg.id')
            ->leftJoin(
                'visibility_group_member member',
                'member.member_type = :typeGrade AND member.visibility_group_id = vg.id',
                [':typeGrade' => VisibilityGroup::MEMBER_TYPE_GRADE]
            )
            ->andWhere([
                'or',
                ['in', 'vg.grade_id', $grades],
                ['in', 'member.member_id', $grades],
            ])
            ->andWhere(['!=', 'name', ''])
            ->andWhere(['!=', 'school_id', 0]);
    }

    /**
     * @inheritdoc
     * @return VisibilityGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VisibilityGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
