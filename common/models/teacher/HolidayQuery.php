<?php

namespace common\models\teacher;


use Yii;

class HolidayQuery extends \common\models\HolidayQuery
{
    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function restrictByRole()
    {
        $actor = Yii::$container->get('Actor');
        $this->andWhere(['school_id' => $actor['school_id']]);
    }
}