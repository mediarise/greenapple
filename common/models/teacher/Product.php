<?php

namespace common\models\teacher;

class Product extends \common\models\Product
{
    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
}
