<?php

namespace common\models\teacher;

/**
 * This is the ActiveQuery class for [[ProductQuery]].
 *
 * @see Product
 */
class ProductQuery extends \common\query\ProductQuery
{
    public function forTeacher($teacherUserId)
    {
        return $this->where(['t.owner_id' => [$teacherUserId, 0]]); // TODO: check that 0 is a right value
    }

    /**
     * @inheritdoc
     * @return Product[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Product|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
