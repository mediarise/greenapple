<?php

namespace common\models\teacher;


use common\models\DocEvent as DocEventBase;
use common\query\EventQuery;

class DocEvent extends DocEventBase
{
    public $student_count;
    public $forms_signed;
    public $forms_to_sign;
    public $products_purchased;
    public $products_to_purchase;

    public function getOrdersRatio()
    {
        return $this->getRatio($this->products_purchased, $this->getTotalOrders());
    }

    public function getFormsRatio()
    {
        return $this->getRatio($this->forms_signed, $this->getTotalForms());
    }

    private function getRatio($count, $to)
    {
        if (empty($to)) {
            return 2;
        }
        return $count / $to;
    }

    public function getTotalOrders()
    {
        if ($this->type == self::EVENT_TYPE_STOCK_THE_CLASSROOM) {
            return $this->products_to_purchase;
        }
        return $this->products_to_purchase * $this->student_count;
    }

    public function getProductPurchased()
    {
        switch ($this->type) {
            case self::EVENT_TYPE_NUTRITION :
                return $this->getNutritionProductPurchased();
                break;
            default:
                return $this->products_purchased;
        }
    }

    protected function getNutritionProductPurchased()
    {
        $data = DocEventBase::find()
            ->asArray()
            ->select([
                'sum(es.products_purchased) as products_purchased'
            ])
            ->alias('e')
            ->leftJoin('doc_event ce', 'ce.predecessor = e.id')
            ->leftJoin('ldg_event_status es', 'es.doc_event_id = ce.id')
            ->where(['e.id' => $this->id])
            ->groupBy('e.id')
            ->one();

        return $data['products_purchased'];
    }

    public function getType()
    {
        return $this->getAttribute('type');
    }

    public function getTotalForms()
    {
        return $this->forms_to_sign * $this->student_count;
    }

    public function isClosed()
    {
        return $this->getProductPurchased() == $this->getTotalOrders();
    }

    /**
     * @return EventQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        return \Yii::createObject(EventQuery::class, [get_called_class()]);
    }
}