<?php

namespace common\models\teacher\event;

use common\components\CommaDividedListConverter;
use common\helpers\ArrayHelper;
use common\models\EventProduct;
use common\models\Product;
use yii\data\ArrayDataProvider;

class ProductSearch extends Product
{
    public $actor;
    public $search;

    public function __construct($config)
    {
        parent::__construct($config);
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'teacherUserId', 'search'
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['teacherUserId', 'search'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @param array $linkedProducts
     * @param $checkedProducts
     * @return ArrayDataProvider
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params, $linkedProducts = [], $checkedProducts = null)
    {
        $actor = $this->actor;
        $query = Product::find()
            ->alias('t')
            ->forThisUser();

        $this->load($params);

        if (!$this->validate()) {
            // $query->where('0=1');
            return new ArrayDataProvider([
                'allModels' => []
            ]);
        }

        $query->andFilterWhere([
            'or',
            ['ilike', 't.sku', $this->search],
            ['ilike', 't.title', $this->search]
        ]);

        $checkedProducts = $checkedProducts ?? $linkedProducts; // current state on form or already saved state
        $checkedProductIds = ArrayHelper::getColumn($checkedProducts, 'product_id', false);

        $products = ArrayHelper::map($query->all(), null, function(Product $product) use ($checkedProducts, $checkedProductIds) {
            $eventProduct = $checkedProducts[$product['id']] ?? null;
            /* @var $eventForm EventProduct */
            return [
                'id' => $product->id,
                'sku' => $product->sku,
                'title' => $product->title,
                'price' => $eventProduct['price'] ?? $product->price,
                'min_quantity' => $eventProduct['min_quantity'] ?? 1,
                'max_quantity' => $eventProduct['max_quantity'] ?? 0,
                'checked' => ArrayHelper::isIn($product['id'], $checkedProductIds),
                'isFree' => $product->is_free,
            ];
        });

        usort($products, function($a, $b) {
            return $a['checked'] < $b['checked'];
        });

        return new ArrayDataProvider([
            'allModels' => $products,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
    }
}