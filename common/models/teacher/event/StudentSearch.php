<?php

namespace common\models\teacher\event;

use common\components\CommaDividedListConverter;
use common\helpers\ArrayHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Student;
use yii\data\ArrayDataProvider;

/**
 * Student represents the model behind the search form of `common\models\Student`.
 */
class StudentSearch extends Student
{
    public $teacherId;
    public $search;

    public function __construct($config)
    {
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacherId', 'search'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param array $checkedStudentIds
     * @return ArrayDataProvider
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params, $checkedStudentIds  = null)
    {
        $query = Student::find()
            ->alias('t')
            ->linkedWithTeacher($this->teacherId);

        $this->load($params);

        if (!$this->validate()) {
            // $query->where('0=1');
            return new ArrayDataProvider([
                'allModels' => []
            ]);
        }

        $query->andFilterWhere([
            'or',
            ['ilike', 't.first_name', $this->search],
            ['ilike', 't.last_name', $this->search],
            ['ilike', 't.oen', $this->search]
        ]);

        $students = ArrayHelper::map($query->all(), null, function($student) use ($checkedStudentIds) {
            /* @var $student \common\models\Student */
            return [
                'id' => $student->id,
                'first_name' => $student->getFullName(),
                'oen' => $student->oen,
                'checked' => ArrayHelper::isIn($student->id, $checkedStudentIds ?? [])
            ];
        });

        usort($students, function($a, $b) {
            return $a['checked'] < $b['checked'];
        });

        return new ArrayDataProvider([
            'allModels' => $students,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
    }
}
