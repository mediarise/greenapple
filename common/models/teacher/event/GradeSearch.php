<?php

namespace common\models\teacher\event;

use common\components\CommaDividedListConverter;
use common\helpers\ArrayHelper;
use common\models\Grade;
use yii\data\ArrayDataProvider;

class GradeSearch extends Grade
{
    public $teacherId;
    public $onlyThisYear;

    public function __construct($config)
    {
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['teacherId', 'onlyThisYear'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @param null $checkedGradeIds
     * @return ArrayDataProvider
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params, $checkedGradeIds = null)
    {
        $query = Grade::find()
            ->alias('t')
            ->linkedWithTeacher($this->teacherId)
            ->linkedWithStudent();

        $this->load($params);

        if (!$this->validate()) {
            // $query->where('0=1');
            return new ArrayDataProvider([
                'allModels' => []
            ]);
        }
        if ($this->onlyThisYear) {
            $query->andFilterWhere(['year' => Grade::getCurrentAcademicYear()]);
        }
        $grades = ArrayHelper::map($query->all(), null, function ($grade) use ($checkedGradeIds) {
            return [
                'id' => $grade->id,
                'name' => $grade->name,
                'year' => $grade->year,
                'checked' => ArrayHelper::isIn($grade->id, $checkedGradeIds ?? [])
            ];
        });

        usort($grades, function ($a, $b) {
            return $a['checked'] < $b['checked'];
        });

        return new ArrayDataProvider([
            'allModels' => $grades,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
    }
}