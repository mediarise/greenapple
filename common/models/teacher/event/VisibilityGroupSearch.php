<?php

namespace common\models\teacher\event;

use common\helpers\ArrayHelper;
use common\models\Grade;
use common\models\Student;
use common\models\teacher\VisibilityGroup;
use yii\data\ArrayDataProvider;

class VisibilityGroupSearch extends VisibilityGroup
{
    public $search;
    public $teacherId;

    public function __construct($config)
    {
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['search', 'teacherId'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @param $checkedGroupIds
     * @return ArrayDataProvider
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params, $checkedGroupIds = null)
    {
        $this->load($params);

        if (!$this->validate()) {
            return new ArrayDataProvider([
                'allModels' => []
            ]);
        }

        $grades = Grade::find()->linkedWithTeacher($this->teacherId)->linkedWithStudent()->all();
        $grades = ArrayHelper::getColumn($grades, 'id');
        $students = Student::find()->linkedWithTeacher($this->teacherId)->all();
        $students = ArrayHelper::getColumn($students, 'id');

        $query = VisibilityGroup::find()
            ->alias('t')
            ->select('t.*, vgm.visibility_group_id')
            ->leftJoin('visibility_group_member vgm', 't.id = vgm.visibility_group_id')
            ->leftJoin('student s', 'vgm.member_id = s.id')
            ->leftJoin('grade g', 'vgm.member_id = g.id')
            ->where([
                'or',
                ['in', 'g.id', $grades],
                ['in', 's.id', $students]
            ]);

        $query->andFilterWhere(['ilike', 't.name', $this->search]);

        $query->andWhere(['t.doc_event_id' => null]);
        $query->groupBy('t.id, vgm.visibility_group_id');

        $visibilityGroups = ArrayHelper::map($query->all(), null, function ($visibilityGroup) use ($checkedGroupIds) {
            return [
                'id' => $visibilityGroup->id,
                'name' => $visibilityGroup->name,
                'checked' => ArrayHelper::isIn($visibilityGroup->id, $checkedGroupIds ?? [])
            ];
        });

        usort($visibilityGroups, function ($a, $b) {
            return $a['checked'] < $b['checked'];
        });

        return new ArrayDataProvider([
            'allModels' => $visibilityGroups,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
    }
}