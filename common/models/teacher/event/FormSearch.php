<?php

namespace common\models\teacher\event;

use common\components\CommaDividedListConverter;
use common\helpers\ArrayHelper;
use common\models\Form;
use common\models\LnkEventForm;

use yii\data\ArrayDataProvider;

class FormSearch extends Form
{
    public $actor;
    public $search;

    public function __construct($config)
    {
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['teacherUserId', 'search'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @param array $linkedForms
     * @param array $checkedForms
     * @return ArrayDataProvider
     * @throws \yii\base\InvalidArgumentException
     * @internal param array $forms
     */
    public function search($params, $linkedForms = [], $checkedForms = null)
    {
        $actor = $this->actor;
        $query = Form::find()
            ->alias('t')
            ->restrictByBoard($actor->board->id)
            ->restrictBySchool($actor->school->id)
            ->restrictByUser($actor->user_id);

        $this->load($params);

        if (!$this->validate()) {
            // $query->where('0=1');
            return new ArrayDataProvider([
                'allModels' => []
            ]);
        }

        $query->andFilterWhere(['ilike', 't.title', $this->search]);

        $checkedForms = $checkedForms ?? $linkedForms; // current state on form or already saved state
        $checkedFormIds = ArrayHelper::getColumn($checkedForms, 'form_id', false);

        $forms = ArrayHelper::map($query->all(), null, function($form) use ($checkedForms, $checkedFormIds) {
            $eventForm = $checkedForms[$form['id']] ?? null;
            /* @var $eventForm LnkEventForm */
            return [
                'id' => $form['id'],
                'title' => $form['title'],
                'is_mandatory' => $eventForm['is_mandatory'] ?? false,
                'checked' => ArrayHelper::isIn($form['id'], $checkedFormIds)
            ];
        });

        usort($forms, function($a, $b) {
            return $a['checked'] < $b['checked'];
        });

        return new ArrayDataProvider([
            'allModels' => $forms,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
    }
}