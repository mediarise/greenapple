<?php

namespace common\models\teacher;

class Form extends \common\models\Form
{
    /**
     * @inheritdoc
     * @return FormQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FormQuery(get_called_class());
    }
}
