<?php

namespace common\models\teacher;

use common\models\VisibilityGroup;
use Yii;
use yii\base\Model;

class VisibilityGroupForm extends Model
{
    public $id;
    public $name;
    public $grade_id;
    public $students = [];
    public $grades = [];
    public $teachers = [];
    public $member_type;

    public function rules()
    {
        return [
            [['name'], 'required'],
            [
                'students', 'required',
                'when' => function (VisibilityGroupForm $model) {
                    return empty($model->grades) && empty($model->teachers);
                },
                'message' => 'You should select at least one student.'
            ],
            [
                'grades', 'required',
                'when' => function (VisibilityGroupForm $model) {
                    return empty($model->students) && empty($model->teachers);
                },
                'message' => 'You should select at least one grade.'
            ],
            [
                'teachers', 'required',
                'when' => function (VisibilityGroupForm $model) {
                    return empty($model->students) && empty($model->grades);
                },
                'message' => 'You should select at least one teacher.'
            ],

            ['grade_id', 'filter', 'filter' => 'intval', 'when' => function ($model) {
                return $model->grade_id;
            }],
            [['name'], 'string', 'min' => 2, 'max' => 255],
            [['id'], 'safe'],
            ['member_type', 'integer'],
        ];
    }

    public function beforeValidate()
    {
        if ($this->member_type != VisibilityGroup::MEMBER_TYPE_STUDENT) {
            $this->grade_id = null;
        }
        return parent::beforeValidate();
    }
}