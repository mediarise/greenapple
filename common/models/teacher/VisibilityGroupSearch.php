<?php

namespace common\models\teacher;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * VisibilityGroupSearch represents the model behind the search form of `common\models\VisibilityGroup`.
 */
class VisibilityGroupSearch extends VisibilityGroup
{
    public $grades = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'board_id', 'school_id', 'grade_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VisibilityGroup::find();

        $query->visibleForTeacher($this->grades);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->pagination->pageSize = 20;

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'board_id' => $this->board_id,
            'school_id' => $this->school_id,
            'grade_id' => $this->grade_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        return $dataProvider;
    }
}
