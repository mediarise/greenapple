<?php

namespace common\models\teacher;

class VisibilityGroup extends \common\models\VisibilityGroup
{
    /**
     * @inheritdoc
     * @return VisibilityGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VisibilityGroupQuery(get_called_class());
    }
}
