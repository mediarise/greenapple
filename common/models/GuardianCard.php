<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "guardian_card".
 *
 * @property int $id
 * @property int $guardian_id
 * @property string $token
 * @property string $mask
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Guardian $guardian
 */
class GuardianCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guardian_card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['guardian_id'], 'default', 'value' => null],
            [['guardian_id'], 'integer'],
            [['token', 'mask'], 'string', 'max' => 255],
            [['guardian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Guardian::class, 'targetAttribute' => ['guardian_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guardian_id' => 'Guardian ID',
            'token' => 'Token',
            'mask' => 'Mask',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuardian()
    {
        return $this->hasOne(Guardian::class, ['id' => 'guardian_id']);
    }
}
