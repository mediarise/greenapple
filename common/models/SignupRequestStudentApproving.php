<?php

namespace common\models;

use backend\exceptions\WrongSignupRequestStatusException;

class SignupRequestStudentApproving extends Student
{
    public $studentId;

    public function approve()
    {
        $studentTemplate = SignupRequestStudent::find()
            ->where(['signup_request_student.id' => $this->studentId])
            ->joinWith('SignupRequestGuardian')
            ->one();

        /* @var $studentTemplate \common\models\SignupRequestStudent */

        if (!$studentTemplate->isOpen()){
            throw new WrongSignupRequestStatusException();
        }

        $transaction = \Yii::$app->db->beginTransaction();
        $this->save();

        $studentTemplate->status = SignupRequestStudent::STATUS_APPROVED;
        $studentTemplate->student_id = $this->id;
        $studentTemplate->save();

        $guardianTemplate = $studentTemplate->guardianTemplate;
        $guardianTemplate->status = SignupRequestGuardian::STATUS_INCOMPLETE;
        $guardianTemplate->save();

        $transaction->commit();
    }

    public function loadFromSignupRequestStudent($id)
    {
        $studentTemplate = SignupRequestStudent::findOne($id);

        $this->attributes = $studentTemplate->attributes;
        $this->studentId = $studentTemplate->id;
    }
}