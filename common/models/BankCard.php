<?php

namespace common\models;

use yii\base\Model;

class BankCard extends Model
{
    public $number;
    public $expireDate;
    public $cvd;

    public function rules()
    {
        return [
            [['number', 'expireDate', 'cvd'], 'required'],
            [['number'], 'string', 'length' => 16],
            [['expireDate'], 'string', 'length' => 5],
            [['expireDate'], 'validateExpireDate'],
            [['cvd'], 'string', 'length' => 3],
            [['cvd'], 'number']
        ];
    }

    public function validateExpireDate($attribute)
    {
        if (mb_strpos($this->expireDate, '/') < 0) {
            $this->addError($attribute, 'format should be "MM/YY"');
            return;
        }

        list($month, $year) = mb_split('/', $this->expireDate);
        if (mb_strlen($month) != 2 && mb_strlen($year) != 2) {
            $this->addError($attribute, 'format should be "MM/YY"');
            return;
        }
        $month = (int)$month;
        if ($month < 1 || $month > 12) {
            $this->addError($attribute, 'wrong month');
            return;
        }
        $year = (int)$year;
        $currentYear = (int)date('y');
        if ($year < $currentYear) {
            $this->addError($attribute, 'wrong year');
            return;
        }
    }

    public function getExpDate()
    {
        list($month, $year) = mb_split('/', $this->expireDate);
        return $year . $month;
    }
}