<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.04.18
 * Time: 16:55
 */

namespace common\models;


use common\helpers\ArrayHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;

class FormSearch extends Form
{
    public $search;

    public function rules()
    {
        return [
            [['search'], 'string'],
            [['control'], 'integer']
        ];
    }

    public function search($params)
    {
        $query = Form::find()->with('lnkEventForms');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $user = Yii::$container->get('Actor');
        $board = $user->board;
        $school = $user->school;
        $query
            ->restrictByBoard($board->id)
            ->restrictBySchool($school->id)
            ->restrictByUser($user->user_id);

        if (!$this->load($params)) {
            return $dataProvider;
        }
        $query
            ->andFilterWhere([
                'or',
                ['ilike', 'title', $this->search],
                ['ilike', 'description', $this->search],
            ]);
        $query->andFilterWhere(['control' => $this->control]);
        return $dataProvider;
    }

    public function searchForEvent($params, $eventId)
    {
        $queryChecked = Form::find();
        $queryUnchecked = Form::find();
        $this->load($params);

        $user = Yii::$container->get('Actor');
        $board = $user->board;
        $school = $user->school;

        $queryChecked
            ->joinWith('lnkEventForms')
            ->where(['lnk_event_form.doc_event_id' => $eventId])
            ->andWhere([
                'or',
                ['owner_id' => $user->user_id],
                ['owner_id' => null]
            ])
            ->andWhere(['board_id' => $board->id])
            ->andWhere(['school_id' => $school->id])
            ->andFilterWhere(['ilike', 'form.title', $this->search]);

        $checkedData = $queryChecked->indexBy('id')->all();
        $checkedIds = array_keys($checkedData);

        $queryUnchecked
            ->where(['not in', 'id', $checkedIds])
            ->andWhere([
                'or',
                ['owner_id' => $user->user_id],
                ['owner_id' => null]
            ])
            ->andWhere(['board_id' => $board->id])
            ->andWhere(['school_id' => $school->id])
            ->andFilterWhere(['ilike', 'form.title', $this->search]);

        $uncheckedData = $queryUnchecked->indexBy('id')->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => ArrayHelper::merge($checkedData, $uncheckedData),
        ]);

        return $dataProvider;
    }
}