<?php

namespace common\models;

use common\components\bl\enum\ProductTax;
use common\components\validators\CostLessThanNetPriceValidator;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_variation".
 *
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property string $price
 * @property string $cost
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $product
 */
class ProductVariation extends \yii\db\ActiveRecord
{
    private static $formNameTemplate = 'ProductVariation[%s]';

    public $index = -1;
    public $toRemove = false;
    public $tax;

    const SCENARIO_PRODUCT_IS_FREE = 1;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_variation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['product_id', 'name', 'price', 'cost', 'index', 'created_at', 'updated_at'],
            self::SCENARIO_PRODUCT_IS_FREE => ['product_id', 'name', 'index', 'created_at', 'updated_at'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        //$isProductFree = $this->getScenario() === self::SCENARIO_PRODUCT_IS_FREE ? 'true' : 'false';
        return [
            [['name'], 'required'],
            [['price', 'cost'], 'required'],
            [['product_id'], 'default', 'value' => null],
            [['product_id'], 'integer'],
            [['price', 'cost'], 'match', 'pattern' => '/^[0-9]*(\.[0-9]+)?$/', 'on' => self::SCENARIO_DEFAULT],
            [
                'cost',
                CostLessThanNetPriceValidator::class,
                'price' => $this->price,
                'skipOnEmpty' => false,
                'when' => function ($model) {
                    return $model->tax == ProductTax::HST_13_INCLUDED;
                },
                'on' => self::SCENARIO_DEFAULT
            ],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['product_id', 'name'], 'unique', 'targetAttribute' => ['product_id', 'name'], 'message' => 'Please select unique name'],
            //[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['index'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'price' => 'Price',
            'cost' => 'Cost',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * {@inheritdoc}
     * @return ProductVariationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductVariationQuery(get_called_class());
    }

    public function formName()
    {
        $id = $this->isNewRecord ? $this->index : $this->id;
        return sprintf(self::$formNameTemplate, $id);
    }
}
