<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * This is the model class for table "visibility_group".
 *
 * @property int $id
 * @property int $doc_event_id
 * @property string $name
 * @property int $board_id
 * @property int $school_id
 * @property int $grade_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Board $board
 * @property Grade $grade
 * @property School $school
 */
class VisibilityGroup extends \yii\db\ActiveRecord
{
    const MEMBER_TYPE_STUDENT = 0x1;
    const MEMBER_TYPE_TEACHER = 0x2;
    const MEMBER_TYPE_GRADE = 0x4;
    const MEMBER_TYPE_SCHOOL = 0x8;
    const MEMBER_TYPE_VISIBILITY_GROUP = 0x10;

    public $members;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visibility_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['board_id', 'school_id', 'grade_id'], 'default', 'value' => null],
            [['board_id', 'school_id', 'grade_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['board_id'], 'exist', 'skipOnError' => true, 'targetClass' => Board::class, 'targetAttribute' => ['board_id' => 'id']],
            [['grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::class, 'targetAttribute' => ['grade_id' => 'id']],
            [['school_id'], 'exist', 'skipOnError' => true, 'targetClass' => School::class, 'targetAttribute' => ['school_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'board_id' => 'Board ID',
            'school_id' => 'School ID',
            'grade_id' => 'Grade ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoard()
    {
        return $this->hasOne(Board::class, ['id' => 'board_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::class, ['id' => 'grade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(School::class, ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::class, ['id' => 'member_id'])
            ->viaTable('visibility_group_member', ['visibility_group_id' => 'id'], function (ActiveQuery $q) {
                $q->where(['visibility_group_member.member_type' => VisibilityGroup::MEMBER_TYPE_STUDENT]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(Teacher::class, ['id' => 'member_id'])
            ->viaTable('visibility_group_member', ['visibility_group_id' => 'id'], function (ActiveQuery $q) {
                $q->where(['visibility_group_member.member_type' => VisibilityGroup::MEMBER_TYPE_TEACHER]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(Grade::class, ['id' => 'member_id'])
            ->viaTable('visibility_group_member', ['visibility_group_id' => 'id'], function (ActiveQuery $q) {
                $q->where(['visibility_group_member.member_type' => VisibilityGroup::MEMBER_TYPE_GRADE]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocEvent()
    {
        return $this->hasOne(DocEvent::class, ['id' => 'doc_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisibilityGroups()
    {
        return $this->hasMany(VisibilityGroup::class, ['id' => 'visibility_group_id'])
            ->viaTable('visibility_group_member', ['member_id' => 'id'], function (ActiveQuery $q) {
                $q->where(['visibility_group.doc_event_id' => null]);
                $q->andWhere(['visibility_group_member.member_type' => VisibilityGroup::MEMBER_TYPE_VISIBILITY_GROUP]);
            });
    }

    public function getMemberCount()
    {
        return (new Query())
            ->from('visibility_group_member')
            ->where(['visibility_group_id' => $this->id])
            ->count();
    }

    /**
     * @inheritdoc
     * @return VisibilityGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VisibilityGroupQuery(get_called_class());
    }
}
