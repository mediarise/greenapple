<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class LnkEventForm
 * @package common\models
 * @property integer $id
 * @property integer $doc_event_id
 * @property integer $product_id
 * @property float $price
 * @property float $cost
 * @property integer $min_quantity
 * @property integer $max_quantity
 * @property int $min_interval
 * @property int $created_at
 * @property int $updated_at
 *
 * @property DocEvent $docEvent
 * @property Product $product
 * @property float $priceWithTax
 */
class EventProduct extends ActiveRecord
{
    private $variation = null;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['due_date', 'product_id', 'min_quantity', 'max_quantity', 'price', 'cost'], 'safe'],
            [['min_quantity', 'max_quantity'], 'integer', 'min' => 0],
            ['min_quantity', 'quantityChecker']
        ];
    }

    public function quantityChecker($attribute)
    {
        if ($this->min_quantity > $this->max_quantity && $this->max_quantity > 0) {
            $this->addError($attribute, 'Min quantity should be less than max quantity or equal.');
            return false;
        }
    }

    /**
     * @param $data
     * @throws \yii\base\InvalidConfigException
     */
    public function loadFromArray($data)
    {
        foreach (static::getTableSchema()->columns as $column) {
            if (isset($data[$column->name])) {
                $this->{$column->name} = $data[$column->name];
            }
        }
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'lnk_event_product';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocEvent()
    {
        return $this->hasOne(DocEvent::class, ['id' => 'doc_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariations()
    {
        return $this->hasMany(ProductVariation::class, ['product_id' => 'id'])
            ->viaTable('product', ['id' => 'product_id']);
    }

    public function applyTax($price)
    {
        return $this->product->applyTax($price);
    }

    public function getVariation()
    {
        if (empty($this->variation)) {
            $product = $this->product;
            if (empty($this->bought_variation)) {
                $this->variation = $product->variations[0];
            } else {
                $variation = array_filter($product->variations, function (ProductVariation $model) {
                    return $model->id == $this->bought_variation;
                });
                $this->variation = array_shift($variation);
            }
        }
        return $this->variation;
    }

}