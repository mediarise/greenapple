<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.04.18
 * Time: 18:32
 */

namespace common\models;


use Yii;
use yii\data\ActiveDataProvider;

class GradeSearch extends Grade
{
    public $name;
    public $schoolName;
    public $boardName;
    public $year;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['year'], 'integer']
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Grade::find();
        $schoolAdminModel = SchoolAdmin::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($schoolAdminModel !== null) {
            $query->joinWith('school')->andWhere(['school.id' => $schoolAdminModel->school_id]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!$this->load($params)) {
            $query->andWhere(['year' => Grade::getCurrentAcademicYear()]);
            return $dataProvider;
        }
        $query->andFilterWhere(['year' => $this->year]);
        $query->andFilterWhere(['ILIKE', 'grade.name', $this->name]);

        return $dataProvider;
    }
}