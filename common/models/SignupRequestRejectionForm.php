<?php

namespace common\models;

use Yii;
use yii\base\Model;

class SignupRequestRejectionForm extends Model
{
    public $rejectReason;

    public function rules()
    {
        return [
            [['rejectReason'], 'string', 'min' => 30],
            [['rejectReason'], 'required']
        ];


    }

    public function reject($signupRequestId)
    {
        $signupRequest = SignupRequestGuardian::findOne($signupRequestId);

        $guardianName = $signupRequest->first_name . ' ' . $signupRequest->last_name;


        Yii::$app->mailer->compose([
            'html' => 'signupRequestRejected-html',
            'text' => 'signupRequestRejected-text'
        ], [
            'guardianName' => $guardianName,
            'rejectReason' => $this->rejectReason,
        ])
            ->
            setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($signupRequest->email)
            ->setSubject('Request rejected for ' . $guardianName)
            ->send();
    }
}