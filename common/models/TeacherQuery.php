<?php


namespace common\models;


use yii\db\ActiveQuery;

class TeacherQuery extends ActiveQuery
{
    public function inVisibilityGroup($visibilityGroupId)
    {
        return $this->joinWith([
            'visibilityGroups' => function (ActiveQuery $q) use ($visibilityGroupId) {
                $q->alias('visibilityGroup');
                $q->where(['visibilityGroup.id' => $visibilityGroupId]);
                return $q;
            }
        ]);
    }

    /**
     * @inheritdoc
     * @return Grade[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Grade|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}