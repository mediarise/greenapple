<?php

namespace common\models;

use common\components\bl\Greeting;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "signup_request".
 *
 * @property int $id
 * @property int $guardian_id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $status
 * @property string $note
 * @property integer $greeting
 * @property int $created_at
 * @property int $updated_at
 *
 * @property SignupRequestStudent[] $signupRequestStudents
 * @property string $statusName
 * @property Guardian $guardian
 */
class SignupRequestGuardian extends ActiveRecord
{
    const STATUS_NEW = 0x1;
    const STATUS_REJECTED = 0x2;
    const STATUS_NOT_INVITED = 0x4;
    const STATUS_CREATED = 0x8;
    const STATUS_INCOMPLETE = 0x10;
    const STATUS_COMPLETED = 0x20;
    const STATUS_INDEFINITE = 0x40;

    public $student_status;


    private static $guardianStatusNames = [
        self::STATUS_NEW => 'New',
        self::STATUS_REJECTED => 'Rejected',
        self::STATUS_NOT_INVITED => 'Not invited',
        self::STATUS_CREATED => 'Created',
        self::STATUS_INCOMPLETE => 'Incomplete',
        self::STATUS_COMPLETED => 'Completed',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'signup_request';
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name', 'status', 'greeting'], 'required'],
            [['email', 'first_name', 'last_name'], 'string', 'max' => 255],
            [['email', 'first_name', 'last_name'], 'trim'],

            [['email'], 'unique', 'message' => 'This email address has already been taken.'],
            [['email'], 'unique', 'targetClass' => User::class, 'message' => 'This email address has already been taken.'],

            [['guardian_id', 'status', 'greeting'], 'integer'],
            [['guardian_id'], 'unique'],
            [['guardian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Guardian::class, 'targetAttribute' => ['guardian_id' => 'id']],

            [['email'], 'email'],

            [['status'], 'in', 'range' => array_keys(self::$guardianStatusNames)],
            [['greeting'], 'in', 'range' => array_keys(Greeting::getNames())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'status' => 'Status',
            'statusName' => 'Status',
            'note' => 'Note',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSignupRequestStudents()
    {
        return $this->hasMany(SignupRequestStudent::class, ['signup_request_id' => 'id']);
    }

    public function getGuardian()
    {
        return $this->hasOne(Guardian::class, ['id' => 'guardian_id']);
    }

    /**
     * @param $id
     * @return ActiveQuery
     */
    public static function getSignupRequestStudentTemplateById($id)
    {
        return SignupRequestStudent::find()
            ->joinWith('grade')
            ->joinWith('grade.school')
            ->joinWith('grade.school.board')
            ->where(['signup_request_id' => $id]);
    }

    public function getStatusName()
    {
        return self::$guardianStatusNames[$this->status];
    }

    public function getAdminStatusName()
    {
        return self::$guardianStatusNames[$this->getAdminStatus()];
    }

    public static function getStatusNameByCode($code)
    {
        return self::$guardianStatusNames[$code];
    }

    public static function getGuardianStatusNames()
    {
        return self::$guardianStatusNames;
    }

    public function isOpen()
    {
        return $this->status == self::STATUS_NEW ||
            $this->status == self::STATUS_INCOMPLETE;
    }

    public function canApprove()
    {
        $canApprove = false;
        foreach ($this->signupRequestStudents as $student) {
            if ($student->isOpen()) {
                return false;
            }
            if ($student->status == SignupRequestStudent::STATUS_APPROVED) {
                $canApprove = true;
            }
        }
        return $canApprove;
    }

    /**
     * @return int
     */
    public function getAdminStatus()
    {
        if ($this->status !== self::STATUS_INDEFINITE) {
            return $this->status;
        }

        if ($this->testStudentStatusFlag(SignupRequestStudent::STATUS_NEW)) {
            return self::STATUS_INCOMPLETE;
        }

        if ($this->testStudentStatusFlag(SignupRequestStudent::STATUS_APPROVED)) {
            return empty($this->guardian->invite_token) ? self::STATUS_NOT_INVITED : self::STATUS_COMPLETED;
        }

        return self::STATUS_REJECTED;
    }

    private function testStudentStatusFlag($flag)
    {
        return ($this->student_status & $flag) == $flag;
    }
}
