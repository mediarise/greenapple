<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_photo".
 *
 * @property int $id
 * @property int $event_id
 * @property string $url
 * @property string $is_main
 * @property string $created_at
 * @property string $updated_at
 *
 * @property DocEvent $event
 */
class EventPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_photo';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'is_main'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['url'], 'string', 'max' => 255],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocEvent::class,
                'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'is_main' => 'Is main',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(DocEvent::class, ['id' => 'event_id']);
    }
}
