<?php

namespace common\models;

use yii\base\Model;

class EulaForm extends Model
{
    public $guardianId;
    public $agree = false;

    public function rules()
    {
        return [
            [['agree', 'guardianId'], 'required'],
            [['agree'], 'boolean'],
            [['agree'], 'compare', 'compareValue' => true, 'message' => 'you must agree to the eula']
        ];
    }
}