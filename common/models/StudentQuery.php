<?php

namespace common\models;

use yii\db\ActiveQuery;

class StudentQuery extends ActiveQuery
{
    public function linkedWithTeacher($teacherId)
    {
        return $this->joinWith([
            'grade' => function(ActiveQuery $q) use ($teacherId) {
                $q->alias('grade');
                $q->joinWith([
                    'teachers' => function(ActiveQuery $q2) use ($teacherId) {
                        $q2->alias('teacher');
                        return $q2->where(['teacher.id' => $teacherId]);
                    }
                ]);
            }
        ]);
    }

    public function linkedWithEvent($eventId)
    {
        return $this->joinWith([
            'visibilityGroups' => function(ActiveQuery $q) use ($eventId) {
                $q->alias('visibilityGroup');
                $q->joinWith([
                    'docEvent' => function(ActiveQuery $q2) use ($eventId) {
                        $q2->alias('docEvent');
                        $q2->where(['docEvent.id' => $eventId]);
                    }
                ]);
            }
        ]);
    }

    public function inVisibilityGroup($visibilityGroupId)
    {
        return $this->joinWith([
            'visibilityGroups' => function(ActiveQuery $q) use ($visibilityGroupId) {
                $q->alias('visibilityGroup');
                $q->where(['visibilityGroup.id' => $visibilityGroupId]);
                return $q;
            }
        ]);
    }

    /**
     * @inheritdoc
     * @return Student[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Student|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
