<?php

namespace common\models;


use yii\base\Model;

class SchoolForm extends Model
{
    public $boardId;
    public $schoolId;
    public $gradeId;

    public function loadFromGrade($grade)
    {
        $this->gradeId = $grade->id;
        $this->schoolId = $grade->school_id;
        $this->boardId = $grade->school->board_id;
    }

    public function rules()
    {
        return [
            [['boardId', 'schoolId', 'gradeId'], 'safe']
        ];
    }
}