<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.04.18
 * Time: 18:45
 */

namespace common\models;

use common\components\Formatter;
use common\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "product".
 *
 * @property string $dueDate
 * @property string $endDate
 * @property string $endTime
 *
 * @property string $mainPhoto
 */
class DocEventForm extends DocEvent
{
    public $date;
    public $time;
    public $mainPhotoUrl;
    public $useDuration = true;

    const SCENARIO_UPDATE = 'update';

    public function afterFind()
    {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->formatBooleanAttributesValue();
            if ($this->duration_unit == null) {
                $this->useDuration = false;
            }
        }
    }

    public function scenarios()
    {
        $commonFields = [
            'creator_id', 'description', 'due_lead_period', 'due_unit', 'dueDate', 'duration', 'duration_unit',
            'end_date', 'endDate', 'endTime', 'featured_image', 'frequency', 'is_parent_action_required',
            'is_repeatable', 'is_required', 'ledger_account_id', 'mainPhotoUrl', 'repeat_on_days', 'repeatOnDays',
            'start_date', 'startDate', 'startTime', 'title', 'umbrella_account', 'repeat_on_week',
        ];
        return [
            self::SCENARIO_DEFAULT => array_merge($commonFields, ['type']),
            self::SCENARIO_UPDATE => array_merge($commonFields, [
                'fundraising_message', 'fundraising_min', 'fundraising_max', 'fundraising_step'
            ]),
        ];
    }

    public function rules()
    {
        return [
            [['title', 'type', 'startDate', 'ledger_account_id', 'umbrella_account'], 'required'],

            [['id', 'creator_id', 'ledger_account_id', 'umbrella_account', 'duration', 'duration_unit', 'frequency',
                'repeat_on_days', 'repeat_on_week', 'due_lead_period', 'due_unit'], 'integer'],

            [['fundraising_min', 'fundraising_max', 'fundraising_step'], 'number'],
            [['fundraising_min', 'fundraising_max', 'fundraising_step'], 'default', 'value' => 0],
            [['duration', 'due_lead_period'], 'default', 'value' => 1],
            [['title', 'description', 'featured_image', 'start_date', 'end_date', 'startTime'], 'string'],
            [['start_date'], 'earlyThanEndDate', 'params' => ['errorAttribute' => 'startDate']],
            [['dueDate'], 'earlyThanEndDate', 'when' => function (DocEventForm $model) {
                return !$model->is_repeatable;
            }],
            [['is_required', 'is_repeatable', 'is_parent_action_required',], 'boolean'],
            [['mainPhotoUrl', 'repeatOnDays', 'umbrella_account', 'fundraising_message'], 'safe'],
            [
                ['duration_unit'], 'required',
                'when' => function (DocEventForm $model) {
                    return $model->end_date == '';
                },
                'whenClient' => 'function (attribute, value) {
                    return $("#' . Html::getInputId($this, 'endDate') . '").val() == ""
                    || $("#' . Html::getInputId($this, 'endTime') . '").val() == "";
                }',
                'message' => 'Fields "{attribute}" must be filled'
            ],
            [
                ['endDate', 'endTime'], 'required',
                'when' => function (DocEventForm $model) {
                    return $model->duration == '' && $model->duration_unit == '';
                },
                'whenClient' => 'function (attribute, value) {
                    return $("#' . Html::getInputId($this, 'duration_unit') . '").val() == "";
                }',
                'message' => 'Field "{attribute}" must be filled'
            ],

            [
                ['dueDate'], 'required',
                'when' => function (DocEventForm $model) {
                    return !$model->is_repeatable;
                },
                'whenClient' => 'isOneTimeEvent',
                'message' => '"{attribute}" must be filled',
            ],

            [
                ['due_lead_period', 'due_unit', 'frequency'], 'required',
                'when' => function (DocEventForm $model) {
                    return $model->is_repeatable;
                },
                'whenClient' => 'isRecurringEvent',
                'message' => '"{attribute}" must be filled'
            ],
            [
                ['repeatOnDays'], 'required',
                'when' => function (DocEventForm $model) {
                    return $model->is_repeatable;
                },
                'whenClient' => 'isRecurringEvent',
                'message' => '"Days" must be chosen'
            ],
            [
                ['repeat_on_week'], 'required',
                'when' => function (DocEventForm $model) {
                    return $model->is_repeatable && $model->frequency == self::FREQUENCY_MONTHLY;
                },
                'message' => '"Week" must be chosen'
            ],
            [
                ['fundraising_step'], 'validateStep',
                'when' => function (DocEventForm $model) {
                    return $model->type == self::EVENT_TYPE_FUNDRAISING;
                },
            ]

        ];
    }

    public function validateStep($attribute)
    {
        if ($this->fundraising_step == 0 || $this->fundraising_max == 0) {
            return true;
        }

        if ($this->fundraising_min + $this->fundraising_step > $this->fundraising_max) {
            $this->addError($attribute, 'min + step must be less than max.');
            return false;
        }
        return true;
    }

    public function earlyThanEndDate($attribute, $params)
    {
        $endDate = new \DateTime($this->end_date);

        $diff = $endDate->diff(new \DateTime($this->{$attribute}));

        if ($diff->invert < 1) {
            $errorAttribute = ArrayHelper::getValue($params, 'errorAttribute', $attribute);
            $this->addError($errorAttribute, $this->getAttributeLabel($errorAttribute) . ' must be early than end date.');
            return false;
        }

        return true;
    }

    public function beforeValidate()
    {
        $this->calculateEndDate();
        $this->setIsRepeatableValue();
        return parent::beforeValidate();
    }

    protected function calculateEndDate()
    {
        if (!$this->duration_unit) {
            return false;
        }
        $endDate = new \DateTime($this->start_date);
        switch ($this->duration_unit) {
            case self::DURATION_UNIT_MINUTE :
                $endDate->modify("+{$this->duration} minutes");
                break;
            case self::DURATION_UNIT_HOUR :
                $endDate->modify("+{$this->duration} hours");
                break;
            case self::DURATION_UNIT_DAY :
                $endDate->modify("+{$this->duration} days");
                break;
        }
        $this->end_date = $endDate->format(Formatter::FULL_DATE_FORMAT);
    }

    protected function formatBooleanAttributesValue()
    {
        $this->is_repeatable = intval($this->is_repeatable);
        $this->is_parent_action_required = intval($this->is_parent_action_required);
    }

    protected function setIsRepeatableValue()
    {
        $this->is_repeatable = $this->isRecurring();
    }

    public function getDueDate()
    {
        $dueDate = $this->due_date;
        if (empty($dueDate)) {
            return Formatter::date(new \DateTime('tomorrow'));
        }
        return Formatter::date($dueDate);
    }

    public function setDueDate($value)
    {
        $t = new \DateTime($this->due_date);
        $ndt = New \DateTime($value);
        $ndt->modify($t->format('H:i:s'));
        $this->due_date = $ndt->format(Formatter::FULL_DATE_FORMAT);
    }

    public function getEndDate()
    {
        $endDate = $this->end_date;
        if (empty($endDate)) {
            return Formatter::date(new \DateTime('tomorrow'));
        }
        return Formatter::date($endDate);
    }

    public function getEndTime()
    {
        return Formatter::time($this->end_date);
    }

    public function setEndDate($value)
    {
        $t = new \DateTime($this->end_date);
        $ndt = New \DateTime($value);
        $ndt->modify($t->format('H:i'));
        $this->end_date = $ndt->format(Formatter::FULL_DATE_FORMAT);
    }

    public function setEndTime($value)
    {
        $t = new \DateTime($this->end_date);
        $ndt = New \DateTime($value);
        $ndt->modify($t->format('Y-m-d'));
        $this->end_date = $ndt->format(Formatter::FULL_DATE_FORMAT);
    }

    public function getStartDate()
    {
        $start_date = $this->start_date;
        if (empty($start_date)) {
            return Formatter::date(new \DateTime('tomorrow'));
        }
        return Formatter::date($this->start_date);
    }

    public function getStartTime()
    {
        return Formatter::time($this->start_date);
    }

    public function setStartDate($value)
    {
        $t = new \DateTime($this->start_date);
        $ndt = New \DateTime($value);
        $ndt->modify($t->format('H:i'));
        $this->start_date = $ndt->format(Formatter::FULL_DATE_FORMAT);
    }

    public function setStartTime($value)
    {
        $t = new \DateTime($this->start_date);
        $ndt = New \DateTime($value);
        $ndt->modify($t->format('Y-m-d'));
        $this->start_date = $ndt->format(Formatter::FULL_DATE_FORMAT);
    }


}