<?php

namespace common\models;


use common\models\Guardian;
use common\models\User;
use frontend\base\EulaNotAcceptedException;
use frontend\base\UserAlreadyRegisteredException;
use yii\base\InvalidArgumentException;

class InviteForm extends ResetPasswordForm
{
    /** @noinspection PhpMissingParentConstructorInspection */
    /**
     * InviteForm constructor.
     * @param string $token
     * @param array $config
     * @throws EulaNotAcceptedException
     * @throws UserAlreadyRegisteredException
     */
    public function __construct(string $token, array $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Confirmation token cannot be blank.');
        }

        $this->_user = Guardian::findUserByInviteToken($token);

        if (!$this->_user) {
            throw new InvalidArgumentException('Wrong confirmation token.');
        }

        if ($this->_user->status == User::STATUS_EULA_NOT_ACCEPTED) {
            throw new EulaNotAcceptedException($this->_user->id);
        }

        if ($this->_user->status == User::STATUS_ACTIVE){
            throw new UserAlreadyRegisteredException();
        }

        if ($this->_user->status != User::STATUS_INVITED) {
            throw new InvalidArgumentException('Wrong confirmation token.');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function accept()
    {
        $this->_user->status = User::STATUS_EULA_NOT_ACCEPTED;
        return $this->resetPassword();
    }
}