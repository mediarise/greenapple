<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "purchase_transaction".
 *
 * @property int $id
 * @property int $guardian_card_id
 * @property int $amount
 * @property int $response_code
 * @property int $iso
 * @property string $date_time
 * @property int $type
 * @property string $number
 * @property string $created_at
 * @property string $updated_at
 *
 * @property GuardianCard $guardianCard
 */
class PurchaseTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_transaction';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['guardian_card_id', 'amount', 'response_code', 'iso', 'date_time', 'type', 'number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guardian_card_id' => 'Guardian Card ID',
            'amount' => 'Amount',
            'response_code' => 'Response Code',
            'iso' => 'Iso',
            'date_time' => 'Date Time',
            'type' => 'Type',
            'number' => 'Number',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuardianCard()
    {
        return $this->hasOne(GuardianCard::class, ['id' => 'guardian_card_id']);
    }
}
