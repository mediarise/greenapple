<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.04.18
 * Time: 17:31
 */

namespace common\models\search;

use common\models\SignupRequestGuardian as SignupRequestGuardianModel;
use yii\data\ActiveDataProvider;


class SignupRequestGuardian extends SignupRequestGuardianModel
{
    public $schoolAdmin;
    public $search;
    public $status;

    public function rules()
    {
        return [
            [['search'], 'string'],
            [['status'], 'integer'],
        ];
    }

    public function search($params)
    {
        $query = SignupRequestGuardianModel::find()
            ->select(['signup_request.*', 'bit_or(signup_request_student.status) AS student_status'])
            ->joinWith('signupRequestStudents', false)
            ->joinWith('signupRequestStudents.grade', false)
            ->where([
                'and', [
                    'grade.school_id' => $this->schoolAdmin->school_id
                ], [
                    'not', [
                        'signup_request.status' => SignupRequestGuardianModel::STATUS_CREATED
                    ]
                    /*'or',
                    ['signup_request.status' => SignupRequestGuardian::STATUS_NEW],
                    ['signup_request_student.status' => SignupRequestStudent::STATUS_NEW]*/
                ]
            ])
            ->groupBy(['signup_request.id']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        if (!$this->load($params)) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'or',
            ['ilike', 'signup_request.email', $this->search],
            ['ilike', 'signup_request.first_name', $this->search],
            ['ilike', 'signup_request.last_name', $this->search],
        ]);
        $query->andFilterWhere(['signup_request.status' => $this->status]);
        return $dataProvider;
    }
}