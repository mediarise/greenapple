<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VisibilityGroup as VisibilityGroupModel;


/**
 * VisibilityGroup represents the model behind the search form of `common\models\VisibilityGroup`.
 */
class VisibilityGroup extends Model
{
    public $search;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VisibilityGroupModel::find();
        $query->where([
            'and',
            ['!=', 'board_id', 0],
            ['!=', 'school_id', 0],
            ['!=', 'name', '']
        ]);

//        VarDumper::dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->sort = [
            'defaultOrder' => [
                'updated_at' => SORT_DESC
            ]
        ];
        $query->andFilterWhere(['ilike', 'name', $this->search]);

        return $dataProvider;
    }
}
