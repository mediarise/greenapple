<?php

namespace common\models\search;

use common\components\bl\DbContext;
use yii\data\ActiveDataProvider;
use common\models\School as SchoolModel;

/**
 * School represents the model behind the search form of `common\models\School`.
 */
class School extends SchoolModel
{
    /** @var DbContext */
    private $dbContext;

    public function __construct(DbContext $dbContext, array $config = [])
    {
        parent::__construct($config);
        $this->dbContext = $dbContext;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->dbContext->findSchool();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['ilike', 'name', $this->name]);

        return $dataProvider;
    }
}
