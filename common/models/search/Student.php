<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Student as StudentModel;

/**
 * Student represents the model behind the search form of `common\models\Student`.
 */
class Student extends StudentModel
{
    public $search;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StudentModel::find()
            ->joinWith('grade')
        ->joinWith('grade.school')
        ->joinWith('grade.school.board');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'or',
            ['ilike', 'last_name', $this->search],
            ['ilike', 'first_name', $this->search],
            ['ilike', 'oen', $this->search],
            ['ilike', 'board.name', $this->search],
            ['ilike', 'school.name', $this->search],
            ['ilike', 'grade.name', $this->search],
        ]);

        return $dataProvider;
    }
}
