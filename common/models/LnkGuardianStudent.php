<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lnk_guardian_student".
 *
 * @property int $id
 * @property int $guardian_id
 * @property int $student_id
 * @property int $relationship
 *
 * @property Guardian $guardian
 * @property Student $student
 */
class LnkGuardianStudent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lnk_guardian_student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['guardian_id', 'student_id', 'relationship'], 'default', 'value' => null],
            [['guardian_id', 'student_id', 'relationship'], 'integer'],
            [['relationship'], 'required'],
            [['guardian_id', 'student_id'], 'unique', 'targetAttribute' => ['guardian_id', 'student_id']],
            [['guardian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Guardian::className(), 'targetAttribute' => ['guardian_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guardian_id' => 'Guardian ID',
            'student_id' => 'Student ID',
            'relationship' => 'Relationship',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuardian()
    {
        return $this->hasOne(Guardian::className(), ['id' => 'guardian_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }
}
