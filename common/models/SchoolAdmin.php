<?php

namespace common\models;

use common\components\bl\Actor;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "school_admin".
 *
 * @property int $id
 * @property int $user_id
 * @property int $school_id
 * @property string $last_name
 * @property string $first_name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property School $school
 * @property User $user
 */
class SchoolAdmin extends Actor
{
    public $role = 'schoolAdmin';
    public $board_id;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'school_id'], 'default', 'value' => null],
            [['user_id', 'school_id'], 'integer'],
            [['user_id'], 'unique'],
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name'], 'string'],
            [['school_id'], 'exist', 'skipOnError' => true, 'targetClass' => School::class, 'targetAttribute' => ['school_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'school_id' => 'School ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(School::class, ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoard()
    {
        return $this->hasOne(Board::class, ['id' => 'board_id'])->via('school');
    }
}
