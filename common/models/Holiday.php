<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "holiday".
 *
 * @property int $id
 * @property int $board_id
 * @property int $school_id
 * @property string $start_date
 * @property string $end_date
 * @property string $title
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Board $board
 * @property School $school
 */
class Holiday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() { return date('Y-m-d H:i:s'); },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'holiday';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['board_id', 'start_date', 'end_date', 'title'], 'required'],
            [['board_id', 'school_id'], 'default', 'value' => null],
            [['board_id', 'school_id'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['start_date', 'end_date'], 'unique'],
            [['board_id'], 'exist', 'skipOnError' => true, 'targetClass' => Board::class, 'targetAttribute' => ['board_id' => 'id']],
            [['school_id'], 'exist', 'skipOnError' => true, 'targetClass' => School::class, 'targetAttribute' => ['school_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'board_id' => 'Board ID',
            'school_id' => 'School ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoard()
    {
        return $this->hasOne(Board::class, ['id' => 'board_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(School::class, ['id' => 'school_id']);
    }

    /**
     * {@inheritdoc}
     * @return HolidayQuery the active query used by this AR class.
     */
    public static function find()
    {
        return Yii::createObject(HolidayQuery::class, [get_called_class()]);
    }
}
