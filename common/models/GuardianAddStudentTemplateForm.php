<?php

namespace common\models;

class GuardianAddStudentTemplateForm extends SignupRequestStudent
{
    public $boardId;
    public $schoolId;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['boardId', 'schoolId'], 'required'],
            [['schoolId'], 'exist', 'skipOnError' => true, 'targetClass' => School::class, 'targetAttribute' => ['schoolId' => 'id']],
            [['boardId'], 'exist', 'skipOnError' => true, 'targetClass' => Board::class, 'targetAttribute' => ['boardId' => 'id']],
        ]);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $this->status = self::STATUS_NEW;
        return parent::save($runValidation, $attributeNames);
    }

}