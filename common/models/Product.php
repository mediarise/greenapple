<?php

namespace common\models;

use common\components\bl\enum\ProductTax;
use common\components\validators\CostLessThanNetPriceValidator;
use common\exception\LogicException;
use common\query\ProductQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $sku
 * @property string $title
 * @property string $description
 * @property string $is_free
 * @property string $price
 * @property int $tax
 * @property string $cost
 * @property integer $board_id
 * @property integer $school_id
 * @property integer $grade_id
 * @property integer $owner_id
 * @property int $supplier_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property string $mainPhoto
 * @property ProductVariation[] $variations
 */
class Product extends \yii\db\ActiveRecord
{
    const PREDEFINED_DONATION = 'donation';

    const SCENARIO_UPDATE = 'update';

    public $mainPhotoUrl;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'sku', 'title', 'description', 'price', 'cost', 'tax', 'is_free', 'mainPhotoUrl',
                'board_id', 'school_id', 'grade_id', 'supplier_id'
            ],
            self::SCENARIO_UPDATE => [
                'sku', 'title', 'description', 'price', 'cost', 'tax', 'mainPhotoUrl',
                'board_id', 'school_id', 'grade_id', 'supplier_id'
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {

            // todo for production leave only ` $this->setJunctionData(); `
            if (Yii::$app->id != 'app-console') {
                $this->setJunctionData();
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            ['is_free', 'required', 'on' => self::SCENARIO_DEFAULT],
            ['is_free', 'boolean', 'on' => self::SCENARIO_DEFAULT],
            [['price', 'cost', 'tax'], 'filter', 'filter' => function ($value) {
                return $this->is_free ? null : $value;
            }],
            [
                'cost',
                CostLessThanNetPriceValidator::class,
                'price' => $this->price,
                'skipOnEmpty' => false,
                'when' => function ($model) {
                    return !$model->is_free && $model->tax == ProductTax::HST_13_INCLUDED;
                }
            ],
            [
                ['price', 'cost', 'tax'], 'required',
                'when' => function ($model) {
                    return !$model->is_free;
                },
                'whenClient' => 'function (attribute, value) {
                    return !$("#' . Html::getInputId($this, 'is_free') . '").prop("checked");
                }'
            ],
            [['price', 'cost'], 'match', 'pattern' => '/^[0-9]*(\.[0-9]+)?$/', 'when' => function ($model) {
                return !$model->is_free;
            }],
            ['tax', 'in', 'range' => ProductTax::list(), 'when' => function ($model) {
                return !$model->is_free;
            }],
            ['sku', 'string', 'min' => 10, 'max' => 50],
            ['title', 'string', 'min' => 2, 'max' => 255],
            ['description', 'string', 'max' => 255],
            ['mainPhotoUrl', 'safe'],
            [['board_id', 'school_id', 'grade_id', 'supplier_id'], 'integer'],
            [['price'], 'notZeroForNonFree'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'SKU',
            'title' => 'Title',
            'description' => 'Description',
            'price' => 'Price',
            'tax' => 'Tax',
            'cost' => 'Cost',
            'supplier_id' => 'Supplier'
        ];
    }

    protected function setJunctionData()
    {
        $user = Yii::$container->get('Actor');
        $board = isset($user->board) ? $user->board : null;
        $school = isset($user->school) ? $user->school : null;

        $this->school_id = $school !== null ? $school->id : null;
        $this->board_id = $board !== null ? $board->id : null;
        $this->owner_id = $user->role === 'teacher' ? $user->user_id : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainPhoto()
    {
        return $this->hasOne(ProductPhoto::class, ['product_id' => 'id'])
            ->andOnCondition(['is_main' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPhotos()
    {
        return $this->hasMany(ProductPhoto::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLnkEventProducts()
    {
        return $this->hasMany(EventProduct::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(DocEvent::class, ['id' => 'doc_event_id'])
            ->via('lnkEventProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariations()
    {
        return $this->hasMany(ProductVariation::class, ['product_id' => 'id'])->orderBy('id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::class, ['id' => 'owner_id']);
    }

    public function getVariationNames()
    {
        static $names = null;
        if (empty($names)) {
            $names = ArrayHelper::map($this->variations, 'id', 'name');
        }
        return $names;
    }

    public function getVariationPrices()
    {
        static $prices = null;
        if (empty($prices)) {
            $prices = ArrayHelper::map($this->variations, 'id', 'price');
        }
        return $prices;
    }

    public function getVariationPrice($variationId)
    {
        $prices = $this->getVariationPrices();

        return $prices[$variationId];
    }

    public function getFinalPrice($variationId)
    {
        if (empty($variationId)) {
            return $this->price;
        }

        return $this->getVariationPrice($variationId);
    }

    public static function getTaxVariants()
    {
        return ProductTax::titles();
    }

    public function getTaxRatio()
    {
        if (empty($this->tax)) {
            return 1;
        }

        return ProductTax::rate($this->tax);
    }

    public function applyTax($price)
    {
        $priceIncludingTax = $price * $this->getTaxRatio();
        if ($this->tax == ProductTax::HST_13_INCLUDED && $priceIncludingTax < $this->cost) {
            throw new LogicException('Cost must be less that net price.');
        }

        return $priceIncludingTax;
    }

    public static function getFundraisingProduct()
    {
        return self::findOne(['title' => self::PREDEFINED_DONATION]);
    }

    public function notZeroForNonFree($attribute)
    {
        if (!$this->is_free && $this->$attribute == 0) {
            $this->addError($attribute, "{$attribute} can not be zero for non-free product");
        }
    }

    /**
     * @return ProductQuery
     */
    public static function find()
    {
        return Yii::createObject(ProductQuery::class, [get_called_class()]);
    }
}
