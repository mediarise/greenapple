<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class DocEventSearch extends Model
{
    public function search($params)
    {
        $user = Yii::$container->get('Actor');
        $board = $user->board;
        $school = $user->school;

        $query = teacher\DocEvent::find()
            ->onlyParents()
            //->restrictByBoard($user->board->id)
            //->restrictBySchool($user->school->id)
            ->restrictByUser($user->user_id)
            ->orderBy('abs(extract(epoch from age(start_date)))');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);
        if (!$this->load($params)) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}