<?php

namespace common\models;

use yii\db\ActiveQuery;
use common\models\VisibilityGroup;

/**
 * This is the ActiveQuery class for [[VisibilityGroup]].
 *
 * @see VisibilityGroup
 */
class VisibilityGroupQuery extends \yii\db\ActiveQuery
{
    public function linkedWithTeacher($teacherId)
    {
        return $this->joinWith([
            'teachers' => function(ActiveQuery $q) use ($teacherId) {
                $q->alias('teacher');
                return $q->where(['teacher.user_id' => $teacherId]);
            }
        ]);
    }

    /**
     * @inheritdoc
     * @return VisibilityGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VisibilityGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
