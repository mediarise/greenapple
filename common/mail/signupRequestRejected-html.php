<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $guardianName string */
/** @var string $rejectionReasoning */
?>

<div class="password-reset">
    <p>Hello <?= Html::encode($fullName) ?>,</p>

    <p>Your signup request was rejected.</p>

    <p><?= $rejectionReasoning ?></p>
</div>
