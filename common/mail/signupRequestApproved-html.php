<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $guardianName string */
/** @var string $rejectionReasoning */
/** @var string $inviteToken */
/** @var string $fullName */
$inviteLink = Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/signup/invite', 'inviteToken' => $inviteToken]);
?>

<div class="password-reset">
    <p>Hello <?= Html::encode($fullName) ?>,</p>

    <p>Follow the link below to continue:</p>

    <p><?= Html::a(Html::encode($inviteLink), $inviteLink) ?></p>
</div>

