<?php

/* @var $this yii\web\View */
/* @var $guardianName string */

$inviteLink = Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/signup/invite', 'inviteToken' => $inviteToken]);

?>

Hello <?= $fullName ?>

Your signup request is rejected

Follow the link below to continue:

<?= $inviteLink ?>