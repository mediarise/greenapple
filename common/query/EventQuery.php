<?php

namespace common\query;

class EventQuery extends \yii\db\ActiveQuery
{
    public function onlyParents()
    {
        return $this->andWhere('predecessor IS NULL');
    }
    /**
     * @param $boardId
     * @return EventQuery
     */
    public function restrictByBoard($boardId)
    {
        return $this->andWhere('coalesce(board_id, :board_id) = :board_id', [
            ':board_id' => $boardId
        ]);
    }

    /**
     * @param $schoolId
     * @return EventQuery
     */
    public function restrictBySchool($schoolId)
    {
        return $this->andWhere('coalesce(school_id, :school_id) = :school_id', [
            ':school_id' => $schoolId
        ]);
    }

    /**
     * @param $userId
     * @return EventQuery
     */
    public function restrictByUser($userId)
    {
        return $this->andWhere('coalesce(creator_id, :creator_id) = :creator_id', [
            ':creator_id' => $userId
        ]);
    }

    /**
     * @return EventQuery
     */
    public function upcoming()
    {
        return $this
            ->select([
                'e.id',
                'start_date',
                'end_date',
                'title',
                'type',
                'student_count',
                'forms_signed',
                'forms_to_sign',
                'products_purchased',
                'products_to_purchase'
            ])
            ->alias('e')
            ->leftJoin('get_event_member_count() ec', 'e.id=ec.event_id')
            ->leftJoin('ldg_event_status l', 'e.id=l.doc_event_id')
            ->andWhere('event_is_active(e)');
    }

    public function filterByProductId($productId)
    {
        if ($productId == null) {
            return $this;
        }
        return $this
            ->alias('e')
            ->leftJoin('lnk_event_product ep', 'e.id=ep.doc_event_id')
            ->andWhere(['ep.product_id' => $productId]);
    }

    public function forThisUser()
    {
        $actor = \Yii::$container->get('Actor');

        return $this
            ->onlyParents()
            ->restrictByUser($actor->user_id);

    }
}