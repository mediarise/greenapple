<?php

namespace common\query;

class ProductQuery extends \yii\db\ActiveQuery
{
    public function withoutPredefined()
    {
        return $this->andWhere('sku IS NOT NULL');
    }

    /**
     * @param $boardId
     * @return ProductQuery
     */
    public function restrictByBoard($boardId)
    {
        return $this->andWhere('coalesce(board_id, :board_id) = :board_id', [
            ':board_id' => $boardId
        ]);
    }

    /**
     * @param $schoolId
     * @return ProductQuery
     */
    public function restrictBySchool($schoolId)
    {
        return $this->andWhere('coalesce(school_id, :school_id) = :school_id', [
            ':school_id' => $schoolId
        ]);
    }

    /**
     * @param $userId
     * @return ProductQuery
     */
    public function restrictByUser($userId)
    {
        return $this->andWhere('coalesce(owner_id, :owner_id) = :owner_id', [
            ':owner_id' => $userId
        ]);
    }

    public function forThisUser()
    {
        $actor = \Yii::$container->get('Actor');

        return $this
            ->withoutPredefined()
            ->restrictByBoard($actor->board_id)
            ->restrictBySchool($actor->school_id)
            ->restrictByUser($actor->user_id);

    }
}