<?php

namespace common\query\schooladmin;

class FormQuery extends \common\query\FormQuery
{
    /**
     * @param $userId
     * @return FormQuery
     */
    public function restrictByUser($userId)
    {
        return $this;
    }
}