<?php

namespace common\query\schooladmin;

class EventQuery extends \common\query\EventQuery
{
    /**
     * @param $userId
     * @return EventQuery
     */
    public function restrictByUser($userId)
    {
        return $this;
    }

    /**
     * @return EventQuery
     */
    public function upcoming()
    {
        return $this
            ->select([
                'e.id',
                'start_date',
                'end_date',
                'title',
                'type',
                'student_count',
                'forms_signed',
                'forms_to_sign',
                'products_purchased',
                'products_to_purchase',
                't.last_name',
                't.first_name',
            ])
            ->alias('e')
            ->leftJoin('get_event_member_count() ec', 'e.id=ec.event_id')
            ->leftJoin('ldg_event_status l', 'e.id=l.doc_event_id')
            ->leftJoin('teacher t', 't.user_id = e.creator_id')
            ->andWhere('event_is_active(e)');
    }
}