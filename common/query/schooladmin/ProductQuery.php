<?php

namespace common\query\schooladmin;

class ProductQuery extends \common\query\ProductQuery
{
    /**
     * @param $userId
     * @return ProductQuery
     */
    public function restrictByUser($userId)
    {
        return $this;
    }
}