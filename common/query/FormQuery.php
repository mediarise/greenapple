<?php

namespace common\query;

class FormQuery extends \yii\db\ActiveQuery
{
    /**
     * @param $boardId
     * @return FormQuery
     */
    public function restrictByBoard($boardId)
    {
        return $this->andWhere('coalesce(board_id, :board_id) = :board_id', [
            ':board_id' => $boardId
        ]);
    }

    /**
     * @param $schoolId
     * @return FormQuery
     */
    public function restrictBySchool($schoolId)
    {
        return $this->andWhere('coalesce(school_id, :school_id) = :school_id', [
            ':school_id' => $schoolId
        ]);
    }

    /**
     * @param $userId
     * @return FormQuery
     */
    public function restrictByUser($userId)
    {
        return $this->andWhere('coalesce(owner_id, :owner_id) = :owner_id', [
            ':owner_id' => $userId
        ]);
    }

    public function forThisUser()
    {
        $actor = \Yii::$container->get('Actor');

        return $this
            ->restrictByBoard($actor->board_id)
            ->restrictBySchool($actor->school_id)
            ->restrictByUser($actor->user_id);

    }
}