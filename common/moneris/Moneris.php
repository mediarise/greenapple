<?php

namespace common\moneris;

use common\models\BankCard;
use mpgCvdInfo;
use mpgHttpsPost;
use mpgRequest;
use mpgTransaction;

class Moneris
{
    public $countryCode = 'CA';
    public $isTestMode = true;

    public $storeId = 'monca02663';
    public $apiToken = '9yr1iw2lIoOin0hsEUFO';

    /**
     * @param string $orderId
     * @param float $amount
     * @param string $data_key
     * @return \mpgResponse
     * @throws TransactionException
     */
    public function purchase(string $orderId, float $amount, string $data_key)
    {
        $mpgTxnData = array(
            'type' => 'res_purchase_cc',
            'data_key' => $data_key,
            'order_id' => $orderId . ($this->isTestMode ? '-' . time() : ''),
            'amount' => $this->isTestMode ? '1.00' : number_format($amount, 2, '.', ''),
            'crypt_type' => '1',
        );

        $mpgTxn = new mpgTransaction($mpgTxnData);


        return $this->request($mpgTxn);

    }

    /**
     * @param $orderId
     * @param BankCard $card
     * @param float $amount
     * @return \mpgResponse
     * @throws TransactionException
     */
    public function purchaseByCard($orderId, BankCard $card, $amount)
    {
        $mpgTxnData = [
            'type' => 'purchase',
            'order_id' => $orderId . $this->isTestMode ? '-' . time() : '',
            'cust_id' => time(),
            'amount' => $this->isTestMode ? '1.00' : number_format($amount, 2, '.', ''),
            'pan' => $card->number,
            'expdate' => $card->getExpDate(),
            'crypt_type' => 1,
        ];
        $mpgTxn = new mpgTransaction($mpgTxnData);

        $mpgTxn->setCvdInfo(new mpgCvdInfo([
            'cvd_indicator' => '1',
            'cvd_value' => $card->cvd,
        ]));

        return $this->request($mpgTxn);
    }

    /**
     * @param \mpgResponse $lastTransactionResponse
     * @return \mpgResponse
     * @throws TransactionException
     */
    public function tokenizeCard(\mpgResponse $lastTransactionResponse)
    {
        $mpgTxnData = [
            'type' => 'res_tokenize_cc',
            'order_id' => $lastTransactionResponse->getReceiptId(),
            'txn_number' => $lastTransactionResponse->getTxnNumber(),
        ];

        $mpgTxn = new mpgTransaction($mpgTxnData);

        return $this->request($mpgTxn);
    }

    /**
     * @param mpgTransaction $mpgTxn
     * @return \mpgResponse
     * @throws TransactionException
     */
    private function request(mpgTransaction $mpgTxn)
    {
        $mpgRequest = new mpgRequest($mpgTxn);
        $mpgRequest->setProcCountryCode($this->countryCode);
        $mpgRequest->setTestMode($this->isTestMode);

        /***************************** HTTPS Post Object *****************************/
        $mpgHttpPost = new mpgHttpsPost($this->storeId, $this->apiToken, $mpgRequest);
        $response = $mpgHttpPost->getMpgResponse();
        if ($response->getResponseCode() >= 50) {
            throw new TransactionException($response->getMessage(), $response->getResponseCode());
        }
        return $response;
    }
}

require_once 'mpgClasses.php';