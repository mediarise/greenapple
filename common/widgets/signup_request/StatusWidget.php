<?php

namespace common\widgets\signup_request;

use common\models\SignupRequestGuardian;
use yii\base\Widget;
use yii\helpers\Html;

class StatusWidget extends Widget
{
    public $status;
    public $options = [];

    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, 'label');
        $this->setColor();
    }

    public function run()
    {
        return Html::tag('span', SignupRequestGuardian::getStatusNameByCode($this->status), $this->options);
    }

    private function setColor()
    {
        switch ($this->status) {
            case SignupRequestGuardian::STATUS_NEW:
                Html::addCssClass($this->options, 'label-lime');
                break;
            case SignupRequestGuardian::STATUS_INCOMPLETE:
                Html::addCssClass($this->options, 'label-white');
                break;
            case SignupRequestGuardian::STATUS_COMPLETED:
                Html::addCssClass($this->options, 'label-primary');
                break;
            default:
                Html::addCssClass($this->options, 'label-blue');
                break;
        }
    }
}