<?php
namespace common\widgets\grid;

use function Stringy\create as s;
use yii\grid\DataColumn;
use yii\helpers\Html;

class LinkColumn extends DataColumn
{
    public $url;
    public $urlOptions;

    private static function formatUrl($model, $url) {
        foreach ($url as $key => $value){
            $value = s($value);
            if (is_string($key) && $value->startsWith('$')){
                $url[$key] = $model[(string) $value->removeLeft('$')];
            }
        }
        return $url;
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        $value = parent::renderDataCellContent($model, $key, $index);

        return Html::a($value, self::formatUrl($model, $this->url), $this->urlOptions);
    }
}