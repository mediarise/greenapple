<?php

namespace common\widgets;

use yii\grid\GridView as YiiGridView;

class AjaxGridView extends YiiGridView
{
    public $renderRowsOnly = false;

    public function run()
    {
        if ($this->renderRowsOnly){
            $this->renderRows();
            return;
        }
        parent::run();
    }

    private function renderRows()
    {
        $models = array_values($this->dataProvider->getModels());
        $keys = $this->dataProvider->getKeys();

        //$rows = [];

        foreach ($models as $index => $model) {
            $key = $keys[$index];
            //$rows[] =
            echo $this->renderTableRow($model, $key, $index);
        }

        //return implode("\n", $rows);
    }
}