<?php

namespace common\widgets;


use kartik\select2\Select2 as BaseSelect2;

class Select2 extends BaseSelect2
{
    public $theme = self::THEME_DEFAULT;

    public $defaultPluginOptions = [
        'allowClear' => true,
    ];

    public function init()
    {
        $this->detectDisability();
        parent::init();
    }

    protected function detectDisability()
    {
        if (count($this->data) == 1) {
            $this->disabled = true;
        }
    }

}