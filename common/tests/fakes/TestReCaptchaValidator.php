<?php

namespace common\tests\fakes;

use himiklab\yii2\recaptcha\ReCaptchaValidator;

class TestReCaptchaValidator extends ReCaptchaValidator
{
    protected function validateValue($value)
    {
        $this->isValid = true;
        return null;
    }
}