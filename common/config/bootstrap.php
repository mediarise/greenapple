<?php

use common\components\UploadedPhotoSaver;
use common\components\UploadedBlobImageSaver;
use common\components\BlobToImageConverter;
use common\models\BlobImageUploadForm;

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('@uploadsDir', dirname(dirname(__DIR__)) . '/frontend/web/uploads');

$container = Yii::$container;

$container->setDefinitions([
    'PhotoSaver' => function () {
        $model = new BlobImageUploadForm();
        $converter = new BlobToImageConverter(['convertTo' => 'jpg']);
        $fileSaver = new UploadedBlobImageSaver(['converter' => $converter]);

        return new UploadedPhotoSaver([
            'model' => $model,
            'fileSaver' => $fileSaver
        ]);
    }
]);

if (YII_ENV_TEST) {
    $container->set('himiklab\yii2\recaptcha\ReCaptcha', 'common\tests\fakes\TestReCaptcha');
    $container->set('himiklab\yii2\recaptcha\ReCaptchaValidator', 'common\tests\fakes\TestReCaptchaValidator');
}