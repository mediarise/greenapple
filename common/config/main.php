<?php

use function Stringy\create as s;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'redis',
            'port' => 6379,
            'database' => 0,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'htmlLayout' => 'layouts/html',
            'textLayout' => 'layouts/text',
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['noreply@greenapplepay.com' => 'Green apple pay'],
            ],
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mx.cappers.ca',
                'username' => 'greenapple@cappers.ca',
                'password' => '74cg6Q27e4',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ]
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@web',
                    'css' => [
                        'css/bootstrap.min.css'
                    ]
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/common.log',
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'MrLogger' => [
            'class' => \common\components\MrLogger::class,
        ],
    ],
    'on afterRequest' => function () {
        Yii::$app->get('MrLogger')->logRequest();
        Yii::$app->get('MrLogger')->logResponse();
    },
    'modules' => [
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',
            'defaultRoute' => '/in/user-management/user/index',
            'on beforeAction' => function (yii\base\ActionEvent $event) {
                $actionId = $event->action->uniqueId;
                if ($actionId == 'user-management/auth/login') {
                    $event->action->controller->layout = 'loginLayout.php';
                } elseif (s($actionId)->startsWith('user-management', true)) {
                    $event->action->controller->layout = '@backend/views/layouts/user-management';
                }
            },
            'on afterAction' => function (yii\base\ActionEvent $event) {
                if ($event->action->uniqueId == 'user-management/auth/login' && !Yii::$app->user->isGuest) {
                    return Yii::$app->controller->redirect(['user/index']);
                }
            },

        ],
    ],
];
