<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'productPhotoPlaceholder' => '/in/images/cropper-placeholder.png',
    'eventPhotoPlaceholder' => '/in/images/cropper-placeholder.png',

    'maxUploadFileSize' => 5242880, // bytes
    'uploadsDir' => 'uploads',
    'uploadsUrl' => 'frontend/web/uploads',
];
