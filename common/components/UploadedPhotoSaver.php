<?php
namespace common\components;

use yii\base\BaseObject;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadedPhotoSaver extends BaseObject
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @var UploadedFileSaver
     */
    public $fileSaver;

    private $errors = [];
    private $photo;


    public function __construct(array $config)
    {
        parent::__construct($config);

        $this->photo = UploadedFile::getInstanceByName($this->model->formName());
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $fileSaver = $this->fileSaver;
        try {
            $fileSaver->save($this->photo);
        } catch (\Exception $e) {
            $this->errors['photo'] = 'Unable to save photo. Please try again later.';
        }

        return $fileSaver->fileInfo;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function validate()
    {
        $model = $this->model;
        $model->file = $this->photo;
        if (!$model->validate()) {
            $this->errors = $model->getErrors();
            return false;
        }

        return true;
    }
}