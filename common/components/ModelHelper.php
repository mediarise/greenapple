<?php

namespace common\components;

use common\helpers\ArrayHelper;
use yii\base\ErrorException;
use yii\base\Model;

class ModelHelper
{
    public static function errorsForForm(Model $model, $formName = null)
    {
        if ($formName === null){
            $formName = $model->formName();
        }
        $result = [];
        foreach ($model->getErrors() as $attribute => $error) {
            $result[strtolower($formName . '-' . $attribute)] = $error;
        }
        return $result;
    }

    public static function errorsForTabularForm(Model $model, $formName = null, $tabularIndex = null)
    {
        if ($formName === null){
            $formName = $model->formName();
        }
        $result = [];
        foreach ($model->getErrors() as $attribute => $error) {
            $index = $tabularIndex
                ? strtolower($formName . '-' . $tabularIndex . '-' . $attribute)
                : strtolower($formName . '-' . $attribute);

            $result[$index] = $error;
        }
        return $result;
    }

    public static function errorsForMultipleForms($forms)
    {
        $errors = [];

        foreach ($forms as $form) {

            $modelSet = $form[0];
            $formName = $form[1] ?? null;
            $tabularIndexField = $form[2] ?? null;

            if (is_array($modelSet)) {
                foreach ($modelSet as $i => $model) {
                    $tabularIndex = $tabularIndexField ? $model->{$tabularIndexField} : $i;
                    $errors = array_merge(self::errorsForTabularForm($model, $formName, $tabularIndex), $errors);
                }
                continue;
            }

            $model = $modelSet; // only one model in set now
            if ($model instanceof \yii\base\Model) {
                $errors = array_merge(self::errorsForForm($model, $formName), $errors);
            }
        }

        return $errors;
    }
}