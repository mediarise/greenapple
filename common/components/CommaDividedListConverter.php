<?php

namespace common\components;

class CommaDividedListConverter
{
    public static function toArray($list)
    {
        if (is_array($list)) {
            return $list;
        }
        $list = trim($list, ',');

        return explode(',', $list);
    }

    public static function toList($array)
    {
        return implode(',', $array);
    }
}