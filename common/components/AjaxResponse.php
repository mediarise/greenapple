<?php

namespace common\components;

class AjaxResponse
{
    const TYPE_INFO = 'info';
    const TYPE_WARNING = 'warning';
    const TYPE_DANGER = 'danger';

    public static function success($response = [])
    {
        return array_merge([
            'result' => 'success',
        ], $response);
    }

    public static function successMessage($message)
    {
        return [
            'result' => 'success',
            'message' => $message
        ];
    }

    public static function error($message)
    {
        return [
            'result' => 'error',
            'message' => $message,
        ];
    }

    public static function info($message, array $additionalFields = [])
    {
        $additionalFields['type'] = self::TYPE_INFO;
        return self::notification($message, $additionalFields);
    }

    public static function warning($message, array $additionalFields)
    {
        $additionalFields['type'] = self::TYPE_WARNING ;
        return self::notification($message, $additionalFields);
    }

    public static function danger($message, array $additionalFields)
    {
        $additionalFields['type'] = self::TYPE_DANGER;
        return self::notification($message, $additionalFields);
    }

    public static function notification($message, $additionalFields)
    {
        return array_merge([
            'result' => 'notification',
            'type' => $additionalFields['type'] ?? self::TYPE_DANGER,
            'message' => $message,
        ], $additionalFields);
    }

    public static function validationError($errors)
    {
        return [
            'result' => 'validation',
            'validation' => $errors,
        ];
    }

    public static function tabularFormValidation($errors)
    {
        return [
            'result' => 'validation',
            'validation' => $errors,
            'isTabularFormValidation' => true,
        ];
    }



}