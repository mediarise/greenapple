<?php /** @noinspection PhpUndefinedClassInspection */

namespace common\components;

use Yii;

class DbHelper
{
    /**
     * @param callable $function
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public static function inTransaction(callable $function)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $result = call_user_func($function);
            $transaction->commit();
            return $result;

        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}