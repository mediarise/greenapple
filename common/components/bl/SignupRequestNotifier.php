<?php

namespace common\components\bl;

use common\components\EMailNotifier;
use common\models\Guardian;
use common\models\SignupRequestGuardian;
use common\models\SignupRequestRejectionForm;
use frontend\models\SignupRequest;

class SignupRequestNotifier
{
    private $notifier;

    public function __construct($to, $template)
    {
        $notifier = new EMailNotifier();
        $notifier->to = $to;
        $notifier->template = $template;
        $this->notifier = $notifier;
    }

    public function notifyReject(SignupRequestGuardian $signupRequest, SignupRequestRejectionForm $rejectionForm)
    {
        $notifier = $this->notifier;
        $notifier->subject = 'Your signup request was rejected.';
        $notifier->params = [
            'fullName' => $signupRequest->getFullName(),
            'rejectionReasoning' => $rejectionForm->rejectReason,
        ];
        $notifier->notify();
    }

    public function notifyApprove(Guardian $guardian)
    {
        $notifier = $this->notifier;
        $notifier->subject = 'Your signup request was approved.';
        $notifier->params = [
            'inviteToken' => $guardian->invite_token,
            'fullName' => $guardian->getFullName()
        ];
        $notifier->notify();
    }

}