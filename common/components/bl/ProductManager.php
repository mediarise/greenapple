<?php

namespace common\components\bl;

use common\models\EventProduct;
use common\models\Product;
use common\models\ProductVariation;
use common\models\Supplier;
use Yii;
use yii\base\BaseObject;
use yii\base\ErrorException;
use common\models\ProductPhoto;
use yii\helpers\ArrayHelper;

class ProductManager extends BaseObject
{
    public function save(Product $product, ProductPhoto $photo = null, $productVariations = [])
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$product->save()) {
                throw new ErrorException('Unable to save product.');
            }

            if ($photo && $product->mainPhotoUrl) {
                ProductPhoto::deleteAll([
                    'product_id' => $product->id,
                    'is_main' => true
                ]);
                $photo->product_id = $product->id;
                $photo->url = $product->mainPhotoUrl;
                $photo->is_main = true;
                if (!$photo->save()) {
                    throw new ErrorException('Unable to save attached photo.');
                }
            }

            foreach ($productVariations as $variation){
                if ($variation->toRemove){
                    $variation->delete();
                    continue;
                }
                $variation->product_id = $product->id;
                if (!$variation->save()) {
                    throw new ErrorException('Unable to save product variation.');
                }
            }

            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }

        return true;
    }

    public function delete($id)
    {
        $product = $this->find($id);
        if (null !== EventProduct::findOne(['product_id' => $id])) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            ProductPhoto::deleteAll(['product_id' => $id]);
            ProductVariation::deleteAll(['product_id' => $id]);
            $product->delete();
            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }

        return true;
    }

    /**
     * @param $id
     * @return \yii\db\ActiveRecord
     * @throws \yii\base\ErrorException
     */
    public function find($id)
    {
        $product = Product::find()
            ->where(['id' => $id])
            ->with('mainPhoto')
            ->one();

        if (!$product) {
            throw new ErrorException('Unable to find product.');
        }

        if ($product->mainPhoto) {
            $product->mainPhotoUrl = $product->mainPhoto->url;
        }

        return $product;
    }

    public function getSuppliers()
    {
        $suppliers = Supplier::find()->select(['id', 'name'])->all();
        return ArrayHelper::map($suppliers, 'id', 'name');
    }

    /**
     * @param ProductVariation[] $variations
     * @param array $data
     * @param $productId
     */
    public function loadVariations(array &$variations, array $data, $productId, $isFree, $taxVariant)
    {
        foreach ($variations as $id => $model) {
            $dataMember = ArrayHelper::remove($data, $id);
            if ($dataMember === null) {
                $model->toRemove = true;
                continue;
            }
            $model->load($dataMember, '');
        }

        foreach ($data as $index => $member) {
            $model = new ProductVariation();
            $model->index = $index;
            if ($isFree){
                $model->setScenario(ProductVariation::SCENARIO_PRODUCT_IS_FREE);
            }
            $model->tax = $taxVariant;
            $model->load($member, '');
            $model->product_id = $productId;
            $variations[$index] = $model;
        }

    }

    /**
     * @param Product $product
     * @param ProductVariation[] $variations
     * @return bool
     */
    public function validate(Product $product, array &$variations)
    {
        $areValidated = $product->validate();
        foreach ($variations as $variation){
            $areValidated = $areValidated && $variation->validate();
        }
        return $areValidated;
    }
}