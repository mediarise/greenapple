<?php

namespace common\components\bl;

use common\components\AjaxResponse;
use common\models\Guardian;
use common\models\SchoolAdmin;
use common\models\SignupRequestGuardian;
use common\models\SignupRequestRejectionForm;
use common\models\SignupRequestStudent;
use common\models\Student;
use common\models\User;
use common\components\DbHelper;
use webvimark\modules\UserManagement\models\User as rbacUser;
use Yii;
use yii\web\BadRequestHttpException;

class SignupRequestManager
{
    private $signupRequestId;
    private $guardian;

    private $students = [];
    /**
     * @var SchoolAdmin
     */
    private $schoolAdmin;

    public function __construct($signupRequestId, SchoolAdmin $schoolAdmin)
    {
        $this->signupRequestId = (int)$signupRequestId;
        $this->schoolAdmin = $schoolAdmin;
    }

    /**
     * @param $signupRequestStudentId
     * @throws \Throwable
     */
    public function rejectStudent($signupRequestStudentId)
    {
        Yii::$app->db->transaction(function ($db) use ($signupRequestStudentId) {
            SignupRequestStudent::updateAll([
                'status' => SignupRequestStudent::STATUS_REJECTED,
            ], [
                'id' => $signupRequestStudentId,
            ]);
        });
        $this->makeIndefinite();
    }

    public function makeIndefinite()
    {
        SignupRequestGuardian::updateAll([
            'status' => SignupRequestGuardian::STATUS_INDEFINITE,
        ], [
            'id' => $this->signupRequestId,
        ]);
    }

    public function invite(SignupRequestNotifier $notifier)
    {
        $request = $this->getSignupRequest();
        if ($request->status === SignupRequestGuardian::STATUS_NEW) {
            return AjaxResponse::error('You need to approve at least one child.');
        }
        if ($request->status === SignupRequestGuardian::STATUS_REJECTED) {
            return AjaxResponse::error('Request is rejected.');
        }

        return DbHelper::inTransaction(function () use ($notifier) {
            $request = $this->getSignupRequest();
            $guardian = $request->guardian;
            if (empty($guardian->invite_token)) {
                $transaction = Yii::$app->db->beginTransaction();

                $guardian->invite_token = Yii::$app->security->generateRandomString();
                $guardian->save(false);

                $user = $guardian->user;
                $user->status = User::STATUS_INVITED;
                $user->save(false);

                $transaction->commit();
            }
            $notifier->notifyApprove($guardian);
            return AjaxResponse::success();
        });
    }

    public function reject(SignupRequestNotifier $notifier = null, $reasonData)
    {
        $rejectForm = new SignupRequestRejectionForm();
        $rejectForm->load($reasonData);
        if (!$rejectForm->validate()) {
            throw new BadRequestHttpException($rejectForm->getFirstError('rejectReason'));
        }
        $request = $this->getSignupRequest();
        $request->status = SignupRequestGuardian::STATUS_REJECTED;
        $request->note = $rejectForm->rejectReason;
        $request->save(false);

        $notifier->notifyReject($this->getSignupRequest(), $rejectForm);
    }

    private function createGuardian()
    {
        $guardian = $this->newGuardian();

        $request = $this->getSignupRequest();
        $user = $this->createUser($request->email);

        $guardian->user_id = $user->id;
        $guardian->save(false);

        $request->updateAttributes([
            'status' => SignupRequestGuardian::STATUS_INDEFINITE,
            'guardian_id' => $guardian->id,
        ]);

        return $guardian;
    }

    public function getStudents()
    {
        if (empty($this->students)) {
            $students = SignupRequestGuardian::getSignupRequestStudentTemplateById($this->signupRequestId)
                ->andWhere(['grade.school_id' => $this->schoolAdmin->school_id])
                ->orderBy('signup_request_student.id')
                ->all();
            foreach ($students as $student) {
                $this->students[$student->id] = $student;
            }
            ksort($this->students);
        }

        return $this->students;
    }

    /**
     * @return SignupRequestGuardian
     */
    public function getSignupRequest()
    {
        static $signupRequest;
        if (empty($signupRequest)) {
            $signupRequest = SignupRequestGuardian::find()
                ->select(['signup_request.*', 'bit_or(signup_request_student.status) AS student_status'])
                ->joinWith('signupRequestStudents', false)
                ->where(['signup_request.id' => $this->signupRequestId])
                ->groupBy('signup_request.id')
                ->one();
        }

        return $signupRequest;
    }

    public function getGuardian()
    {
        if ($this->guardian === null) {
            $request = $this->getSignupRequest();
            if ($request->guardian_id) {
                $this->guardian = Guardian::findOne($request->guardian_id);
            }
        }
        return $this->guardian;
    }

    /**
     * @param $email
     * @return User
     * @throws \yii\base\Exception
     */
    private function createUser($email)
    {
        $user = new User();
        $user->generateAuthKey();
        $user->password = Yii::$app->security->generateRandomString();
        $user->email = $email;
        $user->username = $email;
        $user->status = User::STATUS_INACTIVE;
        $user->save(false);
        rbacUser::assignRole($user->id, 'guardian');
        return $user;
    }

    public function newGuardian()
    {
        $guardian = new Guardian();
        $guardian->load($this->getSignupRequest()->attributes, '');
        return $guardian;
    }

    public function approveStudent($id, $studentSignupRequest)
    {
        $student = new Student();
        $student->load($studentSignupRequest, '');

        if (!$student->validate()) {
            return $student;
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $signupRequestStudent = $this->getStudent($id);

        if ($student->save(false)) {
            $signupRequestStudent->updateAttributes([
                'status' => SignupRequestStudent::STATUS_APPROVED,
                'student_id' => $student->id,
            ]);

            $guardian = $signupRequestStudent->signupRequest->guardian;

            if ($guardian === null) {
                $guardian = $this->createGuardian();
            }

            $student->link('guardians', $guardian, ['relationship' => $signupRequestStudent->relationship]);

            $transaction->commit();
            return $student;
        }

        $transaction->rollBack();

        return $student;
    }

    public function removeRelationship($requestId, $id)
    {
    }

    /**
     * @param $id
     * @return SignupRequestStudent
     */
    public function getStudent($id)
    {
        $result = null;
        if (array_key_exists($id, $this->students)) {
            $result = $this->students[$id];
        } else {
            $result = SignupRequestStudent::findOne($id);
            $this->students[$id] = $result;
        }
        return $result;
    }

    public function newStudent($signupRequestStudent)
    {
        $student = new Student();
        $student->load($signupRequestStudent->attributes, '');

        $birthday = new \DateTime($signupRequestStudent->birthday);
        $student->birthday = $birthday->format('Y-m-d');

        return $student;
    }
}