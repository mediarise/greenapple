<?php

namespace common\components\bl;

use common\models\guardian\DocEvent;
use common\models\VisibilityGroup;
use Yii;
use yii\base\BaseObject;
use yii\base\ErrorException;
use common\models\DocEventForm;
use common\models\EventPhoto;
use common\models\EventVisibilitySection;

abstract class EventManager extends BaseObject
{
    protected $actor;
    protected $event;
    protected $visibilitySection;

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->actor = Yii::$container->get('Actor');
    }

    abstract public function create(DocEventForm $event, EventPhoto $photo, $ownerId);

    abstract public function update(
        DocEventForm $event, EventPhoto $photo, EventVisibilitySection $visibilitySection = null,
        $forms = null, $products  = null
    );

    abstract public function findVisibilitySection($docEventId);

    abstract protected function updateVisibilitySection(EventVisibilitySection $visibilitySection);

    abstract public function publish($docEventId);

    abstract public function find($docEventId);

    public function setActor(Actor $actor)
    {
        $this->actor = $actor;
    }

    protected function attachPhoto(EventPhoto $photo, $url, $eventId)
    {
        if ($photo && $url) {
            EventPhoto::deleteAll([
                'event_id' => $eventId,
                'is_main' => true
            ]);
            $photo->event_id = $eventId;
            $photo->url = $url;
            $photo->is_main = true;
            if (!$photo->save()) {
                throw new ErrorException('Unable to save attached photo.');
            }
        }
    }

    public function delete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            EventPhoto::deleteAll(['event_id' => $id]);
            $event = $this->find($id);
            /* @var $event DocEvent */
            $event->unlinkAll('forms', true);
            $event->unlinkAll('products', true);

            $visibilitySection = VisibilityGroup::find()->where(['doc_event_id' => $id])->one();
            if ($visibilitySection) {
                Yii::$app->db->createCommand()
                    ->delete('visibility_group_member', ['visibility_group_id' => $visibilitySection->id])
                    ->execute();
                $visibilitySection->delete();
            }

            $event->delete();
            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }

        return true;
    }
}