<?php

namespace common\components\bl;

use common\models\Board;
use common\models\Grade;
use common\models\School;
use common\models\SchoolForm;
use yii\helpers\ArrayHelper;

class SchoolHierarchy
{
    private $grade;
    private $school;
    private $board;

    private static $schools = [];
    private static $grades = [];

    public static function getForm($gradeId)
    {
        $grade = Grade::find()
            ->joinWith('school')
            ->where(['id' => $gradeId])
            ->one();

        return new SchoolForm($grade);
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param mixed $grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    }

    /**
     * @return mixed
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * @param mixed $school
     */
    public function setSchool($school)
    {
        $this->school = $school;
    }

    /**
     * @return mixed
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * @param mixed $board
     */
    public function setBoard($board)
    {
        $this->board = $board;
    }

    public static function getBoards()
    {
        $boards = Board::find()->all();
        return ArrayHelper::map($boards, 'id', 'name');
    }

    public static function getSchools($boardId)
    {
        if (empty($boardId)){
            return [];
        }
        if (!array_key_exists($boardId, static::$schools)){
            $schools = School::find()->where(['board_id' => $boardId])->all();
            $schools = ArrayHelper::map($schools, 'id', 'name');
            static::$schools[$boardId] = $schools;
            return $schools;
        }
        return static::$schools[$boardId];
    }

    public static function getGrades($schoolId)
    {
        if (empty($schoolId)){
            return [];
        }
        if (!array_key_exists($schoolId, static::$grades)){
            $grades = Grade::find()
                ->where(['school_id' => $schoolId])
                ->andWhere(['year' => Grade::getCurrentAcademicYear()])
                ->all();
            $grades = ArrayHelper::map($grades, 'id', 'name');
            static::$grades[$schoolId] = $grades;
            return $grades;
        }
        return static::$grades[$schoolId];
    }

}