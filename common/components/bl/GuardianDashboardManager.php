<?php

namespace common\components\bl;

use common\models\guardian\UpcomingEvent;
use Traversable;
use yii\db\Query;

class GuardianDashboardManager
{
    private $upcomingEvents;
    private $pendingEvents;
    private $studentsIds;
    private $guardianId;

    public function __construct($guardianId, $studentsIds)
    {
        $this->studentsIds = $studentsIds;
        $this->guardianId = $guardianId;
    }

    public function getAllEvents()
    {
        if (empty($this->upcomingEvents)) {
            if (empty($this->studentsIds)) {
                return [];
            }
            $this->upcomingEvents = $this->loadUpcomingEvents($this->studentsIds);
        }
        return $this->upcomingEvents;
    }

    /**
     * @return UpcomingEvent[]
     */
    public function getUpcomingEvents()
    {
        static $upcomingEvents = null;
        if (empty($upcomingEvents)){
            $upcomingEvents = array_filter($this->getAllEvents(), function (UpcomingEvent $event) {
                return !$event->is_repeatable;
            });
        }
        return $upcomingEvents;
    }

    public function getPendingEvents()
    {
        if (empty($this->pendingEvents)) {
            $this->pendingEvents = $this->filterPendingEvents();
        }
        return $this->pendingEvents;
    }

    public function getProductHistory()
    {
        return (new Query())
            ->select([
                'l.created_at date',
                'p.title product',
                'e.title event',
                'quantity',
                'action_type action'
            ])
            ->from('ldg_event_activity_purchase l')
            ->leftJoin('product p', 'l.product_id=p.id')
            ->leftJoin('doc_event e', 'l.doc_event_id=e.id')
            ->where('guardian_id = :guardian_id', [':guardian_id' => $this->guardianId])
            ->andWhere(['in', 'student_id', $this->studentsIds]);
    }

    public function getFormHistory()
    {
        return (new Query())
            ->select([
                'l.created_at date',
                'f.title form',
                'e.title event',
                'action_type action'
            ])
            ->from('ldg_event_activity_form l')
            ->leftJoin('form f', 'l.form_id=f.id')
            ->leftJoin('doc_event e', 'l.doc_event_id=e.id')
            ->where('guardian_id = :guardian_id', [':guardian_id' => $this->guardianId])
            ->andWhere(['in', 'student_id', $this->studentsIds]);
    }

    /**
     * @param $students_ids
     * @return array
     * @throws \yii\db\Exception
     */
    protected function loadUpcomingEvents(array $students_ids)
    {

        $ids = implode(',', $students_ids);
        $query = \Yii::$app->db->createCommand(
            "select * from get_upcoming_events(ARRAY[{$ids}]::bigint[]);");
        $reader = $query->query();

        try {
            return iterator_to_array($this->eventFromArray($reader));
        } finally {
            $reader->close();
        }
    }

    protected function filterPendingEvents()
    {
        $events = $this->getAllEvents();

        $result = array_filter($events, function (UpcomingEvent $event) {
            return $event->getStatus() >= UpcomingEvent::STATUS_INCOMPLETE;
        });
        uasort($result, function (UpcomingEvent $event1, UpcomingEvent $event2) {
            return strcmp($event1->due_date, $event2->due_date);
        });

        return $result;
    }

    private function eventFromArray(Traversable $items)
    {
        foreach ($items as $item) {
            yield new UpcomingEvent($item);
        }
    }

}