<?php

namespace common\components\bl;

class Gender
{
    const MALE = 0x1;
    const FEMALE = 0x2;

    private static $genders = [
        self::MALE => 'Male',
        self::FEMALE => 'Female',
    ];

    public static function getNames()
    {
        return self::$genders;
    }

    public static function getName($index)
    {
        return self::$genders[$index];
    }

}