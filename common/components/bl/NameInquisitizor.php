<?php

namespace common\components\bl;

use common\helpers\ArrayHelper;
use Faker\Factory;

class NameInquisitizor
{
    public static function firstName($gender = null)
    {
        $faker = Factory::create();
        $firstName = $faker->firstName($gender);
        if (ArrayHelper::isIn($firstName, ['Christ', 'Chris', 'Christopher'])) {
            $firstName = self::firstName($gender);
        }

        return $firstName;
    }

    public static function lastName()
    {
        $faker = Factory::create();
        $lastName = $faker->lastName;
        if (ArrayHelper::isIn($lastName, ['Christiansen'])) {
            $lastName = self::lastName();
        }

        return $lastName;
    }
}