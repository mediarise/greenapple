<?php

namespace common\components\bl\ledger;

use common\models\DocEvent;
use common\models\EventProduct;
use common\models\Product;
use Yii;
use yii\db\Expression;
use yii\db\Query;

class EventLedger
{
    /**
     * Create ldg_event_status rec for event
     * @param DocEvent $event
     * @return int
     * @throws \yii\db\Exception
     */
    public static function createStatus($event)
    {
        $query = null;

        switch ($event->type) {
            case DocEvent::EVENT_TYPE_FIELD_TRIP:
                $query = self::getInsertStatusCommand($event->id);
                break;
            case DocEvent::EVENT_TYPE_STOCK_THE_CLASSROOM:
            case DocEvent::EVENT_TYPE_SELL_PRODUCT:
            case DocEvent::EVENT_TYPE_NUTRITION_ONE_TIME:
            case DocEvent::EVENT_TYPE_FUNDRAISING_SELL:
                $query = self::getInsertStatusCommand($event->id, null, 0);
                break;
            case DocEvent::EVENT_TYPE_FILL_OUT_FORM:
                $query = self::getInsertStatusCommand($event->id, 0);
                break;
            case DocEvent::EVENT_TYPE_FUNDRAISING:
                $query = self::getInsertStatusCommand($event->id, 1, 0);
                break;
        }

        return Yii::$app->db->createCommand()
            ->insert('ldg_event_status', $query
                ->where(['id' => $event->id])
            )
            ->execute();
    }

    /**
     * @param int $docEventId
     * @return int
     * @throws \yii\db\Exception
     */
    public static function createRepeatableStatus(int $docEventId)
    {
        $rowCount = Yii::$app->db->createCommand()
            ->insert('ldg_event_status', self::getInsertStatusCommand($docEventId, null, 0)
                ->where(['predecessor' => $docEventId])
            )
            ->execute();

        $rowCount += Yii::$app->db->createCommand()
            ->insert('ldg_event_status', self::getInsertStatusCommand($docEventId, null, 0, $rowCount)
                ->where(['id' => $docEventId])
            )
            ->execute();

        return $rowCount;
    }

    private static function getInsertStatusCommand($docEventId, $productsQuantity = null, $formsQuantity = null, $multiplier = 1)
    {
        $eventIdExpression = new Expression((string)$docEventId);
        $zeroExpression = new Expression('0');
        $query = (new Query())
            ->select([
                'doc_event_id' => 'e.id',
                'events_accepted' => $zeroExpression,
                'events_declined' => $zeroExpression,
                'forms_signed' => $zeroExpression,
                'products_purchased' => $zeroExpression,
            ])
            ->from([
                'e' => 'doc_event',
            ]);

        if (is_null($productsQuantity)) {
            $productsQuantity = 'pcq.quantity';
            $query->from['pcq'] = (new Query())
                ->select("count(id) * $multiplier quantity")
                ->from(EventProduct::tableName())
                ->where(['doc_event_id' => $eventIdExpression]);
        } else {
            $productsQuantity = new Expression((string)$productsQuantity);
        }

        if (is_null($formsQuantity)) {
            $formsQuantity = 'fcq.quantity';
            $query->from['fcq'] = (new Query())
                ->select("count(id) * $multiplier quantity")
                ->from('lnk_event_form')
                ->where(['doc_event_id' => $eventIdExpression]);
        } else {
            $formsQuantity = new Expression((string)$formsQuantity);
        }

        $query->addSelect([
            'products_to_purchase' => $productsQuantity,
            'forms_to_sign' => $formsQuantity,
        ]);

        return $query;
    }

    /**
     * Update ldg_event_status rec of event
     * @param int $docEventId
     * @param array $columns
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateStatus(int $docEventId, array $columns)
    {
        return Yii::$app->db->createCommand()
            ->update('ldg_event_status', $columns, ['doc_event_id' => $docEventId])
            ->execute();
    }

    protected static function counterExpression(string $column, string $sign, int $quantity = 1)
    {
        return [$column => new Expression("{$column} {$sign} {$quantity}")];
    }
}