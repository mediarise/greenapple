<?php

namespace common\components\bl\ledger;

use common\components\DbHelper;
use common\exception\LogicException;
use common\models\Cart;
use common\models\EventProduct;
use common\models\guardian\purchase\CartOrderItem;
use common\models\guardian\purchase\OrderItem;
use common\models\guardian\UpcomingEvent;
use Faker\DefaultGenerator;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class GuardianEventLedger extends EventLedger
{
    const ACTION_PURCHASE_PURCHASE = 0x1;
    const ACTION_PURCHASE_RETURN = 0x2;
    const ACTION_PURCHASE_DONATE = 0x4;

    const ACTION_FORM_SIGN = 0x1;
    const ACTION_FORM_CANCEL = 0x2;
    const ACTION_FORM_SKIP = 0x4;

    const ACTION_GENERAL_ACCEPT = 0x1;
    const ACTION_GENERAL_DECLINE = 0x2;
    const ACTION_GENERAL_HIDE = 0x4;

    private static $purchase_actions = [
        self::ACTION_PURCHASE_PURCHASE => 'purchase',
        self::ACTION_PURCHASE_RETURN => 'return',
        self::ACTION_PURCHASE_DONATE => 'donation'
    ];

    private static $form_actions = [
        self::ACTION_FORM_SIGN => 'signed',
        self::ACTION_FORM_CANCEL => 'cancel',
        self::ACTION_FORM_SKIP => 'skip',
    ];

    /** @var int */
    private $guardianId;

    public static function getPurchaseActionLabel($action)
    {
        return self::$purchase_actions[$action];
    }

    public static function getFormActionLabel($action)
    {
        return self::$form_actions[$action];
    }

    public function __construct(int $guardianId)
    {
        $this->guardianId = $guardianId;
    }

    /**
     * @param Cart[] $items
     * @param string $orderId
     * @throws \yii\db\Exception
     */
    public function purchaseByCart(array $items, $orderId)
    {
        $this->baseProductAction($this->orderItemsGenerator($items, $orderId));

        $counters = [];
        foreach ($items as $item) {
            if (isset($counters[$item->doc_event_id])) {
                if (isset($counters[$item->doc_event_id][$item->student_id])) {
                    $counters[$item->doc_event_id][$item->student_id]++;
                } else {
                    $counters[$item->doc_event_id][$item->student_id] = 1;
                }
            } else {
                $counters[$item->doc_event_id] = [$item->student_id => 1];
            }
        }
        foreach ($counters as $docEventId => $students) {
            foreach ($students as $studentId => $quantity) {
                $this->updateCounters($docEventId, $studentId, $quantity, 0);
            }
        }
    }

    /**
     * @param Cart[] $items
     * @param string $orderId
     * @return CartOrderItem|\Generator
     */
    private function orderItemsGenerator(array $items, $orderId)
    {
        foreach ($items as $item) {
            yield new CartOrderItem($item, $orderId);
        }
    }

    /**
     * @param OrderItem $item
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function purchaseProduct(OrderItem $item)
    {
        DbHelper::inTransaction(function () use ($item) {
            $this->baseProductAction($item);
            $this->updateCounters($item->getEventId(), $item->getStudentId(), 1, 0);
        });
    }

    /**
     * @param int $studentId
     * @param EventProduct $eventProduct
     * @param int $quantity
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function donateProduct(int $studentId, EventProduct $eventProduct, int $quantity)
    {
        DbHelper::inTransaction(function () use ($studentId, $eventProduct, $quantity) {
            $this->baseProductAction($studentId, $eventProduct, null, null, $quantity, self::ACTION_PURCHASE_DONATE);
            $this->acceptEvent($eventProduct->doc_event_id, $studentId, false);
            $this->updateCounters($eventProduct->doc_event_id, $studentId, 1, 0);
        });
    }

    /**
     * @param OrderItem[] $items
     * @throws \Throwable
     */
    public function donateProducts($items)
    {
        Yii::$app->db->transaction(function () use ($items) {
            $total = 0;
            foreach ($items as $orderItem) {
                if ($orderItem->quantity <= 0) {
                    continue;
                }
                $total++;
                $this->baseProductAction($items);
            }
            if ($total = 0) {
                return;
            }
            /** @var OrderItem $orderItem */
            $orderItem = array_shift($items);
            $this->updateCounters($orderItem->getEventId(), $orderItem->getStudentId(), $total, 0);
            $previousAction = $this->getPreviousEventAction($orderItem->getEventId(), $orderItem->getStudentId());
            if ($previousAction != self::ACTION_GENERAL_ACCEPT) {
                $this->acceptEvent($orderItem->getEventId(), $orderItem->getStudentId(), false);
            }
        });
    }

    /**
     * @param int $studentId
     * @param EventProduct $eventProduct
     * @param string $orderId
     * @param int $quantity
     * @throws LogicException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function returnProduct(int $studentId, EventProduct $eventProduct, string $orderId, int $quantity)
    {
        $query = (new Query())
            ->select('action_type, sum(quantity)')
            ->from('ldg_event_activity_purchase')
            ->where([
                'doc_event_id' => $eventProduct->doc_event_id,
                'guardian_id' => $this->guardianId,
                'student_id' => $studentId,
                'product_id' => $eventProduct->product_id,
            ])
            ->groupBy('action_type');

        $records = ArrayHelper::map($query->all(), 'action_type', 'sum') + [
                self::ACTION_PURCHASE_PURCHASE => 0,
                self::ACTION_PURCHASE_RETURN => 0,
            ];
        $count = $records[self::ACTION_PURCHASE_PURCHASE] - $records[self::ACTION_PURCHASE_RETURN];

        if ($count <= 0) {
            throw new LogicException('Nothing to return.');
        }

        DbHelper::inTransaction(function () use ($studentId, $eventProduct, $orderId, $quantity) {
            //TODO: add variation parameter
            $this->baseProductAction($studentId, $eventProduct, $orderId, $quantity, self::ACTION_PURCHASE_RETURN);
            $this->updateCounters($eventProduct->doc_event_id, $studentId, -1, 0);
        });
    }

    /**
     * @param OrderItem|OrderItem[]|\Generator $items
     * @throws \yii\db\Exception
     */
    private function baseProductAction($items)
    {
        if ($items instanceof OrderItem) {
            $items = [$items];
        }
        Yii::$app->db->createCommand()
            ->batchInsert('ldg_event_activity_purchase', [
                'doc_event_id', 'guardian_id', 'student_id', 'action_type', 'product_id', 'product_variation_id',
                'order_id', 'quantity', 'price', 'tax', 'total', 'created_at'
            ], $this->purchaseRowGenerator($items))
            ->execute();
    }

    /**
     * @param OrderItem[]|\Generator $items
     * @return \Generator
     */
    private function purchaseRowGenerator($items)
    {
        foreach ($items as $item) {
            yield [
                $item->getEventId(), $item->getGuardianId(), $item->getStudentId(), $item->getActionType(),
                $item->getProductId(), $item->getProductVariationId(), $item->getOrderId(), $item->getQuantity(),
                $item->getPrice(), $item->getTax(), $item->getTotal(),
                new Expression('NOW()'),
            ];
        }
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param bool $useTransaction
     * @throws LogicException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function acceptEvent(int $docEventId, int $studentId, $useTransaction = true)
    {
        if ($this->getPreviousEventAction($docEventId, $studentId) === self::ACTION_GENERAL_ACCEPT) {
            throw new LogicException('Event already accepted.');
        }
        $action = function () use ($docEventId, $studentId) {
            $this->baseEventAction($docEventId, $studentId, self::ACTION_GENERAL_ACCEPT);
            self::updateStatus($docEventId, self::counterExpression('events_accepted', '+'));
            self::updateStudentStatus($docEventId, $studentId, ['status' => UpcomingEvent::STATUS_PLANNED]);
        };

        if ($useTransaction) {
            DbHelper::inTransaction($action);
        } else {
            $action();
        }
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param string $reason
     * @throws LogicException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function declineEvent(int $docEventId, int $studentId, string $reason = '')
    {
        $counters = $this->counterExpression('events_declined', '+');
        $previousAction = $this->getPreviousEventAction($docEventId, $studentId);

        if ($previousAction === self::ACTION_GENERAL_DECLINE) {
            throw new LogicException('Event already declined.');
        } elseif ($previousAction === self::ACTION_GENERAL_ACCEPT) {
            $counters += $this->counterExpression('events_accepted', '-');
        }

        DbHelper::inTransaction(function () use ($docEventId, $studentId, $counters, $reason) {
            $this->baseEventAction($docEventId, $studentId, self::ACTION_GENERAL_DECLINE);
            self::updateStatus($docEventId, $counters);
            self::updateStudentStatus($docEventId, $studentId, [
                'status' => UpcomingEvent::STATUS_DECLINED,
                'cancellation_reason' => $reason,
            ]);
            self::updateParentStatus($docEventId, $studentId);
        });
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @throws \yii\db\Exception
     */
    public function hideEvent(int $docEventId, int $studentId)
    {
        $this->baseEventAction($docEventId, $studentId, self::ACTION_GENERAL_HIDE);
    }

    public function getPreviousEventAction(int $docEventId, int $studentId)
    {
        $query = (new Query())
            ->select('action_type')
            ->from('ldg_event_activity_general')
            ->where([
                'doc_event_id' => $docEventId,
                //'guardian_id' => $this->guardianId,
                'student_id' => $studentId,
            ])
            ->orderBy('created_at desc');

        return $query->scalar();
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param int $actionType
     * @throws \yii\db\Exception
     */
    private function baseEventAction(int $docEventId, int $studentId, int $actionType)
    {
        /** @noinspection MissedFieldInspection */
        Yii::$app->db->createCommand()
            ->insert('ldg_event_activity_general', [
                'doc_event_id' => $docEventId,
                'guardian_id' => $this->guardianId,
                'student_id' => $studentId,
                'action_type' => $actionType,
                'created_at' => new Expression('NOW()'),
            ])
            ->execute();
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param int $formId
     * @throws LogicException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function acceptForm(int $docEventId, int $studentId, int $formId)
    {
        $previousAction = $this->getPreviousFormAction($docEventId, $studentId, $formId);
        if ($previousAction === self::ACTION_FORM_SIGN) {
            throw new LogicException('Form already signed.');
        } elseif ($previousAction === self::ACTION_FORM_CANCEL) {
            throw new LogicException('Form is canceled.');
        }
        DbHelper::inTransaction(function () use ($formId, $docEventId, $studentId) {
            $this->baseFormAction($docEventId, $studentId, $formId, self::ACTION_FORM_SIGN);
            $this->updateCounters($docEventId, $studentId, 0, 1);
        });
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param int $formId
     * @throws LogicException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function cancelForm(int $docEventId, int $studentId, int $formId)
    {
        $previousAction = $this->getPreviousFormAction($docEventId, $studentId, $formId);
        if ($previousAction === self::ACTION_FORM_CANCEL) {
            throw new LogicException('Form already canceled.');
        } elseif ($previousAction !== self::ACTION_FORM_SIGN) {
            throw new LogicException('Form not signed.');
        }
        DbHelper::inTransaction(function () use ($formId, $docEventId, $studentId) {
            $this->baseFormAction($docEventId, $studentId, $formId, self::ACTION_FORM_CANCEL);
            $this->updateCounters($docEventId, $studentId, 0, -1);
        });
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param int $formId
     * @throws LogicException
     * @throws \yii\db\Exception
     */
    public function skipForm(int $docEventId, int $studentId, int $formId)
    {
        $previousAction = $this->getPreviousFormAction($docEventId, $studentId, $formId);
        if (!empty($previousAction)) {
            throw new LogicException('Form has another status.');
        }
        $this->baseFormAction($docEventId, $studentId, $formId, self::ACTION_FORM_SKIP);
    }

    public function getPreviousFormAction(int $docEventId, int $studentId, int $formId)
    {
        $query = (new Query())
            ->select('action_type')
            ->from('ldg_event_activity_form')
            ->where([
                'doc_event_id' => $docEventId,
                //'guardian_id' => $this->guardianId,
                'student_id' => $studentId,
                'form_id' => $formId,
            ])
            ->orderBy('created_at desc');

        return $query->scalar();
    }

    public function getPreviousFormsActions(int $docEventId, int $studentId)
    {
        $query = (new Query())
            ->select(new Expression('DISTINCT ON("form_id") "form_id", "action_type"'))
            ->from('ldg_event_activity_form')
            ->where([
                'doc_event_id' => $docEventId,
                'student_id' => $studentId,
            ])
            ->orderBy('form_id, created_at desc');

        return $query->all();
    }

    public function getLastFormActionDate(int $docEventId, int $studentId, int $formId)
    {
        $query = (new Query())
            ->select('created_at')
            ->from('ldg_event_activity_form')
            ->where([
                'doc_event_id' => $docEventId,
                //'guardian_id' => $this->guardianId,
                'student_id' => $studentId,
                'form_id' => $formId,
                'action_type' => self::ACTION_FORM_SIGN,
            ])
            ->orderBy('created_at desc')
            ->limit(1);

        return $query->scalar();
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param int $formId
     * @param int $actionType
     * @throws \yii\db\Exception
     */
    private function baseFormAction(int $docEventId, int $studentId, int $formId, int $actionType)
    {
        /** @noinspection MissedFieldInspection */
        Yii::$app->db->createCommand()
            ->insert('ldg_event_activity_form', [
                'doc_event_id' => $docEventId,
                'guardian_id' => $this->guardianId,
                'student_id' => $studentId,
                'action_type' => $actionType,
                'form_id' => $formId,
                'created_at' => new Expression('NOW()'),
            ])
            ->execute();
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param int $purchased
     * @param int $signed
     * @throws \yii\db\Exception
     */
    public function updateCounters(int $docEventId, int $studentId, int $purchased, int $signed)
    {
        static $command;
        if (empty($command)) {
            $command = Yii::$app->db->createCommand('SELECT update_status_counters(?,?,?,?,?,?,?,?);')
                ->bindValue(6, UpcomingEvent::STATUS_PLANNED)
                ->bindValue(7, UpcomingEvent::STATUS_DECLINED)
                ->bindValue(8, UpcomingEvent::STATUS_INCOMPLETE);
        }
        $command
            ->bindParam(1, $docEventId)
            ->bindParam(2, $this->guardianId)
            ->bindParam(3, $studentId)
            ->bindParam(4, $purchased)
            ->bindParam(5, $signed);
        $command->execute();
    }

    public function updateParentStatus($docEventId, $studentId)
    {
        static $command;
        if (empty($command)) {
            $command = Yii::$app->db->createCommand('SELECT update_parent_status(?,?,?,?);')
                ->bindValue(3, UpcomingEvent::STATUS_PLANNED)
                ->bindValue(4, UpcomingEvent::STATUS_DECLINED);
        }
        $command
            ->bindParam(1, $docEventId)
            ->bindParam(2, $studentId);
        $command->execute();
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param array $columns
     * @throws \yii\db\Exception
     */
    public function createStudentStatus(int $docEventId, int $studentId, array $columns)
    {
        $columns = array_merge([
            'doc_event_id' => $docEventId,
            'student_id' => $studentId,
            'status' => UpcomingEvent::STATUS_INCOMPLETE,
            'forms_signed' => 0,
            'products_purchased' => 0,
        ], $columns);

        Yii::$app->db->createCommand()
            ->insert('ldg_event_student_status', $columns)
            ->execute();
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @param array $columns
     * @throws \yii\db\Exception
     */
    public function updateStudentStatus(int $docEventId, int $studentId, array $columns)
    {
        if (!$this->studentStatusExists($docEventId, $studentId)) {
            $this->createStudentStatus($docEventId, $studentId, $columns);
        }
        Yii::$app->db->createCommand()
            ->update('ldg_event_student_status', array_merge([
                'status' => UpcomingEvent::STATUS_INCOMPLETE,
            ], $columns), [
                'doc_event_id' => $docEventId,
                'student_id' => $studentId,
            ])
            ->execute();
    }

    /**
     * @param int $docEventId
     * @param int $studentId
     * @return bool
     */
    public function studentStatusExists(int $docEventId, int $studentId)
    {
        return (new Query())
            ->select('id')
            ->from('ldg_event_student_status')
            ->where([
                'doc_event_id' => $docEventId,
                'student_id' => $studentId,
            ])
            ->exists();
    }
}