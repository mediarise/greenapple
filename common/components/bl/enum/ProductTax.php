<?php

namespace common\components\bl\enum;

class ProductTax
{
    const EXEMPT = 0x1;
    const ZERO_RATED = 0x2;
    const HST_13 = 0x4;
    const HST_13_INCLUDED = 0x8;


    public static function list()
    {
        return [
            self::EXEMPT,
            self::ZERO_RATED,
            self::HST_13,
            self::HST_13_INCLUDED,
        ];
    }

    public static function titles()
    {
        return [
            self::EXEMPT => 'Exempt (0%)',
            self::ZERO_RATED => 'Zero-rated (0%)',
            self::HST_13 => 'HST (13%)',
            self::HST_13_INCLUDED => 'HST (13%) Included'
        ];
    }

    public static function rates()
    {
        return [
            self::EXEMPT => 1,
            self::ZERO_RATED => 1,
            self::HST_13 => 1.13,
            self::HST_13_INCLUDED => 1 / 1.13
        ];
    }

    public static function rate($taxVariant)
    {
        if (!in_array($taxVariant, self::list())) {
            throw new \InvalidArgumentException('Invalid tax variant provided.');
        }

        $list = self::rates();
        return $list[$taxVariant];
    }
}