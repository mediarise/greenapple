<?php

namespace common\components\bl\enum;

class WeekDays
{
    const MONDAY = 0x1;
    const TUESDAY = 0x2;
    const WEDNESDAY = 0x4;
    const THURSDAY = 0x8;
    const FRIDAY = 0x10;
    const SATURDAY = 0x20;
    const SUNDAY = 0x40;

    public static function asArray()
    {
        return [
            self::MONDAY => 'Monday',
            self::TUESDAY => 'Tuesday',
            self::WEDNESDAY => 'Wednesday',
            self::THURSDAY => 'Thursday',
            self::FRIDAY => 'Friday',
            self::SATURDAY => 'Saturday',
            self::SUNDAY => 'Sunday',
        ];
    }
}