<?php

namespace common\components\bl\enum;

class PredefinedPeriods
{
    const TODAY = 'today';
    const YESTERDAY = 'yesterday';
    const THIS_WEEK = 'this week';
    const LAST_WEEK = 'last week';
    const THIS_MONTH = 'this month';
    const LAST_MONTH = 'last month';
    const THIS_YEAR = 'this year';
    const LAST_YEAR = 'last year';

    public static function asArray()
    {
        return [
            self::TODAY => self::TODAY,
            self::YESTERDAY => self::YESTERDAY,
            self::THIS_WEEK => self::THIS_WEEK,
            self::LAST_WEEK => self::LAST_WEEK,
            self::THIS_MONTH => self::THIS_MONTH,
            self::LAST_MONTH => self::LAST_MONTH,
            self::THIS_YEAR => self::THIS_YEAR,
            self::LAST_YEAR => self::LAST_YEAR
        ];
    }
}