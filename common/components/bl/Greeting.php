<?php

namespace common\components\bl;

class Greeting
{
    const MR = 0x1;
    const MISS = 0x2;
    const MRS = 0x4;
    const MS = 0x8;

    private static $greetings = [
        self::MR => 'Mr.',
        self::MISS => 'Miss',
        self::MRS => 'Mrs.',
        self::MS => 'Ms.',
    ];

    public static function getNames()
    {
        return self::$greetings;
    }

    public static function getName($index)
    {
        return self::$greetings[$index];
    }
}