<?php

namespace common\components\bl;

use common\components\bl\ledger\GuardianEventLedger;
use common\exception\LogicException;
use common\helpers\ArrayHelper;
use common\models\Cart;
use common\models\EventProduct;
use common\models\GuardianCard;
use common\models\ProductPhoto;
use common\models\ProductVariation;
use common\models\PurchaseTransaction;
use Yii;
use yii\base\Exception;
use yii\db\Query;

class CartManager
{
    const CUSTOMER_TYPE_GUARDIAN = 0x1;
    public $customerId;

    private $cardTokens = [];


    public function __construct(int $guardianId)
    {
        $this->customerId = $guardianId;
    }

    /**
     * @param EventProduct $eventProduct
     * @param array $counters
     * @param int $quantity
     * @throws LogicException
     */
    private function checkQuantity(EventProduct $eventProduct, $counters, int $quantity)
    {
        $inCart = ArrayHelper::remove($counters, 'in_cart', 0);
        $purchased = ArrayHelper::remove($counters, 'purchased', 0);

        $total = $inCart + $purchased;
        $min = max(1, $eventProduct->min_quantity - $total);
        if ($quantity < $min) {
            throw new LogicException("You can't purchase less than $min product(s)");
        }
        if ($eventProduct->max_quantity === 0) {
            return;
        }
        $max = $eventProduct->max_quantity - $total;
        if ($max <= 0) {
            throw new LogicException("You've already purchased max quantity of product");
        }
        if ($quantity > $max) {
            throw new LogicException("You can't purchase greater than $max product(s)");
        }
    }

    /**
     * @param int $studentId
     * @param EventProduct $eventProduct
     * @param int $variationId
     * @param $price
     * @param int $quantity
     * @throws LogicException
     */
    public function addToCart(int $studentId, EventProduct $eventProduct, $variationId, $price, int $quantity)
    {
        if ($quantity <= 0) {
            throw new LogicException("wrong quantity");
        }
        $counters = $this->getPurchasedQuantity($studentId, $eventProduct);
        $this->checkQuantity($eventProduct, $counters, $quantity);

        $cartId = ArrayHelper::remove($counters, 'cart_id');

        if (!empty($cartId)) {
            $inCart = ArrayHelper::remove($counters, 'in_cart', 0);
            Cart::updateAll([
                'quantity' => $inCart + $quantity,
                'total' => $eventProduct->applyTax($price) * $quantity,
            ], ['id' => $cartId]);
            return;
        }

        $cart = new Cart($eventProduct->getAttributes([
            'doc_event_id',
            'product_id',
        ]));
        $cart->price = $price;
        $cart->product_variation_id = $variationId;
        $cart->guardian_id = $this->customerId;
        $cart->student_id = $studentId;
        $cart->quantity = $quantity;
        $cart->tax = $eventProduct->product->tax;
        $cart->total = $eventProduct->applyTax($price) * $quantity;
        $cart->expiry_date = (new \DateTime('next week'))->format('Y-m-d H:i:s');
        $cart->save(false);
    }

    /**
     * @param $eventId
     * @param $studentId
     * @param $products
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function addToCartMany($eventId, $studentId, $products)
    {
        $eventProducts = EventProduct::find()->where([
            'doc_event_id' => $eventId,
        ])->all();

        $variations = array_filter(array_column($products, 'variation'), 'strlen');

        $prices = ProductVariation::find()
            ->select(['id', 'price'])
            ->where(['in', 'id', $variations])
            ->asArray()
            ->all();

        $prices = ArrayHelper::map($prices, 'id', 'price');

        $transaction = Yii::$app->db->beginTransaction();
        foreach ($eventProducts as $eventProduct) {
            $item = ArrayHelper::remove($products, $eventProduct->id, []);
            $quantity = (int)ArrayHelper::remove($item, 'quantity', 0);
            $variation = ArrayHelper::remove($item, 'variation', null);
            if (empty($quantity)) {
                continue;
            }
            try {
                $this->addToCart(
                    $studentId,
                    $eventProduct,
                    $variation,
                    empty($variation) ? $eventProduct->price : $prices[$variation],
                    $quantity);
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        $transaction->commit();
    }

    private function getProductQuantityQuery(int $studentId, EventProduct $eventProduct)
    {
        return (new Query())
            ->select('quantity')
            ->from('cart')
            ->where([
                'guardian_id' => $this->customerId,
                'student_id' => $studentId,
                'doc_event_id' => $eventProduct->doc_event_id,
                'product_id' => $eventProduct->product_id,
            ]);
    }

    public function getPurchasedQuantity(int $studentId, EventProduct $eventProduct)
    {
        $ldgQuery = (new Query())
            ->select([
                'product_id',
                'sum(quantity) purchased'
            ])
            ->from('ldg_event_activity_purchase')
            ->where([
                'student_id' => $studentId,
                'doc_event_id' => $eventProduct->doc_event_id,
                'product_id' => $eventProduct->product_id,
            ])
            ->groupBy('product_id');

        return (new Query())
            ->select([
                'c.id cart_id',
                'c.quantity in_cart',
                'l.purchased',
            ])
            ->from([
                'c' => $this->getProductQuantityQuery($studentId, $eventProduct)
                    ->addSelect(['id', 'product_id']),
            ])
            ->join('FULL OUTER JOIN', ['l' => $ldgQuery], 'c.product_id = l.product_id')
            ->one();
    }

    public function generateOrderId()
    {
        return sprintf('%s-%s-%s',
            self::CUSTOMER_TYPE_GUARDIAN,
            $this->customerId,
            time()
        );
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return array_sum(array_column($this->getItems(), 'total'));
    }

    public function getTotalCount()
    {
        static $count;
        if (empty($count)) {
            $count = (new \yii\db\Query())
                ->select('SUM(quantity)')
                ->from('cart')
                ->where(['guardian_id' => $this->customerId])
                ->scalar();
        }
        return $count;
    }

    public function getProductQuantity(int $studentId, EventProduct $eventProduct)
    {
        return $this->getProductQuantityQuery($studentId, $eventProduct)->scalar();
    }

    /**
     * @return Cart[]
     */
    public function getItems()
    {
        static $items = null;
        if (empty($items)) {
            $items = Cart::find()
                ->select('cart.*, ph.url photo, eph.url event_photo')
                ->joinWith('docEvent')
                ->joinWith('docEvent.mainPhoto eph', false)
                ->with('product', 'eventProduct', 'product.variations', 'student')
                //->joinWith('eventProduct')
                //->joinWith('student')
                ->leftJoin(ProductPhoto::tableName() . ' ph', 'cart.product_id = ph.product_id AND ph.is_main')
                ->where(['guardian_id' => $this->customerId])
                ->orderBy('cart.created_at, cart.id')
                ->all();
        }
        return $items;
    }

    /**
     * @param int $id
     * @return Cart
     */
    public function getItem(int $id)
    {
        $filteredItems = array_filter($this->getItems(), function (Cart $item) use ($id) {
            return $item->id == $id;
        });
        return array_shift($filteredItems);
    }

    public function updateQuantity(int $id, int $quantity)
    {
        $item = $this->getItem($id);

        $counters = $this->getPurchasedQuantity($item->student_id, $item->eventProduct);
        $purchased = ArrayHelper::remove($counters, 'purchased', 0);

        $quantity = max($quantity, 1, $item->eventProduct->min_quantity - $purchased);
        if (!empty($item->eventProduct->max_quantity)) {
            $quantity = min($quantity, max(1, $item->eventProduct->max_quantity - $purchased));
        }
        if ($item->quantity != $quantity) {
            $item->quantity = $quantity;
            $item->total = number_format($item->eventProduct->priceWithTax * $quantity, 2, '.', '');
            $item->updateAttributes(['quantity', 'total']);
        }
    }

    public function updateVariation(int $id, int $variation)
    {
        $item = $this->getItem($id);
        $variations = $item->product->variations;

        $model = array_filter($variations, function ($model) use ($variation) {
            return $model->id == $variation;
        });
        $model = array_shift($model);

        $item->updateAttributes([
            'product_variation_id' => $variation,
            'price' => $model->price,
            'total' => number_format($item->eventProduct->applyTax($model->price) * $item->quantity, 2, '.', ''),
        ]);
    }

    /**
     * @param int $cardId
     * @return PurchaseTransaction
     * @throws \Throwable
     */
    public function beginTransaction($cardId)
    {
        $transaction = new PurchaseTransaction();
        $transaction->guardian_card_id = $cardId;
        $transaction->amount = $this->getAmount();
        if (!$transaction->save(false)) {
            throw new Exception('couldn\'t insert purchaseTransaction');
        }

        return $transaction;
    }

    /**
     * @param PurchaseTransaction $transaction
     * @param \mpgResponse $response
     * @throws \Throwable
     */
    public function endTransaction(PurchaseTransaction $transaction, $response)
    {
        $transaction->response_code = $response->getResponseCode();
        $transaction->iso = $response->getISO();
        $transaction->date_time = $response->getTransDate() . ' ' . $response->getTransTime();
        $transaction->type = $response->getTransType();
        $transaction->amount = $response->getTransAmount();
        $transaction->number = $response->getTxnNumber();

        \Yii::$app->db->transaction(function () use ($transaction) {
            $ledger = new GuardianEventLedger($this->customerId);
            $ledger->purchaseByCart($this->getItems(), $transaction->id);
            Cart::deleteAll(['guardian_id' => $this->customerId]);
            if (!$transaction->save(false)) {
                throw new Exception('couldn\'t update purchaseTransaction');
            }
        });
    }

    /**
     * @throws \Throwable
     */
    public function acceptCart()
    {
        \Yii::$app->db->transaction(function () {
            $ledger = new GuardianEventLedger($this->customerId);
            $ledger->purchaseByCart($this->getItems(), null);
            Cart::deleteAll(['guardian_id' => $this->customerId]);
        });
    }

    public function getCardToken($cardId)
    {
        if (isset($this->cardTokens[$cardId])) {
            return $this->cardTokens[$cardId];
        }
        return $this->cardTokens[$cardId] = GuardianCard::find()
            ->select('token')
            ->where(['id' => $cardId])
            ->scalar();
    }

    public function getCardsCount()
    {
        return GuardianCard::find()
            ->where(['guardian_id' => $this->customerId])
            ->count();
    }

    /**
     * @param int $guardianId
     * @param string $dataKey
     * @param string $cardMask
     * @param int $orderId
     * @return GuardianCard|null
     * @throws \Throwable
     */
    public function registerCard(int $guardianId, $dataKey, $cardMask, int $orderId)
    {
        Yii::$app->db->transaction(function () use ($orderId, $cardMask, $dataKey, $guardianId) {
            $card = new GuardianCard([
                'guardian_id' => $guardianId,
                'token' => $dataKey,
                'mask' => $cardMask,
            ]);
            if (!$card->save(false)) {
                throw new Exception('couldn\'t insert guardianCard');
            }
            PurchaseTransaction::updateAll([
                'guardian_card_id' => $card->id,
            ], [
                'id' => $orderId,
            ]);
        });
        return null;
    }
}