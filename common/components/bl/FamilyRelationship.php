<?php

namespace common\components\bl;


class FamilyRelationship
{
    const PARENT = 0x1;
    const GUARDIAN = 0x2;
    const GRANDPARENT = 0x4;
    const STEPPARENT = 0x8;
    const ADOPTIVE_PARENT = 0x10;
    const AUNT_UNCLE = 0x20;

    private static $relationships = [
        self::PARENT => 'Parent',
        self::GUARDIAN => 'Guardian',
        self::GRANDPARENT => 'Grandparent',
        self::STEPPARENT => 'Stepparent',
        self::ADOPTIVE_PARENT => 'Adoptive parent',
        self::AUNT_UNCLE => 'Aunt/Uncle',
    ];

    public static function getNames()
    {
        return self::$relationships;
    }

    public static function getName($index)
    {
        return self::$relationships[$index];
    }
}