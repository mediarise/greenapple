<?php
namespace common\components\bl\constraint;

use yii\db\ActiveQuery;

class BoardAdminConstraint implements Constraint
{
    private $userId;
    private $boardId;

    public function __construct($userId, $boardId)
    {
        $this->userId = $userId;
        $this->boardId = $boardId;
    }

    public function constrainSchool(ActiveQuery $query)
    {
        $query->where(['school.board_id' => $this->boardId]);
    }

    public function loadDefaultsForSchool($model)
    {
        $model->board_id = $this->boardId;
    }
}