<?php
namespace common\components\bl\constraint;

use yii\db\ActiveQuery;

interface Constraint
{
    public function constrainSchool(ActiveQuery $query);
}