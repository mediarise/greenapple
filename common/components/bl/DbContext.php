<?php
/**
 * Created by PhpStorm.
 * User: crunc
 * Date: 4/27/2018
 * Time: 4:09 PM
 */

namespace common\components\bl;


use common\components\bl\constraint\Constraint;
use common\models\School;

class DbContext
{
    /**
     * @var Constraint
     */
    private $constraint;

    public function __construct(Constraint $constraint)
    {
        $this->constraint = $constraint;
    }

    public function findSchool()
    {
        $query = School::find();
        $this->constraint->constrainSchool($query);
        return $query;
    }
}