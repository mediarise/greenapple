<?php

namespace common\components\bl;

use common\components\AjaxResponse;
use common\models\Guardian;
use common\models\User;
use Yii;
use yii\web\BadRequestHttpException;
use common\components\DbHelper;

class GuardianManager
{
    private $guardians = [];

    public function newGuardian($config)
    {
        $guardian = new Guardian();

        $user = new User();

        if (($guardian->load($config) & $user->load($config, 'User')) &&
            ($guardian->validate() & $user->validate('email'))) {
            $this->fillUser($user);
            $transaction = \Yii::$app->db->beginTransaction();
            $user->save(false);
            $guardian->link('user', $user);
            $transaction->commit();
        }

        return [$guardian, $user];
    }

    private function fillUser($user)
    {
        $user->username = $user->email;
        $user->status = User::STATUS_INACTIVE;
        $user->password_hash = \Yii::$app->security->generateRandomString();
        $user->generateAuthKey();
    }

    public function invite($guardianId, SignupRequestNotifier $notifier)
    {
        return DbHelper::inTransaction(function () use ($guardianId, $notifier) {
            $guardian = $this->getGuardian($guardianId);
            if (!empty($guardian->invite_token)) {
                $notifier->notifyApprove($guardian);
                return AjaxResponse::success();
            }
            $guardian->invite_token = Yii::$app->security->generateRandomString();
            $guardian->save(false);

            $user = $guardian->user;
            $user->status = User::STATUS_INVITED;
            $user->save(false);

            $notifier->notifyApprove($guardian);

            return AjaxResponse::success();
        });
    }

    public function getGuardian($guardianId)
    {
        if (array_key_exists($guardianId, $this->guardians)) {
            return $this->guardians[$guardianId];
        }
        $guardian = Guardian::findOne($guardianId);
        $this->guardians[$guardianId] = $guardian;
        return $guardian;
    }

    public function switchUserStatus($userId)
    {
        $user = User::findOne($userId);
        if (!($user->status === User::STATUS_ACTIVE || $user->status === User::STATUS_BANNED)) {
            throw new BadRequestHttpException('Wrong user status');
        }
        $user->status = $user->status === User::STATUS_BANNED ? User::STATUS_ACTIVE : User::STATUS_BANNED;
        $user->save(false);
        return $user->status;
    }

}