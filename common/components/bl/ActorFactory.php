<?php

namespace common\components\bl;

use common\models\BoardAdmin;
use common\models\SchoolAdmin;
use common\models\Teacher;
use common\models\Guardian;

class ActorFactory
{
    public static function create($userRole, $userId)
    {
        switch ($userRole) {
            case 'guardian':
                $actorClass = Guardian::class;
                break;
            case 'teacher':
                $actorClass = Teacher::class;
                break;
            case 'schoolAdmin':
                $actorClass = SchoolAdmin::class;
                break;
            case 'boardAdmin':
                $actorClass = BoardAdmin::class;
                break;
            default:
                throw new \ErrorException('Invalid actor type.');
        }

        /* @var $actorClass \yii\db\ActiveRecord */
        if ($actor = $actorClass::findOne(['user_id' => $userId])) {
            /* @var $actor Actor */
            $actor->role = $userRole;
            return $actor;
        }

        throw new \ErrorException('Unable to find actor by provided ID.');
    }
}