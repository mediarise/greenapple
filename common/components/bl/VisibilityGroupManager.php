<?php


namespace common\components\bl;


use common\helpers\ArrayHelper;
use common\models\Grade;
use common\models\GradeQuery;
use common\models\SchoolAdmin;
use common\models\Student;
use common\models\Teacher;
use common\models\teacher\VisibilityGroupForm;
use common\models\TeacherQuery;
use common\models\VisibilityGroup;
use ErrorException;
use Yii;
use yii\base\BaseObject;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class VisibilityGroupManager extends BaseObject
{
    /* @var $actor Teacher|SchoolAdmin */
    public $actor;

    /**
     * @param $id
     * @return bool
     * @throws ErrorException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function delete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $visibilityGroup = $this->findModel($id);

            $studentGroupMembers = Student::find()->inVisibilityGroup($id)->all();
            foreach ($studentGroupMembers as $groupMember) {
                $visibilityGroup->unlink('students', $groupMember, true);
            }

            $gradeGroupMembers = Grade::find()->inVisibilityGroup($id)->all();
            foreach ($gradeGroupMembers as $groupMember) {
                $visibilityGroup->unlink('grades', $groupMember, true);
            }

            if (!$visibilityGroup->delete()) {
                throw new ErrorException('Unable to delete visibility group.');
            }

            $transaction->commit();
            return true;

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new ErrorException('Unable to delete visibility group. Please try again later.');
        }

    }

    /**
     * @return GradeQuery
     */
    public function getGradeQuery()
    {
        $schoolId = $this->actor->school->id;
        $query = Grade::find()
            ->alias('t')
            ->select(['t.*', 'count(student.id)'])
            ->linkedWithStudent()
            ->where('student.grade_id = t.id')
            ->andWhere(['t.school_id' => $schoolId])
            ->groupBy('t.id')
            ->having('count(student.id) > 0');
        return $query;
    }

    /**
     * @return TeacherQuery
     */
    public function getTeacherQuery()
    {
        $query = Teacher::find()
            ->alias('t')
            ->where(['t.school_id' => $this->actor->school_id]);
        return $query;
    }

    public function getTeachers()
    {
        $teachers = $this->getTeacherQuery()->all();
        return $teachers ? ArrayHelper::map(
            $teachers,
            function (Teacher $model) {
                return $model->id;
            },
            function (Teacher $model) {
                return $model->getFullName();
            }
        ) : [];
    }

    public function getGradeList($teacherId = null)
    {
        $query = $this->getGradeQuery();
        $query->asArray();

        if ($teacherId !== null) {
            $query->linkedWithTeacher($teacherId);
        }
        $grades = $query->all();
        return $grades ? ArrayHelper::map($grades, 'id', 'name') : [];
    }

    public function getCurrentYearGrades($teacherId = null)
    {
        $query = $this->getGradeQuery();
        $query->asArray()
            ->andWhere(['t.year' => Grade::getCurrentAcademicYear()]);

        if ($teacherId !== null) {
            $query->linkedWithTeacher($teacherId);
        }
        $grades = $query->all();

        return $grades ? ArrayHelper::map($grades, 'id', 'name') : [];
    }

    public function getStudentMemberedList($relatedStudents, array $members = [])
    {
        if ($members) {
            $members = ArrayHelper::getColumn($members, 'id');
        }
        return ArrayHelper::map($relatedStudents, null, function ($student) use ($members) {
            /* @var $student Student */
            return [
                'id' => $student->id,
                'name' => $student->getFullName(),
                'is_member' => ArrayHelper::isIn($student->id, $members)
            ];
        }, 'grade_id');
    }

    public function getGradeMemberedList($relatedGrades, array $members = [])
    {
        if ($members) {
            $members = ArrayHelper::getColumn($members, 'id');
        }

        return ArrayHelper::map($relatedGrades, null, function ($grade) use ($members) {
            /* @var $grade Grade */
            return [
                'id' => $grade->id,
                'name' => $grade->name,
                'is_member' => ArrayHelper::isIn($grade->id, $members)
            ];
        });
    }

    public function getTeacherMemberedList($relatedTeachers, array $members = [])
    {
        if ($members) {
            $members = ArrayHelper::getColumn($members, 'id');
        }

        return ArrayHelper::map($relatedTeachers, null, function (Teacher $teacher) use ($members) {
            /* @var $grade Grade */
            return [
                'id' => $teacher->id,
                'name' => $teacher->getFullName(),
                'is_member' => ArrayHelper::isIn($teacher->id, $members)
            ];
        });
    }

    public function save(VisibilityGroup $visibilityGroup, VisibilityGroupForm $model)
    {
        $visibilityGroup->school_id = $this->actor->school_id;
        $visibilityGroup->board_id = $this->actor->board->id;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$visibilityGroup->save()) {
                throw new ErrorException('Unable to save visibility group.');
            }
            $visibilityGroup->unlinkAll('grades', true);
            $visibilityGroup->unlinkAll('students', true);
            $visibilityGroup->unlinkAll('teachers', true);

            switch ($model->member_type) {
                case VisibilityGroup::MEMBER_TYPE_GRADE :
                    if (empty($model->grades)) {
                        throw new \Exception();
                    }
                    $grades = Grade::find()->where(['id' => $model->grades])->all();
                    foreach ($grades as $grade) {
                        $visibilityGroup->link('grades', $grade, [
                            'member_type' => VisibilityGroup::MEMBER_TYPE_GRADE,
                        ]);
                    }
                    break;

                case VisibilityGroup::MEMBER_TYPE_STUDENT :
                    if (empty($model->students)) {
                        throw new \Exception();
                    }
                    $students = Student::find()->where(['in', 'id', $model->students])->all();
                    foreach ($students as $student) {
                        $visibilityGroup->link('students', $student, [
                            'member_type' => VisibilityGroup::MEMBER_TYPE_STUDENT,
                        ]);
                    }
                    break;
                case VisibilityGroup::MEMBER_TYPE_TEACHER :
                     if (empty($model->teachers)) {
                        throw new \Exception();
                    }
                    $teachers = Teacher::find()->where(['in', 'id', $model->teachers])->all();
                    foreach ($teachers as $teacher) {
                        $visibilityGroup->link('teachers', $teacher, [
                            'member_type' => VisibilityGroup::MEMBER_TYPE_TEACHER,
                        ]);
                    }
                    break;
                default :
                    throw new \Exception();
                    break;
            }
            $transaction->commit();
            return true;

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new ServerErrorHttpException('Unable to save visibility group. Please try again later.');
        }
    }


    /**
     * @param $id
     * @return VisibilityGroup
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if ($visibilityGroup = VisibilityGroup::findOne($id)) {
            return $visibilityGroup;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}