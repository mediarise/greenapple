<?php

namespace common\components\bl;

use yii\db\ActiveRecord;
use common\models\User;

/**
 * @property int $id
 * @property int $user_id
 * @property int $first_name
 * @property int $last_name
 * @property string $role
 *
 * @property User $user
 */
class Actor extends ActiveRecord
{
    public $role;

    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->user->id;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getEmail()
    {
        return $this->user->email;
    }
}