<?php

namespace common\components\bl\teacher;

use common\models\DocEvent;

class NutritionEventSubManager extends EventSubManager
{
    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    public function validatePublish(DocEvent $event)
    {
        parent::validatePublish($event);

        $this->validateOnlyOneProduct($event);
        //$this->validateProductsWithPrice($event);
        $this->validateNoForms($event);
    }
}