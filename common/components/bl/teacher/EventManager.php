<?php

namespace common\components\bl\teacher;

use common\components\bl\ledger\EventLedger;
use common\components\CommaDividedListConverter;
use common\models\DocEvent;
use common\models\teacher\event\ReminderForm;
use common\models\EventProduct;
use common\models\LnkEventForm;
use common\models\Product;
use common\models\Form;
use Yii;
use yii\base\ErrorException;
use common\models\DocEventForm;
use common\models\EventPhoto;
use common\models\EventVisibilitySection;
use common\helpers\ArrayHelper;
use common\models\VisibilityGroup;
use yii\db\Query;

class EventManager extends \common\components\bl\EventManager
{
    /** @var DocEvent */
    private $target;

    /** @var EventSubManager */
    private $subManager;

    /**
     * EventManager constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function setTarget(DocEvent $value)
    {
        $this->target = $value;
        $this->subManager = $this->createSubManager($value);
    }

    private function createSubManager(DocEvent $event)
    {
        switch ($event->type) {
            case DocEvent::EVENT_TYPE_NUTRITION:
            case DocEvent::EVENT_TYPE_NUTRITION_ONE_TIME:
                return new NutritionEventSubManager();
            case DocEvent::EVENT_TYPE_STOCK_THE_CLASSROOM:
            case DocEvent::EVENT_TYPE_DONATIONS:
                return new DonationEventSubManager();
            case DocEvent::EVENT_TYPE_SELL_PRODUCT:
            case DocEvent::EVENT_TYPE_FUNDRAISING_SELL:
                return new SellProductEventSubManager();
            case DocEvent::EVENT_TYPE_FILL_OUT_FORM:
                return new FillOutFormEventSubManager();
            default:
                return new EventSubManager();
        }
    }

    public function publish($id)
    {
        /** @var DocEvent $event */
        $event = $this->find($id);

        if ($event->status !== DocEvent::STATUS_SAVED) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $event->status = DocEvent::STATUS_PUBLISHED;

            if (!$event->save()) {
                throw new ErrorException('Unable to save event.');
            }
            if ($event->is_repeatable) {
                $eventDataProvider = $event->getPeriodicEventManager();
                $eventDataProvider->publish();

                EventLedger::createRepeatableStatus($id);
            } else {
                EventLedger::createStatus($event);
            }
            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }

        return true;
    }

    public function create(DocEventForm $event, EventPhoto $photo, $ownerId)
    {
        $this->event = $event;
        $event->status = DocEvent::STATUS_NEW;

        Yii::$app->db->transaction(function () use ($event, $photo, $ownerId) {
            $event->creator_id = $ownerId;
            if (!$event->save(false)) {
                throw new ErrorException('Unable to create event.');
            }

            $this->attachPhoto($photo, $event->mainPhotoUrl, $event->id);
            $this->createVisibilitySection();
        });
    }

    public function update(
        DocEventForm $event, EventPhoto $photo, EventVisibilitySection $visibilitySection = null,
        $forms = null, $products = null, $reminder = null
    )
    {

        $this->event = $event;
        $event->status = DocEvent::STATUS_SAVED;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$event->save()) {
                throw new ErrorException('Unable to update event.');
            }

            $this->attachPhoto($photo, $event->mainPhotoUrl, $event->id);
            $this->updateVisibilitySection($visibilitySection);
            $this->updateProducts($event->id, $products);
            $this->updateForms($event->id, $forms);
            $this->updateReminder($reminder);

            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    protected function createVisibilitySection()
    {
        $visibilitySection = new EventVisibilitySection();
        $visibilitySection->doc_event_id = $this->event->id;
        if (!$visibilitySection->save()) {
            throw new ErrorException('Unable to save visibility section.');
        }
    }

    protected function updateVisibilitySection(EventVisibilitySection $visibilitySection = null)
    {
        if (!$visibilitySection || !$visibilitySection->getSelectedItems()) {
            return true;
        }

        $members = [];
        $selectedItems = CommaDividedListConverter::toArray(
            $visibilitySection->getSelectedItems()
        );

        foreach ($selectedItems as $memberId) {
            $members[] = [
                $visibilitySection->id,
                $visibilitySection->getMemberType(),
                $memberId
            ];
        }

        Yii::$app->db->transaction(function () use ($visibilitySection, $members) {
            Yii::$app->db->createCommand()
                ->delete('visibility_group_member', ['visibility_group_id' => $visibilitySection->id])
                ->execute();
            Yii::$app->db->createCommand()
                ->batchInsert(
                    'visibility_group_member',
                    ['visibility_group_id', 'member_type', 'member_id'],
                    $members
                )
                ->execute();
        });

        return true;
    }

    public function findVisibilitySection($docEventId)
    {
        $visibilitySection = EventVisibilitySection::find()->where(['doc_event_id' => $docEventId])->one();
        if (!$visibilitySection) {
            throw new ErrorException('Unable to find visibility section.');
        }

        $members = (new Query())
            ->select('*')
            ->from('visibility_group_member vgm')
            ->where(['visibility_group_id' => $visibilitySection->id])
            ->all();

        if (!$members) {
            $visibilitySection->filter_by = EventVisibilitySection::FILTER_BY_STUDENTS;
            return $visibilitySection;
        }

        $memberType = $members[0]['member_type'];
        $memberIds = ArrayHelper::getColumn($members, 'member_id');

        if ($memberType) {
            switch ($memberType) {
                case VisibilityGroup::MEMBER_TYPE_STUDENT:
                    $visibilitySection->filter_by = EventVisibilitySection::FILTER_BY_STUDENTS;
                    $visibilitySection->students = $memberIds;
                    break;
                case VisibilityGroup::MEMBER_TYPE_GRADE:
                    $visibilitySection->filter_by = EventVisibilitySection::FILTER_BY_GRADES;
                    $visibilitySection->grades = $memberIds;
                    break;
                case VisibilityGroup::MEMBER_TYPE_VISIBILITY_GROUP:
                    $visibilitySection->filter_by = EventVisibilitySection::FILTER_BY_VISIBILITY_GROUPS;
                    $visibilitySection->visibility_groups = $memberIds;
                    break;
                default:
                    throw new ErrorException('Invalid visibility section member type.');
            }
        }

        return $visibilitySection;
    }

    public function findProducts($docEventId)
    {
        $event = $this->find($docEventId);

        return Product::find()
            ->alias('t')
            ->indexBy('id')
            ->forThisUser()
            ->all();
    }

    public function findForms($docEventId)
    {
        $event = $this->find($docEventId);

        return Form::find()
            ->alias('t')
            ->indexBy('id')
            ->all();
    }

    protected function updateProducts($docEventId, $checkedProducts = null)
    {
        $event = $this->find($docEventId);
        $products = $this->findProducts($docEventId);

        Yii::$app->db->transaction(function () use ($event, $products, $checkedProducts) {
            $event->unlinkAll('products', true);

            $productIds = ArrayHelper::getColumn($products, 'id', false);

            /* @var $checkedProducts array */
            $dateTime = date('Y-m-d H:i:s');

            $productsToSave = [];
            foreach ($checkedProducts as $checkedProduct) {
                if (!ArrayHelper::isIn($checkedProduct->product_id, $productIds)) {
                    continue;
                }
                /* @var $checkedProduct EventProduct */
                $productsToSave[] = [
                    $event->id,
                    (int)$checkedProduct->product_id,
                    (int)$checkedProduct->price,
                    (int)$checkedProduct->price,
                    (int)$checkedProduct->min_quantity,
                    (int)$checkedProduct->max_quantity,
                    $checkedProduct->created_at ?? $dateTime,
                    $dateTime
                ];
            }

            Yii::$app->db->createCommand()
                ->batchInsert(
                    EventProduct::tableName(),
                    ['doc_event_id', 'product_id', 'price', 'cost', 'min_quantity', 'max_quantity', 'created_at', 'updated_at'],
                    $productsToSave
                )
                ->execute();
        });
    }

    protected function updateForms($docEventId, $checkedForms = null)
    {
        $event = $this->find($docEventId);
        $forms = $this->findForms($docEventId);

        Yii::$app->db->transaction(function () use ($event, $forms, $checkedForms) {
            $event->unlinkAll('forms', true);
            $formIds = ArrayHelper::getColumn($forms, 'id', false);

            /* @var $checkedForms array */
            $dateTime = date('Y-m-d H:i:s');

            $formsToSave = [];
            foreach ($checkedForms as $checkedForm) {
                if (!ArrayHelper::isIn($checkedForm->form_id, $formIds)) {
                    continue;
                }
                /* @var $checkedForm LnkEventForm */
                $formsToSave[] = [
                    $event->id,
                    (int)$checkedForm->form_id,
                    (int)$checkedForm->is_mandatory,
                    $checkedForm->created_at ?? $dateTime,
                    $dateTime
                ];
            }

            Yii::$app->db->createCommand()
                ->batchInsert(
                    'lnk_event_form',
                    ['doc_event_id', 'form_id', 'is_mandatory', 'created_at', 'updated_at'],
                    $formsToSave
                )
                ->execute();
        });
    }

    public function updateReminder(ReminderForm $reminderModel)
    {
        return $reminderModel->save();
    }

    /**
     * @param $id
     * @return \common\models\DocEventForm
     * @throws \yii\base\ErrorException
     */
    public function find($id)
    {
        if ($this->event) {
            return $this->event;
        }

        $event = DocEventForm::find()
            ->where(['id' => $id])
            ->andWhere(['id' => $id])// TODO: event owner_id
            ->with('mainPhoto', 'products')
            ->one();

        if (!$event) {
            throw new ErrorException('Unable to find event.');
        }

        if ($event->mainPhoto) {
            $event->mainPhotoUrl = $event->mainPhoto->url;
        }

        return $this->event = $event;
    }

    /**
     * @throws PublishErrorException
     */
    public function validatePublish()
    {
        $this->subManager->validatePublish($this->event);
    }
}