<?php

namespace common\components\bl\teacher;

use common\models\DocEvent;

class DonationEventSubManager extends EventSubManager
{
    public function validatePublish(DocEvent $event)
    {
        parent::validatePublish($event);

        $this->validateAtLeastOneProduct($event);
        $this->validateFreeProducts($event);
        $this->validateNoForms($event);
    }
}