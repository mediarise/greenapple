<?php

namespace common\components\bl\teacher;

use common\models\DocEvent;
use common\models\Product;
use yii\db\Query;

class EventSubManager
{
    public function validate()
    {

    }

    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    public function validatePublish(DocEvent $event)
    {
        $vgQuery = (new Query())
            ->select('id')
            ->from('visibility_group')
            ->where(['doc_event_id' => $event->id]);

        $hasParticipants = (new Query())
            ->from('visibility_group_member')
            ->where(['visibility_group_id' => $vgQuery])
            ->exists();

        if (!$hasParticipants) {
            throw new PublishErrorException('Please add participants before publishing.', 'visibility-section');
        }
    }

    protected function findProduct(DocEvent $event, $predicate)
    {
        foreach ($event->products as $product) {
            if (call_user_func($predicate, $product)) {
                return true;
            }
        }
        return false;
    }

    protected function hasFreeProduct(DocEvent $event)
    {
        return $this->findProduct($event, function (Product $product) {
            return $product->is_free;
        });
    }

    protected function hasProductWithPrice(DocEvent $event)
    {
        return $this->findProduct($event, function (Product $product) {
            return !$product->is_free;
        });
    }

    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    protected function validateOnlyOneProduct(DocEvent $event)
    {
        if (count($event->products) != 1) {
            throw new PublishErrorException('Please select one product.', 'products');
        }
    }

    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    protected function validateAtLeastOneProduct(DocEvent $event)
    {
        if (count($event->products) < 1) {
            throw new PublishErrorException('Please select at least one product.', 'products');
        }
    }

    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    protected function validateProductsWithPrice(DocEvent $event)
    {
        if ($this->hasFreeProduct($event)) {
            throw new PublishErrorException('Please select product with price.', 'products');
        }
    }

    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    protected function validateFreeProducts(DocEvent $event)
    {
        if ($this->hasProductWithPrice($event)) {
            throw new PublishErrorException('Please select free products.', 'products');
        }
    }

    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    protected function validateNoForms(DocEvent $event)
    {
        if (count($event->forms) > 0) {
            throw new PublishErrorException('Event shouldn\'t have forms', 'forms');
        }
    }

    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    protected function validateNoProducts(DocEvent $event)
    {
        if (count($event->products) > 0) {
            throw new PublishErrorException('Event shouldn\'t have products', 'products');
        }
    }

    /**
     * @param DocEvent $event
     * @throws PublishErrorException
     */
    protected function validateAtLeastOneForm(DocEvent $event) {
        if (count($event->forms) < 1) {
            throw new PublishErrorException('Please select at least one form.', 'forms');
        }
    }
}