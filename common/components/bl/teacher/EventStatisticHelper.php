<?php


namespace common\components\bl\teacher;


use common\helpers\ArrayHelper;
use common\models\teacher\VisibilityGroup;

class EventStatisticHelper
{
    public static function getStudentIds($eventId)
    {
        $mt_tudent = VisibilityGroup::MEMBER_TYPE_STUDENT;
        $mt_teacher = VisibilityGroup::MEMBER_TYPE_TEACHER;
        $mt_grade = VisibilityGroup::MEMBER_TYPE_GRADE;
        $mt_school = VisibilityGroup::MEMBER_TYPE_SCHOOL;
        $mt_group = VisibilityGroup::MEMBER_TYPE_VISIBILITY_GROUP;

        $studentIds = \Yii::$app->db->createCommand(
            "WITH vsm AS (
                  SELECT
                    v.doc_event_id,
                    m2.member_type,
                    m2.member_id
                  FROM visibility_group v
                    LEFT JOIN visibility_group_member m2 ON v.id = m2.visibility_group_id
                  WHERE doc_event_id = '{$eventId}'
              ), vgm AS (
                SELECT
                  vsm.doc_event_id,
                  m2.member_type,
                  m2.member_id
                FROM vsm
                  LEFT JOIN visibility_group_member m2 ON vsm.member_id = m2.visibility_group_id
                WHERE vsm.member_type = {$mt_group}
                UNION
                SELECT *
                FROM vsm
                WHERE vsm.member_type != {$mt_group}
              )
                  SELECT
                    student_id
                  FROM
                    (SELECT DISTINCT
                       doc_event_id,
                       CASE WHEN vgm.member_type = {$mt_tudent}
                         THEN vgm.member_id
                       ELSE s.id END student_id
                     FROM vgm
                       LEFT JOIN lnk_teacher_grade l ON vgm.member_type = {$mt_teacher} AND vgm.member_id = l.teacher_id
                       LEFT JOIN grade g ON vgm.member_type = {$mt_school} AND vgm.member_id = g.school_id
                       LEFT JOIN student s ON (vgm.member_type = {$mt_teacher} AND l.grade_id = s.grade_id)/*by teacher*/ OR
                                              (vgm.member_type = {$mt_grade} AND vgm.member_id = s.grade_id) /*by grade*/ OR
                                              (vgm.member_type = {$mt_school} AND g.id = s.grade_id) /*by school*/) s
                  ;")->queryAll();
        return ArrayHelper::map($studentIds, null, 'student_id');
    }
}