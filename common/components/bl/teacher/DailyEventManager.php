<?php

namespace common\components\bl\teacher;

use common\components\DateHelper;
use common\components\Formatter;
use common\helpers\ArrayHelper;
use common\models\DocEvent;
use common\models\EventProduct;
use common\models\Holiday;
use DateTime;
use Yii;

class DailyEventManager
{
    protected $docEvent;
    protected $firstDocEventId;
    protected $quantity = 1;
    protected $index = 0;

    protected $startDate;
    protected $endDate;

    protected $productMultipliers = [];

    public function __construct(DocEvent $docEvent)
    {
        $this->docEvent = $docEvent;
        $this->startDate = new DateTime($docEvent->start_date);
        $this->endDate = new DateTime($docEvent->end_date);
        DateHelper::endOfDay($this->endDate);
    }

    public function publish()
    {
        $command = Yii::$app->db->createCommand()
            ->batchInsert('doc_event', [
                'predecessor',
                'type',
                'title',
                'is_required',
                'is_parent_action_required',
                'umbrella_account',
                'ledger_account_id',
                'start_date',
                'end_date',
                'due_date',
                'status',
                'created_at',
                'updated_at',
            ], $this->getEventRows());
        $count = $command->execute();
        $lastId = Yii::$app->db->getLastInsertID();

        $this->firstDocEventId = $lastId - $count + 1;

        $command = Yii::$app->db->createCommand()
            ->batchInsert('lnk_event_product', [
                'doc_event_id',
                'product_id',
                'price',
                'cost',
                'min_quantity',
                'max_quantity',
                'min_interval',
                'created_at',
                'updated_at'
            ], $this->getProductRows());
        $command->execute();
    }

    public function getEventRows()
    {
        $docEvent = $this->docEvent;
        foreach ($this->getDays() as $day) {
            $this->productMultipliers[] = $this->quantity;
            $endDate = $this->getEndDate($day);
            yield [
                $docEvent->id, // predecessor
                $docEvent->type,
                $this->getTitle($day, $endDate),
                $docEvent->is_required,
                $docEvent->is_parent_action_required,
                $docEvent->umbrella_account,
                $docEvent->ledger_account_id,
                $day, // start_date
                $endDate,
                $this->getDueDate($day),
                $docEvent->status,
                'NOW()',
                'NOW()',
            ];
        }
    }

    private function getProductRows()
    {
        $products = $this->docEvent->lnkEventProducts;
        $docEventId = $this->firstDocEventId;
        foreach ($this->productMultipliers as $multiplier) {
            foreach ($products as $product) {
                /** @var EventProduct $product */
                yield [
                    $docEventId,
                    $product->product_id,
                    $product->price,
                    $product->cost,
                    $product->min_quantity * $multiplier,
                    $product->max_quantity * $multiplier,
                    $product->min_interval,
                    'NOW()',
                    'NOW()',
                ];
            }
            $docEventId++;
        }
    }

    public function getDays()
    {
        $docEvent = $this->docEvent;
        $daysOfWeek = $docEvent->getRepeatOnDaysNames();
        $holidayDates = Holiday::find()
            ->select('start_date, end_date')
            ->asArray()
            ->where([
                'and',
                ['>', 'start_date', $docEvent->start_date],
                ['<', 'start_date', $docEvent->end_date],
            ])
            ->all();
        $holidays = [];
        foreach ($holidayDates as $dates) {
            $holidays = ArrayHelper::merge($holidays, DateHelper::getDaysBetweenDatesIncluded($dates['start_date'], $dates['end_date']));
        }
        $holidays = array_flip($holidays);

        $formattedStartDate = Formatter::date($this->startDate);
        $endDate = $this->endDate;
        $formattedEndDate = Formatter::date($endDate);

        $current = clone $this->startDate;
        DateHelper::beginningOfWeek($current);


        while ($current <= $endDate) {
            foreach ($daysOfWeek as $dayOfWeek) {
                $date = $current
                    ->modify($dayOfWeek)
                    ->format(Formatter::DATE_FORMAT);

                if (DateHelper::between($date, $formattedStartDate, $formattedEndDate) && !key_exists($date, $holidays)) {
                    yield $date;
                }
            }
            $current->modify('next monday');
        }
    }

    public function getEndDate($date)
    {
        return DateHelper::endOfDay($date);
    }

    public function getTitle($startDate, $endDate)
    {
        return sprintf('%s %s', $this->docEvent->title, Formatter::shortDate($startDate));
    }

    public function getDueDate($day)
    {
        $event = $this->docEvent;

        $day = new DateTime($day);
        $duration = $event->due_lead_period;
        switch ($event->due_unit) {
            case DocEvent::DURATION_UNIT_MINUTE :
                $day->modify("-{$duration} minutes");
                break;
            case DocEvent::DURATION_UNIT_HOUR :
                $day->modify("-{$duration} hours");
                break;
            case DocEvent::DURATION_UNIT_DAY :
                $day->modify("-{$duration} days");
                break;
            case DocEvent::DURATION_UNIT_WEEK :
                $day->modify("-{$duration} weeks");
                break;
            case DocEvent::DURATION_UNIT_MONTH :
                $day->modify("-{$duration} months");
                break;

        }
        return Formatter::dateTime($day);
    }
}