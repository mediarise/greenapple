<?php

namespace common\components\bl\teacher;

use common\components\DateHelper;
use common\components\Formatter;
use DateTime;
use yii\helpers\ArrayHelper;

class WeeklyEventManager extends DailyEventManager
{
    public function getDays()
    {
        $days = ArrayHelper::map(parent::getDays(), function ($day) {
            return $day;
        }, function ($day) {
            return $day;
        }, function ($day) {
            return Formatter::date(DateHelper::beginningOfWeek($day));
        });

        $startDate = $this->startDate;
        $endDate = $this->endDate;
        $current = clone $startDate;

        DateHelper::beginningOfWeek($current);

        while ($current < $endDate) {
            $date = Formatter::date($current);
            if (key_exists($date, $days)) {
                $this->quantity = count($days[$date]);
                $this->index = $this->index + 1;
                yield Formatter::date(DateHelper::max($startDate, $current));
            }
            $current->modify('+7 days');
        }
    }

    public function getEndDate($date)
    {
        $date = new DateTime($date);
        return DateHelper::endOfDay(DateHelper::min($date->modify('sunday'), $this->endDate));
    }

    public function getTitle($startDate, $endDate)
    {
        return sprintf('%s %s - %s', $this->docEvent->title, Formatter::shortDate($startDate), Formatter::shortDate($endDate));
    }
}