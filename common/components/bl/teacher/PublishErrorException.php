<?php

namespace common\components\bl\teacher;

use Throwable;
use yii\base\Exception;

class PublishErrorException extends Exception
{
    public $tab;

    public function __construct(string $message = "", string $tab, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
        $this->tab = $tab;
    }
}