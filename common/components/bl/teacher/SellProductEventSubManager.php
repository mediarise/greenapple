<?php

namespace common\components\bl\teacher;

use common\models\DocEvent;

class SellProductEventSubManager extends EventSubManager
{
    public function validatePublish(DocEvent $event)
    {
        parent::validatePublish($event);

        $this->validateAtLeastOneProduct($event);
        //$this->validateProductsWithPrice($event);
        $this->validateNoForms($event);
    }
}