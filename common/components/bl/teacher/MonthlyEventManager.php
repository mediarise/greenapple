<?php

namespace common\components\bl\teacher;

use common\components\DateHelper;
use common\components\Formatter;
use common\models\DocEvent;
use common\models\Form;
use DateTime;
use yii\helpers\ArrayHelper;

class MonthlyEventManager extends DailyEventManager
{
    public function getDays()
    {
        $days = ArrayHelper::map(parent::getDays(), function ($day) {
            return $day;
        }, function ($day) {
            return $day;
        }, function ($day) {
            return Formatter::date(DateHelper::beginningOfMonth($day));
        });

        $startDate = $this->startDate;
        $endDate = $this->endDate;
        $current = clone $startDate;

        DateHelper::beginningOfMonth($current);

        $week = $this->docEvent->repeat_on_week;

        $function = null;

        switch ($week){
            case DocEvent::WEEK_FIRST:
                $function = 'getFirstWeek';
                break;
            case DocEvent::WEEK_LAST:
                $function = 'getLastWeek';
                break;
            default:
                $function = 'getWeek';
                $week = log($week, 2);
        }

        $function = [$this, $function];

        foreach ($days as $month => &$daysOfMonth) {
            list($beginningOfWeek, $endingOfWeek) = call_user_func($function, $month, $week);
            $daysOfMonth = array_filter($daysOfMonth, function($item) use ($beginningOfWeek, $endingOfWeek){
                return DateHelper::between($item, $beginningOfWeek, $endingOfWeek);
            });
        }
        unset($daysOfMonth);

        while ($current < $endDate) {

            $date = Formatter::date($current);
            if (key_exists($date, $days)) {
                $this->quantity = count($days[$date]);
                if ($this->quantity > 0) {
                    $this->index = $this->index + 1;
                    yield Formatter::date(DateHelper::max($startDate, $current));
                }
            }
            $current->modify('first day of next month');
        }
    }

    public static function getWeek($date, $index)
    {
        $date = new DateTime($date);

        $endOfMonth = clone $date;
        DateHelper::endOfMonth($endOfMonth);

        $beginningOfWeek = clone $date;
        $beginningOfWeek->modify("+ $index weeks");
        DateHelper::beginningOfWeek($beginningOfWeek);

        $beginningOfWeek = DateHelper::max($date, $beginningOfWeek);

        $endingOfWeek = clone $beginningOfWeek;
        DateHelper::endingOfWeek($endingOfWeek);
        $endingOfWeek = DateHelper::min($endingOfWeek, $endOfMonth);

        return [Formatter::date($beginningOfWeek), Formatter::date($endingOfWeek)];
    }

    public static function getFirstWeek($date)
    {
        $endingOfWeek = DateHelper::endingOfWeek($date);
        return [$date, Formatter::date($endingOfWeek)];
    }

    public static function getLastWeek($date)
    {
        $endOfMonth = new DateTime($date);
        DateHelper::endOfMonth($endOfMonth);
        $beginningOfWeek = clone $endOfMonth;
        DateHelper::beginningOfWeek($beginningOfWeek);
        return [Formatter::date($beginningOfWeek), Formatter::date($endOfMonth)];
    }

    public function getEndDate($date)
    {
        $date = new DateTime($date);
        return DateHelper::endOfDay(DateHelper::min(DateHelper::endOfMonth($date), $this->endDate));
    }

    public function getTitle($startDate, $endDate)
    {
        return sprintf('%s %s - %s', $this->docEvent->title, Formatter::shortDate($startDate), Formatter::shortDate($endDate));
    }

}