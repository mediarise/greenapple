<?php

namespace common\components\bl\teacher;

use common\models\DocEvent;

class FillOutFormEventSubManager extends EventSubManager
{
    public function validatePublish(DocEvent $event)
    {
        parent::validatePublish($event);

        $this->validateAtLeastOneForm($event);
        $this->validateNoProducts($event);
    }
}