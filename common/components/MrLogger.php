<?php

namespace common\components;

use \Yii;

/**
 * by @edifanoff
 * Logger Request and Response
 * Class MrLogger
 * @package common\components
 */
class MrLogger
{
    const CATEGORY_REQUEST_NAME = 'REQUEST';
    const CATEGORY_RESPONSE_NAME = 'RESPONSE';

    public $request;
    public $response;

    public function __construct()
    {
        $this->request = Yii::$app->getRequest();
        $this->response = Yii::$app->getResponse();
    }


    public function logRequest()
    {
        $method = strtoupper($this->request->getMethod());
        $uri = $this->request->getPathInfo();
        $bodyAsJson = json_encode($this->request->rawBody);
        $message = "{$method} {$uri} - {$bodyAsJson}";
        Yii::info($message, self::CATEGORY_REQUEST_NAME);
    }


    public function logResponse()
    {
        $method = strtoupper($this->request->getMethod());
        $uri = $this->request->getPathInfo();
        $bodyAsJson = json_encode($this->response->data);
        $message = "{$method} {$uri} - {$bodyAsJson}";
        Yii::info($message, self::CATEGORY_RESPONSE_NAME);
    }
}