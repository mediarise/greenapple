<?php

namespace common\components;

interface Notifier
{
    public function notify();
}