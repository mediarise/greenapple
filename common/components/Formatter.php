<?php

namespace common\components;

class Formatter
{
    const FULL_DATE_FORMAT = 'Y-m-d H:i:s';
    const DATE_FORMAT = 'Y-m-d';
    const SHORT_DATE_FORMAT = 'M d';

    const TIME_FORMAT = 'H:i:s';
    const SHORT_TIME_FORMAT = 'H:i';

    public static function shortDate($date)
    {
        if (is_string($date)) {
            $date = new \DateTime($date);
        }
        return $date->format(self::SHORT_DATE_FORMAT);
    }

    public static function date($date)
    {
        if (is_string($date)) {
            $date = new \DateTime($date);
        }
        if (empty($date)) {
            return date(self::DATE_FORMAT);
        }
        return $date->format(self::DATE_FORMAT);
    }

    public static function dateTime($date)
    {
        if (is_string($date)) {
            $date = new \DateTime($date);
        }
        return $date->format(self::FULL_DATE_FORMAT);
    }

    public static function time($date)
    {
        $dt = new \DateTime($date);
        return $dt->format(self::SHORT_TIME_FORMAT);
    }

    public static function period($start, $end)
    {
        $start = self::shortDate($start);
        $end = self::shortDate($end);
        if ($start === $end) {
            return $start;
        }
        return sprintf('%s - %s', $start, $end);
    }

    public static function ratio($count, $to)
    {
        if (empty($to)) {
            return '-';
        }
        return sprintf('%s/%s', (int)$count, (int)$to);
    }

    public static function productName($name, $variation)
    {
        if (empty($variation)) {
            return $name;
        }
        return sprintf('%s (%s)', $name, $variation);
    }

    public static function toDay($start, $days)
    {
        $date = new \DateTime($start);
        $date->modify(sprintf('+%s day', $days));
        return $date->format(self::SHORT_DATE_FORMAT);
    }
}