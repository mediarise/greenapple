<?php
namespace common\components;

use yii\base\ErrorException;

class BlobToImageConverter
{
    public $convertTo = 'jpg';

    public function convert($sourcePath, $destinationDirectory, $saveAsName)
    {
        $blob = file_get_contents($sourcePath);

        $path = $destinationDirectory . DIRECTORY_SEPARATOR . $saveAsName . '.' . $this->convertTo;

        $blobConverted = imagejpeg(imagecreatefromstring($blob), $path);
        if (!$blobConverted) {
            throw new ErrorException('File cannot be converted.');
        }

        return true;
    }
}