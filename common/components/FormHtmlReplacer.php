<?php


namespace common\components;


use common\models\DocEvent;
use common\models\Form;
use common\models\Guardian;
use common\models\School;
use common\models\Student;
use common\models\Teacher;

class FormHtmlReplacer
{
    /* @var DocEvent */
    public $eventModel;

    /* @var Student */
    public $studentModel;

    /* @var Guardian */
    public $guardianModel;

    /* @var Form */
    public $formModel;

    protected $resultHtml = null;

    public function getResult()
    {
        if ($this->resultHtml === null) {
            $this->process();
        }
        return $this->resultHtml;
    }

    public function process()
    {
        $this->resultHtml = str_replace($this->getVars(), $this->getReplaceData(), $this->formModel->html);
    }

    protected function getVars()
    {
        return array_keys($this->getReplaceData());
    }

    protected function getReplaceData()
    {
        return [
            '{studentName}' => $this->getStudentName(),
            '{parentName}' => $this->getParentName(),
            '{teacherName}' => $this->getTeacherName(),
            '{school}' => $this->getSchool(),
            '{grade}' => $this->getGrade(),
            '{eventTitle}' => $this->getEventTitle(),
            '{eventStartDate}' => $this->getEventStartDate(),
            '{eventEndDate}' => $this->getEventEndDate(),
            '{eventLocation}' => $this->getEventLocation(),
        ];
    }

    protected function getStudentName()
    {
        if ($this->studentModel === null) {
            return '';
        }
        return "{$this->studentModel->first_name} {$this->studentModel->last_name}";
    }

    protected function getParentName()
    {
        if ($this->guardianModel === null) {
            return '';
        }
        return $this->guardianModel->getFullName();
    }

    protected function getTeacherName()
    {
        if ($this->formModel === null) {
            return '';
        }
        /* @var $formOwner Teacher */
        $formOwner = $this->formModel->owner;
        $name = $formOwner !== null ? $formOwner->getFullName() : "";
        return $name;
    }

    protected function getSchool()
    {
        /* @var $school School */
        $school = $this->formModel->school;
        if ($school === null) {
            return '';
        }
        return $school->name;
    }

    protected function getGrade()
    {
        if ($this->studentModel === null) {
            return '';
        }
        return $this->studentModel->grade->name;
    }

    protected function getEventTitle()
    {
        if ($this->eventModel === null) {
            return '';
        }
        return $this->eventModel->title;
    }

    protected function getEventStartDate()
    {
        if ($this->eventModel === null) {
            return '';
        }
        return $this->eventModel->start_date;
    }

    protected function getEventEndDate()
    {
        if ($this->eventModel === null) {
            return '';
        }
        return $this->eventModel->end_date;
    }

    protected function getEventLocation()
    {
        // todo Add event location returning
        return '';
    }
}