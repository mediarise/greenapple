<?php

namespace common\components;

use common\helpers\BackendUrl;
use Throwable;
use function Stringy\create as s;
use yii\web\ForbiddenHttpException;

/** @property $homeUrl string */
class User extends \webvimark\modules\UserManagement\components\UserConfig
{
    private $rolePriority = [
        'activeUser',
        'guardian',
        'teacher',
        'schoolAdmin',
        'boardAdmin',
        'greenappleAdmin'
    ];

    /**
     * @inheritdoc
     */
    public $identityClass = 'webvimark\modules\UserManagement\models\User';

    public $loginUrl = '/site/login';

    /**
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function getHomeUrl()
    {
        try {
            $highestRole = $this->getHighestRole();
        } catch (Throwable $e) {
            throw new ForbiddenHttpException("You are not allowed to perform this action. ");
        }

        $highestRole = (string)s($highestRole)->toLowerCase();

        return BackendUrl::to(["/{$highestRole}/index"]);
    }

    /**
     * @return string
     * @throws Throwable
     */
    public function getHighestRole()
    {
        $identity = $this->getIdentity();
        /* @var $identity \webvimark\modules\UserManagement\models\User */
        if ($identity == null) {
            return null;
        }
        if ($identity->username === 'superadmin') {
            return $this->rolePriority[count($this->rolePriority) - 1];
        }

        $roles = $identity->getRoles()->all();

        $boards = [];

        foreach ($roles as $role) {
            $roleIndex = array_search($role->name, $this->rolePriority);
            if ($roleIndex !== false) {
                $boards[] = $roleIndex;
            }
        }

        return empty($boards) ? null : $this->rolePriority[max($boards)];
    }

}