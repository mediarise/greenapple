<?php

namespace common\components\role_factory;

use common\components\bl\CartManager;
use common\models\Guardian;
use yii\db\ActiveQuery;
use yii\web\View;

class GuardianFactory extends FactoryBase
{
    public function bootstrap()
    {
        $container = \Yii::$container;
        $container->setSingleton(CartManager::class, function(){
            return new CartManager($this->getActor()->getId());
        });
        $container->set('common\models\HolidayQuery', 'common\models\guardian\HolidayQuery');
    }

    public function getCartManager()
    {
        return \Yii::$container->get(CartManager::class);
    }

    public function getViewCreator()
    {
        return function(){
            /** @var \common\components\bl\Actor $actor */
            $view = new View();
            if (!\Yii::$app->request->isPjax) {
                $view->params['cartQuantity'] = $this->getCartManager()->getTotalCount();
            }
            return $view;
        };
    }

    /**
     * @return ActiveQuery
     */
    protected function createActorQuery()
    {
        return Guardian::find();
    }
}