<?php

namespace common\components\role_factory;

use common\models\Teacher;
use yii\db\ActiveQuery;

class TeacherFactory extends FactoryBase
{
    /**
     * @return ActiveQuery
     */
    protected function createActorQuery()
    {
        return Teacher::find()
            ->alias('t')
            ->select('t.*, s.board_id')
            ->joinWith('school s');
    }

    public function bootstrap()
    {
        $container = \Yii::$container;
        $container->set('common\models\SupplierQuery', 'common\models\teacher\SupplierQuery');
        $container->set('common\models\HolidayQuery', 'common\models\teacher\HolidayQuery');
    }
}