<?php

namespace common\components\role_factory;

use common\models\SchoolAdmin;
use common\query\schooladmin\EventQuery;
use common\query\schooladmin\FormQuery;
use common\query\schooladmin\ProductQuery;
use yii\db\ActiveQuery;

class SchoolAdminFactory extends FactoryBase
{
    /**
     * @return ActiveQuery
     */
    protected function createActorQuery()
    {
        return SchoolAdmin::find()
            ->alias('sa')
            ->select('sa.*, s.board_id')
            ->joinWith('school s');
    }

    public function bootstrap()
    {
        $container = \Yii::$container;
        $container->set('common\models\SupplierQuery', 'common\models\schooladmin\SupplierQuery');
        $container->set('common\models\HolidayQuery', 'common\models\schooladmin\HolidayQuery');
    }

    public function getProductQueryCreator()
    {
        return ProductQuery::class;
    }

    public function getFormQueryCreator()
    {
        return FormQuery::class;
    }

    public function getEventQueryCreator()
    {
        return EventQuery::class;
    }
}