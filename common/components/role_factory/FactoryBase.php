<?php

namespace common\components\role_factory;

use common\query\EventQuery;
use common\query\FormQuery;
use common\query\ProductQuery;
use frontend\widgets\SideBarHeader;
use yii\db\ActiveQuery;
use yii\web\View;
use function Stringy\create as s;
use yii\widgets\Breadcrumbs;

abstract class FactoryBase
{
    protected function getUser()
    {
        return \Yii::$app->getUser();
    }

    public function getViewCreator()
    {
        return View::class;
    }

    public function bootstrap()
    {
    }

    public function getProductQueryCreator()
    {
        return ProductQuery::class;
    }

    public function getFormQueryCreator()
    {
        return FormQuery::class;
    }

    public function getEventQueryCreator()
    {
        return EventQuery::class;
    }

    protected function getActor()
    {
        return \Yii::$container->get('Actor');
    }

    private static $classMap = [
        'guardian' => GuardianFactory::class,
        'teacher' => TeacherFactory::class,
        'schooladmin' => SchoolAdminFactory::class,
        'boardadmin' => BoardAdminFactory::class,
        'greenappleadmin' => GreenappleAdminFactory::class,
    ];


    public function getActorCreator()
    {
        return function () {
            $classMap = array_flip(self::$classMap);
            $user = $this->getUser();
            $class = get_class($this);
            if ($user->isGuest) {
                $host = $_SERVER['HTTP_HOST'];
                header("Location: http://{$host}/");
                die();
            }
            if (!isset($classMap[$class]) || $classMap[$class] !== (string)s($user->getHighestRole())->toLowerCase()) {
                $host = $_SERVER['HTTP_HOST'];
                $uri = $user->getHomeUrl();
                header("Location: http://{$host}{$uri}");
                die();
            }

            return $this->createActorQuery()
                ->where(['user_id' => $this->getUser()->getId()])
                ->one();
        };
    }

    public function getSideBarHeaderCreator()
    {
        return SideBarHeader::class;
    }

    public function getConstraintCreator()
    {
        return function () {
            return null;
        };
    }

    public function getBreadcrumbsCreator()
    {
        return function () {
            return new Breadcrumbs([
                'homeLink' => [
                    'label' => 'Home',
                    'url' => [$this->getUser()->getHomeUrl()],
                ]
            ]);
        };
    }

    public static function create(string $role)
    {
        if (isset(self::$classMap[$role])) {
            /** @noinspection PhpUnhandledExceptionInspection */
            return \Yii::createObject(self::$classMap[$role]);
        }
        return null;
    }

    /**
     * @return ActiveQuery
     */
    protected abstract function createActorQuery();
}