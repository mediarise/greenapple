<?php

namespace common\components\role_factory;

use common\components\bl\constraint\BoardAdminConstraint;
use common\models\BoardAdmin;
use yii\db\ActiveQuery;

class BoardAdminFactory extends FactoryBase
{
    public function getConstraintCreator()
    {
        return function () {
            $actor = $this->getActor();
            return new BoardAdminConstraint($actor->user_id, $actor->board_id);
        };
    }

    /**
     * @return ActiveQuery
     */
    protected function createActorQuery()
    {
        return BoardAdmin::find();
    }

    public function bootstrap()
    {
        $container = \Yii::$container;
        $container->set('common\models\SupplierQuery', 'common\models\boardadmin\SupplierQuery');
        $container->set('common\models\HolidayQuery', 'common\models\boardadmin\HolidayQuery');
    }
}