<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\ErrorException;
use yii\base\InvalidArgumentException;

class EMailNotifier implements Notifier
{
    public $to;
    public $template;
    public $params;
    public $subject;

    public function notify()
    {
        Yii::$app->mailer->compose([
            'html' => $this->template . '-html',
            'text' => $this->template . '-text',
        ], $this->params)
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo($this->to)
            ->setSubject($this->subject)
            ->send();
    }
}