<?php
namespace common\components;

use Yii;
use yii\base\BaseObject;
use yii\base\ErrorException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class UploadedFileSaver extends BaseObject
{
    protected $fileBaseName;
    protected $fileExtension;
    protected $fileInfo;

    protected $uploadDirectory;

    /**
     * @var UploadedFile
     */
    protected $file;

    /**
     * @param UploadedFile $file
     * @return array|bool
     * @throws \yii\base\ErrorException
     */
    public function save(UploadedFile $file)
    {
        $this->setFileExtension($file->extension);
        $this->generateFileBaseName();
        $this->createUploadDirectory();

        $path = $this->getFilePath();
        if (!$this->file->saveAs($path)) {
            throw new ErrorException('File was not saved.');
        }

        $this->setFileInfo();

        return true;
    }

    /**
     * @return array
     */
    public function getFileInfo()
    {
        return $this->fileInfo;
    }

    public function getFileBaseName()
    {
        if ($this->fileBaseName) {
            return $this->fileBaseName;
        }

        return $this->generateFileBaseName();
    }

    public function getFileExtension()
    {
        if (!$this->fileExtension) {
            throw new ErrorException('File extension is not defined.');
        }

        return $this->fileExtension;
    }

    public function getFileName()
    {
        return $this->getFileBaseName() . '.' . $this->getFileExtension();
    }

    public function getFilePath()
    {
        return $this->getUploadDirectory() . DIRECTORY_SEPARATOR . $this->getFileName();
    }

    public function getUploadDirectory()
    {
        if ($this->uploadDirectory) {
            return $this->uploadDirectory;
        }

        return $this->createUploadDirectory();
    }

    public function setFileExtension($fileExtension)
    {
        $this->fileExtension = $fileExtension;
    }

    public function setFileInfo()
    {
        $this->fileInfo = [
            'filename' => $this->getFileName(),
            'url' => $this->getUploadedFileUrl(),
        ];
    }

    protected function getUploadedFileUrl()
    {
        if (!$this->uploadDirectory) {
            throw new \ErrorException('File should be uploaded before.');
        }

        return '/' . Yii::$app->params['uploadsDir'] .
            DIRECTORY_SEPARATOR . $this->uploadDirectory .
            DIRECTORY_SEPARATOR . $this->getFileName();
    }

    protected function generateFileBaseName()
    {
        if ($this->fileBaseName) {
            return $this->fileBaseName;
        }

        return $this->fileBaseName = Yii::$app->security->generateRandomString(24);
    }

    protected function createUploadDirectory()
    {
        $this->uploadDirectory = (int)date('Y') . DIRECTORY_SEPARATOR . (int)date('m');
        $path = Yii::getAlias('@uploadsDir') . DIRECTORY_SEPARATOR . $this->uploadDirectory;

        FileHelper::createDirectory($path);

        return $this->uploadDirectory;
    }
}