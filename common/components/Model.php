<?php

namespace common\components;


class Model extends \yii\base\Model
{
    public static function validateMultiple($models, $attributeNames = null)
    {
        $valid = true;
        /* @var $models array */
        foreach ($models as $model) {
            if (is_array($model)) {
                $valid = self::validateMultiple($model, $attributeNames) && $valid;
            }
            if ($model instanceof \yii\db\ActiveRecord) {
                $valid = $model->validate() && $valid;
                continue;
            }
            if ($model instanceof \yii\base\Model) {
                $valid = $model->validate($attributeNames) && $valid;
                continue;
            }
        }

        return $valid;
    }

    public static function loadMultiple($modelClass, $data, $formName = null)
    {
        if (!$data) {
            return false;
        }

        $loadedModels = [];
        if (!$formName) {
            foreach ($data as $i => $dataItem) {
                if ($loadedModel = self::loadSingleModel($modelClass, $dataItem)) {
                    $loadedModels[] = $loadedModel;
                }
            }
        } elseif (isset($data[$formName]) && count ($data[$formName]) > 0) {
            foreach ($data[$formName] as $i => $dataItem) {
                if ($loadedModel = self::loadSingleModel($modelClass, $dataItem)) {
                    $loadedModels[] = $loadedModel;
                }
            }
        }

        return $loadedModels ?? false;
    }

    protected static function loadSingleModel($modelClass, $data)
    {
        $model = new $modelClass();
        /* @var $model Model */
        /* @var $dataItem array|string|integer */
        if ($model->load($data, '')) {
            return $model;
        }

        return false;
    }
}