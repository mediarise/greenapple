<?php

namespace common\components;

class BitHelper
{

    /**
     * Returns true when $var1 and $var2 has same bits
     *
     * @param $var1
     * @param $var2
     * @return bool
     */
    public static function testBits($var1, $var2)
    {
        return ($var1 & $var2) > 0;
    }
}