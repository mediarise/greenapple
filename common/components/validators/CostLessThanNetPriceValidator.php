<?php

namespace common\components\validators;

use common\components\bl\enum\ProductTax;
use yii\validators\Validator;

class CostLessThanNetPriceValidator extends Validator
{
    public $price;

    public function validateAttribute($model, $attribute)
    {
        $priceIncludingTax = (float)$this->price * ProductTax::rate(ProductTax::HST_13_INCLUDED);
        if ($priceIncludingTax < $model->{$attribute}) {
            $this->addError($model, $attribute, 'Cost must be less that net price.');
        }
    }
}