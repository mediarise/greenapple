<?php
namespace common\components;

use Yii;
use yii\web\UploadedFile;

class UploadedBlobImageSaver extends UploadedFileSaver
{
    /**
     * @var BlobToImageConverter
     */
    public $converter;

    /**
     * UploadedBlobImageSaver constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /**
     * @param UploadedFile $file
     * @return array|bool
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\base\ErrorException
     */
    public function save(UploadedFile $file)
    {
        $this->setFileExtension($this->converter->convertTo);
        $this->generateFileBaseName();
        $this->createUploadDirectory();

        $destinationDirectory = Yii::getAlias('@uploadsDir') . DIRECTORY_SEPARATOR . $this->uploadDirectory;
        $this->converter->convert($file->tempName, $destinationDirectory, $this->getFileBaseName());

        $this->setFileInfo();

        return true;
    }
}