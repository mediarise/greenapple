<?php

namespace common\components;

use DateTime;

class DateHelper
{
    public static function endOfDay($date)
    {
        if (is_string($date)) {
            $date = new DateTime($date);
        }
        return Formatter::dateTime($date->modify('23:59:59'));
    }

    public static function beginningOfMonth($date)
    {
        if (is_string($date)) {
            $date = new DateTime($date);
        }
        return $date->modify('first day of this month');
    }

    public static function endOfMonth($date)
    {
        if (is_string($date)) {
            $date = new DateTime($date);
        }
        return $date->modify('last day of this month');
    }

    public static function beginningOfWeek($date)
    {
        if (is_string($date)) {
            $date = new DateTime($date);
        }
        $dayOfWeek = (int)$date->format('w');
        if ($dayOfWeek === 1) {
            return $date;
        }
        $date->modify('last mon');
        return $date;
    }

    public static function endingOfWeek($date)
    {
        if (is_string($date)) {
            $date = new DateTime($date);
        }
        $date->modify('sun');
        return $date;
    }

    public static function between($date, $start, $end)
    {
        return $date >= $start && $date <= $end;
    }

    public static function min(DateTime $startDate, DateTime $current)
    {
        return $startDate < $current ? $startDate : $current;
    }

    public static function max(DateTime $startDate, DateTime $current)
    {
        return $startDate > $current ? $startDate : $current;
    }

    public static function getDaysBetweenDatesIncluded($startDate, $endDate) {
        $day = 86400;
        $startTime = strtotime($startDate);
        $endTime = strtotime($endDate);
        //$numDays = round(($endTime - $startTime) / $day) + 1;
        $numDays = round((($endTime + $day) - $startTime) / $day); // remove increment

        $days = [];

        for ($i = 0; $i < $numDays; $i++) { //change $i to 1
            $days[] = date(Formatter::DATE_FORMAT, ($startTime + ($i * $day)));
        }
        return $days;
    }
}