<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class SignupRequestGuardianFixture extends ActiveFixture
{
    public $modelClass = 'common\models\SignupRequestGuardian';

    public function unload()
    {
        parent::unload();
        $this->resetTable();
    }

    public $unloadFirst = [SignupRequestStudentFixture::class];
}