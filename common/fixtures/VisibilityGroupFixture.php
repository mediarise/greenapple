<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class VisibilityGroupFixture extends ActiveFixture
{
    public $modelClass = 'common\models\VisibilityGroup';

    public function unload()
    {
        parent::unload();
        $this->resetTable();
    }
}