<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class FormFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Form';

    public function unload()
    {
        parent::unload();
        $this->resetTable();
    }
}