<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class DocEventFixture extends ActiveFixture
{
    public $modelClass = 'common\models\DocEvent';

    public function unload()
    {
        parent::unload();
        $this->resetTable();
    }
}