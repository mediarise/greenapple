<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class SignupRequestStudentFixture extends ActiveFixture
{
    public $modelClass = 'common\models\SignupRequestStudent';

    public function unload()
    {
        parent::unload();
        $this->resetTable();
    }

    public $depends = [
        SignupRequestGuardianFixture::class
    ];
}