<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class StudentFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Student';

    public function unload()
    {
        parent::unload();
        $this->resetTable();
    }

    public $unloadFirst = [SignupRequestStudentFixture::class];
}