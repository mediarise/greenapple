<?php
namespace common\extensions\profilePhoto;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class ProfilePhoto extends Widget
{
    public $gender;
    public $photo;
    public $photoIsBlob = false;
    public $cssClass;


    protected $dummyImagePath;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $bundle = ProfilePhotoAsset::register($this->getView());
        $this->dummyImagePath = $bundle->baseUrl;
    }

    public function run()
    {
        if (!$this->photo) {
            $src = sprintf('%s/images/profile-dummy-%s.jpg', $this->dummyImagePath, $this->gender ?: 'default');
        } else {
            if ($this->photoIsBlob) {
                $src = 'data:image/jpeg;base64,' . base64_encode($this->photo);
            } else {
                $src = Url::to('/' . Yii::$app->params['uploadsUrl'] . '/' . $this->photo);
            }
        }

        return Html::img($src, ['class' => $this->cssClass ?? 'img-thumbnail', 'width' => '100%']);
    }
}