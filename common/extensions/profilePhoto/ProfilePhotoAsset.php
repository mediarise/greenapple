<?php
namespace common\extensions\profilePhoto;

use yii\web\AssetBundle;

class ProfilePhotoAsset extends AssetBundle
{
    public $sourcePath = '@common/extensions/profilePhoto/assets';
    public $baseUrl = '/dashboard';
}