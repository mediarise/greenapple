<?php
/** @var \dosamigos\fileupload\FileUpload $this */
/** @var string $input the code for the input */
/** @var string $icon icon template */
/** @var string $label text that should be inside the button */
/** @var string $cssClass button css class */
?>

<span class="ladda-button <?= $cssClass ?> fileinput-button" data-style="zoom-out">
    <?= $icon ?: '' ?>
    <span><?= Yii::t('fileupload', $label) ?></span>
    <!-- The file input field used as target for the file upload widget -->
    <?= $input ?>
</span>
