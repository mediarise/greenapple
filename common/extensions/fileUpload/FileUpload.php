<?php
namespace common\extensions\fileUpload;

use yii\helpers\Html;
use yii\helpers\Json;
use \dosamigos\fileupload\FileUploadPlusAsset;
use \dosamigos\fileupload\FileUploadAsset;

class FileUpload extends \dosamigos\fileupload\FileUpload
{
    public $enabledCsrfValidation = true;
    public $label = 'Upload image';
    public $cssClass = 'btn btn-success';
    public $icon = '<i class="glyphicon glyphicon-plus"></i>';

    protected $defaultClientEvents = [];
    private $js;

    public function run()
    {
        $input = $this->hasModel()
            ? Html::activeFileInput($this->model, $this->attribute, $this->options)
            : Html::fileInput($this->name, $this->value, $this->options);

        echo $this->useDefaultButton
            ? $this->render('uploadButton', [
                'input' => $input,
                'icon' => $this->icon,
                'label' => $this->label,
                'cssClass' => $this->cssClass
            ])
            : $input;

        if ($this->enabledCsrfValidation) {
            $this->addCsrfParam();
        }

        $this->registerClientScript();
    }

    public function registerClientScript()
    {
        $view = $this->getView();

        $this->plus
            ? FileUploadPlusAsset::register($view)
            : FileUploadAsset::register($view);

        $options = Json::encode($this->clientOptions);
        $id = $this->options['id'];

        $this->js[] = ";jQuery('#$id').fileupload($options);";
        $this->addClientEvents($id, $this->clientEvents);

        $view->registerJs(implode("\n", $this->js));
    }

    public function addCsrfParam()
    {
        $this->defaultClientEvents['fileuploadsubmit'] = 'function (e, data) {
            var csrfParam = $(\'meta[name="csrf-param"]\').attr("content");
            var csrfToken = $(\'meta[name="csrf-token"]\').attr("content");
            data.formData[csrfParam] = csrfToken;
        }';
    }

    public function addClientEvents($id, $clientEvents)
    {
        if (count($clientEvents) === 0) {
            return true;
        }

        $this->clientEvents = array_merge($this->defaultClientEvents, $clientEvents);

        foreach ($this->clientEvents as $event => $handler) {
            if (array_key_exists($event, $this->defaultClientEvents)) {
                $handler = $this->mergeEventHandlers($handler, $this->defaultClientEvents[$event]);
            }
            $this->js[] = "jQuery('#$id').on('$event', $handler);";
        }

        return true;
    }

    public function mergeEventHandlers($handler, $defaultHandler)
    {
        $handlerBody = $this->getEventHandlerBody($handler);
        $defaultHandlerBody = $this->getEventHandlerBody($defaultHandler);

        return sprintf('function (e, data) {%s%s%s}', $handlerBody, "\n", $defaultHandlerBody);
    }

    public function getEventHandlerBody($handler)
    {
        $start = strpos($handler, '{') + 1;
        $end = strrpos($handler, '}') - 1;

        if (!$start || !$end || $start > $end) {
            throw new \LogicException('Incorrect event handler format.');
        }

        return substr($handler, $start, $end - $start);
    }
}