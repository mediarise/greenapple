<?php
namespace common\extensions\bootstrap;

use yii\base\Widget;
use yii\helpers\Html;

class ButtonLink extends Widget
{
    public $label = 'Link';
    public $options = [];
    public $icon = false;

    public function run()
    {
        $linkText = Html::tag('span', '', ['class' => $this->icon ?: '']);

        $this->options['href'] =  isset($this->options['href']) ? $this->options['href'] : '#';

        return Html::tag('a', $linkText . $this->label, $this->options);
    }
}