<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
</p>

<a href="https://github.com/yiisoft" target="_blank">
<img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
</a>

<h1 align="center">Yii 2 Advanced Project Template</h1>
<br>
</p>[LINUX]</p>
<p>docker-compose up -d && sleep 5 && docker-compose exec php /usr/local/bin/composer-init && docker-compose exec php /usr/local/bin/xdebug-init</p>
</p>[WINDOWS]</p>
<p>docker-compose up -d && timeout 5 && docker-compose exec php /usr/local/bin/composer-init && docker-compose exec php /usr/local/bin/xdebug-init</p>

</p>Console commands:</p>

<p>
php yii dev/add-school-admin name school-id - create new school admin and assign him to shool-id<br>
php yii dev/add-signup-request gradeId{1}, gradeId{n}... - create new signup request with n children
and assign them to grades
</p>
<p>
php yii seed/seed-users - create users: schooladmin{1-10}, activeuser{1-10}, teacher{1-10}, guardian{1-10},
boardadmin{1-10}, greenappleadmin{1-10} 
</p>